/**
 * Created by Freddy on 7/11/2017.
 */


$('#dob, #breed').on('change',function () {
    $('#dob-val').text($('#dob').val());
    $('#breed-val').text($('#breed').val());
});


function initializeDogSelect(url, reload){

    $('#submit_form').on('submit',function(e){
        e.preventDefault();
        var self = $(this);
        var _self = this;
        $('.button-submit').attr('disabled',true);

            $.ajax({
                url: self.attr('action'), // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(_self), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false//
                beforeSend : function( xhr ){
                    $.blockUI({
                        message: '<h1> please wait...</h1>',
                        css: {
                            border: 'none',
                            padding: '15px',
                            'z-index' : 10000,
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .8,
                            color: '#fff'
                        }});
                }
            }).success(function(data) {
                $.unblockUI();
                if($.isEmptyObject(data.error)){
                    swal("Registered!", "Registration was successful!", "success");
                    setTimeout(function () {
                        if (reload === true){
                            location.href=url;
                        } else {
                            location.href=url+"/"+data.dog_id

                        }
                    },2000)
                }else{
                    printErrorMsg(data.error);
                    $('html, body').animate({ scrollTop: 2 }, 'slow');
                    $('.button-submit').removeAttr('disabled');
                }
            }).fail(function(){
                $.unblockUI();
                alert('failed registering a dog!');
                $('.button-submit').removeAttr('disabled');
            });
    });

    $(".dog_parent").select2({
        ajax: {

            url: "select-dogs",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                    g: $(this).data('gender')
                };
            },
            processResults: function (data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;

                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });

    function formatRepo (dog) {
        if (dog.loading) return dog.text;

        // var markup = "<div class='select2-result-repository clearfix'>" +
        //         //"<div class='select2-result-repository__avatar'><img width='70' height='50' src='{{url('images/catalog')}}/" + dog.image_name+ "' /></div>" +
        //     "<div class='select2-result-repository__avatar'><img width='70' height='50' src='/images/catalog/" + dog.image_name+ "' /></div>" +
        //     "<div class='select2-result-repository__meta'>" +
        //     "<div class='select2-result-repository__title'>" + dog.name + "</div>"+
        //     "<div class='select2-result-repository__title'>" + dog.registration_number + "</div>";

        var markup = "<div class='select2-result-repository clearfix'>" +
        "<div class='select2-result-repository__avatar'><img width='70' height='50' src='https://i.pinimg.com/600x315/1c/8b/8b/1c8b8b31deca8c38226398e29c4700d4.jpg' /></div>" +
        "<div class='select2-result-repository__meta'>" +
        "<div class='select2-result-repository__title'>" + dog.name + "</div>" +
        "<div class='select2-result-repository__title'>" + dog.registration_number + "</div>";

        if (dog.breeder_name) {
            markup += "<div class='select2-result-repository__description'>" +" Breed : "+ dog.breeder_name + "</div>";
        }
//                 += "<div class='select2-result-repository__statistics'>" +
//                        "<div class='select2-result-repository__forks'><i class='fa fa-flash'></i> " + repo.forks_count + " Forks</div>" +
//                        "<div class='select2-result-repository__stargazers'><i class='fa fa-star'></i> " + repo.stargazers_count + " Stars</div>" +
//                        "<div class='select2-result-repository__watchers'><i class='fa fa-eye'></i> " + repo.watchers_count + " Watchers</div>" +
//                        "</div>" +
        markup += "</div></div>";

        return markup;
    }

    function formatRepoSelection (dog) {
        return dog.registration_number;
    }
}

function printErrorMsg (msg) {

    $(".print-error-msg").find("ul").html('');

    $(".print-error-msg").css('display','block');

    $.each( msg, function( key, value ) {

        $(".print-error-msg").find("ul").append('<li>'+value+'</li>');

    });

}
