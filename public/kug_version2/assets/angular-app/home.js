

var app = angular.module('HomeApp', ['ngAnimate'], function($interpolateProvider){
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('"/home/restaurants"');
    $stateProvider.state('home', {
        url: '"/home/restaurants"',
        views: {
            content: {
                templateUrl: "",
                controller: function($scope,$http,$timeout){
                    //$http.get("/statistics/dashboard-data")
                    //    .then(function(response) {
                    //        $scope.dashboardData = response.data;
                    //    });
                }
            }
        }
    });

    $stateProvider.state('orders', {
        url: '/orders/:location_id',
        views: {
            content: {
                templateUrl: admin_root + "/orders",
                controller: function($scope,$http, $stateParams){
                    $scope.$parent.loadOrders($stateParams.location_id);
                    $scope.$parent.ordersCurrentLocation = $stateParams.location_id;
                }
            }
        }
    });

    $stateProvider.state('order', {
        url: '/order/:order_id',
        views: {
            content: {
                templateUrl: admin_root + "/order",
                controller: function($scope,$http, $stateParams){
                    $scope.order = {};
                    $scope.$parent.startLoading();
                    $http.get('/orders/order/'+$stateParams.order_id)
                        .then(function(response){
                            $scope.order = response.data;
                            $scope.$parent.stopLoading();
                        })
                }
            }
        }
    });

});

