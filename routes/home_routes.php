<?php

use App\Dog;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Barryvdh\DomPDF\PDF;



Route::get('/user',function (){
    return Auth::user()->id;
});

Route::any('/', 'HomeController@getIndex');

//Route::post('', 'HomeController@getIndex');

Route::get('/check-dog-name', 'HomeController@getCheckDogName');

Route::get('/select-dogs', 'HomeController@getSelectDogs');

Route::get('/type-ahead-dogs', 'HomeController@getTypeAheadDogs');

Route::get('/dogs', 'HomeController@getSelectDogs');

Route::get('/contact-us', 'HomeController@getContactUs');

Route::get('/show-dog/{id}', 'HomeController@getShowDog');

Route::get('/main-table', 'HomeController@getMainTable');

Route::get('/add-new-column', 'HomeController@getAddNewColumn');

Route::get('/search-results', 'HomeController@getSearchResults');

Route::get('/dog-by-name/{name}', 'HomeController@getDogByName');

Route::post('/add-to-pedigree-table', 'HomeController@insertToGenerationTable');

Route::post('/register-new-dog-on-table', 'HomeController@registerNewDogOnTable');

Route::post('/regenerate-table', 'HomeController@postRegenerateTables');

Route::get('/pdf-certificate/{id}', 'HomeController@getPdfCertificate');

//Route::get('/password/reset', 'HomeController@getPdfCertificate');

Route::get('/status', 'HomeController@getStatus');

Route::get('/not-confirmed', 'HomeController@getNotConfirmedPage');

Route::get('/phpmyadmin',function (){
    return redirect()->back();
});

Route::get('/download-certificate', function (Request $request){
    if (! $request->hasValidSignature()) {
        abort(401, "Certificate download url has expired!");
    }

//    try {
        $id = $request->query("dog_id");
//    $url = "https://kennelunionofghana.com/print-certificate/";
//        $url = "https://kennelunionofghana.com/pdf/$id";
        $url = url("pdf/$id");
        $user_id = $request->query("user_id");
        $owner_name = $request->query("name");

         $response = response()->stream(function () use ($id, $owner_name, $url) {
            if (empty($owner_name)) {
                echo file_get_contents($url . $id);
            } else {
                echo file_get_contents($url . $id . "?name=" . $owner_name);
            }
        });

        $name = "$id.pdf";

        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $name,
            str_replace('%', '', Str::ascii($name))
        );

        $response->headers->set('Content-Disposition', $disposition);

        return $response;
//    }catch (Exception $exception){
//        return $exception->getMessage();
//    }
})->name("download-certificate");

//Route::get("certificate","CertificationController@getIndex");

Route::get("/print-certificate/{id}",function(Request $request, $id) {

    try {
        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
            ->leftJoin('users','users.id','=','dogs.user_id')
            ->where('dogs.id',$id)
            ->select(DB::raw('dogs.*,breeders.name as breed,
          dog_relationships.father,dog_relationships.mother,
          users.first_name,users.last_name,users.kennel_name'))
            ->first();
        $type_certificate = "PEDIGREE";

        $dogs_active = 'active';

        \Cache::forget('serial_number');
        \Cache::rememberForever('serial_number', function () {
            return App\Helpers\NumberGeneration::generateUsingDate();
        });

        $file_name = time().$id.'.pdf';
        return \PDF::loadView("main-cert",compact('dog','type_certificate','dogs_active'))
            ->setPaper('a2', 'portrait')
            ->save(public_path()."/$file_name");
//            ->download($file_name);

    }catch(Exception $ex){
        return $ex->getMessage();
        return view("download-cert", compact("id"));
    }

    return view("download-cert", compact('id'));

});



Route::get("/pdf/{id}",'DownloadsController@saveCertificate');

//Route::get("/pdf/{id}",function($id){
//    $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
//        ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
//        ->leftJoin('users','users.id','=','dogs.user_id')
//        ->where('dogs.id',$id)
//        ->select(DB::raw('dogs.*,breeders.name as breed,
//          dog_relationships.father,dog_relationships.mother,
//          users.first_name,users.last_name,users.kennel_name'))
//        ->first();
//    $type_certificate = "PEDIGREE";
//
//    $dogs_active = 'active';
//
//    \Cache::forget('serial_number');
//    \Cache::rememberForever('serial_number', function () {
//        return App\Helpers\NumberGeneration::generateUsingDate();
//    });
//
////    3508 x 2480 px
////    $customPaper = array(0,0,578,791);
//    $customPaper = array(0,0,3508,2480);
//    $customPaper = array(0,0,1500,2480);
////    $customPaper = array(0,0,1500,1220);
//    $customPaper = array(0,0,1500,2000);
//
//    $file_name = time().$id.'.pdf';
//
//    return \PDF::loadView("main-cert",compact('dog','type_certificate','dogs_active'))
//
//        ->setPaper($customPaper, 'landscape')
////        ->setPaper($customPaper, 'portrait')
////        ->setPaper('a2', 'portrait')
////        ->setPaper('a3', 'landscape')
//        ->save(public_path()."/$file_name")->download($file_name);
////        ->stream(time().$id.'.pdf');
////        ->download(time().$id.'.pdf');
//});

//Route::get("/print-certificate/{id}",function(Request $request, $id) {
//    $name = $request->query("name");
//    $file_name = \Webpatser\Uuid\Uuid::generate();
//    $url = 'https://kennelunionofghana.com/pdf-certificate/';
//
//    try {
//
//        \Spatie\Browsershot\Browsershot::url($url . $id . "?name=".$name)
////            ->windowSize(1800, 1385)
//            ->windowSize(1800, 1430)
//            ->save(public_path() . "/images/" . $file_name . ".png");
////            ->save(public_path() . "/images/" . $file_name . ".pdf");
//
//        $filepath = public_path() . "/images/" . $file_name . ".png";
////        $filepath = public_path() . "/images/" . $file_name . ".pdf";
//        return Response::download($filepath);
//
//    }catch(Exception $ex){
//
//        return $ex->getMessage();
//        return view("download-cert", compact("id"));
//
//    }
//
//    return view("download-cert", compact('id'));
//
//});

Route::get("/numbers",function (){
        return '#' . str_pad(10000, 8, "0", STR_PAD_LEFT);
});
