<?php

use App\HelperClasses\DogHelper;
use App\Jobs\GenerateDogJob;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

include_once 'navigation_routes.php';

include_once 'admin_routes.php';

include_once 'member_routes.php';

include_once 'home_routes.php';

Auth::routes();

\Route::get('/regenerate-dog/{number}',function($number){

    $dog = \App\Dog::whereRegistrationNumber($number)->first()->id;

    DogHelper::generateAncestorsFromRegistrationNumber($number,true);

//	return redirect()->to("/version2/dog/$dog");
    return redirect()->to("/main-table?id=$dog");
});

Route::get('/regenerate-dog-generations/{id}',function($id){
        try {
            $dog1 = \App\DogGeneration::whereDogId($id)->first();

            if ($dog1->delete()){
                $dog = \App\Dog::find($id);
                    GenerateDogJob::dispatchNow($dog->registration_number);

                return json_encode(["message" => "successfully regenerated generations table! Go back to previous page"]);
            }

        }catch (Exception $exception){
            return response()->json(["message" => "failed", "error" => $exception->getMessage() ],500);
        }
});

Route::get('/empty-json-structure/{dog_id}',function($dog_id){

    $structure = DogHelper::setUpGenerationJsonStructure();

    if(!\App\DogGeneration::whereDogId($dog_id)->first()){
        \App\DogGeneration::create([
            'id'               => \Webpatser\Uuid\Uuid::generate(),
            'dog_id'           => $dog_id,
            'first_generation' => $structure['first-generation'],
            'second_generation' => $structure['second-generation'],
            'third_generation' => $structure['third-generation'],
            'fourth_generation' => $structure['fourth-generation']
        ]);
    }else{
        \App\DogGeneration::whereDogId($dog_id)->first()->update([
            'first_generation' => $structure['first-generation'],
            'second_generation' => $structure['second-generation'],
            'third_generation' => $structure['third-generation'],
            'fourth_generation' => $structure['fourth-generation']
        ]);
    }
});

Route::get('/test2/{number}',function($number){

});

Route::get("/fire",function (){
//        event(new \App\Events\TestEvent("job",json_encode(["displayName" => "table"])));
        event(new \App\Events\TestEvent());
    return 'ok';
});

Route::get('test3',function() {
//	return \App\HelperClasses\Pedigree::generateAncestorsFromRegistrationNumber(201711021915);
    return \App\HelperClasses\Pedigree::generateFirstGenerationDamPartialAncestorsFromRegistrationNumber( 201802102296 );
});

Route::get("/{id}", "CertificationController@getIndex");

Route::get("/check-duplicates", function(){

});

Route::get("/naya-dogs/{name}",function($name){
    $dogs =  \App\Dog::where('name', 'LIKE', '%' . $name . '%')->take(8)->get();

    foreach ($dogs as $dog){
        $dog_relationship =\App\DogRelationship::whereDogId($dog->id)->first();
        if ($dog_relationship){
            $dog_relationship->update([
                "father" => "202102033885",
                "mother"=>  "202102023870"
            ]);
            return response()->json(["message" => "updated!"]);
        }else{
           \App\DogRelationship::create([
               "id" => \Webpatser\Uuid\Uuid::generate(),
               "dog_id" => $dog->id,
               "father" => "202102033885",
               "mother"=>  "202102023870"
           ]);
           return response()->json(["message" => "created!"]);
        }
    }
});

