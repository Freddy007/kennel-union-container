<?php


Route::group(['middleware' => ['auth','confirmed'], 'prefix' => 'member'], function() {

    Route::get( '/', 'MemberController@getIndex' );

    Route::get( '/dog/{id}', 'MemberController@getDog' );

    Route::get( '/show-dog/{id}', 'MemberController@getShowDog' );

    Route::get( '/edit-dog/{id}', 'MemberController@getEditDog' );

    Route::post( '/update-dog/{id}', 'MemberController@postUpdateDog' );

//    Route::get( '/register-dog', 'MemberController@getRegisterDog' );

//    Route::post( '/register-dog', 'MemberController@postRegisterNewDog' );

//    Route::get( '/register-litter', 'MemberController@getRegisterLitter' );

    Route::get( '/add-new-column', 'MemberController@getAddNewColumn' );

    Route::post( '/register-litter', 'MemberController@postRegisterLitter' );

    Route::get( '/dog-by-name/{name}', 'MemberController@getDogByName' );

    Route::get( '/select-dogs/', 'MemberController@getSelectDogs' );

    Route::get('/type-ahead-dogs', 'MemberController@getTypeAheadDogs');

    Route::post('/request-delete/{id}', 'MemberController@postRequestDelete');

    Route::post('/request-certificate/{id}', 'MemberController@postRequestCertificate');

});
