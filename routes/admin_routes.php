<?php

Route::group(['middleware' => ['auth', 'admin','confirmed'], 'prefix' => 'version2'], function(){

    Route::get('/', 'AdminController@getIndex');

    Route::get('/profile', 'AdminController@getProfile')->name('profile');

//    Route::get('/all-dogs', 'AdminController@getAllDogs');

    Route::get('/all-dogs', 'AdminController@getAllDogsTable');

    Route::get('/archives', 'AdminController@getArchives');

    Route::post('/restore-dog/{id}', 'AdminController@postRestoreDog');

    Route::get('/duplicates', 'AdminController@getDuplicates');

    Route::post('/generate-new-number/{id}/{dog_id}', 'AdminController@postGenerateNewNumber');

    Route::get('/all-dogs-data', 'AdminController@getAllDogsData');

    Route::get('/dog/{id}', 'AdminController@getDog');

    Route::get('/all-members', 'AdminController@getAllMembers');

    Route::get('/members-data', 'AdminController@getMembersData');

    Route::get('/all-breeds', 'AdminController@getAllBreeds');

    Route::post('/breeder/{id}', 'AdminController@postBreeder');

    Route::post('/admin/breeder/{id}', 'AdminController@postBreeder');

    Route::post('/edit-breeder/{id}', 'AdminController@postEditBreeder');

    Route::post('/register-breeder', 'AdminController@postRegisterBreeder');

    Route::get('/certificate-requests', 'AdminController@getCertificateRequests');

    Route::get('/user-dogs/{user_id}', 'AdminController@getUserDogs');

    Route::get('/show-dog/{id}', 'AdminController@getShowDog');

    Route::get('/edit-dog/{id}', 'AdminController@getEditDog');

    Route::post('/update-dog/{id}', 'AdminController@postUpdateDog');

    Route::get('/change-role/{id}', 'AdminController@postChangeRole');

    Route::post('/delete-member/{id}', 'AdminController@postDeleteMember');

    Route::get('/register-new-dog', 'AdminController@getRegisterNewDog');

    Route::post('/register-new-dog', 'AdminController@postRegisterNewDog');

    Route::get('/select-dogs', 'AdminController@getSelectDogs');

    Route::get('/type-ahead-dogs', 'AdminController@getTypeAheadDogs');

    Route::get('/dog-by-name/{id}', 'AdminController@getDogByName');

    Route::get('/register-litter', 'AdminController@getRegisterLitter');

    Route::post('/register-litter', 'AdminController@postRegisterLitter');

    Route::get('/add-new-column', 'AdminController@getAddNewColumn');

    Route::post('/confirm-dog/{id}', 'AdminController@postConfirmDog');

    Route::post('/confirm-member/{id}', 'AdminController@postConfirmMember');

    Route::post('/archive-dog/{id}', 'AdminController@postArchiveDog');

    Route::get('/temporal-url', 'AdminController@getTemporalUrl');

    Route::get('/sibling-dogs/{id}', 'AdminController@getSiblingDogs')->name("siblings");

    Route::post('/approve-certificate-request/{id}', 'AdminController@postApproveCertificateRequest');

    Route::post('/update-profile-account', 'AdminController@postUpdateProfileAccount');

    Route::post('/update-password', 'AdminController@postUpdatePassword');

    Route::get('/downloads', 'DownloadsController@index');

    Route::get('/downloads/certificate/{id}', 'DownloadsController@downloadCertificate')->name("certificate");



});
