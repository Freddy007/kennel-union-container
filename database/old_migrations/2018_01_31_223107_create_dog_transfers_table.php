<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDogTransfersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dog_transfers', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->char('dog_id', 36)->index('dog_transfers_dog_id_foreign');
			$table->char('owner_id', 36)->index('dog_transfers_owner_id_foreign');
			$table->timestamps();
			$table->string('transfer_serial_number', 100)->nullable();
			$table->boolean('confirmed');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dog_transfers');
	}

}
