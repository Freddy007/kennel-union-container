<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('dogs', function(Blueprint $table)
		{
//			$table->foreign('breeder_id', 'dogs_ibfk_1')->references('id')->on('breeders')->onUpdate('RESTRICT')->onDelete('CASCADE');
//			$table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('dogs', function(Blueprint $table)
		{
//			$table->dropForeign('dogs_ibfk_1');
//			$table->dropForeign('dogs_user_id_foreign');
		});
	}

}
