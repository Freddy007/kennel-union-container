<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRecordersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('recorders', function(Blueprint $table)
		{
			$table->foreign('dog_id', 'recorder_dog_id_foreign')->references('id')->on('dogs')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('member_id', 'recorder_member_id_foreign')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('staff_id', 'recorder_staff_id_foreign')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recorders', function(Blueprint $table)
		{
			$table->dropForeign('recorder_dog_id_foreign');
			$table->dropForeign('recorder_member_id_foreign');
			$table->dropForeign('recorder_staff_id_foreign');
		});
	}

}
