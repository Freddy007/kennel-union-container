<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->char('id', 36)->default('')->primary();
			$table->string('title');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('kennel_name', 100)->nullable();
			$table->string('email')->unique();
			$table->string('phone');
			$table->string('password', 60);
			$table->boolean('confirmed');
			$table->boolean('administrator');
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
