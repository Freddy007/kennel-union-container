<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDogRelationshipsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dog_relationships', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->char('dog_id', 36)->index('dog_relationships_dog_id_foreign');
			$table->string('father')->nullable();
			$table->string('mother')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dog_relationships');
	}

}
