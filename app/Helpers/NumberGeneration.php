<?php

/**
 * Created by IntelliJ IDEA.
 * User: Jimmy
 * Date: 12/3/2015
 * Time: 11:50 AM
 */
namespace App\Helpers;
class NumberGeneration
{
	public static function generateUsingDate(){

		$rand = array(
			rand(1, 4),
			rand(3, 7),
			rand(4, 9),
			rand(0, 9),
		);

		shuffle($rand);

		$rand = array_filter(explode("\r\n", chunk_split(implode('', $rand), 1)), function($x){
			if($x === '0'){
				return true;
			}else if($x){
				return true;
			}else{
				return false;
			}
		});

		$time = date('U', strtotime('now'));

		$tmp = array();
		$n = count($rand) + strlen($time);
		$c = 0;
		$d = 0;
		for($i = 0; $i < $n; $i++){
			if(isset($rand[$i])){
				$tmp[$c++] = $rand[$i];
				if($i % 2 == 0){
					$tmp[$c++] = $time[$d++];
					//$tmp[$c++] = '['.$i.'='.$time[$d++].']';
				}

			}else if(isset($time[$d])){
				//$tmp[$c++] = '['.$i.'='.$time[$d++].']';
				$tmp[$c++] = $time[$d++];
			}

		}
		return implode('', $tmp);
	}
}
