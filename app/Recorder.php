<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Recorder extends Model
{
    //
    protected $fillable = ['id','member_id','staff_id','dog_id','action'];


    public static function Log($member_id,$staff_id,$dog_id,$action){

        static::create([
            'id' => Uuid::generate(),
            'member_id' => $member_id,
            'staff_id'  => $staff_id,
            'dog_id'    => $dog_id,
            'action'   => $action
        ]);

    }

    public static function LogByUser($user){
       return static::whereMemberId($user)->get();
    }


}
