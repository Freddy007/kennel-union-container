<?php

namespace App;

use App\HelperClasses\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Webpatser\Uuid\Uuid;


class Dog extends Model
{
//	use Uuids;
    use SoftDeletes;


//    protected  $fillable = [
//        'id','user_id','name','dob','coat','breeder_id',
//        'sex','registration_number','titles','performance_titles',
//        'colour','height','elbow_ed_results','appraisal_score','hip_hd_results','tattoo_number',
//        'microchip_number','DNA','other_health_checks','image_name','needs_generation',
//        'confirmed','transferred','delete_request','dead','viewed','owner',
//        'other_registration_number','owner', 'offspring_count','deleted_at'
//    ];

    protected $guarded = [];
    protected $dates = ['deleted_at'];
    protected $casts = [
    	                'id' => 'string',
                        'user_id'=>'string',
                        'breeder_id'=>'string',
    ];

    /**
     * @return bool|string
     */
    public static function getLastDogTime(){
        $dog = DB::table('dogs')->latest()->first();
        return date($dog->created_at);
    }

    public function breed(){
        return $this->belongsTo(Breeder::class,"breeder_id");
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function generations(){
    	return $this->hasMany(DogGeneration::class);
    }

    public function duplicates(){
        return $this->hasMany(DogDuplicate::class);
    }

    public function relationships(){
        return $this->hasMany(DogRelationship::class);
    }


    /**
     * @return string
     */
    public static function generateRegistrationNumber(){
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $count = Dog::withTrashed()->get('name')->count() + 1;
//        $count = Dog::all('name')->count() + 1;
        return  $year . $month . $day . $count;
    }

    /**
     * @param $request
     */
    public static function createDog($request)
    {
        static::create($request);
    }

    /**
     * @param $request
     * @var Request $request
     */
    public static function registerLitter($request){

        // fetch input from fields
        $names = \Input::get('name',null);
        $genders = \Input::get('sex',null);
        $colours = \Input::get('colour',null);
        $heights = \Input::get('height',null);
        $coats = \Input::get('coat',null);
        $microchip_numbers = \Input::get('microchip_number',null);


        $father = Dog::getDog($request->father)? Dog::getDog($request->father)->registration_number : '' ;
        $mother = Dog::getDog($request->mother) ? Dog::getDog($request->mother)->registration_number : '';

        DB::transaction(function() use ($request,$father,$mother,$names,$genders,$colours,$heights,$coats,$microchip_numbers){

            foreach($names as $key=> $name){
                $sex = $genders[$key];
                $colour = $colours[$key];
                $height = $heights[$key];
                $coat = $coats[$key];
                $microchip_number = $microchip_numbers[$key];

                if(!$name == "" || $sex) {
                    $id = Uuid::generate();
//
                    Dog::create(
                        [
                        'id' => $id, 'name' => $name, 'breeder_id' => $request->breeder_id,
                        'registration_number' => static::generateRegistrationNumber(),
                        'user_id' => \Auth::id(), 'dob' => $request->dob,
                        'sex' => $sex,
                        'colour' => $colour,
                        'height' => $height,
                        'coat' => $coat,
                        'microchip_number' => $microchip_number
                        ]
                    );

                    DogRelationship::create([
                        'id' => Uuid::generate(), 'dog_id' => $id,
                        'father' => $father, 'mother' => $mother
                    ]);
                }
            }
        });
    }

    /**
     * @param $request
     * @param $dog_id
     * @throws \Throwable
     */
    public static function updateDog($request,$dog_id){

        /** @var Request $request */
        DB::transaction(function()use($request,$dog_id){
            $path = '/public/images/catalog';

            $dog_model = Dog::find($dog_id);

            $imageName = $request->hasFile('pic') ? 'image_' . $dog_id . '.' . $request->file('pic')->getClientOriginalExtension(): '';

            if($request->hasFile('pic')) {
//
                if ($dog_model->image_name !== null ){
                    \File::delete($path.'/'.$imageName);
                }

                $request->file('pic')->move(base_path() . $path, $imageName);

                $dog_model->update([
                    "image_name" => $imageName
                ]);
            }

		        $dog_model->update([
			        'name' => $request->name,
			        'other_registration_number' => $request->other_registration_number,
			        'dob' => $request->dob,
			        'coat' => $request->coat,
			        'breeder_id' => $request->breeder_id,
			        'sex' => $request->sex,
			        'titles' => $request->titles,
			        'performance_titles' => $request->performance_titles,
			        'colour' => $request->colour,
			        'height' => $request->height,
			        'elbow_ed_results' => $request->elbow_ed_results,
			        'appraisal_score' => $request->appraisal_score,
			        'hip_hd_results' => $request->hip_hd_results,
			        'tattoo_number' => $request->tattoo_number,
			        'microchip_number' => $request->microchip_number,
			        'DNA'  => $request->DNA,
			        'other_health_checks' => $request->other_health_checks
		        ]);





	        if($request->user_id == ''){
		        DB::statement( "UPDATE `dogs` SET user_id = NULL WHERE dogs.id =:dog",array('dog'=>$dog_id));
	        }else{
	        	$dog_model->update([
	        		'user_id' => $request->user_id
		        ]);
	        }



//	        DB::statement( "UPDATE `dogs` SET `user_id` = NULL WHERE `dogs`.`id` = '7663ece0-0cd0-11e8-a139-43df67a5776d'");
//	        DB::statement( "UPDATE `dogs` SET `user_id` = NULL WHERE `dogs`.`id` = ':dog'",['dog'=>$dog_id]);
//	        DB::statement( 'ALTER TABLE HS_Request AUTO_INCREMENT=:incrementStart', array('incrementStart' => 9999) );
        });
    }


    /**
     * @return int
     */
    public static function checkUnconfirmedDogs(){
        $dogs =  Dog::where('confirmed',false)->get();
        return  $counts = count($dogs);
    }


    public static function dogsByMonth($month){
        $counts = DB::table('dogs')->whereMonth('created_at','=',$month)->count();
        return $counts;
    }
    public static function dogTransfersByMonth($month){
        $counts = DB::table('dog_transfers')->whereMonth('created_at','=',$month)->count();
        return $counts;
    }

    public static function getRelationship($number){
        try {
            $dog = Dog::where('registration_number', $number)->first()->name;

        }catch (\Exception $e){
            return ' ';
        }
        return $dog;
    }

//    public static function deleteRequests($dog_id){
//      return  $status =  Dog::where('id',$dog_id)->where('delete_request',true)->first() ? true : false;
//    }

    public static function getDogId($number){
        try {
            $dog = Dog::where('registration_number',$number)->first()->id;

        }catch(\Exception $e){
            return '';
        }
        return $dog;
    }

    public static function getDog($dog_id){

        return Dog::where('id',$dog_id)->first();
    }

    public static function getDogByName($dog_name){

        return Dog::where('name',$dog_name)->first();
    }

    public static function getParent($number,$parent){
        try {
            $id = Dog::getDogId($number);
            $registration_number = DogRelationship::where('dog_id', $id)->first()->$parent;
            return Dog::getRelationship($registration_number);
        }catch (\Exception $e){
            return '';
        }
    }

	public static function getParents($number,$parent){
		try {
			$id = Dog::getDogId($number);

			$registration_number = DogRelationship::where('dog_id', $id)->first()->$parent;
			return Dog::getRelationship($registration_number);
		}catch (\Exception $e){
			return '';
		}
	}

    public static function getParentId($number,$parent,$any='id'){
        try {
            $id = Dog::getDogId($number);
            $registration_number = DogRelationship::where('dog_id', $id)->first()->$parent;
            return Dog::where('registration_number', $registration_number)->first()->$any;
        }catch (\Exception $e){
            return '';
        }
    }

    /**
     * @param $builder
     * @param Request $request
     * @return mixed
     */
    public static function queryDogsByField($builder, $request){
        switch($request->query('dogs')) {
            case "unconfirmed":
                $dogsData = $builder->where('dogs.confirmed',false)->get();
                break;
            case "confirmed":
                $dogsData = $builder->where('dogs.confirmed',true)->get();
                break;
            case "male":
                $dogsData = $builder->whereSex('male')->get();
                break;
            case "female":
                $dogsData = $builder->whereSex('female')->get();
                break;

            case "siblings":
                $dog_id = $request->query("dog_id");
                $relationship = \App\DogRelationship::whereDogId($request->query("dog_id"))->first();
                $father = $relationship->father;
                $mother = $relationship->mother;
                $relationship_ids = \App\DogRelationship::where("dog_id","<>",$dog_id)
                    ->whereNotNull("mother")->whereNotNull("father")
                    ->whereMother($mother)->whereFather($father)->pluck("dog_id");
                    $dogsData = $builder->whereIn('dogs.id', $relationship_ids)->get();
                break;

            default:
                if ($request->has("q")){
                    $query = $request->query("q");
                    $dogsData = $builder
                        ->orWhere('dogs.name', 'LIKE', '%' . $query . '%')
                        ->orWhere('dogs.registration_number', 'LIKE', '%' . $query . '%');
                }else{

                    /** @var Dog $builder */
                    \Cache::get('dogs');

                    $dogsData = \Cache::get('all_dogs', function () use($builder){
                        return   $dogsData = $builder->orderBy('created_at','desc')->get();
                    });
//                    $dogsData = \Cache::rememberForever('dogs', function() use( $builder) {
//                        return  $dogsData = $builder->orderBy('created_at','desc')->get();
//                    });

                    return $dogsData;

                }
        }

        return $dogsData;
    }

    /**
     * @param $sex
     * @return string
     */
    public static function getGender($sex){
        if($sex == 'male'){
            return 'Sire';
        }
        return  'Dam';
    }

    //  Advanced search function for Home Page
    public static function advancedSearch($request,$builder)
    {
        if ($request->query('name')) {
            $dogs = $builder->where('dogs.name', 'LIKE', '%' . $request->name . '%')->paginate(10);
        } elseif ($request->query('breed')) {
            $dogs = $builder->where('breeders.id', $request->breed)->paginate(10);
        } elseif ($request->query('colour')) {
            $dogs = $builder->where('colour', 'LIKE', '%' . $request->colour . '%')->paginate(10);
        } elseif ($request->query('sex')) {
            $dogs = $builder->where('sex', 'LIKE', '%'. $request->query('sex').'%')->paginate(10);
        }elseif($request->query('name') && $request->query('breed')){
            $dogs = $builder->where('name','LIKE','%'.$request->query('name').'%')
                ->where('breeders.id','LIKE','%'.$request->query('breed').'%')
                ->paginate(10);
        }

//      return  $paginator = new LengthAwarePaginator(range(1,count($dogs)),count($dogs),10,
//            Paginator::resolveCurrentPage(),
//            ['path' => Paginator::resolveCurrentPath()]
//        );
        return $dogs;
    }

    // Method for registering Dogs

    public static function registerNewDog(Request $request){

        $father = Dog::getDog($request->father)? Dog::getDog($request->father)->registration_number : '' ;
        $mother = Dog::getDog($request->mother) ? Dog::getDog($request->mother)->registration_number : '';

        try {

            $id = Uuid::generate();
            $year = date('Y');
            $month = date('m');
            $day = date('d');
            $count = Dog::all('name')->count() + 1;
            $registration_number = $year . $month . $day . $count;
            $user = $request->has('user_id') ? $request->user_id : '';

            $path = '/public/images/catalog';

            $imageName = $request->hasFile('pic') ? 'image_' . $id . '.' . $request->file('pic')->getClientOriginalExtension(): '';

            if($request->hasFile('pic')) {
                $request->file('pic')->move(base_path() . $path, $imageName);
            }

            Dog::create([
                'id' => $id, 'name' => $request->name,
                'user_id' => $request->$user,
                'registration_number' => $registration_number,
                'breeder_id' => $request->breeder_id,
                'dob' => $request->dob, 'sex' => $request->sex,
                'titles' => $request->titles,
                'coat'=>$request->coat,'other_certifications'=>$request->other_certifications,
                'performance_titles' => $request->performance_titles, 'colour' => $request->colour,
                'height' => $request->height, 'elbow_ed_results' => $request->elbow_ed_results,
                'hip_hd_results' => $request->hip_hd_results, 'tattoo_number' => $request->tattoo_number,
                'microchip_number' => $request->microchip_number, 'DNA' => $request->DNA,
                'other_health_checks' => $request->other_health_checks, 'image_name' => $imageName
            ]);


            $relationship_id = Uuid::generate();
            DogRelationship::create(['id'=>$relationship_id,'dog_id'=>$id,'father'=>$father,'mother'=>$mother]);

            flash()->success('Successfully registered a new dog !');
            return redirect('admin/all-dogs');
        } catch(\Exception $e){
            flash()->error('there seems to be an error in saving.');
            return \Redirect::back();
        }
    }

    public static function getUserDogs($user_id){
        $dogs = \DB::table('dogs')
                   ->leftJoin('users','users.id','=','dogs.user_id')
                   ->leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                   ->where('user_id',$user_id)
                   ->select(\DB::raw('dogs.*,CONCAT(users.first_name," ",users.last_name)
                 as user_name,breeders.name as breeder_name'))
                   ->paginate();
        return $dogs;
    }
}

