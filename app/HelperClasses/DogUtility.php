<?php
/**
 * Created by IntelliJ IDEA.
 * User: Freddy
 * Date: 7/13/2018
 * Time: 2:35 AM
 */

namespace App\HelperClasses;


 use App\DogGeneration;
 use Illuminate\Http\Request;

 class DogUtility {

	 /**
	  * @var
	  */
	 private $builder;

	 /**
	  * @var $request Request
	  */
	 private $request;

	 private $position;

	 private $name;

	 public static $FIRST_GENERATION = "first_generation";


	 /**
	  * DogUtility constructor.
	  *
	  */
	 public  function __construct() {

//	 	 $this->request = $request;
		 $this->generation           = $this->request->generation."_generation";
		 $this->position             = explode(',',$this->request->position);
		 $this->name                 = $this->request->name;
		 $this->registration_number  = $this->request->registration_number;
		 $this->titles               = $this->request->titles;
		 $this->dog_id               = $this->request->dog_id;
		 $this->dog_been_inserted_id       = $this->request->id;
		 $this->father               = explode(',',$this->request->get('father'));
		 $this->mother               = explode(',',$this->request->get('mother'));
		 $this->parent_generation    = $this->mother[0].'_generation';
		 $this->builder              = DogGeneration::whereDogId($this->dog_id)->first();

		 $this->generation_results = json_decode( json_encode( $this->builder->$this->generation ), true );

		 $this->parent_generation_results = json_decode( json_encode( $this->builder->$this->parent_generation ), true );

		 $this->dog_been_inserted_builder = DogGeneration::whereDogId($this->dog_been_inserted_id)->first();

		 $this->dog_been_inserted_first_generation_results = json_decode(json_encode($this->dog_been_inserted_builder->first_generation),true);
	 }


	 public function isFirstGeneration(){

	 	if($this->generation == static::$FIRST_GENERATION){
	 		return true;
	    }else{
	 		return false;
	    }
	 }


 }