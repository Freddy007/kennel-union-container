<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DogDuplicate extends Model
{
    protected $table = "dog_duplicates";

    protected $fillable = ["id","dog_id","registration_number","new_registration_number","fixed"];

    public function dog(){
        return $this->belongsTo(Dog::class,"dog_id");
    }
}
