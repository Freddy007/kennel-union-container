<?php

namespace App;

use DB;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Yajra\Datatables\Datatables;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $casts = [
        "id" => "string"
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','first_name','last_name','title',
        'kennel_name','phone', 'email', 'password','confirmed',
        'administrator',"member_id"
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function dogs(){
        return $this->hasMany('App\Dog');
    }

    public static function checkUnconfirmedMembers(){
        $members = DB::table('users')->where('confirmed',false)->count();
           // $counts = count($members);
            return $members;
    }

    public static function getLastMemberTime(){
        $member = DB::table('users')->latest()->first();
        return date($member->created_at);
    }

    public static function membersByMonth($month){
        $counts = DB::table('users')->whereMonth('created_at','=',$month)->count();
        return $counts;
    }

    public static function membersDataForTable($request){
//        $builder = static::all();
        $builder = DB::table('users');

        switch($request->query('members')){
            case "unconfirmed":
                $membersData = $builder->where('confirmed',false)->get();
                break;
            case "confirmed":
                $membersData = $builder->where('confirmed',true)->get();
                break;
            case "administrator":
                $membersData = $builder->where('administrator',true)->get();
                break;
            case "member":
                $membersData = $builder->where('administrator',false)->get();
                break;
            default:
                $membersData  =$builder->orderBy('created_at','desc')->get();
        }

        $members = new Collection();

        foreach($membersData as $member){

            $status = $member->confirmed == true ? "<button id=\"confirm-member-$member->id\" data-status=$member->confirmed  class=\"btn btn-success btn-xs\"><strong><i class=\"fa fa-thumbs-o-down\"></i></strong></button>":
                "<button id=\"confirm-member-$member->id\" data-status=$member->confirmed class=\"btn btn-success btn-xs\"><strong><i class=\"fa fa-thumbs-o-up\"></i></strong></button>";

            $members->push([
                'id' => $member->id,
                'name'=> $member->first_name .' '.$member->last_name,
                'kennel_name'=> $member->kennel_name,
//                'email'=> $member->email,
                'phone'=> $member->phone,
                'created_at'=> $member->created_at,
                'dogs'=> "<a href=\"/version2/user-dogs/$member->id\" class=\"btn btn-success btn-xs\">View Dogs</a>",
                'role' => $member->administrator ? 'Administrator' : 'Member',
                'status' => $member->confirmed == true ? "Confirmed " : "Unconfirmed",
                'action' => $status . "<a href=\"/admin/change-role/$member->id\" class=\"btn btn-success btn-xs\">change role</a>
                                    <button class=\"btn btn-danger btn-xs\" id=\"delete-member-$member->id\"><i class='fa fa-remove'><i></button>"
            ]);
        }
        return Datatables::of($members)->make(true);
    }


    /**
     * Check if the user is the super admin
     *
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->administrator == true;
    }

    /**
     * Check if the user is the super admin or admin
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->administrator == true;
    }


    /**
     * Check if the user is a member
     *
     * @return bool
     */
    public function isMember()
    {
        return $this->administrator == false;
    }

    /**
     * Check if the user is a member
     *
     * @return bool
     */
    public function isConfirmed()
    {
        return $this->confirmed == true;
    }

}
