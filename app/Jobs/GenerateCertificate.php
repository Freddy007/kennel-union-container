<?php

namespace App\Jobs;

use App\Dog;
use App\Download;
use App\Helpers\NumberGeneration;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;

class GenerateCertificate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $dog_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dog_id)
    {
        $this->dog_id = $dog_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $dog = $this->getDog($this->dog_id);
        $type_certificate = "PEDIGREE";

        $dogs_active = 'active';

        \Cache::forget('serial_number');
        \Cache::rememberForever('serial_number', function () {
            return NumberGeneration::generateUsingDate();
        });

        $customPaper = array(0,0,1500,2000);

        $file_name = Uuid::generate()->string.'-'.$this->dog_id.'.pdf';
//        $file_name = time().$this->dog_id.'.pdf';
        $path = url("/certificates/$file_name");

        $this->saveInDownloads($dog,$file_name, $path);

        \PDF::loadView("main-cert",compact('dog','type_certificate','dogs_active'))
            ->setPaper($customPaper, 'landscape')
            ->save(public_path("certificates")."/$file_name");
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDog($id)
    {
        $dog = Dog::leftJoin('breeders', 'breeders.id', '=', 'dogs.breeder_id')
            ->leftJoin('dog_relationships', 'dog_relationships.dog_id', '=', 'dogs.id')
            ->leftJoin('users', 'users.id', '=', 'dogs.user_id')
            ->where('dogs.id', $id)
            ->select(DB::raw('dogs.*,breeders.name as breed,
          dog_relationships.father,dog_relationships.mother,
          users.first_name,users.last_name,users.kennel_name'))
            ->first();
        return $dog;
    }

    /**
     * @param $dog
     * @param string $path
     * @throws \Exception
     */
    public function saveInDownloads($dog, $file_name, string $path): void
    {
        Download::create([
            "id" => Uuid::generate(),
            "name" => $dog->name,
            "dog_id" => $dog->id,
            "status" => 0,
            "file_url" => $path,
            "file_path" => $file_name
        ]);
    }
}
