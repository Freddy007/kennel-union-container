<?php

namespace App\Jobs;

use App\DogGeneration;
use App\HelperClasses\DogHelper;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Bus\Dispatchable;

class GenerateDogJob
{
    use Dispatchable;

    protected $dog;
    protected $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dog)
    {
        $this->dog = $dog;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DogHelper::generateAncestorsFromRegistrationNumber($this->dog, false);
    }
}
