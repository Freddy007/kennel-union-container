<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IssuedCertificate extends Model
{
    protected $fillable = ["id","dog_id","serial_number",'user_id'];
}
