<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Breeder
 *
 * @property string $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Breeder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Breeder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Breeder query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Breeder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Breeder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Breeder whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Breeder whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Breeder extends Model
{
    protected $fillable = ['id','name'];

    protected $casts = [
        "id" => "string"
    ];
}
