<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Breed
 *
 * @property string $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Dog $dog
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Breed newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Breed newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Breed query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Breed whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Breed whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Breed whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Breed whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Breed extends Model
{
    //
    protected $table = 'breeders';

    protected $casts = [
        "id" => "string"
    ];

    public function dog(){
        return $this->belongsTo('App\Dog');
    }
}
