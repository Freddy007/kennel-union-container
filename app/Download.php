<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
    //
    protected $guarded = [];

    protected $table = "downloads";

    protected $casts = [
        "id" => "string",
        "dog_id" => "string"
    ];
}
