<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class DownloadsProcessed
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $job;
    public $payload;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($job,$payload)
    {
        $this->job = $job;
        $this->payload = $payload;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
//        return new PrivateChannel('downloads-channel');
        return ['downloads-channel'];
    }

    public function broadcastWith()
    {
        $payload = $this->payload;
        $job = $this->job;
        return compact("payload", "job");
    }
}
