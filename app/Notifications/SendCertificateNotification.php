<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendCertificateNotification extends Notification
{
    use Queueable;

    private $user;
    private $dog;
    private $download_link;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $dog, $download_link)
    {
        $this->user = $user;
        $this->dog = $dog;
        $this->download_link = $download_link;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line("Your request for " .ucwords(strtolower($this->dog->name))."'s certificate has been approved! This download link is only valid for 72 hours.")
                    ->action('Download certificate', $this->download_link)
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
