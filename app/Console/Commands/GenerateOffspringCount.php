<?php

namespace App\Console\Commands;

use App\Dog;
use App\DogGeneration;
use App\DogRelationship;
use Illuminate\Console\Command;

//use Illuminate\Console\Command;


class GenerateOffspringCount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:count';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate offspring count';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

    	try{

		    $i = 0;

//		    foreach($dogs as $dog){
//
//			    $this->comment( "creating offspring for $dog->name => ".$i++ . PHP_EOL);
//		    }

            Dog::select('id', 'name', 'registration_number')->chunk(100, function($dogs) use($i){
                foreach ($dogs as $dog) {
                    $offspringCount = DogRelationship::orWhere("father", $dog->registration_number)
                        ->orWhere("mother", $dog->registration_number)->count();
                    Dog::find($dog->id)->update([
                        "offspring_count" => $offspringCount
                    ]);

                    $this->comment( "creating offspring count for $dog->name => ".$i++ . PHP_EOL);
                }
            });


        }catch (\Exception $exception){
		    $this->comment( $exception->getMessage().PHP_EOL
			    .$exception->getTraceAsString()

		    );

	    }

//        $this->comment(PHP_EOL.Inspiring::quote().PHP_EOL);
    }
}
