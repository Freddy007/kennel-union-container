<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class GenerateMemberIds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:member-ids';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $i = 0;
        foreach (User::all() as $user){
            $i++;
            $registration_number =   "KUG-".str_pad($i, 4, "0", STR_PAD_LEFT);
            User::find($user->id)->update([
                "member_id" => $registration_number
            ]);

            $this->comment( "new member id for $user->email => ".$i . PHP_EOL);

        }

    }
}
