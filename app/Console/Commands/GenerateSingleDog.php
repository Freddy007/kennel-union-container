<?php

namespace App\Console\Commands;

use App\Dog;
use App\DogGeneration;
use App\HelperClasses\DogHelper;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class GenerateSingleDog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:single-dog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate single dog generations';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	try{
		    $dogs = Dog::whereNeedsGeneration(true)->get();

		    $i = 0;

		    foreach($dogs as $dog){
			    DogHelper::generateAncestorsFromRegistrationNumber($dog->registration_number);
			    $this->comment( "creating generations for $dog->name => ".$i++ . PHP_EOL);
			    Dog::find($dog->id)->update(["needs_generation" => false]);
		    }

	    }catch (\Exception $exception){
		    $this->comment( $exception->getMessage().PHP_EOL
			    .$exception->getTraceAsString()
		    );

	    }
//        $this->comment(PHP_EOL.Inspiring::quote().PHP_EOL);
    }
}
