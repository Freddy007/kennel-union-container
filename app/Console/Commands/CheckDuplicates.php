<?php

namespace App\Console\Commands;

use App\Dog;
use App\DogDuplicate;
use App\DogGeneration;
use App\DogRelationship;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class CheckDuplicates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:duplicates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check dog registration duplicates';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DogDuplicate::truncate();
    	try{
		    $dogs = Dog::orderBy('created_at','asc')->get();

		    $i = 0;

		    foreach($dogs as $dog){
		        $count = Dog::whereRegistrationNumber($dog->registration_number)->count();
		        $offspringCount = DogRelationship::orWhere("father",$dog->registration_number)->orWhere("mother",$dog->registration_number)->count();

		        if ($count > 1){
                        DogDuplicate::create([
                            "dog_id" => $dog->id,
                            "registration_number" => $dog->registration_number,
                            "fixed" => false
                        ]);

                        $this->comment("duplicates found for $dog->registration_number => $dog->name => " . $count . PHP_EOL);
                        $this->comment("Offspring affected found for $dog->registration_number  => " . $offspringCount . PHP_EOL);
                    }
		    }


	    }catch (\Exception $exception){
		    $this->comment( $exception->getMessage().PHP_EOL
			    .$exception->getTraceAsString()

		    );

	    }

//        $this->comment(PHP_EOL.Inspiring::quote().PHP_EOL);
    }
}
