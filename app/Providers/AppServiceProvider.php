<?php

namespace App\Providers;

use App\Events\DownloadsProcessed;
use App\Generic;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Queue::after(function (JobProcessed $event) {
//             $event->connectionName
            $job     =  ($event->job);
            $payload = json_encode($event->job->payload());
            DownloadsProcessed::dispatch($job,$payload);
        });
    }
}
