<?php

namespace App\Providers;

use App\Events\DownloadsProcessed;
use App\Listeners\DownloadsProcessedListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        DownloadsProcessed::class => [
            DownloadsProcessedListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
//        Event::listen(
//            PodcastProcessed::class,
//            [SendPodcastNotification::class, 'handle']
//        );
        parent::boot();

        //
    }
}
