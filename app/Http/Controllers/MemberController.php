<?php

namespace App\Http\Controllers;

use App\DogRelationship;
use App\DogTransfer;
use App\HelperClasses\DogHelper;
use App\Helpers\NumberGeneration;
use App\Owner;
use App\RequestedCertificate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Webpatser\Uuid\Uuid;
use App\Dog;
use App\Http\Requests;
use DB;
use App\Breeder;
use App\User;
use Webpatser\Uuid\UuidFacade;

class
MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request)
    {
        $id = \Auth::getUser()->id;
        $dogs = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            //->where('confirmed', true)
                   ->where('user_id', $id)
                   ->select(DB::raw('dogs.*,breeders.id as breeder_id,breeders.name as
                              breeder_name,breeders.id as breeder_id'))
                   ->orderBy('dogs.created_at','desc')
                   ->paginate(10);

        if($request->query('term')){
            $query = $request->term;
            $dogs = Dog::where('dogs.name','LIKE','%'. $query.'%')
                       ->orWhere('dogs.registration_number', 'LIKE', '%' . $query . '%')
//                ->orWhere('breeder_name', 'LIKE', '%' . $query . '%')
                       ->orWhere('dogs.sex','LIKE','%'. $query.'%')
                       ->orderBy('created_at','desc')
                       ->paginate(10);
        }
        $breeders = Breeder::all();


//        return view('member.index', compact('dogs','breeders','manage_dogs'
        return view('version2.member.index', compact('dogs','breeders'));
    }

    public function getDog(Request $request,$dog_id){

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                  ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
                  ->where('dogs.id',$dog_id)
                  ->select(DB::raw('dogs.*,breeders.name as breeder_name,dog_relationships.father,dog_relationships.mother'))
                  ->first();


        if($request->ajax()){
            return view('version2.admin.partials.pedigree_table_partial',compact('dog'));
        }

        return view('version2.member.dog',compact('dog'));
    }


    public function getShowDog($dog_id)
    {
        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                  ->where('dogs.id',$dog_id)
                  ->select(DB::raw('dogs.*,breeders.name as breeder_name'))
                  ->first();
        $dog_relationships = DogRelationship::where('dog_id',$dog_id)->get();


        $dogs_active = 'active';

        return view('member.show',compact('dog','dog_relationships','dogs_active'
        ));
    }

    public function getEditDog($dog_id)
    {
        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                  ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
                  ->where('dogs.id',$dog_id)
                  ->select(DB::raw('dogs.*,breeders.name as breeder_name,dog_relationships.father,dog_relationships.mother'))
                  ->first();

        $breeders = Breeder::all();
        $dogs_active = 'active';

//        return view('member.edit',compact('dog','breeders','dogs_active'
        return view('version2.member.edit_dog',compact('dog','breeders','dogs_active'
        ));
    }

    public function postUpdateDog(Request $request, $dog_id){
        DB::transaction(function()use($request,$dog_id){
            Dog::where('id',$dog_id)->update($request->except(['_token','father','mother']));
            DogRelationship::where('dog_id',$dog_id)->update(['father'=> $request->father,'mother'=> $request->mother]);
        });

        flash()->success('Successfully updated dog info!');
        return redirect()->back();
    }

    public function getRegisterDog(){
        $breeds = Breeder::all();
        return view('version2.member.register_new_dog',compact('breeds'));
    }

    public function postRequestCertificate($dog_id){
            $id = Uuid::generate();
            RequestedCertificate::create(
                ['id' => $id,
                    'dog_id' => $dog_id,
                    'user_id' => \Auth::getUser()->id,
                    'honoured' => false
                ]);
    }

    public function postRegisterNewDog(Request $request){

        \Cache::forget('dogs');
        $this->validate($request,['name'=>'required','sex'=>'required',
            'dob'=>'required','breeder_id'=>
            'required','colour'=>'required']);

        try {

            $dog = new DogHelper($request);
            $dog_id = $dog->beginRegistrationProcess();
            return response()->json(['message'=>'Added a new dog','dog_id' =>$dog_id])
                             ->setStatusCode(200);
        } catch(\Exception $e){

            return response()->json(['message'=>'failed!','error'=>$e->getMessage()])->setStatusCode(500);
        }
    }

    public function getRegisterLitter(){

        $breeds = Breeder::all();
        $users = \Cache::get('users');
//        $users = User::all('id','first_name','last_name');
//        $dogs_active = 'active';

        return view('version2.member.register_litter',compact('breeds','users','dogs_active'));
    }

    public function postRegisterLitter(Request $request){

        try{
//            $this->validate($request,['name'=>'required','dob'=>'required','father'=> 'required','breeder_id'=> 'required']);

            $dog = new DogHelper($request);
            $dog->registerLitter();

        }catch(\Exception $e){
            return response()->json(['error'=>$e->getMessage()]);
        }

        return redirect()->back();

    }

    public function getTransferDog(){


        $transfers= 'active';

        return view('member.transfer_dog',compact('dogs','transfers'));
    }

    public function postTransferDog(Request $request){

        $this->validate($request,['first_name'=>'required','last_name'=>'required','phone_number'=>'required','email'=>'required']);

        DB::transaction(function() use($request){
            $transfer_id = Uuid::generate();
            $owner_id  = Uuid::generate();

            Owner::create([
                'id'=> $owner_id, 'first_name'=> $request->first_name,'last_name'=> $request->last_name,
                'phone_number'=> $request->phone_number,'sec_phone_number'=>$request->sec_phone_number,'email'=>$request->email,'notes'=>$request->notes
            ]);
            DogTransfer::create(['id'=>$transfer_id,'dog_id'=> $request->dog,'owner_id'=>$owner_id,'transfer_serial_number'=> NumberGeneration::generateUsingDate()]);
            Dog::where('id',$request->dog)->update(['transferred'=>true]);
        });

        //$transferred_dog = Dog::where('id',$request->dog)->first()->name;

        flash()->success('Request for dog transfer submitted !');
        return redirect('/member');
    }

    public function postRequestDelete($dog_id){
        Dog::where('id',$dog_id)
           ->update([
               'delete_request' => true
           ]);
//        flash()->success('Delete request successfully submitted to admin');
        return redirect()->back();
    }

    public function getDogs(Request $request){

        if($request->has('g')){
            $items = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                        ->where('sex',$request->query('g'))
                        ->where(function(\Illuminate\Database\Eloquent\Builder $query) use($request){
                            $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
                                  ->orWhere('registration_number','LIKE','%'.$request->query('q').'%');
                        })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
                        ->forPage($request->query('page')?:1,15)
                        ->paginate();
        }else{
            $items = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                        ->where(function(\Illuminate\Database\Eloquent\Builder $query) use($request){
                            $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
                                  ->orWhere('registration_number','LIKE','%'.$request->query('q').'%');
                        })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
                        ->forPage($request->query('page')?:1,15)
                        ->paginate();
        }

        return array('total_count'=>$items->total(),'items'=>$items->jsonSerialize());
    }


    public function getDogByName(Request $request,$dog_name){
        $gender = $request->query('gender') == 'sire' ? "male" :"female";
//    	$dog = Dog::whereName(urldecode($dog_name))->first();
        $dog = Dog::whereId(urldecode($dog_name))->first();

        if($request->query('gender')){
            if($dog){
                if($gender == $dog->sex){
                    return response()->json(
                        [
                            'id' => $dog->id,
                            'name' => $dog->name,
                            'sex' => $dog->sex,
                            'registration_number' => $dog->registration_number,
                            'titles' => $dog->titles
                        ])->setStatusCode(200);
                }else{
                    return response()->json(
                        [
                            'id' => $dog->id,
                            'name' => $dog->name,
                            'sex' => $dog->sex,
                            'registration_number' => $dog->registration_number,
                            'titles' => $dog->titles
                        ])->setStatusCode(403);
                }

            }else{
                return response()->json(['name' => 'empty'])->setStatusCode(400);
            }
        }else{
            if($dog){
                return response()->json(['message' => 'failed'])->setStatusCode(400);

            }else{
                return response()->json(['name' => 'empty'])->setStatusCode(200);

            }
        }
    }

    public function getSelectDogs(Request $request){

        if($request->has('g')){
            $items = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                        ->where('sex',$request->query('g'))
                        ->where(function(\Illuminate\Database\Eloquent\Builder $query) use($request){
                            $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
                                  ->orWhere('registration_number','LIKE','%'.$request->query('q').'%');
                        })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
                        ->forPage($request->query('page')?:1,15)
                        ->paginate();
        }else{
            $items = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                        ->where(function(\Illuminate\Database\Eloquent\Builder $query) use($request){
                            $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
                                  ->orWhere('registration_number','LIKE','%'.$request->query('q').'%');
                        })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
                        ->forPage($request->query('page')?:1,15)
                        ->paginate();
        }

        return array('total_count'=>$items->total(),'items'=>$items->jsonSerialize());
    }

    public function getTypeAheadDogs(Request $request){

//        if($request->has('g')){
//            $items = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
//                ->where('sex',$request->query('g'))
//                ->where(function(\Illuminate\Database\Eloquent\Builder $query) use($request){
//                    $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
//                        ->orWhere('registration_number','LIKE','%'.$request->query('q').'%');
//                })->select(\Illuminate\Support\Facades\DB::raw('dogs.*,breeders.name as breeder_name'))
//                ->pluck("name");
////            023529
//        }else{
//            $items = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
//                ->where(function(\Illuminate\Database\Eloquent\Builder $query) use($request){
//                    $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
//                        ->orWhere('registration_number','LIKE','%'.$request->query('q').'%');
//                })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
////                ->forPage($request->query('page')?:1,15)
//                ->pluck("name");
//        }
//
//        return $items;
//        return array('total_count'=>$items->total(),'items'=>$items->jsonSerialize());

        if($request->has('g')){
            $items = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                ->where('sex',$request->query('g'))
                ->where(function(\Illuminate\Database\Eloquent\Builder $query) use($request){
                    $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
                        ->orWhere('registration_number','LIKE','%'.$request->query('q').'%');
                })->select(\Illuminate\Support\Facades\DB::raw('dogs.*,breeders.name as breeder_name'))
                ->pluck("name");
        }else{
            $items = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                ->where(function(\Illuminate\Database\Eloquent\Builder $query) use($request){
                    $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
                        ->orWhere('registration_number','LIKE','%'.$request->query('q').'%');
                })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
//                ->forPage($request->query('page')?:1,15)
                ->pluck("name");
        }

        return $items;
    }


    public function getAddNewColumn(){
        return view('version2.admin.partials.add_new_column');
    }

    /**
     * @param $user_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|string
     */
    public function getUserDogs($user_id){

        try {
            $dogs = Dog::getUserDogs($user_id);

            $user = User::findOrFail($user_id);
            $first_name = $user->first_name;
            $last_name = $user->last_name;
            $user = \Auth::getUser()->first_name;

        }catch(\Exception $e){
            flash('There is an error!');
            return $e->getMessage();
        }

        $members = 'active';
        return view('version2.admin.member_dogs',compact('dogs','first_name','last_name','user','members'
        ));
    }



}
