<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;
//use Anam\PhantomMagick\Converter;
use App\DogRelationship;
use App\HelperClasses\DogHelper;
use App\Jobs\GenerateDogJob;
use Artesaos\SEOTools\SEOMeta;
use App;
//use Barryvdh\DomPDF\Facade as PDF;
use DB;
use App\Dog;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Exception;
use http\Env\Response;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use App\HelperClasses\DogUtility;
use Webpatser\Uuid\Uuid;

class HomeController extends Controller
{
    use Authorizable;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function postAddToPedigreeTable(Request $request){

        try{

            DB::transaction(function() use($request){

                $generation          = $request->generation."_generation";
                $position            = explode(',',$request->position);
                $name                = $request->name;
                $registration_number = $request->registration_number;
                $titles              = $request->titles;
                $dog_id              = $request->dog_id;
                $dog_been_inserted_id  = $request->id;
                $father               = explode(',',$request->get('father'));
                $mother               = explode(',',$request->get('mother'));
                $parent_generation    = $mother[0].'_generation';

                $builder = App\DogGeneration::whereDogId($dog_id)->first();

                $generation_results = json_decode( json_encode( $builder->$generation ), true );

                $parent_generation_results = json_decode( json_encode( $builder->$parent_generation ), true );

                $dog_been_inserted_builder = App\DogGeneration::whereDogId($dog_been_inserted_id)->first();

                $dog_been_inserted_first_generation_results = json_decode(json_encode($dog_been_inserted_builder->first_generation),true);
//
                if($generation == DogUtility::$FIRST_GENERATION) {
                    unset( $generation_results[ $position[0] ] );

                    $generation_results[ $position[0] ] = [
                        'name'                => $name,
                        'registration_number' => $registration_number,
                        'titles'              => $titles
                    ];

                    if ( $position[0] == 'sire' ) {

                        $current_dog_builder = Dog::find($dog_id)->first();

                        DogRelationship::whereDogId($dog_id)->first()->update([
                            'father' => $registration_number
                        ]);

                        DogHelper::generateAncestorsFromRegistrationNumber($current_dog_builder->registration_number,true);

//					    Pedigree::SaveFirstGenerationSirePartialAncestors($registration_number,$generation,$generation_results,$builder);

                    } elseif ( $position == 'dam' ) {

                        $current_dog_builder = Dog::find( $dog_id )->first();

                        DogRelationship::whereDogId( $dog_id )->first()->update( [
                            'mother' => $registration_number
                        ]);

                        DogHelper::generateAncestorsFromRegistrationNumber( $current_dog_builder->registration_number, true );
                    }
                }else {

                    unset( $generation_results[ $position[0] ][ $position[1] ] );
                    $generation_results[ $position[0] ][ $position[1] ] = [
                        'name'                => $name,
                        'registration_number' => $registration_number,
                        'titles'              => $titles
                    ];
                }



                if( $request->mother !== 'undefined' && $request->father !== 'undefined'){
                    unset($parent_generation_results[$father[1]][$father[2]]);

                    $parent_generation_results[$father[1]][$father[2]] = [
                        'name'                => $dog_been_inserted_first_generation_results['sire']['name'],
                        'registration_number' => $dog_been_inserted_first_generation_results['sire']['registration_number'],
                        'titles'              => $dog_been_inserted_first_generation_results['sire']['titles']
                    ];

                    unset($parent_generation_results[$mother[1]][$mother[2]]);
                    $parent_generation_results[$mother[1]][$mother[2]] = [
                        'name'                => $dog_been_inserted_first_generation_results['dam']['name'],
                        'registration_number' => $dog_been_inserted_first_generation_results['dam']['registration_number'],
                        'titles'              => $dog_been_inserted_first_generation_results['dam']['titles']
                    ];

                    $builder->update([
                        $generation         =>  $generation_results,
                        $parent_generation  =>  $parent_generation_results
                    ]);

                    DogRelationship::whereDogId($dog_been_inserted_id)->first()->update([
                        'father'  => $dog_been_inserted_first_generation_results['sire']['registration_number'],
                        'mother'  => $dog_been_inserted_first_generation_results['dam']['registration_number']
                    ]);

                }else{
                    $builder->update([
                        $generation         =>  $generation_results
                    ]);
                }
            });

            return response()->json([
                'message' => 'success',
            ])->setStatusCode(200);

        }catch(\Exception $e){
//
            return response()->json([
                'message' => 'failed',
                'error'   => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ])->setStatusCode(500);
        }
    }

    public function insertToGenerationTable(Request $request){

        try {
            $generation = $request->generation;
            $position = explode(',', $request->position);
            $offspring = explode(',', $request->offspring);
            $name = $request->name;
            $registration_number = $request->registration_number;
            $titles = $request->titles;
            $dog_id = $request->dog_id;
            $dog_been_inserted_id = $request->id;
            $father = explode(',', $request->get('father'));
            $mother = explode(',', $request->get('mother'));
            $builder = App\DogGeneration::whereDogId($dog_id)->first();
            $dog_been_inserted_builder = Dog::whereId($dog_been_inserted_id)->first();
            $_generation = "_generation";

            if ($offspring[0] === "undefined" && $generation === "first") {
                $dog_relationship = DogRelationship::where("dog_id", $dog_id)->first();
                if ($position[0] === "sire") {
                    if ($dog_relationship){
                        DogRelationship::where("dog_id", $dog_id)->update([
                            "father" => $registration_number
                        ]);
                    }else{
                        DogRelationship::create([
                            "id" => Uuid::generate(),
                            "dog_id" => $dog_id,
                            "father" => $registration_number
                        ]);
                    }
                }

                if ($position[0] === "dam") {
                    if ($dog_relationship) {
                        DogRelationship::where("dog_id", $dog_id)->update([
                            "mother" => $registration_number
                        ]);
                    }else{
                        DogRelationship::create([
                            "id" => Uuid::generate(),
                            "dog_id" => $dog_id,
                            "mother" => $registration_number
                        ]);
                    }
                }

                return response()->json(["offspring" => $offspring, "position" => $position,
                    "dog_id" => $dog_id], 200);

            }

            if (sizeof($offspring) === 2) {
                $first = $offspring[0] . $_generation;
                $offspringPosition = $builder->$first;
                $second = $offspringPosition[$offspring[1]];
                $offspring_registration_number = $second["registration_number"];

                $this->insertParent($dog_been_inserted_builder, $position, $offspring_registration_number, $registration_number);

                return response()->json(["generations" => $builder,
                    "offspring" => $offspring, "position" => $builder[$generation . "_generation"],
                    "first" => $offspringPosition, "type" => gettype($offspringPosition),
                    "second" => $second, "name" => $second["name"]], 200);
            }

            if (sizeof($offspring) === 3) {
                $first = $offspring[0] . $_generation;
                $offspringPosition = $builder->$first;
                $secondOffspringPosition = $offspring[1];
                $thirdOffspringPosition = $offspring[2];
                $object = ($builder->$first["$secondOffspringPosition"]["$thirdOffspringPosition"]);
                try {
                    $offspring_registration_number = $object["registration_number"];

                    $this->insertParent($dog_been_inserted_builder, $position,
                        $offspring_registration_number, $registration_number);

                    return response()->json([
                        "generations" => $builder,
                        "offspring" => $offspring,
                        "position" => $builder[$generation . "_generation"],
                        "first" => $offspringPosition,
                        "second" => $secondOffspringPosition,
                        "type" => gettype($offspringPosition) ,
                        "third" => $thirdOffspringPosition,
                        "object" => $object,
                        "registration_number" => $offspring_registration_number
                    ], 200);
                }catch (\Exception $exception){
                    return response()->json(["generations" => $builder,
                        "offspring" => $offspring, "position" => $builder[$generation . "_generation"],
                        "first" => $offspringPosition, "type" => gettype($offspringPosition), "second" => $object,
                        "third" => $thirdOffspringPosition ], 500);
                }
            }
        }catch (\Exception $exception){
            return response()->json(["status" => "failed", "error" => $exception->getMessage(), "line" =>$exception->getLine()],500);
        }

        return response()->json(["status" => "success"],200);
    }

    public function registerNewDogOnTable(Request $request){
        $offspring = explode(',', $request->offspring);
        $position = explode(',', $request->position);
        $generation = $request->generation;
        $dog_id = $request->dog_id;
        $registration_number = $request->registration_number;
        $builder = App\DogGeneration::whereDogId($dog_id)->first();
        $current_dog = Dog::find($dog_id);
        $_generation = "_generation";

        if ($offspring[0] === "undefined" && $generation === "first") {

            $dog = new DogHelper($request);
            $registered_dog_id = $dog->beginRegistrationProcess();
            $dog_relationship = DogRelationship::Where("dog_id", $registered_dog_id)->first();
            if ($position[0] === "sire") {

                if ($dog_relationship){
                    DogRelationship::where("dog_id", $dog_id)->update([
                        "father" => $dog->registrationNumber
                    ]);
                }else{
                    DogRelationship::create([
                        "id" => Uuid::generate(),
                        "dog_id" => $dog_id,
                        "father" => $dog->registrationNumber
                    ]);
                }
            }

            if ($position[0] === "dam") {

                if ($dog_relationship){
                    DogRelationship::where("dog_id", $dog_id)->update([
                        "mother" => $dog->registrationNumber
                    ]);
                }else{
                    DogRelationship::create([
                        "id" => Uuid::generate(),
                        "dog_id" => $dog_id,
                        "mother" => $dog->registrationNumber
                    ]);
                }
            }


            return response()->json(["offspring" => $offspring, "position" => $position,
                "dog_id" => $dog_id], 200);
        }

        if (sizeof($offspring) === 2) {
            $first = $offspring[0] . $_generation;
            $offspringPosition = $builder->$first;
            $second = $offspringPosition[$offspring[1]];
            $offspring_registration_number = $second["registration_number"];
            $offspring_id = Dog::whereRegistrationNumber($offspring_registration_number)->first()->id;
            $dog = new DogHelper($request);
            $registered_dog_id = $dog->beginRegistrationProcess();
            $dog_relationship = DogRelationship::where("dog_id", $offspring_id)->first();

                if ($position[0] === "sire") {
                    if ($dog_relationship) {
                        DogRelationship::where("dog_id", $offspring_id)->update([
                            "father" => Dog::find($registered_dog_id)->registration_number
                        ]);
                    }else {
                        DogRelationship::create([
                            "id" => Uuid::generate(),
                            "dog_id" => $offspring_id,
                            "father" => Dog::find($registered_dog_id)->registration_number
                        ]);
                    }
                }

                if ($position[0] === "dam") {

                    if ($dog_relationship) {
                        DogRelationship::where("dog_id", $offspring_id)->update([
                            "mother" => Dog::find($registered_dog_id)->registration_number
                        ]);
                    }else {
                        DogRelationship::create([
                            "id" => Uuid::generate(),
                            "dog_id" => $offspring_id,
                            "mother" => Dog::find($registered_dog_id)->registration_number
                        ]);
                    }
                }

            return response()->json(['message'=>'Added a new dog','dog_id' =>$dog_id,
                "offspring" => $second,
                "offspring_id" => $offspring_id,
                "reg_number" => $dog->registrationNumber,
                "registered_dog_id" => $registered_dog_id])
                ->setStatusCode(200);

            return response()->json(["generations" => $builder,
                "offspring" => $offspring, "position" => $builder[$generation . "_generation"],
                "first" => $offspringPosition, "type" => gettype($offspringPosition), "second" => $second, "name" => $second["name"]], 200);
        }

        if (sizeof($offspring) === 3) {
            $first = $offspring[0] . $_generation;
            $offspringPosition = $builder->$first;
            $secondOffspringPosition = $offspring[1];
            $thirdOffspringPosition = $offspring[2];
            $object = ($builder->$first["$secondOffspringPosition"]["$thirdOffspringPosition"]);
            try {
                $offspring_registration_number = $object["registration_number"];
                $offspring_id = Dog::whereRegistrationNumber($offspring_registration_number)->first()->id;
                $dog = new DogHelper($request);
                $registered_dog_id = $dog->beginRegistrationProcess();
                $dog_relationship = DogRelationship::where("dog_id", $offspring_id)->first();


                if ($position[0] === "sire") {
                    if ($dog_relationship){
                        DogRelationship::where("dog_id", $offspring_id)->update([
                            "father" => Dog::find($registered_dog_id)->registration_number
                        ]);
                    }else{
                        DogRelationship::create([
                            "id" => Uuid::generate(),
                            "dog_id" => $offspring_id,
                            "father" => Dog::find($registered_dog_id)->registration_number
                        ]);
                    }
                }

                if ($position[0] === "dam") {

                    if ($dog_relationship){
                        DogRelationship::where("dog_id", $offspring_id)->update([
                            DogHelper::MOTHER => Dog::find($registered_dog_id)->registration_number
                        ]);
                    }else{
                        DogRelationship::create([
                            "id" => Uuid::generate(),
                            "dog_id" => $offspring_id,
                            DogHelper::MOTHER => Dog::find($registered_dog_id)->registration_number
                        ]);
                    }
                }

                return response()->json(["generations" => $builder,
                    "offspring" => $offspring, "position" => $builder[$generation . "_generation"],
                    "first" => $offspringPosition, "type" => gettype($offspringPosition) ,
                    "third" => $thirdOffspringPosition ], 200);
            }catch (\Exception $exception){
                return response()->json(["generations" => $builder,
                    "offspring" => $offspring, "position" => $position[0],
                    "first" => $offspringPosition, "type" => gettype($offspringPosition), "second" => $object,
                    "third" => $thirdOffspringPosition ], 500);
            }
        }


    }

    public function getIndex(Request $request){

        \Artesaos\SEOTools\Facades\SEOMeta::setTitle('Home');
        \Artesaos\SEOTools\Facades\SEOMeta::setDescription('Kennel Union Ghana Main Website');
        \Artesaos\SEOTools\Facades\SEOMeta::setCanonical('http://kennelunionofghana.com');

        try {

            $builder = Dog::leftJoin('breeders', 'breeders.id', '=', 'dogs.breeder_id')
                          ->leftJoin('users', 'users.id', '=', 'dogs.user_id')
                          ->select(DB::raw('dogs.*,breeders.name as breeder_name,breeders.id as breeder_id,users.kennel_name'))
                          ->where('dogs.confirmed', true);

            $dogs = $builder->orderBy('created_at', 'desc')->paginate(4);

            $breeds = App\Breed::all();
        } catch(\Exception $e){
            flash()->error('Error Loading the page '.$e->getMessage());
            \Redirect::back();
        }

        return view('index3',compact('dogs','breeds'));
    }

    public function getSearchResults(Request $request){
        $name_query = $request->query('name');
        $sex_query = $request->query('sex');
        $breed_query = $request->query('breed');

//        return $builder = Dog::with("breed","user")
//            ->whereConfirmed(true)
//            ->where('name', 'LIKE', '%' . $name_query . '%')
//                ->orWhere('breeder_id', "=",$breed_query)
//                ->orWhere('dogs.sex', $sex_query )

        $builder = Dog::leftJoin('breeders', 'breeders.id', '=', 'dogs.breeder_id')
                      ->leftJoin('users', 'users.id', '=', 'dogs.user_id')
                      ->select(DB::raw('dogs.*,breeders.name as breeder_name,breeders.id as breeder_id,users.kennel_name'))
                      ->where('dogs.confirmed', true);

        $dogs = $builder->where('dogs.name', 'LIKE', '%' . $name_query . '%')
                        ->orWhere('breeder_id',$breed_query)
                        ->orWhere('dogs.sex', $sex_query )
                        ->orWhere('dogs.microchip_number', $name_query)
                        ->orWhere('dogs.tattoo_number', $name_query)
                        ->orWhere('dogs.registration_number', $name_query)
                        ->get();

        $paginator = new LengthAwarePaginator(range(1,count($dogs)),count($dogs),12,
            Paginator::resolveCurrentPage(),
            ['path' => Paginator::resolveCurrentPath()]
        );

        $dogs = $builder->orderBy('created_at', 'desc')->paginate(12);
        $breeds =  $breed_query == "all"?  App\Breeder::all() : App\Breed::whereId($breed_query)->first();
//        return gettype($breeds);

        return view('search_items',compact('dogs','paginator','breeds',"breed_query"));
    }

    public function getMembers(){
        return view('members.index');
    }

    public function getShowDog($dog_id){

//        try{
            $dog = Dog::leftJoin('breeders', 'breeders.id', '=', 'dogs.breeder_id')
                      ->leftJoin('dog_relationships', 'dog_relationships.dog_id', '=', 'dogs.id')
                      ->leftJoin('users','users.id','=','dogs.user_id')
                      ->where('dogs.id', $dog_id)
                      ->select(DB::raw('dogs.*,breeders.name as breeder_name,dog_relationships.father,
                dog_relationships.mother,CONCAT(users.first_name," ",users.last_name)
                as member, users.kennel_name, users.phone,users.email'))
                      ->first();

            \Artesaos\SEOTools\Facades\SEOMeta::setTitle($dog->name);
            \Artesaos\SEOTools\Facades\SEOMeta::setDescription($dog->breeder_name);
            \Artesaos\SEOTools\Facades\SEOMeta::addMeta('article:published_time', $dog->created_at->toW3CString(), 'property');
            \Artesaos\SEOTools\Facades\SEOMeta::addMeta('article:section', $dog->breeder_name, 'property');
            \Artesaos\SEOTools\Facades\SEOMeta::addKeyword(['pedigree', 'Rottweiller', 'dogs','American Bulldog','French Bulldog']);

            $view_count = $dog->viewed + 1;
            Dog::where('id', $dog_id)->update(['viewed' => $view_count]);

            return view('dog_detail',compact('dog'));
//        }catch(\Exception $e){

//            flash()->error('Unable to fetch dog\'s\ details at this time');
//            return redirect()->back();
//        }
    }

    public function getDogs(){
        $breeders= DB::table('breeders')->select('name');
        $dogs = DB::table('dogs')->union($breeders)->lists('name');
        return json_encode($dogs);
    }

    public function getSelectDogs(Request $request){

        if($request->has('g')){
            $items = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                        ->where('sex',$request->query('g'))
                        ->where(function(Builder $query) use($request){
                            $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
                                  ->orWhere('registration_number','LIKE','%'.$request->query('q').'%');
                        })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
                        ->forPage($request->query('page')?:1,15)
                ->paginate();
        }else{
            $items = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                        ->where(function(Builder $query) use($request){
                            $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
                                  ->orWhere('registration_number','LIKE','%'.$request->query('q').'%');
                        })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
                        ->forPage($request->query('page')?:1,15)
                        ->paginate();
        }

        return array('total_count'=>$items->total(),'items'=>$items->jsonSerialize());
    }

    public function getTypeAheadDogs(Request $request){

        $items = Dog::with("duplicates")->leftJoin('breeders','breeders.id','=','dogs.breeder_id');
//            ->leftJoin('dog_duplicates','dog_duplicates.dog_id','=','dogs.id');
        if($request->has('g')) {

            $items->where('sex', $request->query('g'))
                ->where(function (Builder $query) use ($request) {
                    $query->where('dogs.name', 'LIKE', '%' . $request->query('q') . '%')
                        ->orWhere('registration_number', 'LIKE', '%' . $request->query('q') . '%');
                })->select(\Illuminate\Support\Facades\DB::raw('dogs.*,breeders.name as breeder_name'))
                ->pluck("name");


        }else if ($request->has("type") && $request->query("archives")){
            $items = Dog::onlyTrashed()
                ->leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                ->leftJoin('dog_duplicates','dog_duplicates.dog_id','=','dogs.id')
               ->where(function(Builder $query) use($request){
                    $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
                        ->orWhere('dogs.registration_number','LIKE','%'.$request->query('q').'%');
                })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
                    ->forPage($request->query('page')?:1,15);

        }else if ($request->has("type") && $request->query("archives")){
            $items = Dog::onlyTrashed()
                ->leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                ->leftJoin('dog_duplicates','dog_duplicates.dog_id','=','dogs.id')
                ->where(function(Builder $query) use($request){
                    $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
                        ->orWhere('dogs.registration_number','LIKE','%'.$request->query('q').'%');
                })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
                ->forPage($request->query('page')?:1,15);
        }else{

          $items->where(function(Builder $query) use($request){
                    $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
                        ->orWhere('dogs.registration_number','LIKE','%'.$request->query('q').'%');
          })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
              ->forPage($request->query('page')?:1,15);
        }

        return $items->get(['duplicate_registration_number']);
    }


    public function getShowOffspring(Request $request,$registration_number){

        try {
            $builder = DogRelationship::leftJoin('dogs', 'dogs.id', '=', 'dog_relationships.dog_id')
                                      ->leftJoin('breeders', 'breeders.id', '=', 'dogs.breeder_id')
                                      ->select(DB::raw('dogs.*,breeders.name as breeder_name'))
                                      ->where('father', $registration_number)
                                      ->orWhere('mother', $registration_number);

            if ($request->query('term')) {
                $builder->where('dogs.name', 'LIKE', '%' . $request->query('term') . '%')->paginate(10);
            }
            $offspring = $builder->paginate(10);
            $dog = Dog::where('registration_number', $registration_number)->select(DB::raw('name'))->first();
        }catch (\Exception $e){
            flash()->error("Error Fetching Detail.");
            return redirect('/');
        }

        $index = 'active';

        return view('offspring',compact('offspring','dog','index'));
    }

    public function getCheckDogName($name){

        $dog = Dog::whereRaw('LOWER(`name`) like ?', array( $name))->first();
        $checkNameAvailabilty =  $dog ? true : false;
        if($checkNameAvailabilty == true){
            return  \Response::json([
                'error' => [
                    'message' => 'dog name already exists !']], 502);
        }
    }

    public function getAddNewColumn(){
        return view('version2.admin.partials.add_new_column');
    }

    public function getProfileSettings(){

        $id = \Auth::user()->id;


        $user = App\User::find($id);

        $users = \App\User::all('id','email','phone','first_name','last_name');

        $dogs_count = Dog::where('user_id',$id)->count();

        return view('profile_settings',compact('users','user','dogs_count','index'));
    }

    public function postSettings(Request $request)
    {
        \Cache::forget('notification_email');
        \Cache::rememberForever('notification_email', function () use ($request) {
            return $request->email;
        });

        \Cache::forget('notification_phone_number');
        \Cache::rememberForever('notification_phone_number', function () use ($request) {
            return $request->phone_number;
        });

        App\User::find(\Auth::id())->update($request->all());

        return redirect()->back();

    }

    public function getSerial(){
        $counts = App\Helpers\NumberGeneration::generateUsingDate();
        return $counts;
    }

    public function getPrintPdf($dog_id){
        if(\Auth::guest()) {
            return 'You are not allowed access to this page';
        } elseif(!\Auth::guest() && \Auth::user()->administrator == false){
            return 'You are not allowed access to this page';
        }
        else {
            self::printPdfInline('http://pedigree.kennelunionghana.com/pdf-certificate/'.$dog_id);
        }
    }

    public function getPdfCertificate( $dog_id){

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                  ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
                  ->leftJoin('users','users.id','=','dogs.user_id')
                  ->where('dogs.id',$dog_id)
                  ->select(DB::raw('dogs.*,breeders.name as breed,dog_relationships.father,dog_relationships.mother,
                              users.first_name,users.last_name,users.kennel_name'))
                  ->first();
        // $view_count = $dog->viewed + 1;
        //Dog::where('id',$dog_id)->update(['viewed'=> $view_count]);
        $type_certificate = "PEDIGREE";

        $dogs_active = 'active';

        \Cache::forget('serial_number');
        \Cache::rememberForever('serial_number', function () {
            return App\Helpers\NumberGeneration::generateUsingDate();
        });

//         $pdf = PDF::loadView('main-cert', compact('dog','type_certificate','dogs_active'));

//        return $pdf->download('disney.pdf');

//        return view('demo-cert',compact('dog','type_certificate','dogs_active'));
        return view('main-cert',compact('dog','type_certificate','dogs_active'));
    }

//    public function getPrintFullCert(Request $request,$dog_id){
//
//        if(\Auth::guest()) {
//            return 'You are not allowed access to this page';
//        } elseif(!\Auth::guest() && \Auth::user()->administrator == false){
//            return 'You are not allowed access to this page';
//        }else {
//            $options = [
//                'width' => 1800,
//                'height'=>1292,
//                'quality' => 980
////                'quality' => 1280
//            ];
//
//            $name = $request->query('name')? "name=".$request->query('name')."&":'';
//            $phone = $request->query('phone')?"phone=".$request->query('phone')."&":'';
//            $address = $request->query('address')?"address=".$request->query('address'):'';
//
//            Converter::make(url("pdf-certificate/$dog_id?$name$phone$address"))
//                ->toPng($options)
//                ->download($dog_id.'.png');
//        }
//    }

//    public function getTransferCert($dog_id){
//
//        if(\Auth::guest()) {
//            return 'You are not allowed access to this page';
//        } elseif(!\Auth::guest() && \Auth::user()->administrator == false){
//            return 'You are not allowed access to this page';
//        }else {
//            $options = [
//                'width' => 1800,
//                'height'=>1292,
//                'quality' => 980
////                'quality' => 1280
//            ];
//
////            Converter::make('http://kug.dev/admin/transfer-certificate/'.$dog_id)
//            Converter::make('http://pedigree.kennelunionghana.com/admin/transfer-certificate/'.$dog_id)
//                ->toPng($options)
//                ->download($dog_id.'.png');
//        }
//    }

    public function getTransferCertificate($dog_id){

        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                  ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
                  ->leftJoin('users','users.id','=','dogs.user_id')
                  ->leftJoin('dog_transfers','dog_transfers.dog_id','=','dogs.id')
                  ->leftJoin('owners','owners.id','=','dog_transfers.owner_id')
                  ->where('dogs.id',$dog_id)
                  ->select(DB::raw('dogs.*,breeders.name as breed,dog_relationships.father,dog_relationships.mother,
                              users.first_name,users.last_name,CONCAT(owners.first_name," ", owners.last_name) as new_owner,
                              owners.phone_number,owners.notes,
                              dog_transfers.transfer_serial_number'))
                  ->first();
        \Cache::forget('serial_number');
        \Cache::rememberForever('serial_number', function () {
            return NumberGeneration::generateUsingDate();
        });

        $type_certificate = "BIRTH";

        $dogs_active = 'active';

        return view('admin.transfer_certificate',compact('dog','dog_id',
            'dogs_active','type_certificate'
        ));

    }


    public function getLandscapePdf($dog_id){

        $dog = Dog::leftJoin('breeders', 'breeders.id', '=', 'dogs.breeder_id')
                  ->leftJoin('dog_relationships', 'dog_relationships.dog_id', '=', 'dogs.id')
                  ->leftJoin('users', 'users.id', '=', 'dogs.user_id')
                  ->where('dogs.id', $dog_id)
                  ->select(DB::raw('dogs.*,breeders.name as breed,dog_relationships.father,dog_relationships.mother,
                              users.first_name,users.last_name'))
                  ->first();
        // $view_count = $dog->viewed + 1;
        //Dog::where('id',$dog_id)->update(['viewed'=> $view_count]);
        $type_certificate = "PEDIGREE";

        $dogs_active = 'active';

        \Cache::forget('serial_number');
        \Cache::rememberForever('serial_number', function () {
            return App\Helpers\NumberGeneration::generateUsingDate();
        });

        return view('certificates.landscape-cert',compact('dog','type_certificate','dogs_active'));
    }


    // demo cert for trials
    public function getDemoCert( $dog_id){

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                  ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
                  ->leftJoin('users','users.id','=','dogs.user_id')
                  ->where('dogs.id',$dog_id)
                  ->select(DB::raw('dogs.*,breeders.name as breed,dog_relationships.father,dog_relationships.mother,
                              users.first_name,users.last_name'))
                  ->first();
        // $view_count = $dog->viewed + 1;
        //Dog::where('id',$dog_id)->update(['viewed'=> $view_count]);
        $type_certificate = "PEDIGREE";

        $dogs_active = 'active';

        \Cache::forget('serial_number');
        \Cache::rememberForever('serial_number', function () {
            return App\Helpers\NumberGeneration::generateUsingDate();
        });

        return view('demo-cert',compact('dog','type_certificate','dogs_active'));
    }

    public function getContactUs(){
        return view('contact_us');
    }



//    function printPdfInline($content){
//        $pdf = new  \mikehaertl\wkhtmlto\Pdf(array(
//            'binary' => '/opt/wkhtmltox/bin/wkhtmltopdf',
//            'commandOptions' => array(
//                'escapeCommand' => false,
//                'useExec' => true,
//            ),
//
//            'no-outline',         // Make Chrome not complain
//            'margin-top'    => 0,
//            'margin-right'  => 0,
//            'margin-bottom' => 0,
//            'margin-left'   => 0,
//
//
//            // Default page options
//            'disable-smart-shrinking',
//            'user-style-sheet' => public_path().'/css/pdf.css',
//
//        ));
//
//        $pdf->setOptions(array('orientation' => 'landscape', 'margin-top' => '0', 'margin-right' => '0', 'margin-bottom' => '0', 'margin-left' => '0'));
//
////        $pdf->setOptions(array(
////            'orientation' => 'landscape'
////        ));
//
////        $pdf->setOptions(array('orientation' => 'landscape', 'margin-top' => '0', 'margin-right' => '0', 'margin-bottom' => '0', 'margin-left' => '0'));
//
//        $pdf->addPage($content);
//        // Save the PDF
//        // ... or send to client as file download
//        $pdf->send('pedigree_certificate_'.\Auth::user()->id.'.pdf');
//
//        if (!$pdf->send()) {
//            throw new \Exception('Could not create PDF: '.$pdf->getError());
//        }
//    }
//    public function getGeneratePdf($dog_id){
//
//        $options = [
//            'format' => 'A4',
//            'zoomfactor' => 1,
//            'orientation' => 'landscape',
//            'margin' => '1cm'
//        ];
//
//        Converter::make('http://pedigree.kennelunionghana.com/pdf-certificate/'.$dog_id)
//            ->toPng($options)
//            ->download($dog_id.'.png');
//    }


    public function getDogByName(Request $request,$dog_name){
        $gender = $request->query('gender') == 'sire' ? "male" :"female";
//    	$dog = Dog::whereName(urldecode($dog_name))->first();
        $dog = Dog::whereRegistrationNumber(urldecode($dog_name))->first();

        if($request->query('gender')){
            if($dog){
                if($gender == $dog->sex){
                    return response()->json(
                        [
                            'id' => $dog->id,
                            'name' => $dog->name,
                            'sex' => $dog->sex,
                            'registration_number' => $dog->registration_number,
                            'titles' => $dog->titles
                        ])->setStatusCode(200);
                }else{
                    return response()->json(
                        [
                            'id' => $dog->id,
                            'name' => $dog->name,
                            'sex' => $dog->sex,
                            'registration_number' => $dog->registration_number,
                            'titles' => $dog->titles
                        ])->setStatusCode(403);
                }

            }else{
                return response()->json(['name' => 'empty'])->setStatusCode(400);
            }
        }else{
            if($dog){
                return response()->json(['message' => 'failed'])->setStatusCode(400);

            }else{
                return response()->json(['name' => 'empty'])->setStatusCode(200);

            }
        }
    }

    public function getDogInfo(Request $request,$dog_name){
        $gender = $request->query('gender') == 'sire' ? "male" :"female";
//    	$dog = Dog::whereName(urldecode($dog_name))->first();
//        $dog = Dog::whereId(urldecode($dog_name))->first();
        $dog = Dog::whereName(urldecode($dog_name))->first();

            if($dog){
                    return response()->json(
                        [
                            'id' => $dog->id,
                            'name' => $dog->name,
                            'sex' => $dog->sex,
                            'registration_number' => $dog->registration_number,
                            'titles' => $dog->titles
                        ])->setStatusCode(200);
                }else{
                    return response()->json(
                        ["data"=> "failed"])->setStatusCode(500);
                }
    }

    public function getMainTable(Request $request){
        try {

            $id = $request->query("id");

            $dog = \App\Dog::find($id);

//            return view("partials.table_minimal", compact("dog"));
            return view("main_table", compact("dog"));
        }catch (\Exception $exception){
            return redirect('/regenerate-dog-generations/'.$request->query("id"));
        }
    }

    public function postRegenerateTables(Request $request){
        try {
            $dogGeneration = \App\DogGeneration::whereDogId($request->id)->first();
//            $dogRelationShip = DogRelationship::whereDogId($request->id)->first();
            if ($dogGeneration->delete()){
                $dog = \App\Dog::find($request->id);
                GenerateDogJob::dispatchNow($dog->registration_number);
                return json_encode(["message" => "successfully regenerated generations table! Go back to previous page"]);
            }

        }catch (Exception $exception){
            return response()->json(["message" => "failed", "error" => $exception->getMessage() ],500);
        }
    }

    /**
     * @param $dog_been_inserted_builder
     * @param array $position
     * @param $offspring_registration_number
     * @param $registration_number
     */
    public function insertParent($dog_been_inserted_builder, array $position, $offspring_registration_number, $registration_number): void
    {
        $dog = Dog::whereRegistrationNumber($offspring_registration_number)->first();
        $dog_relationship = DogRelationship::where("dog_id", $dog->id)->first();

        if ( $position[0] === 'sire') {
            if ($dog_relationship){
                DogRelationship::where("dog_id", $dog->id)->update([
                    "father" => $registration_number
                ]);
            }else{
                DogRelationship::create([
                    "id" => Uuid::generate(),
                    "dog_id" => $dog->id,
                    "father" => $registration_number
                ]);
            }

        }

        if ($position[0] === 'dam') {
            if ($dog_relationship){
                DogRelationship::where("dog_id",$dog->id)->update([
                    "mother" => $registration_number
                ]);
            }else{
                DogRelationship::create([
                    "id" => Uuid::generate(),
                    "dog_id" => $dog->id,
                    "mother" => $registration_number
                ]);
            }

        }
    }

    public function insertAndRegisterParent($request, array $position,
                                            $offspring_registration_number, $registration_number): void
    {
        $dog = Dog::whereRegistrationNumber($offspring_registration_number)->first();

        if ( $position[0] === 'sire') {

            DogRelationship::where("dog_id",$dog->id)->update([
                "father" => $registration_number
            ]);

            if ($position[0] === 'dam') {
                DogRelationship::where("dog_id",$dog->id)->update([
                    "mother" => $registration_number
                ]);
            }
        }
    }

    public function getStatus(){
        return response()->json(["message" => "success"],200);
    }

    public function getNotConfirmedPage(){
        return view("errors.not_confirmed");
    }

}
