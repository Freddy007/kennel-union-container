<?php

namespace App\Http\Controllers;

use App\Breed;
use App\Breeder;
use App\Dog;
use App\DogDuplicate;
use App\DogGeneration;
use App\DogRelationship;
use App\Events\SomeEvent;
use App\HelperClasses\Pedigree;
use App\Helpers\NumberGeneration;
use app\Helpers\RegistrationNumber\RegistrationNumber;
use App\Notifications\SendCertificateNotification;
use App\Recorder;
use App\RequestedCertificate;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Webpatser\Uuid\Uuid;
use Yajra\Datatables\Datatables;

/**
 * Class AdminController
 * @package App\Http\Controllers
 */
class AdminController extends Controller
{

	public function __construct() {
	    $this->middleware("auth");
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request)
    {
        if ($request->ajax()) {

            $collection = new Collection();
            $membercollection = new Collection();
            $offspringcollection = new Collection();
            $breeds = Breed::all();

            foreach ($breeds as $breed) {
                $dogsCount = Dog::where("breeder_id", $breed->id)->count();
                $breedArray = ['name' => $breed->name, 'y' => $dogsCount];
                $collection->add($breedArray);
            }

            foreach (User::all() as $member){
                $dogsCount = Dog::where("user_id", $member->id)->count();
                $memberArray = ['name' => $member->kennel_name, 'y' => $dogsCount];
                $membercollection->add($memberArray);
            }

            $dogs = Dog::orderByDesc("offspring_count")->take(30)->get();

                    foreach ($dogs as $dog) {
                        $offspringCount = DogRelationship::orWhere("father", $dog->registration_number)
                            ->orWhere("mother",$dog->registration_number)->count();
                        $offspringArray = ['name' => $dog->name, 'y' => $offspringCount];
                        $offspringcollection->add($offspringArray);
                    }
//                });

            return response()->json(['breeds' => $collection, 'members' => $membercollection, "offspring" => $offspringcollection],200);
        }
        return view('version2.admin.dashboard');
    }

//    public function getIndex()
//    {
//        \Cache::remember('dashboard_members',360,function(){
//            $total =  User::count();
//            $confirmed_count = \App\User::whereConfirmed(true)->count();
//            $confirmed_percent = intval($confirmed_count  / \App\User::count() * 100);
//            return ['total' => $total,'confirmed_percent'=> $confirmed_percent];
//        });
//
//        \Cache::remember('dashboard_dogs',360,function(){
//            $total =  Dog::count();
//            $confirmed_count = \App\Dog::whereConfirmed(true)->count();
//            $confirmed_percent = intval($confirmed_count  / \App\Dog::count() * 100);
//            return ['total' => $total,'confirmed_percent'=> $confirmed_percent];
//        });
//
//        \Cache::remember('dashboard_breeds',360,function(){
//            $total =  Breed::count();
////            $confirmed_count = \App\Breed::whereConfirmed(true)->count();
////            $confirmed_percent = intval($confirmed_count  / \App\Breed::count() * 100);
//            return ['total' => $total,'confirmed_percent'=> ''];
//        });
//
//        \Cache::remember('dashboard_certificates',360,function(){
//            $total =  RequestedCertificate::count();
//            $confirmed_count = RequestedCertificate::whereHonoured(true)->count();
//            $confirmed_percent = intval($confirmed_count  / RequestedCertificate::count() * 100);
//            return ['total' => $total,'confirmed_percent'=> $confirmed_percent];
//        });
//
//
//        return view('version2.admin.dashboard',compact('members'));
//    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDemo()
    {
        //
        return view('version2.admin.demo');
    }

    /**
     * Show the form for creating a registering a new Dog.
     *
     * @return \Illuminate\Http\Response
     */

    public function getRegisterDog()
    {
        return view('version2.admin.register_new_dog');
    }



    public function getDeletedDogs(Request $request){

	    switch($request->query('dogs')) {
		    case "unconfirmed":
			    $query = "?dogs=unconfirmed";
			    break;
		    case "confirmed":
			    $query = "?dogs=confirmed";
			    break;
		    case "male":
			    $query = "?dogs=male";
			    break;
		    case "female":
			    $query = "?dogs=female";
			    break;

		    default:
			    $query = "";
	    }

	    return view('version2.admin.all_dogs',compact('query'));
	    return view('version2.admin.all_dogs_2',compact('query'));
    }

	public function getDeletedDogsData(Request $request){
		$dogs = Dog::onlyTrashed()->get();
	}

	/**
     * @param Request $request
     * @return mixed
     */
    public function getMembersData(Request $request){

        $builder = \DB::table('users');

        switch($request->query('members')){
            case "unconfirmed":
                $membersData = $builder->where('confirmed',false)->get();
                break;
            case "confirmed":
                $membersData = $builder->where('confirmed',true)->get();
                break;
            case "administrator":
                $membersData = $builder->where('administrator',true)->get();
                break;
            case "member":
                $membersData = $builder->where('administrator',false)->get();
                break;
            default:
                $membersData  =$builder->orderBy('created_at','desc')->get();
        }

        return Datatables::of($membersData)

                            ->addColumn('member_id', function ($member) {
                                return view( 'version2.admin.partials.actions.member.member_id', compact('member'));
                            })
                         ->addColumn('name', function ($member) {
                             return view( 'version2.admin.partials.actions.member.name', compact('member'));
                         })
                         ->editColumn('kennel_name', function($member){
                             return view( 'version2.admin.partials.actions.member.kennel_name', compact('member'));
                         })
                         ->editColumn('phone', function($member){
                             return view( 'version2.admin.partials.actions.member.phone', compact('member'));
                         })
                         ->editColumn('created_at',  function ($member) {
                             return view( 'version2.admin.partials.actions.member.created_at', compact('member'));
                         })
                         ->editColumn('dogs', function($member){
                             return view( 'version2.admin.partials.actions.member.dogs', compact('member'));
                         })

                         ->editColumn('role', function($member){
                             return view( 'version2.admin.partials.actions.member.role', compact('member'));
                         })

                        ->editColumn('status', function($member){
                            return view( 'version2.admin.partials.actions.member.status', compact('member'));
                        })

                         ->editColumn('action', function($member){
                             return view( 'version2.admin.partials.actions.member.action', compact('member'));
                         })
                         ->rawColumns([ 'name', 'kennel_name', 'phone', 'created_at', 'dogs', 'role','status', 'action' ])
                         ->make(true);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getAllMembers(Request $request){

        switch($request->query('members')) {
            case "unconfirmed":
                $query = "?members=unconfirmed";
                break;
            case "confirmed":
                $query = "?members=confirmed";
                break;
            case "administrator":
                $query = "?members=administrator";
                break;
            case "member":
                $query = "?members=member";
                break;

            default:
                $query = "";
        }

        return view('version2.admin.all_members',compact('query'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAllDogs(Request $request){


        switch($request->query('dogs')) {
            case "unconfirmed":
                $query = "?dogs=unconfirmed";
                break;
            case "confirmed":
                $query = "?dogs=confirmed";
                break;
            case "male":
                $query = "?dogs=male";
                break;
            case "female":
                $query = "?dogs=female";
                break;
            case "search":
                $query = "?dogs=search&q=".$request->query("q");
                break;

            case "siblings":
                $query = "?dogs=siblings&dog_id=".$request->query("dog_id");
                break;

            default:
                $query = "";

            if ($request->has("q")){
                $query = "?q=".$request->query("q");
            }

            if ($request->has("dog_id")){
                $query = "?dogs=siblings&dog_id=".$request->query("dog_id");
            }

        }

        return view('version2.admin.all_dogs',compact('query'));
//        return view('version2.admin.all_dogs_3',compact('query'));
    }

    public function getAllDogsData(Request $request){

        $dogs_builder = Dog::with(["user","relationships","breed"]);

        switch($request->query('dogs')) {
            case "unconfirmed":
                $dogsData = $dogs_builder->where('confirmed',false)->get();
                break;
            case "confirmed":
                $dogsData = $dogs_builder->where('confirmed',true)->get();
                break;
            case "male":
                $dogsData = $dogs_builder->whereSex('male')->get();
                break;
            case "female":
                $dogsData = $dogs_builder->whereSex('female')->get();
                break;

            default:
                if ($request->has("q")){
                    $query = $request->query("q");
                    $dogsData = $dogs_builder
                        ->orWhere('name', 'LIKE', '%' . $query . '%')
                        ->orWhere('registration_number', 'LIKE', '%' . $query . '%');
                }else {

                    /** @var Dog $builder */
                    \Cache::get('dogs');

                    $dogsData = \Cache::get('all_dogs', function () use ($dogs_builder) {
                        return $dogsData = $dogs_builder->orderBy('created_at', 'desc')->get();
                    });
//                    $dogsData = \Cache::rememberForever('dogs', function() use( $builder) {
//                        return  $dogsData = $builder->orderBy('created_at','desc')->get();
//                    });

                }
                }

        return Datatables::of($dogsData)

            ->addColumn('image', function ($dog) {
                return view( 'version2.admin.partials.actions.dog.image', compact('dog'));
            })

            ->addColumn('name', function ($dog) {
                return view( 'version2.admin.partials.actions.dog.name', compact('dog'));
            })
            ->addColumn('siblings', function ($dog) {
                return view( 'version2.admin.partials.actions.dog.siblings', compact('dog'));
            })
            ->addColumn('offspring', function ($dog) {
                return view( 'version2.admin.partials.actions.dog.offspring', compact('dog'));
            })
            ->editColumn('dob', function($dog){
                return view( 'version2.admin.partials.actions.dog.dob', compact('dog'));
            })
            ->editColumn('breed', function($dog){
                return view( 'version2.admin.partials.actions.dog.breed', compact('dog'));
            })
            ->editColumn('sex',  function ($dog) {
                return view( 'version2.admin.partials.actions.dog.sex', compact('dog'));
            })
            ->editColumn('registration_number', function($dog){
                return view( 'version2.admin.partials.actions.dog.registration_number', compact('dog'));
            })

            ->editColumn('breeder', function($dog){
                return view( 'version2.admin.partials.actions.dog.breeder', compact('dog'));
            })

            ->editColumn('status', function($dog){
                return view( 'version2.admin.partials.actions.dog.status', compact('dog'));
            })

            ->editColumn('delete_request', function($dog){
                return view( 'version2.admin.partials.actions.dog.delete', compact('dog'));
            })

            ->editColumn('certificate', function($dog){
                return view( 'version2.admin.partials.actions.dog.certificate', compact('dog'));
            })

            ->editColumn('action', function($dog){
                return view( 'version2.admin.partials.actions.dog.action', compact('dog'));
            })
            ->rawColumns([ 'name', 'dob', 'breed', 'sex', 'registration_number', 'breeder','status','delete_request', 'certificate', 'action' ])
            ->make(true);

    }

    public function getAllDogsTable(Request $request){
        $query = $request->query("q");
        $dogs_builder = Dog::with(["user:id,first_name,last_name,kennel_name"])
            ->leftJoin("breeders","breeders.id","=","dogs.breeder_id");

        if ($request->has("q")) {
            $dogsData = $dogs_builder
                ->orWhere('dogs.name', 'LIKE', '%' . $query . '%')
                ->orWhere('registration_number', 'LIKE', '%' . $query . '%')
                ->selectRaw("dogs.name,dogs.dob,dogs.sex,
                dogs.registration_number,dogs.id,breeders.id as breed_id,breeders.name as breed")
                ->paginate(15);
        }

        $dogsData = $dogs_builder->selectRaw("dogs.*,breeders.name as breed")
            ->selectRaw("dogs.name,dogs.dob,dogs.sex,
                dogs.registration_number,dogs.id,breeders.id as breed_id,breeders.name as breed")
            ->orderBy('created_at', 'desc')->paginate(15);

//        switch($request->query('dogs')) {
////            case "unconfirmed":
////                $dogsData = $dogs_builder->where('confirmed',false)->get();
////                $paginator = new LengthAwarePaginator(range(1,count($dogsData)),count($dogsData),15,
////                    Paginator::resolveCurrentPage(),
////                    ['path' => Paginator::resolveCurrentPath()]
////                );
////                $dogsData = $dogs_builder->where('confirmed',false)->paginate(15);
////                break;
////            case "confirmed":
////                $dogsData = $dogs_builder->where('confirmed',true)->get();
////                $paginator = new LengthAwarePaginator(range(1,count($dogsData)),count($dogsData),15,
////                    Paginator::resolveCurrentPage(),
////                    ['path' => Paginator::resolveCurrentPath()]
////                );
////                $dogsData = $dogs_builder->where('confirmed',true)->paginate(15);
////                break;
////            case "male":
////                $dogsData = $dogs_builder->whereSex('male')->get();
////                $paginator = new LengthAwarePaginator(range(1,count($dogsData)),count($dogsData),15,
////                    Paginator::resolveCurrentPage(),
////                    ['path' => Paginator::resolveCurrentPath()]
////                );
////                $dogsData = $dogs_builder->whereSex('male')->orderBy('created_at', 'desc')->paginate(15);
////                break;
//            case "female":
//                $dogsData = $dogs_builder->whereSex('female')->get();
//                $paginator = new LengthAwarePaginator(range(1,count($dogsData)),count($dogsData),15,
//                    Paginator::resolveCurrentPage(),
//                    ['path' => Paginator::resolveCurrentPath()]
//                );
//                $dogsData = $dogs_builder->whereSex('female')->orderBy('created_at', 'desc')->paginate(15);
//                break;
//
//            default:
////                if ($request->has("q")){
////                    $query = $request->query("q");
////                    $dogsData = $dogs_builder
////                        ->orWhere('name', 'LIKE', '%' . $query . '%')
////                        ->orWhere('registration_number', 'LIKE', '%' . $query . '%')->paginate(15);
////
////                    $paginator = new LengthAwarePaginator(range(1,count($dogsData)),count($dogsData),15,
////                        Paginator::resolveCurrentPage(),
////                        ['path' => Paginator::resolveCurrentPath()]
////                    );
////                }else {
//
//                    $dogsData = $dogs_builder->orderBy('created_at', 'desc')->paginate();
//
////                    $paginator = new LengthAwarePaginator(range(1,count($dogs)),count($dogs),15,
////                        Paginator::resolveCurrentPage(),
////                        ['path' => Paginator::resolveCurrentPath()]
////                    );
//
////                    $dogsData = $dogs_builder->orderBy('created_at', 'desc')->paginate(15);
//
//                    $paginator = false;
////                }
//        }

        return view('version2.admin.all_dogs_2',compact('dogsData'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getAllBreeds(Request $request){
//        if(\Gate::denies('administer')){
//            flash()->error('You are not authorized to access that page !');
//            return redirect('/');
//        }
        $breeders = Breed::paginate();
        if ($request->term) {
            $query = $request->term;
            $breeders = Breed::where('name', 'LIKE', '%' . $query . '%')->get();
        }
        $paginator = new LengthAwarePaginator(range(1,count($breeders)),count($breeders),10,
            \Illuminate\Pagination\Paginator::resolveCurrentPage(),
            ['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]
        );

        $breeders_menu = 'active';

        return view('version2.admin.all_breeds',compact('breeders','breeders_menu','paginator'));
    }


    /**
     * @param $user_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|string
     */
    public function getUserDogs($user_id){

        try {
            $dogs = Dog::getUserDogs($user_id);

            $user = User::findOrFail($user_id);
            $first_name = $user->first_name;
            $last_name = $user->last_name;
            $user = \Auth::getUser()->first_name;

        }catch(\Exception $e){
            flash('There is an error!');
            return $e->getMessage();
        }

        $members = 'active';
        return view('version2.admin.member_dogs',compact('dogs','first_name','last_name','user','members'
        ));
    }

    public function getSiblingDogs($id){

            $relationship = \App\DogRelationship::whereDogId($id)->first();
            $father = $relationship->father;
            $mother = $relationship->mother;

            $relationship_ids = \App\DogRelationship::where("dog_id","<>",$id)
                ->where("mother","<>","")->where("father","<>","")
                ->whereMother($mother)->whereFather($father)->pluck("dog_id");

            $builder = Dog::leftJoin('users','users.id','=','dogs.user_id')
                ->leftJoin("dog_relationships","dog_relationships.dog_id","=","dogs.id")
                ->leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                ->select(DB::raw('dogs.*,dog_relationships.father, dog_relationships.mother,
            CONCAT(users.first_name," ",users.last_name) as owner,breeders.name as breed'));

            $dogs = $builder->whereIn('dogs.id', $relationship_ids)->withTrashed()->paginate();

        return view('version2.admin.sibling_dogs',compact('dogs'));
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getCertificateRequests(){
//        if(\Gate::denies('administer')){
//            flash()->error('You are not authorized to access that page !');
//            return redirect('/');
//        }
        $requests = RequestedCertificate::leftJoin('users','users.id','=','requested_certificates.user_id')
            ->leftJoin('dogs','dogs.id','=','requested_certificates.dog_id')
            ->select(\DB::raw('CONCAT(users.first_name," ", users.last_name) as member, users.kennel_name, users.phone, dogs.name,
                       dogs.registration_number,requested_certificates.honoured,requested_certificates.created_at,requested_certificates.id,requested_certificates.dog_id'))
            ->orderBy('requested_certificates.honoured')
            ->paginate();


        $certificate ='active';
        //var_dump($requests);exit;
        return view('version2.admin.certificate_requests',compact('requests','certificate'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getRegisterNewDog(){
//        if(\Gate::denies('administer')){
//            flash()->error('You are not authorized to access that page !');
//            return redirect('/');
//        }

       $breeds = Breeder::all();

       $users = User::all('id','first_name','last_name');

        return view('version2.admin.register_new_dog',compact('breeds','users'));
    }

    public function postRegisterNewDog(Request $request){

//        \Cache::forget('dogs');

        $validator = Validator::make($request->all(), [
            'name'=>'required',
            'sex'=>'required',
            'dob'=>'required',
            'breeder_id'=> 'required',
            'colour'=>'required'
        ]);

        if ($validator->passes()) {

            $dog = new \App\HelperClasses\DogHelper($request);
            $dog_id = $dog->beginRegistrationProcess();
            return response()->json(['message'=>'Added a new dog','dog_id' =>$dog_id])
                             ->setStatusCode(200);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getRegisterLitter(){
        $breeds = Breeder::all();

        $users = User::all('id','first_name','last_name');

        return view('version2.admin.register_litter',compact('breeds','users'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postRegisterLitter(Request $request){

        try{

	        $dog = (new \App\HelperClasses\DogHelper($request));
	         $dog->registerLitter();

	        return response()->json(['data' => ''])->setStatusCode(200);

        }catch(\Exception $e){

            return response()->json(
            	[
            		'error'=>$e->getMessage(),
		            'trace' => $e->getTraceAsString()
	            ])->setStatusCode(500);
        }
    }

    /**
     * @param $dog_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getPedigreeCertificate($dog_id){
        if(\Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
            ->leftJoin('users','users.id','=','dogs.user_id')
            ->where('dogs.id',$dog_id)
            ->select(\DB::raw('dogs.*,breeders.name as breed,dog_relationships.father,dog_relationships.mother,
                              users.first_name,users.last_name,users.kennel_name'))
            ->first();
        // $view_count = $dog->viewed + 1;
        //Dog::where('id',$dog_id)->update(['viewed'=> $view_count]);
        $type_certificate = "PEDIGREE";

        \Cache::forget('serial_number');
        \Cache::rememberForever('serial_number', function () {
            return NumberGeneration::generateUsingDate();
        });

        return view('version2.admin.pedigree-certificate',compact('dog','type_certificate'));
//        return view('version2.admin.certificates.certificate',compact('dog','type_certificate'));
    }

    public function postAddDogToGenerations(Request $request){


	    try {
		    $reg_number = RegistrationNumber::generateRegistrationNumber();

		    DB::transaction( function () use ( $request,$reg_number ) {

			    $builder            = DogGeneration::where( 'dog_id', $request->dog_id )->first();
			    $generation         = $request->generation . "_generation";
			    $position           =  explode( ",", $request->get( 'position' ) );

			    $generation_results = json_decode( json_encode( $builder->$generation ), true );

			    $dog_id     = Uuid::generate();

			    Dog::create(
				    [
					    'id'                  => $dog_id,
//					    'user_id'             => \Auth::id(),
					    'name'                => $request->name,
					    'dob'                 => $request->dob,
					    'coat'                => $request->dob,
					    'breeder_id'          => $request->breeder_id,
//					    'sex'                 => $position[1] == 'sire' ? 'male' : 'female',
					    'sex'                 => $this->getSex($position),
					    'registration_number' => $reg_number,
					    'titles'              => $request->titles,
					    'performance_titles'  => $request->performance_titles,
					    'colour'              => $request->colour,
					    'height'              => $request->height,
					    'elbow_ed_results'    => $request->elbow_ed_results,
					    'appraisal_score'     => $request->appraisal_score,
					    'hip_hd_results'      => $request->hip_hd_results,
					    'tattoo_number'       => $request->tattoo_number,
					    'microchip_number'    => $request->microchip_number,
					    'DNA'                 => $request->DNA,
					    'other_health_checks' => $request->other_health_checks
				    ]
			    );

			    if(!isset($position[1])){
				    unset( $generation_results[ $position[0]]);
				    $generation_results[ $position[0]] = [
					    'name'                => $request->name,
					    'registration_number' => $reg_number,
					    'titles'              => $request->titles
				    ];
			    }else{
				    unset( $generation_results[ $position[0] ][ $position[1]]);
				    $generation_results[ $position[0] ][ $position[1] ] = [
					    'name'                => $request->name,
					    'registration_number' => $reg_number,
					    'titles'              => $request->titles
				    ];
			    }

			    if ( $generation_model = DogGeneration::whereDogId( $request->dog_id)->first() ) {
				    $generation_model->update( [
					    $generation => $generation_results
				    ]);
			    } else {
				    DogGeneration::create( [
					    'id'        => Uuid::generate(),
					    'dog_id'    => $request->dog_id,
					    $generation => $generation_results
				    ]);
			    }

			    $mother_info        = explode(",",$request->get('mother'));
			    $father_info        = explode(",",$request->get('father'));

			    $father_gen         = $father_info[0]."_generation";

			    $dog_relationship = DogRelationship::whereDogId($dog_id)->first();
			    if($dog_relationship){
				    $dog_relationship->update([
					    'father' =>  $request->mother !== 'undefined' && $request->father !== 'undefined'  ?
						    json_decode(json_encode($builder->$father_gen),true)[$father_info[1]][$father_info[2]]['registration_number'] : '',

					    'mother' =>  $request->mother !== 'undefined' && $request->father !== 'undefined'  ?
						    json_decode(json_encode($builder->$father_gen),true)[$mother_info[1]][$mother_info[2]]['registration_number'] : ''
				    ]);
			    }else{
				    $builder->$father_gen;
				    DogRelationship::create([
						    'id' => Uuid::generate(),
						    'dog_id' => $dog_id,
						    'father' => $request->mother !== 'undefined' && $request->father !== 'undefined'  ?
							    json_decode(json_encode($builder->$father_gen),true)[$father_info[1]][$father_info[2]]['registration_number'] : '',
						    'mother' =>  $request->mother !== 'undefined' && $request->father !== 'undefined' ?
							    json_decode(json_encode($builder->$father_gen),true)[$mother_info[1]][$mother_info[2]]['registration_number']:''
					    ]
				    );
			    }

			    if($request->offspring !== 'undefined' ){
				    Pedigree::generateOffSpringFirstGenerations($request,$builder,$reg_number,$this->getSex($position));
			    }
		    });

		    return response()->json(['message' => 'success',
		                             'request' => $request->mother,
		                             'dog_id' => $reg_number
		    ])->setStatusCode(200);

	    }catch(\Exception $e){
		    return response()->json(['message' => 'failed',
		                             'error' => $e->getMessage(),
		                             'trace' => $e->getTraceAsString(),
			                         'request' => $request->mother
		    ])->setStatusCode(500);
	    }
    }


    public function postGenerateAncestors($dog_id){
    	try{
		    \App\HelperClasses\DogHelper::generateAncestorsFromRegistrationNumber($dog_id);
		    return response()->json(['message' => 'success'])->setStatusCode(200);
	    }catch(\Exception $e){
		    return response()->json(['message' => 'failed'])->setStatusCode(500);
	    }
    }

    public function postAddToPedigreeTable(Request $request){

    	try{
//    		$pedigree = new Pedigree($request);
//    		$pedigree->addToPedigreeTable();
		    $generation          = $request->generation."_generation";


		    DB::transaction(function() use($request,$generation){

			    $position            = explode(',',$request->position);
			    $name                = $request->name;
			    $registration_number = $request->registration_number;
			    $titles              = $request->titles;
			    $dog_id              = $request->dog_id;
			    $dog_been_inserted_id  = $request->id;
			    $father               = explode(',',$request->get('father'));
			    $mother               = explode(',',$request->get('mother'));
			    $parent_generation    = $mother[0].'_generation';

			    $builder = DogGeneration::whereDogId($dog_id)->first();

			    $generation_results = json_decode( json_encode( $builder->$generation ), true );

			    $parent_generation_results = json_decode( json_encode( $builder->$parent_generation ), true );

			    $dog_been_inserted_builder = DogGeneration::whereDogId($dog_been_inserted_id)->first();

			    $dog_been_inserted_first_generation_results = json_decode(json_encode($dog_been_inserted_builder->first_generation),true);
//
			    if($generation == 'first_generation') {
				    unset( $generation_results[ $position[0] ] );

				    $generation_results[ $position[0] ] = [
					    'name'                => $name,
					    'registration_number' => $registration_number,
					    'titles'              => $titles
				    ];

				    if ( $position[0] == 'sire' ) {

					    $current_dog_builder = Dog::find($dog_id)->first();

					    DogRelationship::whereDogId($dog_id)->first()->update([
						    'father' => $registration_number
					    ]);

//					    \App\HelperClasses\Dog::generateAncestorsFromRegistrationNumber($current_dog_builder->registration_number,true);
//
					    Pedigree::SaveFirstGenerationSirePartialAncestors($registration_number,$generation,$generation_results,$builder);

				    } elseif ( $position[0] == 'dam' ) {

					    $current_dog_builder = Dog::find( $dog_id )->first();

					    DogRelationship::whereDogId( $dog_id )->first()->update( [
						    'mother' => $registration_number
					    ]);

					    Pedigree::SaveFirstGenerationDamPartialAncestors($registration_number,$generation,$generation_results,$builder);

//					    \App\HelperClasses\Dog::generateAncestorsFromRegistrationNumber( $current_dog_builder->registration_number, true );
				    }
			    }else {
					    unset( $generation_results[ $position[0] ][ $position[1] ] );
					    $generation_results[ $position[0] ][ $position[1] ] = [
						    'name'                => $name,
						    'registration_number' => $registration_number,
						    'titles'              => $titles
					    ];
				    }

				    if( $request->mother !== 'undefined' && $request->father !== 'undefined'){
					    unset($parent_generation_results[$father[1]][$father[2]]);

					    $parent_generation_results[$father[1]][$father[2]] = [
						    'name'                => $dog_been_inserted_first_generation_results['sire']['name'],
						    'registration_number' => $dog_been_inserted_first_generation_results['sire']['registration_number'],
						    'titles'              => $dog_been_inserted_first_generation_results['sire']['titles']
					    ];

					    unset($parent_generation_results[$mother[1]][$mother[2]]);
					    $parent_generation_results[$mother[1]][$mother[2]] = [
						    'name'                => $dog_been_inserted_first_generation_results['dam']['name'],
						    'registration_number' => $dog_been_inserted_first_generation_results['dam']['registration_number'],
						    'titles'              => $dog_been_inserted_first_generation_results['dam']['titles']
					    ];

					    $builder->update([
						    $generation         =>  $generation_results,
						    $parent_generation  =>  $parent_generation_results
					    ]);

//					    DogRelationship::whereDogId($dog_been_inserted_id)->first()->update([
//						    'father'  => $dog_been_inserted_first_generation_results['sire']['registration_number'],
//						    'mother'  => $dog_been_inserted_first_generation_results['dam']['registration_number']
//					    ]);

				    }else{
					    $builder->update([
						    $generation         =>  $generation_results
					    ]);
				    }

			    if($request->offspring !== 'undefined' ){
				    Pedigree::generateOffSpringFirstGenerations($request,$builder,$registration_number,$this->getSex($position));
			    }
		    });

		    return response()->json([
			    'message' => 'success',
			    'redirect' => $generation == 'third_generation' ? false: true
		    ])->setStatusCode(200);

	       }catch(\Exception $e){
//
			    return response()->json([
				    'message' => 'failed',
				    'error'   => $e->getMessage(),
				    'trace' => $e->getTraceAsString()
			    ])->setStatusCode(500);
	    }
    }

    /**
     * @param $dog_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getDog(Request $request, $dog_id){

//        $number = Dog::whereId($dog_id)->first()->registration_number;

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
            ->where('dogs.id',$dog_id)
            ->select(DB::raw('dogs.*,breeders.name as breeder_name,dog_relationships.father,dog_relationships.mother'))
            ->first();

        if($request->ajax()){
	        return view('version2.admin.partials.pedigree_table_partial',compact('dog','dog_relationships'));
        }else{
	        return view('version2.admin.dog',compact('dog'));

        }
    }

    public function getEditDog($dog_id){

//        if(\Gate::denies('administer')){
//            flash()->error('You are not authorized to access that page !');
//            return redirect('/');
//        }

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                  ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
                  ->leftJoin('users','users.id','=','dogs.user_id')
                  ->where('dogs.id',$dog_id)
                  ->select(DB::raw('dogs.*,breeders.name as breeder_name,dog_relationships.father,dog_relationships.mother,
            CONCAT(users.first_name," ",users.last_name) as member, users.id as member_id'))
                  ->first();

        $breeders = Breeder::all();

        $users = User::all();

        return view('version2.admin.edit_dog',compact('dog','breeders','users'));
    }
    /**
     * @param Request $request
     * @param $dog_id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postUpdateDog(Request $request, $dog_id){
//        return $request;
//        if(\Gate::denies('administer')){
//            flash()->error('You are not authorized to access that page !');
//            return redirect('/');
//        }

        try {

            Dog::updateDog($request,$dog_id);
            flash()->success("You have successfully updated a dog's info! ");

        }catch(\Exception $e){
            flash()->error("Error updating a dog! ". $e->getMessage());
        }

        return \Redirect::back();
    }

    public function getLogger(){
       $logger =  Recorder::orderBy('created_at','desc')->paginate();
        return view('version2.admin.logger',compact('logger'));
    }

    public function postConfirmDog($dog_id,Request $request){

            DB::transaction(function()use($request,$dog_id){
                Dog::where('id',$dog_id)->update(['confirmed'=> true ]);
            });
            flash()->success('A new dog has been confirmed!');
//            return redirect()->back();
    }

    public function postConfirmMember($id,Request $request){

        try{
            DB::transaction(function()use($request,$id){
                User::where('id',$id)->update(['confirmed'=> $request->status ]);
            });

            if ($request->status == 1){
                flash()->success('A new member has been confirmed!');
            }else{
                flash()->warning('Member has been revoked!');
            }

            return redirect()->back();
        }catch(\Exception $e){
            flash()->error('Error confirming member! '.$e->getMessage());
            return redirect()->back();
        }
    }


    public function getProfile(){
        $user_builder = \Auth::user();
        return view('version2.admin.profile_account', compact('user_builder'));
    }

    public function getProfileAccount(){
        $user_builder = \Auth::user();
        return view('version2.admin.profile_account',compact('user_builder'));
    }

    public function postUpdateProfileAccount(Request $request){

        flash()->success('Successfully updated your profile!');
        User::find(\Auth::id())->update($request->all());

        return redirect()->back();
    }

	private function getSex($position){
		if(isset($position[1]) && $position[1] == 'sire'){
			return 'male';
		}elseif (isset($position[1]) && $position[1] == 'dam'){
			return 'female';
		}else{
			if($position[0] == 'sire'){
				return 'male';
			}else{
				return 'female';
			}
		}
	}

    //update profile password action
    public function postUpdatePassword(Request $request){
        $this->validate($request,[
            'password' => 'required',
            'new_password' => 'required',
        ]);
        if(\Hash::check($request->password,\Auth::user()->password)){
            if($request->new_password == $request->confirm_password){
                User::find(\Auth::id())->update([
                    'password' => bcrypt($request->new_password)
                ]);
                flash()->success('Successfully updated the password!');
            }else {
                flash()->error('New password and confirm password do not match!');
            }

            return redirect()->back();

        }else{
            flash()->warning('Passwords do not match!');
            return redirect()->back();
        }
    }

    public function postRegisterBreeder(Request $request){

        if(Breeder::where('name',$request->name)->first()){
            flash()->error('Breed already exists.');
        } else {
            $id = Uuid::generate();

            Breeder::create(['id' => $id, 'name' => $request->name]);
        }

        \Cache::forget('breeds');

        return redirect()->back();
    }


    public function postBreeder($breeder_id){

        return Breeder::where('id',$breeder_id)->first()->name;
    }

    public function postEditBreeder(Request $request,$breeder_id){
        Breeder::where('id',$breeder_id)->update(['name' => $request->edited_name]);
        flash()->success('Successfully updated a breeder ');
        return redirect()->back();
    }

    public function getShowDog($dog_id){

//        if( \Gate::denies('administer')){
//            flash()->error('You are not authorized to access that page !');
//            return redirect('/');
//        }

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                  ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
                  ->where('dogs.id',$dog_id)
                  ->select( \DB::raw('dogs.*,breeders.name as breeder_name,dog_relationships.father,dog_relationships.mother'))
                  ->first();

        $dogs_active = 'active';

        return view('admin.show_dog',compact('dog','dog_relationships', 'dogs_active'
        ));
    }


    public function getDogEdit($dog_id){

//        if( \Gate::denies('administer')){
//            flash()->error('You are not authorized to access that page !');
//            return redirect('/');
//        }

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                  ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
                  ->leftJoin('users','users.id','=','dogs.user_id')
                  ->where('dogs.id',$dog_id)
                  ->select( \DB::raw('dogs.*,breeders.name as breeder_name,dog_relationships.father,dog_relationships.mother,
            CONCAT(users.first_name," ",users.last_name) as member, users.id as member_id'))
                  ->first();
        //w
        $breeders = Breeder::all();
        $users = User::all('id','first_name','last_name');

        $dogs_active = 'active';

        return view('admin.member_dog_edit',compact('dog','breeders','users','dogs_active' ));
    }

    public function postChangeRole($user_id){

//        if( \Gate::denies('administer')){
//            flash()->error('You are not authorized to access that page !');
//            return redirect('/');
//        }

        if(User::where('id',$user_id)->first()->administrator == true){
            $update = false;
        }else{
            $update = true;
        }

        User::where('id',$user_id)->update(['administrator' => $update]);
        $user = User::where('id',$user_id)->first();

        flash()->success('you have successfully changed the role of ' . $user->first_name .' !' );
        return redirect()->back();
    }

    public function postDeleteMember($user_id){
        User::destroy($user_id);
        flash()->message('Member successfully deleted');

        return \Redirect::back();
    }


    public function getDogs(){
        $breeders= \DB::table('breeders')->select('name');
        $dogs = DB::table('dogs')->union($breeders)->lists('name');
        return json_encode($dogs);
    }

    public function getSelectDogs(Request $request){

        if($request->has('g')){
            $items = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                        ->where('sex',$request->query('g'))
                        ->where(function(\Illuminate\Database\Eloquent\Builder $query) use($request){
                            $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
                                  ->orWhere('registration_number','LIKE','%'.$request->query('q').'%');
                        })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
                        ->forPage($request->query('page')?:1,15)
                        ->paginate();
        }else{
            $items = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                        ->where(function(\Illuminate\Database\Eloquent\Builder $query) use($request){
                            $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
                                  ->orWhere('registration_number','LIKE','%'.$request->query('q').'%');
                        })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
                        ->forPage($request->query('page')?:1,15)
                        ->paginate();
        }

        return array('total_count'=>$items->total(),'items'=>$items->jsonSerialize());
    }

    public function getTypeAheadDogs(Request $request){

        if($request->has('g')){
            $items = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                ->where('sex',$request->query('g'))
                ->where(function(\Illuminate\Database\Eloquent\Builder $query) use($request){
                    $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
                        ->orWhere('registration_number','LIKE','%'.$request->query('q').'%');
                })->select(\Illuminate\Support\Facades\DB::raw('dogs.*,breeders.name as breeder_name'))
                ->pluck("name");
        }else{
            $items = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                ->where(function(\Illuminate\Database\Eloquent\Builder $query) use($request){
                    $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
                        ->orWhere('registration_number','LIKE','%'.$request->query('q').'%');
                })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
//                ->forPage($request->query('page')?:1,15)
                ->pluck("name");
        }

        return $items;
//        return array('total_count'=>$items->total(),'items'=>$items->jsonSerialize());
    }

//    public function getTypeAheadDogs(Request $request){
//
//        $items = Dog::with("duplicates")->leftJoin('breeders','breeders.id','=','dogs.breeder_id');
////            ->leftJoin('dog_duplicates','dog_duplicates.dog_id','=','dogs.id');
//        if($request->has('g')) {
//
//            $items->where('sex', $request->query('g'))
//                ->where(function (Builder $query) use ($request) {
//                    $query->where('dogs.name', 'LIKE', '%' . $request->query('q') . '%')
//                        ->orWhere('registration_number', 'LIKE', '%' . $request->query('q') . '%');
//                })->select(\Illuminate\Support\Facades\DB::raw('dogs.*,breeders.name as breeder_name'))
//                ->pluck("name");
//
//
//        }else if ($request->has("type") && $request->query("archives")){
//            $items = Dog::onlyTrashed()
//                ->leftJoin('breeders','breeders.id','=','dogs.breeder_id')
//                ->leftJoin('dog_duplicates','dog_duplicates.dog_id','=','dogs.id')
//                ->where(function(Builder $query) use($request){
//                    $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
//                        ->orWhere('dogs.registration_number','LIKE','%'.$request->query('q').'%');
//                })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
//                ->forPage($request->query('page')?:1,15);
//
//        }else if ($request->has("type") && $request->query("archives")){
//            $items = Dog::onlyTrashed()
//                ->leftJoin('breeders','breeders.id','=','dogs.breeder_id')
//                ->leftJoin('dog_duplicates','dog_duplicates.dog_id','=','dogs.id')
//                ->where(function(Builder $query) use($request){
//                    $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
//                        ->orWhere('dogs.registration_number','LIKE','%'.$request->query('q').'%');
//                })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
//                ->forPage($request->query('page')?:1,15);
//        }else{
//
//            $items->where(function(Builder $query) use($request){
//                $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
//                    ->orWhere('dogs.registration_number','LIKE','%'.$request->query('q').'%');
//            })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
//                ->forPage($request->query('page')?:1,15);
//        }
//
//        return $items->get(['duplicate_registration_number']);
//    }


    public function getDogByName(Request $request,$dog_name){
        $gender = $request->query('gender') == 'sire' ? "male" :"female";
//    	$dog = Dog::whereName(urldecode($dog_name))->first();
        $dog = Dog::whereId(urldecode($dog_name))->first();

        if($request->query('gender')){
            if($dog){
                if($gender == $dog->sex){
                    return response()->json(
                        [
                            'id' => $dog->id,
                            'name' => $dog->name,
                            'sex' => $dog->sex,
                            'registration_number' => $dog->registration_number,
                            'titles' => $dog->titles
                        ])->setStatusCode(200);
                }else{
                    return response()->json(
                        [
                            'id' => $dog->id,
                            'name' => $dog->name,
                            'sex' => $dog->sex,
                            'registration_number' => $dog->registration_number,
                            'titles' => $dog->titles
                        ])->setStatusCode(403);
                }

            }else{
                return response()->json(['name' => 'empty'])->setStatusCode(400);
            }
        }else{
            if($dog){
                return response()->json(['message' => 'failed'])->setStatusCode(400);

            }else{
                return response()->json(['name' => 'empty'])->setStatusCode(200);
            }
        }
    }

    public function getAddNewColumn(){
        return view('version2.admin.partials.add_new_column');
    }


    public function getArchives(Request $request){
        $dogs = Dog::onlyTrashed()->paginate();
        return view('version2.admin.archives',compact('dogs'));
    }

    public function postArchiveDog($dog_id){
        Dog::find($dog_id)->delete();
        return redirect()->back();
    }

    public function postRestoreDog($id){
        $dog = Dog::onlyTrashed()->find($id);
        $dog->update([
            "deleted_at" => null
        ]);
    }

    public function getTemporalUrl(){
        return \URL::temporarySignedRoute(
            'unsubscribe', now()->addMinutes(30), ['user' => 1]
        );
    }

    public function postApproveCertificateRequest(Request $request, $dog_id){

        $dog = Dog::find($dog_id);
        $name = $request->query("name");
        $url =  \URL::temporarySignedRoute('download-certificate', now()->addHours(72),
            ['dog_id' => $dog_id,"user_name" => $name]);
        $certificate_request = RequestedCertificate::whereDogId($dog_id)->first();
        $expiry_date = Carbon::today()->addHours(72)->toDateTimeString();
        if ($certificate_request) {
            $certificate_request->update([
                "honoured" => 1,
                "approved" => 1,
                "download_url" => $url,
                "certificate_expiry" => $expiry_date
            ]);
        }

        $user = User::find($dog->user_id);
        \Notification::send($user, new SendCertificateNotification($user, $dog, $url));
    }

    public function getDuplicates(){
        $dogs = DogDuplicate::with(["dog" => function($dog){
            return $dog->with("user")->get();
        }])->paginate(10);
        return view('version2.admin.duplicates',compact('dogs'));
    }

    public function postGenerateNewNumber($id,$dog_id){

        try {
            DB::transaction(function () use ($id, $dog_id) {
                    $year = date('Y');
                    $month = date('m');
                    $day = date('d');
                    $milli = date("U");
                    $count = Dog::withTrashed()->get()->count() + 1 + $milli;
                    $registration_number =   $year . $month . $day . $count;

                $duplicate = DogDuplicate::find($id);
                $duplicate->update([
                    "new_registration_number" => $registration_number,
                    "fixed"                   => true
                ]);

                $dog = Dog::find($dog_id);
                $dog->update([
                    "registration_number" => $registration_number
                ]);
            });
        } catch (\Throwable $e) {
            return response()->json(["message" => $e->getMessage()]);
        }
    }

}
