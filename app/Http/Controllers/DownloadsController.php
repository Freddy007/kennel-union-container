<?php

namespace App\Http\Controllers;

use App\Download;
use App\Jobs\GenerateCertificate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class DownloadsController extends Controller
{
    //

    public function index(){
        $downloads = Download::orderByDesc("created_at")->paginate(15);
        return view("version2.admin.downloads",compact("downloads"));
    }

    public function saveCertificate($id){
        GenerateCertificate::dispatch($id);

        return redirect()->back()->with("success","Certificate generation queued. Check Downloads to download your certificate!");
        return response()->json(["message" => "success"],200);
    }


    public function downloadCertificate(Request $request, $id){
        $download = Download::whereDogId($id)->latest()->first();
        $file_path = public_path("certificates")."/".$download->file_path;
        return response()->download($file_path,$download->file_path);

    }
}
