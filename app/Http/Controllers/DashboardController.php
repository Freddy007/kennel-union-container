<?php

namespace App\Http\Controllers;

use App\Breeder;
use App\DogTransfer;
use App\IssuedCertificate;
use App\Helpers\NumberGeneration;
use App\Recorder;
use App\RequestedCertificate;
use App\User;
use App\Dog;
use App\Owner;
use Carbon\Carbon;
use Faker\Provider\Image;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;
//use Spatie\Activitylog\Models\Activity;
use Webpatser\Uuid\Uuid;
use App\DogRelationship;
use Gate;
use mikehaertl\wkhtmlto;
use Yajra\Datatables\Datatables;


class DashboardController extends Controller
{

    use NotificationJobTrait;

	/**
	 * @param Request $request
	 */
	public function getPush_slack_notification( Request $request ) {
		$this->pushSlackNotification( Auth::getUser()->first_name . ' with phone number ' . Auth::getUser()->phone . ' is reporting a duplicated registration number ' . $request->report_registration_number, '#KENNEL-UNION' );
	}

	protected function authorizeAdmin(){
        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }
    }

    public function getIndex()
    {
        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        $dashboard = 'active';
//        Activity::log(Auth::getUser()->id,'logged');

        return view('admin.index', compact('dashboard'));
    }

    public function postReportNumber(Request $request){
	    $this->getPush_slack_notification( $request );

//    $this->pushSlackNotification($request->name.' with Phone number: '.$request->phone_number.' attempted to register on kodesms.com','#kodesms');
        return Auth::getUser()->first_name .', Your report has been received .Thank you ';

    }

    public function getMembers(Request $request)
    {
        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        switch($request->query('members')){
            case "unconfirmed":
                $query = "?members=unconfirmed";
                break;
            case "confirmed":
                 $query = "?members=confirmed";
                break;
            case "administrator":
                $query = "?members=administrator";
                break;
            case "member":
                $query = "?members=member";
                break;

            default:
                $query = "";
        }

//        $query = $request->query('members') == "unconfirmed" ? "?members=unconfirmed":"";

        $members = 'active';

        return view('admin.members',compact('users','index','members','query'));

    }

    public function getMembersData(Request $request){

        return User::membersDataForTable($request);
    }

    public function postDeleteMember($user_id){
        User::destroy($user_id);
        flash()->message('Member successfully deleted');

        return \Redirect::back();
    }

    public function getRegisterDog(){

        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        $breeders = Breeder::all();
        $users = User::all('id','first_name','last_name');

        $dogs_active = 'active';


        return view('admin.register_dog',compact('breeders','users','dogs_active'
        ));
    }

    public function postConfirm(Request $request,$user_id){

        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        $status = $request->status;
        $flash = $status ? flash()->success('A new member has been confirmed!'): flash()->success("A member's priviledge has been revoked!");;

        User::where('id',$user_id)->update(['confirmed'=>$status]);

        $member = User::where('id',$user_id)->first();

        $email = $member->email;
        $name  = $member->first_name .' '.$member->last_name;

        $admin = Auth::user();
        $admin_member = $admin->first_name ." ".$admin->last_name;

        $message  = $status ? "$admin_member confirmed $name ($email) " : "$admin_member revoked $name ($email) membership ";

//        $action = "Admin Member $admin_member just confirmed $name with email($email) ";

//        Recorder::Log($user_id,$user_id,'',$message);

        try {
            $message = 'Hello ,' . $name . ' your Account  has been confirmed by Kennel Union Ghana.
                    log in now at http://www.pedigree.kennelunionghana.com for more details.';
            sendEmail($email, $message, ' Confirmation Message');



//            Activity::log('logged');

        }catch(\Exception $e){
//            flash()->error('Email could not be sent.');
            return redirect()->back();
        }
        return redirect()->back();

    }

    public function getAllDogs(Request $request){

        $dogs_active = 'active';
        $query = $request->query('dogs') == "unconfirmed" ? "?dogs=unconfirmed":"";


//        return view('admin.all_dogs',compact('dogs','breeders','users','dogs_active','paginator'));
        return view('admin.all_dogs',compact('dogs_active','query'));
    }

    // demo view for testing datatables

    public function getRegisteredDogs(){
        return view('admin.dogs');
    }

    public function getArrayData()
    {
        $dogsData = User::all();
        $users = new Collection();
//        $faker = Faker::create();

        foreach($dogsData as $data){
            $users->push([
                'id' => $data->id
            ]);
        }

//        for ($i = 0; $i < count($dogsData); $i++) {
//            $users->push([
//                'id'         => $i + 1,
//                'name'       => 'fred'.$i,
//                'email'      => 'fred@gmail'.$i.'.com',
//                'created_at' => Carbon::now()->format('m-d-Y'),
//                'updated_at' => Carbon::now()->format('m-d-Y'),
//                'action' => "<button class='btn btn-success'>View </button>",
//            ]);
//        }

//        return Datatables::of($users)->make(true);
        return $users;
    }

    public function getAllDogsData(Request $request){

        $builder = Dog::leftJoin('users','users.id','=','dogs.user_id')
            ->leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->select(DB::raw('dogs.*,CONCAT(users.first_name," ",users.last_name) as owner,breeders.name as breed'));

        $dogsData = Dog::queryDogsByField($builder,$request);

        $dogs = new Collection();

        foreach($dogsData as $dog){
            $dob = Carbon::createFromFormat('Y-m-d',$dog->dob);
            $difference = $dob->diffInMonths(Carbon::now());
            $dogs->push([
                'id' => $dog->id,
                'name'=> $dog->name,
                'dob'=> $dog->dob,
                'breed'=> $dog->breed,
                'sex'=> $dog->sex,
                'registration_number'=> $dog->registration_number,
                'breeder'=> $dog->owner == null ? '' : $dog->owner,
                'status' => $dog->confirmed == true ? "confirmed " : "<button id=\"btnConfirm-$dog->id\" class=\"btn btn-success btn-sm\"
                            data-toggle=\"tooltip\" data-placement=\"left\" title=\"confirm new dog\"><i class=\"fa fa-thumbs-o-up\"></i></button>",
            'certificate' => "<a class='btn btn-success btn-xs add-owner-detail' href='#' data-id=$dog->id> certificate</a>",

            'action' => " <a href=\"/version2/dog/$dog->id\" class=\"btn btn-default btn-xs\"><i class='fa fa-eye'></i></a>".
                "<a href=\"/version2/edit-dog/$dog->id\" class=\"btn btn-warning btn-xs\"><i class=\"fa fa-pencil-square-o\"></i> </a>".
                "<button class=\"btn btn-danger btn-xs\" id=\"delete-dog-$dog->id\"
                  data-toggle=\"tooltip\" data-placement=\"left\" title=\"move $dog->name to trash\"><i class=\"fa fa-trash\"></i></button>"
//                "<button class=\"btn btn-info btn-xs\" id=\"message-owner-$dog->id\"
//                  data-toggle=\"tooltip\" data-placement=\"left\" title=\"move $dog->name to trash\"><i class=\"fa fa-comment-o\"></i></button>"
            ]);
        }

        return Datatables::of($dogs)->make(true);

    }

    public function getTrash(Request $request){

        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        $builder = Dog::leftJoin('users','users.id','=','dogs.user_id')
            ->leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->select(DB::raw('dogs.*,CONCAT(users.first_name," ",users.last_name) as owner,breeders.name as breeder_name'));

        if($request->query('dogs')){
            $dogs = $builder->orderBy('confirmed')->paginate(10);
        }

        if($request->query('term')){
            $query = $request->term;
            $dogs = $builder->where('dogs.name','LIKE','%'.$query.'%')
                ->orWhere('breeders.name','LIKE','%'.$query.'%')
                ->orderBy('dogs.created_at','desc')
                ->onlyTrashed()
                ->paginate(10);
        }

        $dogs = $builder ->orderBy('dogs.created_at','desc')
            ->paginate(10);

        $breeders = Breeder::all();
        $users = User::all();

        $dogs = $builder->onlyTrashed()->paginate(8);


        $dogs_active = 'active';

        return view('admin.trash',compact('dogs','breeders','users', 'dogs_active'));
    }

    public function postRegisterDog(Request $request){
        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        \Cache::forget('dogs');
        $this->validate($request,['name'=>'required','sex'=>'required','dob'=>'required',
            'breeder_id'=> 'required','colour'=>'required']);
        $father = Dog::getDog($request->father) ? Dog::getDog($request->father)->registration_number : '' ;
        $mother = Dog::getDog($request->mother) ? Dog::getDog($request->mother)->registration_number : '';
        try {
            if(Dog::whereRegistrationNumber($request->registration_number)->first()){
                Dog::createDog($request->except('registration_number'));
                $reg_number = Dog::generateRegistrationNumber();
                Dog::find($request->id)->update([
                    'registration_number' => $reg_number
                ]);
                DogRelationship::create(['id'=>Uuid::generate(),'dog_id'=>$request->id,'father'=>$father,'mother'=>$mother]);
            }else{
                Dog::createDog($request->all());
                DogRelationship::create(['id'=>Uuid::generate(),'dog_id'=>$request->id,'father'=>$father,'mother'=>$mother]);
            }

//            flash()->success('successfully registered a new dog pending confirmation!');
            return redirect()->back();
        } catch(\Exception $e){
            return $e;
//            flash()->error('there seems to be an error in saving.');
//            return \Redirect::back();
        }
    }

    public function getDog($user_id){

        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        try {
            $dogs = DB::table('dogs')
                ->leftJoin('users','users.id','=','dogs.user_id')
                ->leftJoin('breeders','breeders.id','=','dogs.breeder_id')
                ->where('user_id',$user_id)
                ->select(DB::raw('dogs.*,CONCAT(users.first_name," ",users.last_name) as user_name,breeders.name as breeder_name'))
                ->paginate();
            $user = User::findOrFail($user_id);
            $first_name = $user->first_name;
            $last_name = $user->last_name;
            $user = \Auth::getUser()->first_name;

            $users = User::all('id','email','phone','first_name','last_name');

        }catch(\Exception $e){
          return 'there is an error';
        }

        $members = 'active';

        // $dogs =  Dog::where('user_id',$user_id)->get();
        return view('admin.member_dogs',compact('dogs','first_name','last_name','user','members'

        ));
    }

    public function postConfirmDog($dog_id,Request $request){

        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        try {
            DB::transaction(function () use ($request, $dog_id) {
                Dog::where('id', $dog_id)->update(['confirmed' => $request->status]);
                $dog = Dog::leftJoin('users', 'users.id', '=', 'dogs.user_id')
                    ->where('dogs.id', $dog_id)
                    ->select(DB::raw('users.*,dogs.name as dog_name'))
                    ->first();

            });

            flash()->success('Successfully confirmed a dog!');
            return \Redirect::back();
        }catch(\Exception $e){
            flash()->success('Error confirming dogs!');
            return \Redirect::back();
        }
    }

    public function getDogShow($dog_id){

        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
            ->where('dogs.id',$dog_id)
            ->select(DB::raw('dogs.*,breeders.name as breeder_name,dog_relationships.father,dog_relationships.mother'))
            ->first();

        $dogs_active = 'active';

        return view('admin.show_dog',compact('dog','dog_relationships', 'dogs_active'
        ));
    }

    public function getDogEdit($dog_id){

        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
            ->leftJoin('users','users.id','=','dogs.user_id')
            ->where('dogs.id',$dog_id)
            ->select(DB::raw('dogs.*,breeders.name as breeder_name,dog_relationships.father,dog_relationships.mother,
            CONCAT(users.first_name," ",users.last_name) as member, users.id as member_id'))
            ->first();
        //var_dump($dog);exit;
        $breeders = Breeder::all();
        $users = User::all('id','first_name','last_name');

        $dogs_active = 'active';


        return view('admin.member_dog_edit',compact('dog','breeders','users','dogs_active'
        ));
    }

    public function postDogUpdate(Request $request, $dog_id){
        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        DB::transaction(function()use($request,$dog_id){

            Dog::where('id',$dog_id)->update([
                'name' => $request->name, 'user_id' => $request->user_id == '' ? null : $request->user_id,
                'registration_number' => $request->registration_number,
                 'breeder_id' => $request->breeder_id,
                'dob' => $request->dob,
                'coat' => $request->coat,
                'sex' => $request->sex,
                'titles' => $request->titles,
                'performance_titles' => $request->performance_titles,
                'colour' => $request->colour,
                'height' => $request->height,
                'elbow_ed_results' => $request->elbow_ed_results,
                'appraisal_score' => $request->appraisal_score,
                'hip_hd_results' => $request->hip_hd_results,
                'tattoo_number' => $request->tattoo_number,
                'microchip_number' => $request->microchip_number,
                'DNA' => $request->DNA,
                'other_health_checks' => $request->other_health_checks
            ]);
            DogRelationship::where('dog_id',$dog_id)->update(['father'=> $request->father,'mother'=> $request->mother]);
        });

        flash()->success("You have successfully updated a dog's info. ");
        return \Redirect::back();
    }

    public function postDeleteDog($dog_id){

        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        Dog::find($dog_id)->delete();
        flash()->success('Successfully deleted a dog');
        return redirect()->back();
    }

    public function getAllBreeders(Request $request){

        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }
        $breeders = Breeder::paginate();
        if ($request->term) {
            $query = $request->term;
            $breeders = Breeder::where('name', 'LIKE', '%' . $query . '%')->get();
        }
        $paginator = new LengthAwarePaginator(range(1,count($breeders)),count($breeders),10,
            \Illuminate\Pagination\Paginator::resolveCurrentPage(),
            ['path' => \Illuminate\Pagination\Paginator::resolveCurrentPath()]
        );

        $breeders_menu = 'active';

        return view('admin.breeders',compact('breeders','breeders_menu','paginator'));
    }

    public function getBreederDogs($breeder_id){

        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        $dogs =  Breeder::leftJoin('dogs','dogs.breeder_id','=','breeders.id')
            ->where('breeders.id',$breeder_id)
            ->select(DB::raw('dogs.*'))
            ->orderBy('dogs.created_at','desc')
            ->get();

        $breeder= Breeder::find($breeder_id);

        $breeders_menu = 'active';

        return view('admin.breeder_dogs',compact('dogs','breeder','breeders_menu'));
    }

    public function postRegisterBreeder(Request $request){

        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }
        if(Breeder::where('name',$request->name)->first()){
            flash()->error('Breed already exists.');
        } else {
            $id = Uuid::generate();

            Breeder::create(['id' => $id, 'name' => $request->name]);
        }

        \Cache::forget('breeds');

        return redirect()->back();
    }


    public function postBreeder($breeder_id){

        return Breeder::where('id',$breeder_id)->first()->name;
    }

    public function postEditBreeder(Request $request,$breeder_id){
        Breeder::where('id',$breeder_id)->update(['name' => $request->edited_name]);
        flash()->success('Successfully updated a breeder ');
        return redirect()->back();
    }


    public function getSettings(){

        $users = User::all('id','email','phone','first_name','last_name');
        $index = 'active';

        return view('admin.settings',compact('users','index'));
    }

    public function postSettings(Request $request){

        \Cache::forget('notification_email');
        \Cache::rememberForever('notification_email',function()use($request){
            return $request->email;
        });


        \Cache::forget('notification_phone_number');
        \Cache::rememberForever('notification_phone_number',function()use($request){
            return $request->phone;
        });
    }

    public function postDelete($dog_id){
        Dog::find($dog_id)->delete();
        flash()->success('successfully deleted a dog');
        return redirect()->back();
    }

    public function postRestore($dog_id){
        Dog::onlyTrashed()->where('id',$dog_id)->restore();
        flash()->success('Dog has been successfully been restored.');
        return redirect()->back();

    }

    public function postPermanentlyDelete($dog_id){
        $dog = Dog::onlyTrashed()->where('id',$dog_id)->first();
        $dog->forceDelete();
        flash()->success('permanently deleted a dog');
        return redirect()->back();
    }

    public function getCertificate( $dog_id){
        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
            ->leftJoin('users','users.id','=','dogs.user_id')
            ->where('dogs.id',$dog_id)
            ->select(DB::raw('dogs.*,breeders.name as breeder_name,dog_relationships.father,dog_relationships.mother,
                              users.first_name,users.last_name'))
            ->first();

        // \Cache::forget('serial_number');
        \Cache::rememberForever('serial_number', function () {
            return NumberGeneration::generateUsingDate();
        });

        $type_certificate ="BIRTH";

        $dogs_active = 'active';

        return view('admin.certificate',compact('dog','type_certificate','dogs_active'));

    }

    public function getPedigreeCertificate( $dog_id){
        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
            ->leftJoin('users','users.id','=','dogs.user_id')
            ->where('dogs.id',$dog_id)
            ->select(DB::raw('dogs.*,breeders.name as breed,dog_relationships.father,dog_relationships.mother,
                              users.first_name,users.last_name,users.kennel_name'))
            ->first();
        // $view_count = $dog->viewed + 1;
        //Dog::where('id',$dog_id)->update(['viewed'=> $view_count]);
        $type_certificate = "PEDIGREE";

        $dogs_active = 'active';

        \Cache::forget('serial_number');
        \Cache::rememberForever('serial_number', function () {
            return NumberGeneration::generateUsingDate();
        });

        return view('admin.pdfcert',compact('dog','type_certificate','dogs_active'));
    }

    public function getChangeRole($user_id){

        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        if(User::where('id',$user_id)->first()->administrator == true){
            $update = false;
        }else{
            $update = true;
        }

        User::where('id',$user_id)->update(['administrator' => $update]);
        $user = User::where('id',$user_id)->first();

        flash()->success('you have successfully changed the role of ' . $user->first_name .' !' );
        return redirect()->back();
    }

    public function getPdf($dog_id){

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
            ->leftJoin('users','users.id','=','dogs.user_id')
            ->where('dogs.id',$dog_id)
            ->select(DB::raw('dogs.*,breeders.name as breeder_name,dog_relationships.father,
                              dog_relationships.mother, users.first_name,users.last_name'))
            ->first();
        $index = '';
        $dashboard = '';
        $members = '';
        $dogs_active = 'active';
        $breeders_menu = '';
        $manage_dogs = '';
        $transfers='';

        $data =['dog'=> $dog,'index'=>$index,'dashboard'=> $dashboard,'members'=> $members,
            'dogs_active'=> $dogs_active,'breeders_menu'=> $breeders_menu,'manage_dogs'=>$manage_dogs];

        $pdf = \PDF::loadView('partials.pdf_table',$data);
        return $pdf->download('admin.pdf');
    }

    public function getDogTransfers(Request $request){
        $builder =  DogTransfer::leftJoin('dogs','dogs.id','=','dog_transfers.dog_id')
            ->leftJoin('owners','owners.id','=','dog_transfers.owner_id')
            ->leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->leftJoin('users','users.id','=','dogs.user_id');

        if($request->query('term')){
            $query = $request->term;
            $dogs = $builder->where('dogs.name','LIKE','%'. $query.'%')
                ->orWhere('transfer_serial_number','LIKE','%'.$query.'%')
                ->orWhere('microchip_number','LIKE','%'.$query.'%')
                ->orWhere('dogs.registration_number', 'LIKE', '%' . $query . '%')
                ->orWhere('breeders.name', 'LIKE', '%' . $query . '%')
                ->orWhere('dogs.sex','LIKE','%'. $query.'%')
                ->select(DB::raw('dogs.*,CONCAT(users.first_name," ", users.last_name) as member, breeders.name as breeder_name,
                            CONCAT(owners.first_name," ",owners.last_name)as owner,dog_transfers.created_at as transfer_date,
                            dog_transfers.transfer_serial_number,dog_transfers.id as transfer_id, dog_transfers.confirmed as dog_status'))->paginate();
        } else {

            $dogs = $builder->select(DB::raw('dogs.*,CONCAT(users.first_name," ", users.last_name) as member, breeders.name as breeder_name,
                            CONCAT(owners.first_name," ",owners.last_name)as owner,dog_transfers.created_at as transfer_date,
                            dog_transfers.transfer_serial_number,dog_transfers.id as transfer_id,dog_transfers.confirmed as dog_status'))
                ->paginate();
        }

        $transfers= 'active';

        return view('admin.all_transfers',compact('dogs','transfers'));
    }

    public function getDogs(Request $request){

        $items = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            // ->where('sex',$request->query('g'))
            ->where('confirmed',true)
//            ->where('transferred',false)
            ->where(function(\Illuminate\Database\Eloquent\Builder $query) use($request){
                $query->where('dogs.name','LIKE','%'.$request->query('q').'%')
                    ->orWhere('registration_number','LIKE','%'.$request->query('q').'%');
            })->select(DB::raw('dogs.*,breeders.name as breeder_name'))
            ->forPage($request->query('page')?:1,15)
            ->paginate();

        return array('total_count'=>$items->total(),'items'=>$items->jsonSerialize());
    }

    public function getTransferDog(){

        $transfers= 'active';

        return view('admin.transfer_dog',compact('dogs','transfers'
        ));
    }

    public function postTransferDog(Request $request){

        $this->validate($request,['first_name'=>'required','last_name'=>'required','phone_number'=>'required','email'=>'required']);

        DB::transaction(function() use($request){
            $transfer_id = Uuid::generate();
            $owner_id  = Uuid::generate();

            Owner::create([
                'id'=> $owner_id, 'first_name'=> $request->first_name,'last_name'=> $request->last_name,
                'phone_number'=> $request->phone_number,'sec_phone_number'=>$request->sec_phone_number,'email'=>$request->email,'notes'=>$request->notes
            ]);
            DogTransfer::create(['id'=>$transfer_id,'dog_id'=> $request->dog,'owner_id'=>$owner_id,'transfer_serial_number'=> NumberGeneration::generateUsingDate(),'confirmed'=> true]);
            Dog::where('id',$request->dog)->update(['transferred'=>true]);
        });

        $transferred_dog = Dog::where('id',$request->dog)->first()->name;

        flash()->success($transferred_dog .' has  changed ownership !');
        return redirect()->back();

    }

    public function getEditTransferDog($dog_transfer_id){
        $dog = DogTransfer::leftJoin('owners','owners.id','=','dog_transfers.owner_id')
            ->where('dog_transfers.id',$dog_transfer_id)
            ->select(DB::raw('owners.*,dog_transfers.id as transfer_id'))
            ->first();


        $dogs_active = 'active';
        return view('admin.edit_transfer_dog',compact('dog','dogs_active'));

    }

    public function postUpdateTransferDog(Request $request,$transfer_id){
        DogTransfer::find($transfer_id)->update($request->all());
        return redirect()->back();
    }

    public function getTransferCertificate($dog_id){

//        if(Gate::denies('administer')){
//            flash()->error('You are not authorized to access that page !');
//            return redirect('/');
//        }

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
            ->leftJoin('users','users.id','=','dogs.user_id')
            ->leftJoin('dog_transfers','dog_transfers.dog_id','=','dogs.id')
            ->leftJoin('owners','owners.id','=','dog_transfers.owner_id')
            ->where('dogs.id',$dog_id)
            ->select(DB::raw('dogs.*,breeders.name as breed,dog_relationships.father,dog_relationships.mother,
                              users.first_name,users.last_name,CONCAT(owners.first_name," ", owners.last_name) as new_owner,owners.phone_number,owners.notes,
                              dog_transfers.transfer_serial_number'))
            ->first();
        \Cache::forget('serial_number');
        \Cache::rememberForever('serial_number', function () {
            return NumberGeneration::generateUsingDate();
        });
        // $view_count = $dog->viewed + 1;
        //Dog::where('id',$dog_id)->update(['viewed'=> $view_count]);

        $type_certificate = "BIRTH";

        $dogs_active = 'active';

        return view('admin.transfer_certificate',compact('dog','dog_id',
            'dogs_active','type_certificate'
        ));

    }


    public function getRegisterLitter(){
        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        $breeders = Breeder::all();
        $users = User::all('id','first_name','last_name');


        $dogs_active = 'active';

        return view('admin.register_litter',compact('breeders','users','dogs_active'
        ));
    }

    public function postRegisterLitter(Request $request){
        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }

        $this->validate($request,['name'=>'required','dob'=>'required','father'=> 'required',
            'mother'=>'required','breed'=> 'required']);

        $names = \Input::get('name',null);
        $genders = \Input::get('sex',null);

        $father = Dog::getDog($request->father)? Dog::getDog($request->father)->registration_number : '' ;
        $mother = Dog::getDog($request->mother) ? Dog::getDog($request->mother)->registration_number : '';


        DB::transaction(function() use ($request,$father,$mother,$names,$genders){
            foreach($names as $key=> $name){
                $year = date('Y');
                $month = date('m');
                $day = date('d');
                $count = Dog::all('name')->count() + 1;
                $registration_number = $year . $month . $day . $count;

                $sex = $genders[$key];
                if(!$name == "" || $sex) {
                    $id = \Uuid::generate();
                    $relationship_id = Uuid::generate();
                    Dog::create(['id' => $id, 'name' => $name, 'breeder_id' => $request->breed, 'registration_number' => $registration_number, 'user_id' => $request->user_id, 'dob' => $request->dob,'coat'=>$request->coat,
                        'colour'=>$request->colour, 'sex' => $sex]);
                    DogRelationship::create(['id' => $relationship_id, 'dog_id' => $id, 'father' => $father, 'mother' => $mother]);
                }
            }
        });

        return redirect()->back();

    }

    public function getCertificateRequests(){
        if(Gate::denies('administer')){
            flash()->error('You are not authorized to access that page !');
            return redirect('/');
        }
        $requests = RequestedCertificate::leftJoin('users','users.id','=','requested_certificates.user_id')
            ->leftJoin('dogs','dogs.id','=','requested_certificates.dog_id')
            ->select(DB::raw('CONCAT(users.first_name," ", users.last_name) as member, users.kennel_name, users.phone, dogs.name,
                       dogs.registration_number,requested_certificates.honoured,requested_certificates.created_at,requested_certificates.id'))
            ->orderBy('requested_certificates.honoured')
            ->paginate();


        $certificate ='active';
        //var_dump($requests);exit;
        return view('admin.certificate_requests',compact('requests','certificate'));
    }

    public function getHonourCertificate($request_id){
        RequestedCertificate::where('id',$request_id)->update(['honoured'=> 1]);
        flash()->success('Successfully honoured a certificate request');
        return redirect()->back();
    }

    public function postIssueCertificate($dog_id){
        try {
            $serial_number=  \Cache::pull('serial_number');
            $id = Uuid::generate();
            IssuedCertificate::create(["id" => $id,"dog_id" => $dog_id, "serial_number" => $serial_number,'user_id'=> \Auth::User()->id]);
        }catch(\Exception $e){
            return "Error Issuing Certificate". $e->getMessage();
        }
        return "Successfully Issued Certificate";

    }

    public function getIssuedCertificates(){
        $certificates = IssuedCertificate::leftJoin('dogs','dogs.id','=','issued_certificates.dog_id')
            ->leftJoin('users','users.id','=','issued_certificates.user_id')
            ->select(DB::raw('dogs.id,dogs.name as dog,dogs.registration_number,CONCAT(users.first_name," ",users.last_name) as member,
            issued_certificates.serial_number,issued_certificates.created_at'))
            ->paginate();

        $certificate = 'active';

        return view('admin.issued_certificates',compact('certificates','certificate'));
    }

    public function getPrintPdf(){
        self::printPdfInline('http://pedigree.kennelunionghana.com/admin/pdf-certificate/d95be0e0-49bc-11e6-93b0-37275c5cbdc1');
    }


    public function getPdfCertificate( $dog_id){
//        if(Gate::denies('administer')){
//            flash()->error('You are not authorized to access that page !');
//            return redirect('/');
//        }

        $dog = Dog::leftJoin('breeders','breeders.id','=','dogs.breeder_id')
            ->leftJoin('dog_relationships','dog_relationships.dog_id','=','dogs.id')
            ->leftJoin('users','users.id','=','dogs.user_id')
            ->where('dogs.id',$dog_id)
            ->select(DB::raw('dogs.*,breeders.name as breed,dog_relationships.father,dog_relationships.mother,
                              users.first_name,users.last_name'))
            ->first();
        // $view_count = $dog->viewed + 1;
        //Dog::where('id',$dog_id)->update(['viewed'=> $view_count]);
        $type_certificate = "PEDIGREE";

        $dogs_active = 'active';

        \Cache::forget('serial_number');
        \Cache::rememberForever('serial_number', function () {
            return NumberGeneration::generateUsingDate();
        });

        return view('admin.pdfcert',compact('dog','type_certificate','dogs_active'));
    }

    function printPdfInline($content){
        $pdf = new  \mikehaertl\wkhtmlto\Pdf(array(
            'binary' => '/opt/wkhtmltox/bin/wkhtmltopdf',
            'commandOptions' => array(
                'escapeCommand' => false,
                'useExec' => true
            ),
            'print-media-type'
        ));
        $pdf->addPage($content);
        if (!$pdf->send()) {
            throw new \Exception('Could not create PDF: '.$pdf->getError());
        }
    }
}
