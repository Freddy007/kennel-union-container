<?php

namespace App\Http\Middleware;

use Closure;

class IsConfirmed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user() && \Auth::user()->isConfirmed()) {
            return $next( $request );
        }

        flash()->info("Your account has not been confirmed yet!");
        return redirect('/not-confirmed');
    }
}
