<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/',
		'/add-new-column',
        '/member/register-dog',
        '/member/register-litter',
        '/member/check-dog/*',
        '/show-offspring/*',
        '/admin/breeder/*',
        '/admin/issue-certificate/*',
        '/check-dog-name/*',
        '/admin/report-number',
        '/admin/all-dogs-data',
        '/admin/array-data',
        '/admin/members-data',
        '/admin/register-dog',

        //  version 2
        'version2/all-breeds',
        '/version2/register-new-dog',
        'version2/register-litter',
        'version2/members-data',
	    '/version2/generate-ancestors/*',
	    '/version2/add-to-pedigree-table',
	    '/generate-ancestors/*',
	    '/add-to-pedigree-table',
	    '/regenerate-table',
        '/member/request-delete/*',
        '/member/request-certificate/*',
        '/version2/approve-certificate-request/*',
        '/version2/archive-dog/*',
        '/version2/confirm-dog/*',
        '/version2/generate-new-number/*/*',
        '/version2/restore-dog/*',
    ];
}
