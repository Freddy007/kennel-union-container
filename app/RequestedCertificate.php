<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class RequestedCertificate extends Model
{
    //
    protected $fillable = ['id','dog_id','user_id','honoured','download_url','certificate_expiry'];

    public static function getRequestedCertificates(){
         $certificates = RequestedCertificate::where('honoured',false)->get();
          return count($certificates);
    }

    public static function getLastRequestedCertificateTime(){
        $certificate_requests = DB::table('requested_certificates')->latest()->first();
        return date($certificate_requests->created_at);
    }

    public static function RequestsByMonth($month){
        $counts = DB::table('requested_certificates')->whereMonth('created_at','=',$month)->count();
        return $counts;
    }

    public static function getCertificateRequestStatus($dog_id){
        $request = RequestedCertificate::where('dog_id',$dog_id)->first();

        if ($request){
            if ($request->honoured == false){
                echo 'Request Pending';
            }else {
                echo 'Certificate Request Honoured';
            }
        }elseif(!$request){
            echo 'Request New';
        }

    }
}

