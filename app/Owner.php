<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    //
    protected $fillable = ['id','first_name','last_name','phone_number','secondary_phone_number','email','notes'];
}
