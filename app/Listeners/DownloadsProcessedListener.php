<?php

namespace App\Listeners;

use App\Download;
use App\Events\DownloadsProcessed;
use App\Generic;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class DownloadsProcessedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DownloadsProcessed  $event
     * @return void
     */
    public function handle(DownloadsProcessed $event)
    {
        $generate_certificate_job = "App\\Jobs\\GenerateCertificate";
        $data  = json_decode($event->payload);
        Generic::create([
            "data_one" => $data->displayName,
            "data_two" => $event->payload,
            "data_three" => ($event->payload),
            "type"      => $data->displayName
        ]);

        if ($data->displayName == $generate_certificate_job) {
            $download = Download::whereStatus(false)->latest()->first();
            $download->update([
                "status" => 1
            ]);
        }
    }
}
