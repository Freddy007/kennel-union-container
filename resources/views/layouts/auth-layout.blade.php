<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->

<head>
    <title>Login &middot; </title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{--<meta name="description" content="">--}}
    {{--<meta name="author" content="">--}}
    {!! \Artesaos\SEOTools\Facades\SEOMeta::generate() !!}

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Google Font: Open Sans -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,800,800italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{asset('../../bower_components/fontawesome/css/font-awesome.min.css')}}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('../../bower_components/bootstrap/dist/css/bootstrap.min.css')}}">


    <!-- App CSS -->
    <link rel="stylesheet" href="{{asset('css/mvpready-admin.css')}}">
    <!-- <link rel="stylesheet" href="./css/custom.css"> -->


    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="account-bg">

<header class="navbar" role="banner" style="background-color: white; border: none;">

    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-cog"></i>
            </button>

            <a href="{{url('/')}}" class="navbar-brand navbar-brand-img">
                {{--<img src="img/logo.png" alt="MVP Ready">--}}

                <h2>KENNEL UNION OF GHANA PEDIGREE DATABASE</h2>
            </a>
        </div> <!-- /.navbar-header -->

        <nav class="collapse navbar-collapse" role="navigation">

            <ul class="nav navbar-nav navbar-left">

                @can('administer')

                <li class="dropdown navbar-notification">

                    <a href="" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell navbar-notification-icon"></i>
                        <span class="visible-xs-inline">&nbsp;Notifications</span>
                        <b class="badge badge-primary">{{\App\Dog::checkUnconfirmedDogs() + \App\User::checkUnconfirmedMembers()}} </b>
                    </a>

                    <div class="dropdown-menu">

                        <div class="dropdown-header">&nbsp;Notifications</div>

                        <div class="notification-list">
                            <?php ?>
                            @if(\App\User::checkUnconfirmedMembers() == 0)


                            @else
                                <a href="{{url('/admin/members?members=unconfirmed')}}" class="notification">
                                    <span class="notification-icon"><i class="fa fa-user text-primary"></i></span>
                                    <span class="notification-title">New Members</span>
                                    {{--{{\App\User::getLastMemberTime()}}--}}
                                    <span class="notification-description"><strong>{{\App\User::checkUnconfirmedMembers()}}</strong> Member(s) awaiting confirmation.</span>
                                    {{--<span class="notification-time">20 minutes ago</span>--}}
                                </a> <!-- / .notification -->
                            @endif

                            @if(\App\Dog::checkUnconfirmedDogs() == 0)

                            @else
                                <a href="{{url('/admin/all-dogs?dogs=unconfirmed')}}" class="notification">
                                    <span class="notification-icon"><i class="fa fa-list text-secondary"></i></span>
                                    <span class="notification-title">New Dogs </span>
                                    <span class="notification-description"><strong>{{\App\Dog::checkUnconfirmedDogs()}}</strong> New Dog(s) Awaiting confirmation</span>
                                    {{--<span class="notification-time">20 minutes ago</span>--}}
                                </a> <!-- / .notification -->
                            @endif

                        </div> <!-- / .notification-list -->

                    </div> <!-- / .dropdown-menu -->

                </li>

                <li class="dropdown navbar-notification">

                </li>

                <li class="dropdown navbar-notification empty">

                    <a href="page-notifications.html" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-warning navbar-notification-icon"></i>
                        <span class="visible-xs-inline">&nbsp;&nbsp;Alerts</span>
                    </a>

                    <div class="dropdown-menu">

                        <div class="dropdown-header">Alerts</div>

                        <div class="notification-list">

                            <h4 class="notification-empty-title">No alerts here.</h4>
                            <p class="notification-empty-text">Check out what other makers are doing on Explore!</p>

                        </div> <!-- / .notification-list -->

                        <a href="page-notifications.html" class="notification-link">View All Alerts</a>

                    </div> <!-- / .dropdown-menu -->

                </li>
                @endcan

            </ul>

            <ul class="nav navbar-nav navbar-right">

                <li>
                    {{--<a href="javsacript:;"> {{'Hello ' . Auth::getUser()->first_name .' '. Auth::getUser()->last_name }}</a>--}}
                </li>

                {{--<li>--}}
                {{--<a href="javascript:;">Support</a>--}}
                {{--</li>--}}
                @can('administer')
                <li class="dropdown navbar-profile">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
                        {{--<img src="" class="navbar-profile-avatar" alt="">--}}
                        {{ Auth::getUser()->first_name .' '. Auth::getUser()->last_name }}
                        {{--<span class="visible-xs-inline">@peterlandt &nbsp;</span>--}}
                        <i class="fa fa-caret-down"></i>
                    </a>

                    <ul class="dropdown-menu" role="menu">

                        <li>
                            <a href="">
                                <i class="fa fa-cogs"></i>
                                &nbsp;&nbsp;Profile
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="{{url('auth/logout')}}">
                                <i class="fa fa-sign-out"></i>
                                &nbsp;&nbsp;Logout
                            </a>
                        </li>

                    </ul>

                </li>
                @elseif(Auth::check() == true )
                    <li class="dropdown navbar-profile">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
                            {{--<img src="" class="navbar-profile-avatar" alt="">--}}
                            {{ Auth::getUser()->first_name .' '. Auth::getUser()->last_name }}
                            {{--<span class="visible-xs-inline">@peterlandt &nbsp;</span>--}}
                            <i class="fa fa-caret-down"></i>
                        </a>

                        <ul class="dropdown-menu" role="menu">

                            <li>
                                <a href="">
                                    <i class="fa fa-cogs"></i>
                                    &nbsp;&nbsp;Profile
                                </a>
                            </li>

                            <li class="divider"></li>

                            <li>
                                <a href="{{url('auth/logout')}}">
                                    <i class="fa fa-sign-out"></i>
                                    &nbsp;&nbsp;Logout
                                </a>
                            </li>

                        </ul>

                    </li>

                @else
                    <li><a href="{{url('auth/register')}}">Register Now </a></li>
                    <li> <a  href="{{url('auth/login')}}">Log In</a></li>
                    @endcan


            </ul>

        </nav>

    </div> <!-- /.container -->

</header>

<br class="xs-80">


@yield('content')
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Core JS -->
<script src="../../bower_components/jquery/dist/jquery.js"></script>
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="../../bower_components/slimscroll/jquery.slimscroll.min.js"></script>


<!-- App JS -->
<script src="../../global/js/mvpready-core.js"></script>
<script src="../../global/js/mvpready-helpers.js"></script>
<script src="js/mvpready-admin.js"></script>


<!-- Demo JS -->
<script src="../../global/js/mvpready-account.js"></script>



</body>

</html>