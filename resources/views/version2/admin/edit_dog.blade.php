@extends('version2.layouts.admin_layout')

@section('styles')

@endsection

@section('scripts')

    <script src="{{asset('kug_version2/assets/global/scripts/select2.min.js')}}" type="text/javascript"></script>

    <script>
        $("#user_id").select2();

    </script>

@endsection


@section('content')
        <!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Edit dog
                        {{--<small>managed datatable samples</small>--}}
                    </h1>
                </div>
                <!-- END PAGE TITLE -->

            </div>
        </div>
        <!-- END PAGE HEAD-->

        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{url('/version2')}}">Dashboard</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{url('/version2/all-dogs')}}">all dogs</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    {{--<li>--}}
                        {{--<a href="#">Tables</a>--}}
                        {{--<i class="fa fa-circle"></i>--}}
                    {{--</li>--}}
                    <li>
                        <span>edit dog</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">

                    <div class="row">
                        <div class="col-md-12">
                            @include('flash::message')
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        {{--<i class="icon-equalizer font-red-sunglo"></i>--}}
                                        <span class="caption-subject font-red-sunglo bold uppercase">{{ucwords(strtolower($dog->name))}}</span>
                                        {{--<span class="caption-helper">{{$dog->name}}</span>--}}
                                    </div>
                                    <div class="actions">
                                        <div class="portlet-input input-inline input-small">
                                            <div class="input-icon right">
                                                {{--<i class="icon-magnifier"></i>--}}
                                                {{--<input type="text" class="form-control input-circle" placeholder="search..."> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    {{--<form action="#" class="form-horizontal">--}}
                                    <form action="{{url('/version2/update-dog',$dog->id)}}" class="form-horizontal" method="post">

                                        {!! csrf_field() !!}


                                        <div class="form-body">

                                            <div class="form-group">

                                                <label class="col-md-3 control-label">Registration date</label>
                                                <div class="col-md-4">
                                                    <p class="form-control-static"> {{$dog->created_at}} </p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                {{--<label class="col-md-3 control-label">Registration number</label>--}}
                                                {{--<div class="col-md-4">--}}
                                                {{--<input type="text" class="form-control" placeholder="Enter text" value="{{$dog->registration_number}}">--}}
                                                {{--<span class="help-block">  </span>--}}
                                                {{--</div>--}}

                                                <label class="col-md-3 control-label">Registration Number</label>

                                                <div class="col-md-4">
                                                    <p class="form-control-static"> {{$dog->registration_number}} </p>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Other registration Number</label>

                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="other_registration_number" placeholder="other registration number" value="{{$dog->other_registration_number}}">
{{--                                                    <p class="form-control-static"> {{$dog->other_registration_number}} </p>--}}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Name</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="name" placeholder="Name" value="{{$dog->name}}">
                                                </div>
                                            </div>

                                            {{--<div class="form-group">--}}
                                                {{--<label class="col-md-3 control-label">Father</label>--}}
                                                {{--<div class="col-md-4">--}}

                                                    {{--<input type="text" class="form-control" name="father" placeholder="Father" value="{{$dog->father}}">--}}
                                                {{--</div>--}}
                                            {{--</div>--}}


                                            {{--<div class="form-group">--}}
                                                {{--<label class="col-md-3 control-label">Mother</label>--}}
                                                {{--<div class="col-md-4">--}}

                                                    {{--<input type="text" class="form-control" name="mother" placeholder="Mother" value="{{$dog->mother}}">--}}
                                                    {{--</div>--}}
                                            {{--</div>--}}


                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Date of Birth</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="dob" placeholder="dob" value="{{$dog->dob}}"> </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Breed</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="breeder_id">
                                        <option value="{{$dog->breeder_id}}">{{$dog->breeder_name}}</option>
                                        @foreach($breeders as $breed)
                                        @if ($dog->breeder_id != $breed->id)
                                            <option value="{{$breed->id}}">{{$breed->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                                {{--<input type="text" class="form-control" placeholder="breed" value="{{$dog->breeder_name}}"> </div>--}}
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Gender</label>
                                                <div class="col-md-4">
                                                    <select name="sex" class="form-control">
                                                        @if($dog->sex == 'female')
                                                            <option>{{$dog->sex}}</option>
                                                            <option value="male">Male</option>
                                                        @else
                                                            <option>{{$dog->sex}}</option>
                                                            <option value="female">Female</option>
                                                        @endif
                                                    </select>
                                                    {{--<input type="text" name="sex" class="form-control" placeholder="sex" value="{{$dog->sex}}">--}}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Tattoo Number</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="tattoo_number" placeholder="Tattoo number" value="{{$dog->tattoo_number}}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Microchip Number</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="microchip_number" placeholder="Microchip number" value="{{$dog->microchip_number}}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Colour</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" placeholder="Colour" name="colour" value="{{$dog->colour}}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Height(inches)</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="height" placeholder="Height" value="{{$dog->height}}">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Coat</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="coat" placeholder="Coat" value="{{$dog->coat}}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">DNA ID</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="DNA" placeholder="DNA ID" value="{{$dog->DNA}}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Titles</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="titles" placeholder="Titles" value="{{$dog->titles}}">
                                                </div>
                                            </div>

                                            <div class="form-group last">
                                                <label class="col-md-3 control-label">Member</label>
                                                <div class="col-md-4">
                                                    <select  class="form-control"  name="user_id" >
                                                        <option value="{{$dog->member_id}}">{{$dog->member}}</option>
                                                        <option value="">Not a member</option>

                                                    @foreach($users as $user)
                                                            <option value="{{$user->id}}">{{$user->first_name .' '.$user->last_name}}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Image</label>
                                                <div class="col-md-4">
                                                    <input type="file" class="form-control" name="pic" placeholder="Titles" >
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">Submit</button>
                                                    <button type="button" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

@endsection
