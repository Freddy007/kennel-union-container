@extends('version2.layouts.admin_layout')

@section('scripts')
    <style>

        .tt-menu,
        .gist {
            text-align: left;
        }

        html {
            font: normal normal normal 18px/1.2 "Helvetica Neue", Roboto, "Segoe UI", Calibri, sans-serif;
            color: #292f33;
        }

        a {
            color: #03739c;
            text-decoration: none;
        }

        a:hover {
            text-decoration: underline;
        }

        .table-of-contents li {
            display: inline-block;
            *display: inline;
            zoom: 1;
        }

        .table-of-contents li a {
            font-size: 16px;
            color: #999;
        }

        p + p {
            margin: 30px 0 0 0;
        }

        .title {
            margin: 20px 0 0 0;
            font-size: 64px;
        }

        .example {
            padding: 30px 0;
        }

        .example-name {
            margin: 20px 0;
            font-size: 32px;
        }

        .demo {
            position: relative;
            *z-index: 1;
            margin: 50px 0;
        }

        .typeahead,
        .tt-query,
        .tt-hint {
            width: 300px;
            height: 40px;
            padding: 8px 12px;
            font-size: 17px;
            line-height: 20px;
            border: 2px solid #ccc;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border-radius: 6px;
            outline: none;
        }


        .typeahead {
            background-color: #fff;
        }

        .typeahead:focus {
            border: 2px solid #0097cf;
        }

        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
            color: #999
        }

        .tt-menu {
            width: 422px;
            margin: 12px 0;
            padding: 8px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
            -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
            box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            font-size: 18px;
            line-height: 24px;
        }

        .tt-suggestion:hover {
            cursor: pointer;
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion.tt-cursor {
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion p {
            margin: 0;
        }

        .gist {
            font-size: 14px;
        }

        .select2-container .select2-selection--single .select2-selection__rendered {
            display: block;
            padding-left: 8px;
            padding-right: 20px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            width: 300px !important;
        }


    </style>

@include('version2.admin.partials.register_dog_partials')

<script>

    Handlebars.registerHelper('notEquals', function(arg1, options) {
        // return (arg1 === arg2) ? options.fn(this) : options.inverse(this);
        return (arg1 !== "") ? options.fn(this) : options.inverse(this);
    });
    let url = "{{ url("/version2/dog") }}";
    // initializeDogSelect('/version2/dog');
    initializeDogSelect(url);

    $('.custom_registration_number').hide();

    $('#dog-not-born-in-ghana').on('click',function () {
        if($(this).is(':checked')){
            $('.custom_registration_number').show()
        }else{
            $('.custom_registration_number').hide();
        }
    });

    $('#name').on('change',function(){
//        alert('hello');
        var name = $('#name').val();
        jQuery.ajax('dog-by-name/'+name,function(data){
        }).fail(function (data) {
            $('.help-block.name').text(" This dog's name already exists!").css('color','red');
            $('#submit_form > div > div.form-actions > div > div > a.btn.btn-outline.green.button-next').hide()
        }).done(function (data) {
            $('#submit_form > div > div.form-actions > div > div > a.btn.btn-outline.green.button-next').show();
            $('.help-block.name').text(' ');
        })
    });

    var substringMatcher = function(strings) {
        return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strings, function(i, string) {
                if (substrRegex.test(string)) {
                    matches.push(string);
                }
            });

            cb(matches);
        };
    };

    // Suggestion engine
    var dogs = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: "{{url('/type-ahead-dogs?q=%QUERY')}}",
        remote: {
            url: "{{url('/type-ahead-dogs?q=%QUERY')}}",
            wildcard: '%QUERY',
            filter: dogs => $.map(dogs, dog => ({
                value: dog.name,
                id: dog.id,
                registration_number: dog.registration_number,
                sex: dog.sex,
                old: dog.duplicates.length > 0 ? dog.duplicates[0]['registration_number'] : ""
            }))
        }
    });

    $('.parent').typeahead(null, {
        displayKey: 'value',
        display: 'registration_number',
        source: dogs.ttAdapter(),
        templates: {
            empty: [
                '<div class="empty-message">',
                'unable to find any dog that matches the current query',
                '</div>'
            ].join('\n'),
            suggestion: Handlebars.compile(
                '<div>' +
                ' <div style="border-bottom: 1px solid #cccccc; text-transform: lowercase"><strong>@{{value}}</strong> – @{{registration_number}} – @{{sex}}</div> ' +
                '<div style="color: red"> ' +
                '@{{#notEquals old}}\n' +
                'Old number - @{{ old }}\n' +
                '- duplicated registration number' +
                '@{{/notEquals}}</div>' +
                '</div>'
            )
        }
    });


</script>

@endsection


@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>REGISTER NEW DOG
                        <small>form wizard </small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->
                <div class="page-toolbar">
                    <!-- BEGIN THEME PANEL -->
                    <!-- END THEME PANEL -->
                </div>
                <!-- END PAGE TOOLBAR -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content" >
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{url('/version2/all-dogs')}}">Dashboard</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                    <a href="{{url('/version2/all-dogs')}}">dogs</a>
                    <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>register new dog</span>
                    </li>
                </ul>

                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">

                    @include('version2.admin.partials.register_dog_form')

                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
@endsection
