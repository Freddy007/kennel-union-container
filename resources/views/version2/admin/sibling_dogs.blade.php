@extends('version2.layouts.admin_layout')

@section('styles')

    <link href="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

    @endsection

    @section('scripts')
            <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('kug_version2/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>


    <script>
        jQuery(document).ready(function() {
            $(".confirm").on("click", function () {
                var confirmed = $(this).data("confirmed");
                var name = $(this).data("title");
                var id = $(this).data("id");

                Swal.fire({
                    title: 'Are you sure?',
                    text: "This dog will be confirmed!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, confirm this dog!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        confirmDog(id);

                        setTimeout(function () {
                            location.reload()
                        },3000)
                        // table.page(page).draw('page');
                        // table.draw();
                    }
                })
            });

            $(".delete-request").off().on("click", function () {
                let dog_id = $(this).data("id");
                // let page = table.page.info().page;

                Swal.fire({
                    title: 'Are you sure?',
                    text: "This dog will be moved to archives!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        archiveDog(dog_id);

                        // setTimeout(function () {
                        //     location.reload()
                        // },3000)
                        // table.page(page).draw('page');
                    }

                })
            })

            $(".approve-certificate").off().on("click", function () {
                let dog_id = $(this).data("id");

                Swal.fire({
                    title: 'Are you sure?',
                    text: "This will approve certificate request!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Approve it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        approveCertificate(dog_id);
                        // table.page(page).draw('page');
                    }

                });
            });

            $(".download-certificate").off().on("click", function (event) {
                event.preventDefault();
                const download_base_url = "http://18.194.254.77/certification/public";

                let dog_id = $(this).data("id");
                enterNewOwner(dog_id);

                $(document).on('click', '.download', function () {
                    let name = $("#swal2-input").val();
                    Swal.fire('Downloading...', '', 'info');
                    if (name) {
                        window.location.href = `${download_base_url}/${dog_id}?name=${name}`;
                    } else {
                        window.location.href = `${download_base_url}/${dog_id}`;
                    }
                    console.log('Button 1');
                });
                $(document).on('click', '.sendToMember', function () {
                    console.log('Button 2');
                    let name = $("#swal2-input").val();
                    approveCertificate(dog_id, name);
                    Swal.fire('Certificate will be sent to the member', '', 'info');
                });
            })

            function confirmDog(id) {
                $.post("/version2/confirm-dog/" + id, function (data) {
                    Swal.fire(
                        'Confirmed!',
                        'Dog is now confirmed!',
                        'success'
                    )

                });
            }

            function archiveDog(id) {
                $.post("/version2/archive-dog/" + id, function (data) {
                    Swal.fire('Archived!', 'Dog is now archived.', 'success')
                });
            }

            function approveCertificate(id, owner_name) {
                let url = name === "" ? "/version2/approve-certificate-request/" + id : "/version2/approve-certificate-request/" + id + "?name=" + owner_name;
                $.post(url, function (data) {
                    Swal.fire('Approved!', 'Download url has been generated and send to member.', 'success')
                });
            }

            async function enterNewOwner(dog_id) {
                const download_base_url = "http://18.194.254.77/certification/public";
                const {value: name} = await Swal.fire({
                    title: "Enter new owner's name",
                    html:
                        '<input maxlength="100" autocapitalize="off" autocorrect="off" class="swal2-input" id="swal2-input" placeholder="Enter name(not required)" type="text" style="display: flex;">' +
                        '<button type="button" role="button" tabindex="0" class="SwalBtn1 download customSwalBtn">' + 'Download' + '</button>' +
                        '<button type="button" role="button" tabindex="0" class="SwalBtn2 sendToMember customSwalBtn">' + 'Send to member' + '</button>',
                    showCancelButton: false,
                    showConfirmButton: false
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    let owner_name = result.value;
                    console.log(result);

                    if (result.isDenied) {
                        approveCertificate(dog_id, owner_name);
                        Swal.fire('Certificate will be sent to the member ' + owner_name, '', 'info');
                    }

                    if (result.isConfirmed) {
                        Swal.fire("Downloading..." + owner_name, '', 'success');
                        if (owner_name) {
                            window.location.href = `${download_base_url}/${dog_id}?name=${owner_name}`;
                        } else {
                            window.location.href = `${download_base_url}/${dog_id}`;
                        }
                    }
                });
            }
        });


    </script>

    @endsection


    @section('content')
            <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <div class="container">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                        <h1>Siblings
                            {{--<small>managed datatable samples</small>--}}
                        </h1>
                    </div>
                    <!-- END PAGE TITLE -->

                </div>
            </div>
            <!-- END PAGE HEAD-->

            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{url('version2')}}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{url('/version2/all-dogs')}}">dogs</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>siblings</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">

                        <div class="row">
                            <div class="col-md-12">
                                @include('flash::message')

                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            {{--<i class="icon-settings font-dark"></i>--}}
                                            <span class="caption-subject bold uppercase">
{{--                                                        {{$first_name .' '.  $last_name."'s Dogs" }}--}}

                                            </span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="btn-group">
{{--                                                        {{$dogs->count() >1 ? $dogs->count() ." dogs":$dogs->count(). " dog" }} registered--}}

                                                    </div>
                                                </div>
                                                <div class="col-md-6">
{{--                                                    <div class="btn-group pull-right">--}}
{{--                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools--}}
{{--                                                            <i class="fa fa-angle-down"></i>--}}
{{--                                                        </button>--}}
{{--                                                        <ul class="dropdown-menu pull-right">--}}
{{--                                                            <li>--}}
{{--                                                                <a href="javascript:;">--}}
{{--                                                                    <i class="fa fa-print"></i> Print </a>--}}
{{--                                                            </li>--}}
{{--                                                            <li>--}}
{{--                                                                <a href="javascript:;">--}}
{{--                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>--}}
{{--                                                            </li>--}}
{{--                                                            <li>--}}
{{--                                                                <a href="javascript:;">--}}
{{--                                                                    <i class="fa fa-file-excel-o"></i> Export to Excel </a>--}}
{{--                                                            </li>--}}
{{--                                                        </ul>--}}
{{--                                                    </div>--}}
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover order-column" id="datatable-buttons">
                                            <thead>
                                            <tr>
                                                <th> Name</th>
                                                <th>Breed</th>
                                                <th>Sex</th>
                                                <th>Registration No.</th>
                                                <th>Registered Date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($dogs as $dog)
                                                <tr>
                                                    <td><a href="{{url('version2/dog',$dog->id)}}">{{$dog->name}}</a></td>
                                                    <td>{{$dog->breed}}</td>
                                                    <td>{{$dog->sex}}</td>
                                                    <td>{{$dog->registration_number}}</td>
                                                    <td>{{$dog->created_at}}</td>
                                                    @if($dog->confirmed == true)
                                                        <td>{{'Confirmed'}}</td>
                                                    @else
                                                        <td><button data-id="{{$dog->id}}" class="btn btn-success btn-sm confirm">Confirm</button></td>
                                                    @endif
                                                    <td>
                                                        <a class="btn btn-success btn-xs" href="{{url('version2/dog',$dog->id)}}"> View</a>
                                                        <a class="btn btn-success btn-xs" href="{{url('version2/edit-dog',$dog->id)}}"> <i class="fa fa-edit"></i></a>
                                                        <button  data-id="{{$dog->id}}" class="btn btn-danger btn-xs delete-request"><i data-id="{{$dog->id}}" class="fa fa-remove"></i></button>
                                                    </td>
                                                </tr>

                                            @endforeach

                                            </tbody>
                                        </table>
                                        <?php
                                        if(isset($paginator)){ ?>
                                        {{--{!! $paginator->render() !!}--}}
                                        {!! $paginator->appends(['term' => Request::get('term')])->render() !!}

                                        <?php   }else {?>
                                        {!! $dogs->render() !!}
                                        <?php } ?>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->

@endsection
