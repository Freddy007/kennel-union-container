@extends('version2.layouts.admin_layout')

@section('styles')

    <link href="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

    @endsection

    @section('scripts')
            <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('kug_version2/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>



            <script>

        jQuery(document).ready(function(){

            $(".approve-request").off().on("click",function(){

                let dog_id = $(this).data("id");
                // let page = table.page.info().page;

                Swal.fire({
                    title: 'Are you sure?',
                    text: "This will approve certificate request!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Approve it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        approveCertificate(dog_id);
                    }

                })
            });

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        });

        function approveCertificate(id) {
            $.post("/version2/approve-certificate-request/" + id, function (data) {
                Swal.fire('Approved!', 'Download url has been generated for member.', 'success')
            }).done(function() {
                setTimeout(function(){
                    location.reload();
                },4000);
            });;
        }

    </script>

    @endsection


    @section('content')
            <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <div class="container">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                        <h1>All Certificate Requests
                            {{--<small>managed datatable samples</small>--}}
                        </h1>
                    </div>
                    <!-- END PAGE TITLE -->

                </div>
            </div>
            <!-- END PAGE HEAD-->

            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{url('/version2')}}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        {{--<li>--}}
                            {{--<a href="{{url('/version2/all-dogs')}}">dogs</a>--}}
                            {{--<i class="fa fa-circle"></i>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<a href="#">Tables</a>--}}
                        {{--<i class="fa fa-circle"></i>--}}
                        {{--</li>--}}
                        <li>
                            <span>certificate requests</span>
                        </li>
                    </ul>

                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        {{--<div class="m-heading-1 border-green m-bordered">--}}
                        {{--<h3>DataTables jQuery Plugin</h3>--}}
                        {{--<p> DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, and will add advanced interaction controls to any HTML table. </p>--}}
                        {{--<p> For more info please check out--}}
                        {{--<a class="btn red btn-outline" href="http://datatables.net/" target="_blank">the official documentation</a>--}}
                        {{--</p>--}}
                        {{--</div>--}}

                        <div class="row">
                            <div class="col-md-12">
                                @include('flash::message')

                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            {{--<i class="icon-settings font-dark"></i>--}}
                                            <span class="caption-subject bold uppercase">
                                                       Certificate Requests

                                            </span>
                                        </div>
                                        {{--<div class="actions">--}}
                                        {{--<div class="btn-group btn-group-devided" data-toggle="buttons">--}}
                                        {{--<label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">--}}
                                        {{--<input type="radio" name="options" class="toggle" id="option1">Actions</label>--}}
                                        {{--<label class="btn btn-transparent dark btn-outline btn-circle btn-sm">--}}
                                        {{--<input type="radio" name="options" class="toggle" id="option2">Settings</label>--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="btn-group">
                                                        <button id="sample_editable_1_new" class="btn sbold green"> Back
                                                        <i class="fa fa-backward"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover order-column" id="datatable-buttons">
                                            <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Requester</th>
                                                <th>Phone Number</th>
                                                <th>Kennel</th>
                                                <th>Dog</th>
                                                <th>Registration Number</th>
                                                <th>Date Requested</th>
                                                <th>Approved</th>
                                            </tr>

                                            </thead>

                                          <tbody>
                                          <?php $i = $requests->firstItem(); ?>
                                          @foreach($requests as $request)
                                              <tr>
                                                  <td>{{$i++}}</td>
                                                  <td>{{$request->member}}</td>
                                                  <td>{{$request->phone}}</td>
                                                  <td>{{$request->kennel_name}}</td>
                                                  <td>{{$request->name}}</td>
                                                  <td>{{$request->registration_number}}</td>
                                                  <td>{{$request->created_at}}</td>
                                                  <td>
                                                      @if($request->honoured == false)
                                                          <btn  class="btn btn-success btn-sm approve-request" data-id="{{$request->dog_id}}">Approve request</btn>
                                                      @else
                                                          <strong>Approved</strong>
                                                      @endif
                                                  </td>
                                              </tr>

                                          @endforeach
                                          </tbody>
                                        </table>
                                        <?php
                                        if(isset($paginator)){ ?>
                                        {{--{!! $paginator->render() !!}--}}
                                        {!! $paginator->appends(['term' => Request::get('term')])->render() !!}

                                        <?php   }else {?>
                                        {!! $requests->render() !!}
                                        <?php } ?>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->

@endsection
