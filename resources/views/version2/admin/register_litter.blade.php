@extends('version2.layouts.admin_layout')

@section('scripts')

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('kug_version2/assets/global/scripts/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{{asset('kug_version2/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('kug_version2/assets/pages/scripts/form-wizard.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('kug_version2/assets/register_dog_scripts.js')}}" type="text/javascript"></script>

    <script src="{{asset('js/blockUI.js')}}"></script>

    <script>


        $('#add-new-column').on('click',function(){
//        alert('hello');
//        $( ".additional-columns" ).load( "/add-new-column");
            $.ajax({
                url: 'add-new-column',
                dataType: "html",
                success: function (data) {
                    $('.additional-columns').append(data);
                }
            });

            setTimeout(function () {
                let columns = $(".columns");
//            alert($('.columns').length + ' colour:'+ $('.colour').length+' colour object:'+$('.colour'));
                let dogsCount = columns.length === 1 ? " puppy ":" puppies ";
                $('.count').text('You are registering '+columns.length+ dogsCount);
//            console.log(document.getElementsByClassName('colour')[0].getAttribute("value"))
            },2000)
        });

        $(document).on('change', '.name', function (e) {

            let name = $(this).val();
            let self = $(this);
            let nextBtn = $('#submit_form > div > div.form-actions > div > div > a.btn.btn-outline.green.button-next');
            let addColumnBtn = $('#add-new-column');
            $.ajax({
                url : 'dog-by-name/'+name,
                beforeSend: function (xhr) {
                    $.blockUI({
                        message : "checking if name exists...",
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: "rgba('0,0,0,0.5')",
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            'z-index': '100000',
                            color: '#000',
                            'font-size' : '30px'
                        }
                    });
                },

            }).fail(function (data) {
                nextBtn.hide();
                addColumnBtn.hide();
                $.unblockUI();
                swal('Error','This puppy is already in the database!','error');
            }).done(function (data) {
                $.unblockUI();
                nextBtn.show();
                addColumnBtn.show();
                $('.help-block.name').text(' ');
            });
        });

        let url = "{{ url("/version2/all-dogs") }}";

        initializeDogSelect(url, true);

        $('.owner-details').hide();

        $('#different-owner').on('click',function () {
            if($(this).is(':checked')){
                $('.owner-details').show()
            }else{
                $('.owner-details').hide();
            }
        })

        var substringMatcher = function(strings) {
            return function findMatches(q, cb) {
                var matches, substringRegex;

                // an array that will be populated with substring matches
                matches = [];

                // regex used to determine if a string contains the substring `q`
                substrRegex = new RegExp(q, 'i');

                // iterate through the pool of strings and for any string that
                // contains the substring `q`, add it to the `matches` array
                $.each(strings, function(i, string) {
                    if (substrRegex.test(string)) {
                        matches.push(string);
                    }
                });

                cb(matches);
            };
        };

        // Suggestion engine
        var courses = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: 'type-ahead-dogs?q=%QUERY',
            remote: {
                url: 'type-ahead-dogs?q=%QUERY',
                wildcard: '%QUERY'
            }
        });

        // $('#courses').typeahead({
        $('.parent').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'courses',
            source: courses,
        });

    </script>

    <!-- END PAGE LEVEL SCRIPTS -->

@endsection


@section('content')
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <div class="container">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                        <h1>REGISTER LITTER
                            <small>form wizard </small>
                        </h1>
                    </div>
                    <!-- END PAGE TITLE -->
                    <!-- BEGIN PAGE TOOLBAR -->
                    <div class="page-toolbar">
                        <!-- BEGIN THEME PANEL -->
                        <!-- END THEME PANEL -->
                    </div>
                    <!-- END PAGE TOOLBAR -->
                </div>
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content" >
                <div class="container">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{url('/version2/all-dogs')}}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{url('/version2/all-dogs')}}">dogs</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>register litter</span>
                        </li>
                    </ul>

                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        <div class="row">
                            {{--<div class="col-md-12" ng-app="adminApp">--}}
                            <div class="col-md-12">
                                {{--<div class="m-heading-1 border-green m-bordered">--}}
                                {{--<h3>Twitter Bootstrap Wizard Plugin</h3>--}}
                                {{--<p> This twitter bootstrap plugin builds a wizard out of a formatter tabbable structure. It allows to build a wizard functionality using buttons to go through the different wizard steps and using events allows to hook--}}
                                {{--into each step individually. </p>--}}
                                {{--<p> For more info please check out--}}
                                {{--<a class="btn red btn-outline" href="http://vadimg.com/twitter-bootstrap-wizard-example" target="_blank">the official documentation</a>--}}
                                {{--</p>--}}
                                {{--</div>--}}
                                {{--<div class="portlet light " id="form_wizard_1" ng-controller = "DogRegistrationController">--}}
                                <div class="portlet light " id="form_wizard_1">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class=" icon-layers font-red"></i>
                                            <span class="caption-subject font-red bold uppercase"> Litter Registration Wizard -
                                                    <span class="step-title"> Step 1 of 3 </span>
                                                </span>
                                        </div>
                                        <div class="actions">
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-cloud-upload"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-wrench"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-trash"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form action="{{url('version2/register-litter')}}" class="form-horizontal" id="submit_form" method="POST">
                                            {{--<form action="{{url('admin/register-dog')}}" class="form-horizontal" id="submit_form" method="POST">--}}
                                            <div class="form-wizard">
                                                <div class="form-body">
                                                    <ul class="nav nav-pills nav-justified steps">
                                                        <li>
                                                            <a href="#tab1" data-toggle="tab" class="step">
                                                                <span class="number"> 1 </span>
                                                                <span class="desc">
                                                                        <i class="fa fa-check"></i> Basic Details </span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab2" data-toggle="tab" class="step">
                                                                <span class="number"> 2 </span>
                                                                <span class="desc">
                                                                        <i class="fa fa-check"></i> Secondary Details </span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab3" data-toggle="tab" class="step">
                                                                <span class="number">3 </span>
                                                                <span class="desc">
                                                                        <i class="fa fa-check"></i> Confirm </span>
                                                            </a>
                                                        </li>
                                                    </ul>

                                                    <div id="bar" class="progress progress-striped" role="progressbar">
                                                        <div class="progress-bar progress-bar-success"> </div>
                                                    </div>
                                                    <div class="tab-content">
                                                        <div class="alert alert-danger display-none">
                                                            <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                                        <div class="alert alert-success display-none">
                                                            <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                                        <div class="tab-pane active" id="tab1">
                                                            <h3 class="block">Basic details</h3>

                                                            <div class="columns" style="border: 1px solid #cccccc; border-radius: 7px; width: 50%; margin: auto; margin-bottom: 10px; padding: 10px 5px 0 5px;">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Name
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" class="form-control name" name="name[]" id="name" class="name" required />
                                                                        <span class="help-block">  </span>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Gender
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-9">
                                                                        <select class="form-control" name="sex[]">
                                                                            <option></option>
                                                                            <option>Male</option>
                                                                            <option>Female</option>
                                                                        </select>
                                                                        <span class="help-block">  </span>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Colour
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" class="form-control colour" name="colour[]" id="colour" value="" required />
                                                                        <span class="help-block">  </span>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Height
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-9">
                                                                        <input type="number" class="form-control height" name="height[]" id="height" required />
                                                                        <span class="help-block">  </span>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Coat
                                                                        <span class="required"> * </span>
                                                                    </label>
                                                                    <div class="col-md-9">
                                                                        <select class="form-control coat" name="coat[]" id="coat" required>

                                                                            <option>short</option>
                                                                            <option>long</option>
                                                                            <option>flat</option>
                                                                            <option>combination</option>
                                                                            <option>double</option>
                                                                            <option>heavy</option>
                                                                            <option>silky</option>
                                                                            <option>curly</option>
                                                                            <option>wire</option>

                                                                        </select>
                                                                        {{--<input type="text" class="form-control coat" name="coat[]" id="coat" required />--}}
                                                                        <span class="help-block">  </span>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Microchip number
                                                                        <span class="required"> </span>
                                                                    </label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" class="form-control microchip_number" name="microchip_number[]" id="coat" required />
                                                                        <span class="help-block">  </span>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="additional-columns"></div>

                                                            <div class="form-group text-center">
                                                                <button type="button" class="btn btn-outline green add-new-column" id="add-new-column"> Add puppy
                                                                    <i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>


                                                        </div>
                                                        <div class="tab-pane" id="tab2">
                                                            <h3 class="block">Secondary Details</h3>

                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Date of Birth
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-4">
                                                                    <input type="date" class="form-control" name="dob" id="dob" required />
                                                                    <span class="help-block">  </span>
                                                                </div>
                                                            </div>

{{--                                                            <div class="form-group">--}}
{{--                                                                <label class="control-label col-md-3">Father(Sire)--}}
{{--                                                                    --}}{{--<span class="required"> * </span>--}}
{{--                                                                </label>--}}
{{--                                                                <div class="col-md-4">--}}
{{--                                                                    <select class="dog_parent form-control"  id="father" name="father"  data-gender="male" style="width: 320px;" >--}}
{{--                                                                        <option value=""></option>--}}
{{--                                                                    </select>--}}
{{--                                                                    <span class="help-block">  </span>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
{{--                                                            --}}
{{--                                                            <div class="form-group">--}}
{{--                                                                <label class="control-label col-md-3">Mother(Dam)--}}
{{--                                                                    --}}{{--<span class="required"> * </span>--}}
{{--                                                                </label>--}}
{{--                                                                <div class="col-md-4">--}}
{{--                                                                    <select class="dog_parent form-control" name="mother" id="mother" data-gender="female" style="width: 320px;">--}}
{{--                                                                        <option value=""></option>--}}
{{--                                                                    </select>--}}
{{--                                                                    <span class="help-block">  </span>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}

                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Father(Sire)
                                                                    {{--<span class="required"> * </span>--}}
                                                                </label>
                                                                <div class="col-md-4">

                                                                    <div class="typeahead-container">

                                                                        <input class="typeahead parent" type="text" name="father" placeholder="Father of dog">

                                                                    </div>
                                                                    <span class="help-block">  </span>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Mother(Dam)
                                                                    {{--<span class="required"> * </span>--}}
                                                                </label>
                                                                <div class="col-md-4">

                                                                    <div class="typeahead-container">
                                                                        <input class="typeahead parent" type="text" name="mother" placeholder="Mother of dog">

                                                                    </div>

                                                                    <span class="help-block">  </span>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Breed
                                                                    <span class="required"> * </span>
                                                                    {{--<span> * </span>--}}

                                                                </label>
                                                                <div class="col-md-4">
                                                                    {{--<select ng-model="selectedName" ng-options="item as item.name for item in breeds track by item.id" class="form-control" id="breed" name="breed">--}}
                                                                    <select  class="form-control" id="breed" name="breeder_id" required>
                                                                        <option></option>
                                                                        @foreach($breeds as $breed)
                                                                            <option value="{{$breed->id}}">{{$breed->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <span class="help-block">  </span>


                                                                    {{--<select ng-options="item as item.label for item in items track by item.id" ng-model="selected"></select>--}}
                                                                    {{--<select  name="breed" id="breed" class="form-control" required>--}}
                                                                    {{--<option ng-repeat="breed in breeds"  >@{{ breed.name }}</option>--}}
                                                                    {{--</select>--}}
                                                                </div>
                                                            </div>

{{--                                                            <div class="form-group">--}}
{{--                                                                <label class="control-label col-md-3">Breeder is different owner:</label>--}}
{{--                                                                <div class="col-md-4">--}}
{{--                                                                    <input type="checkbox" name="different_owner" id="different-owner">--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}

{{--                                                            <div class="form-group owner-details">--}}
{{--                                                                <label class="control-label col-md-3">Owner--}}
{{--                                                                    <span class="required"> * </span>--}}
{{--                                                                    --}}{{--<span> * </span>--}}

{{--                                                                </label>--}}
{{--                                                                <div class="col-md-4">--}}
{{--                                                                    <input class="form-control" type="text" name="owner" id="owner">--}}
{{--                                                                    <span class="help-block">  </span>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}

                                                        </div>

                                                        <div class="tab-pane" id="tab3">
                                                            <h3 class="block">Confirm Details</h3>
                                                            {{--<h4 class="form-section"> Details</h4>--}}
                                                            <h4 class="count text-center"></h4>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <a href="javascript:;" class="btn default button-previous">
                                                                <i class="fa fa-angle-left"></i> Back </a>
                                                            <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                                                <i class="fa fa-angle-right"></i>
                                                            </a>

                                                            <button type="submit" class="btn green button-submit">Submit
                                                                <i class="fa fa-check"></i>
                                                            </button>
                                                            {{--<a href="javascript:;" class="btn green button-submit"> Submit--}}
                                                            {{--<i class="fa fa-check"></i>--}}
                                                            {{--</a>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->

@endsection
