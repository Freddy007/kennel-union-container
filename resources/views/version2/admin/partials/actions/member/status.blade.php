{{--<span>{{  $member->confirmed == true ? "Confirmed " : "Unconfirmed" }}</span>--}}

@php
    $confirmed = $member->confirmed
@endphp
@if(!$confirmed)
    <button class='btn btn-success confirm btn-sm' data-title="{{$member->first_name}}"
            data-confirmed="{{$member->confirmed ? true : false}}"
            data-id="{{$member->id}}" id="{{$member->id}}"> Confirm </button>
@else

      <button class='btn btn-danger btn-sm confirm' data-title="{{$member->first_name}}"
                 data-confirmed="{{$member->confirmed ? true : false}}"
                 data-id="{{$member->id}}" id="{{$member->id}}"> Revoke
      </button>

@endif
