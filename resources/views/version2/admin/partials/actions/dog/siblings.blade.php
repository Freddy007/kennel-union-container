
@php
$relationship = \App\DogRelationship::whereDogId($dog->id)->first();
$father = $relationship->father;
$mother = $relationship->mother;
$siblings_count = \App\DogRelationship::where("dog_id","<>",$dog->id)
->where("mother","<>","")->where("father","<>","")
->whereMother($mother)->whereFather($father)->count();
@endphp

{{--{{ $dog->name }}--}}
{{--<a href="{{route("siblings",$dog->id)}}" >{{ $siblings_count }}</a>--}}
{{ $siblings_count }}
