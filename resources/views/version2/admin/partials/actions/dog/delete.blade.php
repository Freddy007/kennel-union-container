@php
   $confirmed = $dog->delete_request;
   $certificate_request = \App\RequestedCertificate::whereDogId($dog->id)->first();

@endphp

    @if($confirmed)
        <button class='btn btn-danger delete-request' data-title="{{$dog->name}}" data-delete-request="{{$dog->delete_request ? true : false}}"
                data-id="{{$dog->id}}" id="{{$dog->id}}"> Approve delete </button>
    @endif

<br>
<br>

@if($certificate_request && !$certificate_request->honoured)
    <button class='btn btn-success approve-certificate' data-title="{{$dog->name}}"
            data-id="{{$dog->id}}" id="{{$dog->id}}"> Approve certificate </button>
@endif

@if(!$confirmed && !$certificate_request)
    No requests
@endif

@if(!$confirmed && $certificate_request && $certificate_request->honoured)
    No requests
@endif


