<script src="{{asset('kug_version2/assets/global/scripts/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{asset('kug_version2/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/0.11.1/typeahead.bundle.min.js" type="text/javascript"></script>
{{--<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>--}}
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('kug_version2/assets/pages/scripts/form-wizard.min.js')}}" type="text/javascript"></script>

<script src="{{asset('kug_version2/assets/register_dog_scripts.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.min.js" type="text/javascript"></script>