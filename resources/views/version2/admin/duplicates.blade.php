@extends('version2.layouts.admin_layout')

@section('styles')
    <link href="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{"/css/all_dogs.css"}}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('kug_version2/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/0.11.1/typeahead.bundle.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script>


        Handlebars.registerHelper('notEquals', function(arg1, options) {
            // return (arg1 === arg2) ? options.fn(this) : options.inverse(this);
            return (arg1 !== "") ? options.fn(this) : options.inverse(this);
        });

        let substringMatcher = function(strings) {
            return function findMatches(q, cb) {
                var matches, substringRegex;

                matches = [];
                substrRegex = new RegExp(q, 'i');

                $.each(strings, function(i, string) {
                    if (substrRegex.test(string)) {
                        matches.push(string);
                    }
                });

                cb(matches);
            };
        };

        // Suggestion engine
        const dogs = new Bloodhound({
            datumTokenizer: datum => Bloodhound.tokenizers.whitespace(datum.value),
            queryTokenizer: Bloodhound.tokenizers.whitespace,

            prefetch: "{{url('/type-ahead-dogs?q=%QUERY')}}&type=duplicates",
            remote: {
                url: "{{url('/type-ahead-dogs?q=%QUERY')}}&type=duplicates",
                wildcard: '%QUERY',
                filter: dogs => $.map(dogs, dog => ({
                    value: dog.name,
                    id: dog.id,
                    registration_number: dog.registration_number,
                    sex: dog.sex,
                    old: dog.duplicates.length > 0 ? dog.duplicates[0]['registration_number'] : ""
                }))
            }
        });

        dogs.initialize();
        // Instantiate the Typeahead UI
        $('#name').typeahead(null, {
            displayKey: 'value',
            display: 'registration_number',
            source: dogs.ttAdapter(),
            templates: {
                empty: [
                    '<div class="empty-message">',
                    'unable to find any dog that matches the current query',
                    '</div>'
                ].join('\n'),
                suggestion: Handlebars.compile(
                    '<div>' +
                    ' <div style="border-bottom: 1px solid #cccccc"><strong>@{{value}}</strong> – @{{registration_number}} – @{{sex}}</div> ' +
                    '<div style="color: red"> ' +
                    '@{{#notEquals old}}\n' +
                    'Old number - @{{ old }}\n' +
                    '- duplicated registration number' +
                    '@{{/notEquals}}</div>' +
                    '</div>'
                )
            }
        });

        jQuery(document).ready(function() {

            $.get("{{url('/type-ahead-dogs?q=daisy')}}",function (data){
                console.log(data[0]["duplicates"][0]);
            });

            $(".confirm").on("click", function () {
                var confirmed = $(this).data("confirmed");
                var name = $(this).data("title");
                var id = $(this).data("id");

                Swal.fire({
                    title: 'Are you sure?',
                    text: "This dog will be confirmed!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, confirm this dog!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        confirmDog(id);
                        location.reload();
                    }
                })
            });

            $(".generate-new").off().on("click", function () {
                let id = $(this).data("id");
                let dog_id = $(this).data("dog");

                Swal.fire({
                    title: 'Are you sure?',
                    text: "This will generate a new registration number!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Continue!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        generateNewNumber(id,dog_id);

                        setTimeout(function (){
                            location.reload();
                        },3000);
                    }
                });
            });

            function archiveDog(id) {
                $.post("/version2/archive-dog/" + id, function (data) {
                    Swal.fire('Archived!', 'Dog is now archived.', 'success')
                });
            }

            function generateNewNumber(id,dog_id) {
                let url =  "/version2/generate-new-number/" + id +"/" + dog_id ;

                $.post(url, function (data) {
                    Swal.fire(
                        'Generated!',
                        'New registration number has been generated.',
                        'success'
                    )
                });
            }

            async function enterNewOwner(dog_id){
                const download_base_url = "http://18.194.254.77/certification/public";
                const { value: name } = await Swal.fire({
                    title: "Enter new owner's name",
                    html:
                        '<input maxlength="100" autocapitalize="off" autocorrect="off" class="swal2-input" id="swal2-input" placeholder="Enter name(not required)" type="text" style="display: flex;">' +
                        '<button type="button" role="button" tabindex="0" class="SwalBtn1 download customSwalBtn">' + 'Download' + '</button>' +
                        '<button type="button" role="button" tabindex="0" class="SwalBtn2 sendToMember customSwalBtn">' + 'Send to member' + '</button>',
                    showCancelButton: false,
                    showConfirmButton: false
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    let owner_name = result.value;
                    console.log(result);

                    if (result.isDenied) {
                        approveCertificate(dog_id, owner_name);
                        Swal.fire('Certificate will be sent to the member '  + owner_name, '', 'info');
                    }

                    if (result.isConfirmed) {
                        Swal.fire("Downloading..." + owner_name, '', 'success');
                        if (owner_name) {
                            window.location.href = `${download_base_url}/${dog_id}?name=${owner_name}`;
                        }else{
                            window.location.href = `${download_base_url}/${dog_id}`;
                        }
                    }
                });
            }

        });

    </script>

@endsection


@section('content')
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <div class="container">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                        <h1>All Dogs
                            {{--<small>managed datatable samples</small>--}}
                        </h1>
                    </div>
                    <!-- END PAGE TITLE -->

                </div>
            </div>
            <!-- END PAGE HEAD-->


            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">

                <div id="overlay"><h2>Loading .. Please wait</h2></div>

                <div class="container-fluid">
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{url('/version2')}}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        {{--<li>--}}
                        {{--<a href="#">More</a>--}}
                        {{--<i class="fa fa-circle"></i>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<a href="#">Tables</a>--}}
                        {{--<i class="fa fa-circle"></i>--}}
                        {{--</li>--}}
                        <li>
                            <span>All Dogs</span>
                        </li>
                    </ul>

                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        <div class="row">
                            <div class="col-md-12">
                                @include('flash::message')

                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            {{--                                            <i class="icon-settings font-dark"></i>--}}
                                            <span class="caption-subject bold uppercase">
                                                {{\App\DogDuplicate::count()}} dogs duplicates
                                            </span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="btn-group">
                                                        <a href="{{url('version2/register-new-dog')}}" id="sample_editable_1_new" class="btn sbold green"> Add Dog
                                                            <i class="fa fa-plus"></i>
                                                        </a>


                                                        <a href="{{url('version2/register-litter')}}" id="sample_editable_1_new" class="btn sbold yellow"> Add Litter
                                                            <i class="fa fa-plus"></i>
                                                        </a>

                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
{{--                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Query by--}}
{{--                                                            <i class="fa fa-angle-down"></i>--}}
{{--                                                        </button>--}}
{{--                                                        <ul class="dropdown-menu pull-right">--}}
{{--                                                            <li>--}}
{{--                                                                <a >--}}
{{--                                                                    Registration number duplicates </a>--}}
{{--                                                            </li>--}}

{{--                                                            <li>--}}
{{--                                                                <a>--}}
{{--                                                                    Name duplicates </a>--}}
{{--                                                            </li>--}}

{{--                                                        </ul>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row margin-bottom-10">
                                            <form>
                                                <div class="col-md-3">

                                                </div>
                                                <div class="col-md-6 margin-top-10">
{{--                                                    <label for="name"></label><input type="text" class="form-control typeahead" id="name" placeholder="Search dogs by name or registration number here..." name="q" value="{{Request::query("q")}}">--}}

                                                </div>

                                                <div class="col-md-2 margin-top-15">
{{--                                                    <button class="btn btn-success margin-bottom-5" type="submit">Search</button>--}}
                                                </div>

                                            </form>
                                        </div>


                                        <table class="table table-striped table-bordered table-hover order-column">
                                            <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Name</th>
                                                <th>Siblings</th>
                                                <th>Offspring</th>
                                                <th>Date of birth</th>
                                                <th>Breed </th>
                                                <th>Sex </th>
                                                <th>Registration number </th>
                                                <th>New registration number </th>
                                                <th>Breeder</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            @foreach($dogs as $dog )
                                                <tr>

                                                    <td>

                                                        <img width="70" height="70"
                                                             src={{$dog->dog->image_name == null ? url('https://placehold.it/300x200/cccccc/000000?text=no+image+uploaded'):"/images/catalog/$dog->image_name"}} alt="dog pic"/>

                                                    </td>
                                                    <td>{{ $dog->dog->name }}</td>
                                                    <td>
                                                        @php
                                                            $relationship = \App\DogRelationship::whereDogId($dog->dog->id)->first();
                                                            $father = $relationship->father;
                                                            $mother = $relationship->mother;
                                                            $siblings_count = \App\DogRelationship::where("dog_id","<>",$dog->dog->id)
                                                            ->where("mother","<>","")->where("father","<>","")
                                                            ->whereMother($mother)->whereFather($father)->count();
                                                        @endphp

                                                        {{ $siblings_count }}
                                                    </td>

                                                    <td>
                                                        @php
                                                            $count = \App\DogRelationship::orWhere("mother",$dog["dog"]->registration_number)
                                                            ->orWhere("father",$dog->registration_number)->count();
                                                        @endphp

                                                        {{ $count }}
                                                    </td>

                                                    <td>{{$dog->dog->dob}}</td>
                                                    <td>{{ \App\Breeder::whereId($dog->dog->breeder_id)->first()->name}}</td>
                                                    <td>{{ $dog->dog->sex == "male" ? "sire" : "dam"  }}</td>
                                                    <td>{{ $dog->registration_number }}</td>
                                                    <td>{{ $dog->new_registration_number }}</td>
                                                    <td>{{$dog->dog->user["first_name"]}} {{$dog->dog->user['last_name']}} ( {{ $dog->dog->user['kennel_name'] }} )</td>
                                                    <td>@php
                                                            $fixed = $dog->fixed
                                                        @endphp
                                                        @if(!$fixed)
                                                            <button class='btn btn-success generate-new'
                                                                    data-title="{{$dog->dog->name}}"
                                                                    data-id="{{$dog->id}}" data-dog="{{$dog->dog->id}}" > Generate <br>new </button>
                                                        @else

                                                            <i>new number generated</i>

                                                        @endif
                                                    </td>

                                                </tr>
                                            @endforeach
                                        </table>

                                        <div class="text-center">
                                            <ul class="pagination-custom list-unstyled list-inline">

                                                <?php
                                                if(isset($paginator)){ ?>
                                                {!! $paginator->render() !!}
                                                {!! $paginator->appends(
                                                ['dogs' => Request::get('dogs'),
                                                  'q'=>Request::get('q')
                                                                        ])->render()
                                                !!}

                                                <?php   }else {?>
                                                {!! $dogs->render() !!}
                                                <?php } ?>

                                                {{--                    <li><a href="#" class="btn btn-sm btn-default">&laquo;</a></li>--}}
                                                {{--                    <li><a href="#" class="btn btn-sm btn-success">1</a></li>--}}
                                                {{--                    <li><a href="#" class="btn btn-sm btn-default">2</a></li>--}}
                                                {{--                    <li><a href="#" class="btn btn-sm btn-default">3</a></li>--}}
                                                {{--                    <li><a href="#" class="btn btn-sm btn-default">4</a></li>--}}
                                                {{--                    <li><a href="#" class="btn btn-sm btn-default">5</a></li>--}}
                                                {{--                    <li><a href="#" class="btn btn-sm btn-default">&raquo;</a></li>--}}
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->

@endsection
