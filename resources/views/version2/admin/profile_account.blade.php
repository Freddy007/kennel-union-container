@extends('version2.layouts.admin_layout')

@section('styles')
    <link href="{{asset('kug_version2/assets/pages/css/profile.min.css')}}" rel="stylesheet" type="text/css" />

    @endsection

    @section('content')
            <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <div class="container">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                        <h1>User Profile
                            <small></small>
                        </h1>
                    </div>
                    <!-- END PAGE TITLE -->

                </div>
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{url('/version2/')}}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="#">User profile</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Account</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN PROFILE SIDEBAR -->
                                <div class="profile-sidebar">
                                    <!-- PORTLET MAIN -->
                                    <div class="portlet light profile-sidebar-portlet ">
                                        <!-- SIDEBAR USERPIC -->
                                        <div class="profile-userpic">
                                            <img src="http://placehold.it/100x100"
                                                 class="img-responsive" alt="">
                                            {{--<img src="{{asset('kug_version2/assets/pages/media/profile/profile_user.jpg')}}" class="img-responsive" alt=""> --}}
                                        </div>
                                        <!-- END SIDEBAR USERPIC -->
                                        <!-- SIDEBAR USER TITLE -->
                                        <div class="profile-usertitle">
                                            <div class="profile-usertitle-name"> {{Auth::user()->first_name.' '.Auth::user()->last_name}} </div>
                                            <div class="profile-usertitle-job">
                                                @if(Auth::user()->isAdmin())
                                                    Administrator
                                                    @else
                                                    Member
                                                @endif
                                            </div>
                                        </div>
                                        <!-- END SIDEBAR USER TITLE -->
                                        <!-- SIDEBAR BUTTONS -->
                                        {{--<div class="profile-userbuttons">--}}
                                        {{--<button type="button" class="btn btn-circle green btn-sm">Follow</button>--}}
                                        {{--<button type="button" class="btn btn-circle red btn-sm">Message</button>--}}
                                        {{--</div>--}}
                                        <!-- END SIDEBAR BUTTONS -->
                                        <!-- SIDEBAR MENU -->
                                        <div class="profile-usermenu">
                                            <ul class="nav">
{{--                                                <li>--}}
{{--                                                    <a href="{{url('/version2/profile')}}">--}}
{{--                                                        <i class="icon-home"></i> Overview </a>--}}
{{--                                                </li>--}}
                                                <li class="active">
                                                    <a href="{{url('/version2/profile')}}">
                                                        <i class="icon-settings">

                                                        </i> Account Settings </a>
                                                </li>

                                                {{--<li>--}}
                                                    {{--<a href="page_user_profile_1_help.html">--}}
                                                        {{--<i class="icon-info"></i> Help </a>--}}
                                                {{--</li>--}}

                                            </ul>
                                        </div>
                                        <!-- END MENU -->
                                    </div>
                                    <!-- END PORTLET MAIN -->

                                </div>
                                <!-- END BEGIN PROFILE SIDEBAR -->

                                <!-- BEGIN PROFILE CONTENT -->
                                <div class="profile-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @include('flash::message')

                                            <div class="portlet light ">
                                                <div class="portlet-title tabbable-line">
                                                    <div class="caption caption-md">
                                                        <i class="icon-globe theme-font hide"></i>
                                                        <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                                    </div>
                                                    <ul class="nav nav-tabs">
                                                        <li class="active">
                                                            <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                                        </li>
                                                        {{--<li>--}}
                                                            {{--<a href="#tab_1_2" data-toggle="tab">Change Avatar</a>--}}
                                                        {{--</li>--}}
                                                        <li>
                                                            <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab_1_4" data-toggle="tab">Privacy Settings</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="tab-content">
                                                        <!-- PERSONAL INFO TAB -->
                                                        <div class="tab-pane active" id="tab_1_1">
                                                            <form role="form" action="{{url('version2/update-profile-account')}}" method="post">
                                                                {!! csrf_field() !!}
                                                                <div class="form-group">
                                                                    <label class="control-label">First Name</label>
                                                                    <input type="text" placeholder="John" class="form-control" name="first_name" value="{{$user_builder ? $user_builder->first_name: ""}}" required /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Last Name</label>
                                                                    <input type="text" placeholder="Doe" class="form-control" name="last_name" value="{{$user_builder ? $user_builder->last_name: ""}}" required /> </div>

                                                                <div class="form-group">
                                                                    <label class="control-label">Kennel Name</label>
                                                                    <input type="text" placeholder="Doe" class="form-control" name="kennel_name" value="{{$user_builder ? $user_builder->kennel_name: ""}}" required /> </div>

                                                                <div class="form-group">
                                                                    <label class="control-label">Mobile Number</label>
                                                                    <input type="text" placeholder="0240 000 000 " class="form-control" name="phone" value="{{$user_builder ? $user_builder->phone: ""}}" required /> </div>

                                                                {{--<div class="form-group">--}}
                                                                    {{--<label class="control-label">Email</label>--}}
                                                                    {{--<input type="text" placeholder="johndoe@email.com " class="form-control" value="{{$user_builder ? $user_builder->email: ""}}" /> --}}
                                                                {{--</div>--}}

                                                                {{--<div class="form-group">--}}
                                                                    {{--<label class="control-label">Interests</label>--}}
                                                                    {{--<input type="text" placeholder="Design, Web etc." class="form-control" /> </div>--}}
                                                                {{--<div class="form-group">--}}
                                                                    {{--<label class="control-label">Occupation</label>--}}
                                                                    {{--<input type="text" placeholder="Web Developer" class="form-control" /> </div>--}}
                                                                {{--<div class="form-group">--}}
                                                                    {{--<label class="control-label">About</label>--}}
                                                                    {{--<textarea class="form-control" rows="3" placeholder="We are KeenThemes!!!"></textarea>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="form-group">--}}
                                                                    {{--<label class="control-label">Website Url</label>--}}
                                                                    {{--<input type="text" placeholder="http://www.mywebsite.com" class="form-control" /> --}}
                                                                {{--</div>--}}
                                                                <div class="margiv-top-10">
                                                                    {{--<a href="javascript:;" class="btn green"> Save Changes </a>--}}
                                                                    <button type="submit" class="btn green"> Save Changes </button>
                                                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- END PERSONAL INFO TAB -->
                                                        <!-- CHANGE AVATAR TAB -->
                                                        {{--<div class="tab-pane" id="tab_1_2">--}}
                                                            {{--<p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt--}}
                                                                {{--laborum eiusmod. </p>--}}
                                                            {{--<form action="#" role="form">--}}
                                                                {{--<div class="form-group">--}}
                                                                    {{--<div class="fileinput fileinput-new" data-provides="fileinput">--}}
                                                                        {{--<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">--}}
                                                                            {{--<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>--}}
                                                                        {{--<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>--}}
                                                                        {{--<div>--}}
                                                                                {{--<span class="btn default btn-file">--}}
                                                                                    {{--<span class="fileinput-new"> Select image </span>--}}
                                                                                    {{--<span class="fileinput-exists"> Change </span>--}}
                                                                                    {{--<input type="file" name="..."> </span>--}}
                                                                            {{--<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                    {{--<div class="clearfix margin-top-10">--}}
                                                                        {{--<span class="label label-danger">NOTE! </span>--}}
                                                                        {{--<span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="margin-top-10">--}}
                                                                    {{--<a href="javascript:;" class="btn green"> Submit </a>--}}
                                                                    {{--<a href="javascript:;" class="btn default"> Cancel </a>--}}
                                                                {{--</div>--}}
                                                            {{--</form>--}}
                                                        {{--</div>--}}
                                                        <!-- END CHANGE AVATAR TAB -->
                                                        <!-- CHANGE PASSWORD TAB -->
                                                        <div class="tab-pane" id="tab_1_3">
                                                            <form action="{{url('version2/update-password')}}" method="post">
                                                                {!! csrf_field() !!}
                                                                <div class="form-group">
                                                                    <label class="control-label">Current Password</label>
                                                                    <input type="password" name="password" class="form-control" required /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">New Password</label>
                                                                    <input type="password" name="new_password" class="form-control" required /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Re-type New Password</label>
                                                                    <input type="password" name="confirm_password" class="form-control" required /> </div>
                                                                <div class="margin-top-10">
                                                                    {{--<a href="javascript:;" class="btn green"> Change Password </a>--}}
                                                                    <button type="submit" class="btn green"> Change Password </button>
                                                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- END CHANGE PASSWORD TAB -->
                                                        <!-- PRIVACY SETTINGS TAB -->
                                                        <div class="tab-pane" id="tab_1_4">
                                                            <form action="#">
                                                                <table class="table table-light table-hover">
                                                                    <tr>
                                                                        <td> Receive email notifications  </td>
                                                                        <td>
                                                                            <label class="uniform-inline">
                                                                                <input type="radio" name="optionsRadios1" value="option1" /> Yes </label>
                                                                            <label class="uniform-inline">
                                                                                <input type="radio" name="optionsRadios1" value="option2" checked/> No </label>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td> Receive sms notifications  </td>
                                                                        <td>
                                                                            <label class="uniform-inline">
                                                                                <input type="radio" name="optionsRadios2" value="option1" /> Yes </label>
                                                                            <label class="uniform-inline">
                                                                                <input type="radio" name="optionsRadios2" value="option2" checked/> No </label>
                                                                        </td>
                                                                    </tr>

                                                                </table>
                                                                <!--end profile-settings-->
                                                                <div class="margin-top-10">
                                                                    <a href="javascript:;" class="btn red"> Save Changes </a>
                                                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- END PRIVACY SETTINGS TAB -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PROFILE CONTENT -->


                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->
@endsection
