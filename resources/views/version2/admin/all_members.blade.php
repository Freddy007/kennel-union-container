@extends('version2.layouts.admin_layout')

@section('styles')

    <link href="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/searchpanes/1.2.0/css/searchPanes.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css" rel="stylesheet" type="text/css" />

    <style>
        tfoot input {
            width: 100%;
            padding: 3px;
            box-sizing: border-box;
        }
    </style>
@endsection

@section('scripts')
    {{--<script src="{{asset('datatables/jquery.dataTables.min.js')}}"></script>--}}

    <script src="{{asset('kug_version2/assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>

    <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('kug_version2/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>

{{--<script src=" https://cdn.datatables.net/searchpanes/1.2.0/js/dataTables.searchPanes.min.js" type="text/javascript"></script>--}}
{{--<script src=" https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js" type="text/javascript"></script>--}}
<script src="//code.highcharts.com/highcharts.js" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function(){

            $('#datatable-buttons tfoot th').each( function () {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            });

            let query = "{{$query}}";

            let table = $('#datatable-buttons').DataTable({
                pageLength: 25,
                processing: true,
                serverSide: true,
                ajax: 'members-data'+query,
                order: [[ 3, "desc" ]],
                columns: [

                    { data: "member_id",name:'member_id'},
                    { data: "name",name:'name'},
                    { data: "kennel_name",name:'kennel_name' },
                    { data: "phone",name:'phone' },
                    { data: "created_at", name:'created_at'},
                    {data : 'dogs', name: 'dogs'},
                    {data : "role", name:'role'},
                    {data : "status", name:'status'},
                    {data : 'action', name : 'action'}
                ],
                "initComplete": function( settings, json ) {
                    registerDeleteEvent();
                    this.api().columns().every( function () {
                        var that = this;

                        $( 'input', this.footer() ).on( 'keyup change clear', function () {
                            if ( that.search() !== this.value ) {
                                that.search( this.value ).draw();
                            }
                        } );
                    } );
                },
            });

            // Create the chart with initial data
            var container = $('<div/>').insertBefore(table.table().container());

            var chart = Highcharts.chart(container[0], {
                chart: {
                    type: 'pie',
                },
                title: {
                    text: 'Dogs per member',
                },
                series: [
                    {
                        data: chartData(table),
                    },
                ],
            });

            function chartData(table) {
                var counts = {};

                // Count the number of entries for each member
                table.column(1, {search:'applied', order:'applied'})
                    .data()
                    .each(function (val,key) {
                            let dogsCountStringPos = val.indexOf("-dogs-");
                            let subPos = val.substr(dogsCountStringPos+6);
                               counts[val] = subPos.length > 13 ? parseInt(val.substr(dogsCountStringPos+6,dogsCountStringPos+7))
                                   : counts[val] = parseInt(val.charAt(dogsCountStringPos+6));
                    });

                // And map it to the format highcharts uses
                return $.map(counts, function (val, key) {
                    return {
                        name: key,
                        y: val,
                    };
                });
            }

            registerDeleteEvent();

            let url = "{{ url("/version2/delete-member") }}";

            function registerDeleteEvent(){
                $.get('members-data',function(xhr){
                    $.each(xhr.data, function(index, element) {
                        $('#delete-member-'+element.id).off('click').on('click',function(){
                            $('#delete-modal').modal();
                            $('.delete-name').text(element.name);
                            $('#delete-member-form').attr('action',url+ '/'+element.id);
                        });
                    });
                })
            }

            table.on( 'draw.dt', function () {
                console.log( 'Redraw occurred at: '+new Date().getTime() );
                registerDeleteEvent();

                $(".confirm").on("click",function () {
                    // alert($(this).data("confirmed"))
                    var confirmed = $(this).data("confirmed");
                    if (confirmed == 1){
                        $("#action-container").html(
                            `  <select class="form-control" name="status">
                                   <option value="{{'0'}}">Revoke</option>
                                </select>`
                        )
                    }else{
                        $("#action-container").html(
                            `  <select class="form-control" name="status">
                                    <option value="{{'1'}}">Confirm</option>

                                </select>`
                        )
                    }
                    var name = $(this).data("title");
                    var id = $(this).data("id");
                    $('#confirm-modal').modal();
                    $('.confirm-name').text(name);
                    $('#confirm-member-form').attr('action','/version2/confirm-member/'+id);
                })

                chart.series[0].setData(chartData(table));
            });
        });

    </script>

@endsection

@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>All Members
                        {{--<small>managed datatable samples</small>--}}
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->
                <div class="page-toolbar">

                </div>
                <!-- END PAGE TOOLBAR -->
            </div>
        </div>
        <!-- END PAGE HEAD-->

                <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{url('/version2')}}">Dashboard</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>All Members</span>
                    </li>
                </ul>

                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">

                    <div class="row">
                        <div class="col-md-12">
{{--                            @include('flash::message')--}}

                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        {{--<i class="icon-settings font-dark"></i>--}}
                                        <span class="caption-subject bold uppercase"> Members</span>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
                                                <input type="radio" name="options" class="toggle" id="option1"></label>
                                            <label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
                                                <input type="radio" name="options" class="toggle" id="option2"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group">
{{--                                                    <button id="sample_editable_1_new" class="btn sbold green"> Add New--}}
{{--                                                        <i class="fa fa-plus"></i>--}}
{{--                                                    </button>--}}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="btn-group pull-right">
                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Query by
                                                        <i class="fa fa-angle-down"></i>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right">
                                                        <li>
                                                            <a href="{{url('/version2/all-members?members=confirmed')}}">
                                                                {{--<i class="fa fa-print"></i> --}}
                                                                Confirmed only </a>
                                                        </li>

                                                        <li>
                                                            <a href="{{url('/version2/all-members?members=unconfirmed')}}">

                                                            {{--<i class="fa fa-print"></i> --}}
                                                                Unconfirmed only </a>
                                                        </li>
                                                        <li>
                                                            <a href="{{url('/version2/all-members?members=administrator')}}">

                                                                {{--<i class="fa fa-file-pdf-o"></i>--}}
                                                                Administrators only  </a>
                                                        </li>

                                                        <li>
                                                            <a href="{{url('/version2/all-members?members=member')}}">
                                                                {{--<i class="fa fa-file-pdf-o"></i>--}}
                                                                Members only </a>
                                                        </li>
                                                        {{--<li>--}}
                                                            {{--<a href="javascript:;">--}}
                                                                {{--<i class="fa fa-file-excel-o"></i>  </a>--}}
                                                        {{--</li>--}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover order-column" id="datatable-buttons">
                                        <thead>
                                        <tr>
                                            <th>Member Id.</th>
                                            <th> Name</th>
                                            {{--<th>Last Name</th>--}}
                                            <th>Kennel Name</th>
                                            {{--<th>Email</th>--}}
                                            <th>Phone</th>
                                            <th>Registered Date</th>
                                            <th>Dog(s)</th>
                                            <th>Role</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>

                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

    <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="delete-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
{{--                    <h4 class="modal-title" id="myModalLabel"><strong>Delete  <span class="delete-name"></span> </strong></h4>--}}
                    <h4 class="modal-title" id="myModalLabel"><strong>Delete </strong></h4>
                </div>
                <div class="modal-body">
                    <form method="post" id="delete-member-form" >
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-2">
                                <label>
{{--                                    This Action will delete <em><span class="delete-name"></span></em> and dogs submitted .--}}
                                    {{--<br><br>--}}
                                    Are you sure you want to continue with this action ?
                                </label>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
{{--                            <button type="submit" class="btn btn-success">Delete <span class="delete-name"></span></button>--}}
                            <button type="submit" class="btn btn-success">Delete</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong><span class="confirm-name"></span> </strong></h4>
                </div>
                <div class="modal-body">
                    <form id="confirm-member-form" method="post">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-2">

                                <label>Confirm Action?</label>
                            </div>
                            <div class="col-sm-4" id="action-container">

                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Change</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
