@extends('version2.layouts.admin_layout')

@section('styles')
    <link href="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{"/css/all_dogs.css"}}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('kug_version2/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/0.11.1/typeahead.bundle.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>


    <script>

        Handlebars.registerHelper('notEquals', function(arg1, options) {
            return (arg1 !== "") ? options.fn(this) : options.inverse(this);
        });

        let substringMatcher = function(strings) {
            return function findMatches(q, cb) {
                var matches, substringRegex;

                matches = [];
                substrRegex = new RegExp(q, 'i');

                $.each(strings, function(i, string) {
                    if (substrRegex.test(string)) {
                        matches.push(string);
                    }
                });
                cb(matches);
            };
        };

        // Suggestion engine
        const dogs = new Bloodhound({
            datumTokenizer: datum => Bloodhound.tokenizers.whitespace(datum.value),
            queryTokenizer: Bloodhound.tokenizers.whitespace,

            prefetch: "{{url('/type-ahead-dogs?q=%QUERY')}}",
            remote: {
                url: "{{url('/type-ahead-dogs?q=%QUERY')}}",
                wildcard: '%QUERY',
                filter: dogs => $.map(dogs, dog => ({
                    value: dog.name,
                    id: dog.id,
                    registration_number: dog.registration_number,
                    sex: dog.sex,
                    old: dog.duplicates.length > 0 ? dog.duplicates[0]['registration_number'] : ""
                }))
            }
        });

        dogs.initialize();
        // Instantiate the Typeahead UI
        $('#name').typeahead(null, {
            displayKey: 'value',
            display: 'registration_number',
            source: dogs.ttAdapter(),
            templates: {
                empty: [
                    '<div class="empty-message">',
                    'unable to find any dog that matches the current query',
                    '</div>'
                ].join('\n'),
                suggestion: Handlebars.compile(
                    '<div>' +
                    ' <div style="border-bottom: 1px solid #cccccc;"><strong>@{{value}}</strong> – @{{registration_number}} – @{{sex}}</div> ' +
                    '<div style="color: red"> ' +
                    '@{{#notEquals old}}\n' +
                    'Old number - @{{ old }}\n' +
                    '- duplicated registration number' +
                    '@{{/notEquals}}</div>' +
                    '</div>'
                )
            }
        });

        jQuery(document).ready(function() {

            $(".confirm").on("click", function () {
                var confirmed = $(this).data("confirmed");
                var name = $(this).data("title");
                var id = $(this).data("id");

                Swal.fire({
                    title: 'Are you sure?',
                    text: "This dog will be confirmed!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, confirm this dog!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        confirmDog(id);
                        location.reload();
                        // table.page(page).draw('page');
                        // table.draw();
                    }
                })
            });

            $(".delete-request").off().on("click", function () {
                let dog_id = $(this).data("id");
                // let page = table.page.info().page;

                Swal.fire({
                    title: 'Are you sure?',
                    text: "This dog will be moved to archives!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        archiveDog(dog_id);
                        location.reload();
                        // table.page(page).draw('page');
                    }

                })
            });

            $(".approve-certificate").off().on("click", function () {
                let dog_id = $(this).data("id");

                Swal.fire({
                    title: 'Are you sure?',
                    text: "This will approve certificate request!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Approve it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        approveCertificate(dog_id);
                        // table.page(page).draw('page');
                        location.reload();

                    }

                });
            });

            $(".download-certificate").off().on("click", function (event) {
                event.preventDefault();
                // const download_base_url = "http://18.194.254.77/certification/public";
                const download_base_url = "/pdf";

                let dog_id = $(this).data("id");
                enterNewOwner(dog_id);

                $(document).on('click', '.download', function() {
                    let name = $("#swal2-input").val();
                    Swal.fire('Downloading...', '','info')
                    // window.location.href = `${download_base_url}/${dog_id}`;

                    if (name) {
                        window.location.href = `${download_base_url}/${dog_id}?name=${name}`;
                    } else {
                        window.location.href = `${download_base_url}/${dog_id}`;
                    }

                    setTimeout(function(){
                        // location.reload();
                    },3000)
                    console.log('Button 1');
                });

                $(document).on('click', '.sendToMember', function() {
                    console.log('Button 2');
                    let name = $("#swal2-input").val();
                    approveCertificate(dog_id, name);
                    Swal.fire('Certificate will be sent to the member', '','info');
                });
            });

            function confirmDog(id) {
                $.post("/version2/confirm-dog/"+id, function (data) {
                    Swal.fire(
                        'Confirmed!',
                        'Dog is now confirmed!',
                        'success'
                    )

                });
            }

            function archiveDog(id) {
                $.post("/version2/archive-dog/" + id, function (data) {
                    Swal.fire('Archived!', 'Dog is now archived.', 'success')
                });
            }

            function approveCertificate(id,owner_name) {
                let url = name === ""? "/version2/approve-certificate-request/" + id : "/version2/approve-certificate-request/" + id + "?name="+owner_name;


                $.post(url, function (data) {
                    Swal.fire(
                        'Approved!',
                        'Download url has been generated and sent to member.',
                        'success'
                    )
                });
            }

            async function enterNewOwner(dog_id){
                // const download_base_url = "http://18.194.254.77/certification/public";
                const download_base_url = "/pdf";

                const { value: name } = await Swal.fire({
                    title: "Enter new owner's name",
                    html:
                        '<input maxlength="100" autocapitalize="off" autocorrect="off" class="swal2-input" id="swal2-input" placeholder="Enter name(not required)" type="text" style="display: flex;">' +
                        '<button type="button" role="button" tabindex="0" class="SwalBtn1 download customSwalBtn">' + 'Download' + '</button>' +
                        '<button type="button" role="button" tabindex="0" class="SwalBtn2 sendToMember customSwalBtn">' + 'Send to member' + '</button>',
                    showCancelButton: false,
                    showConfirmButton: false
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    let owner_name = result.value;
                    console.log(result);
                });
            }

        });

    </script>

@endsection


@section('content')
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <div class="container">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                        <h1>All Dogs
                            {{--<small>managed datatable samples</small>--}}
                        </h1>
                    </div>
                    <!-- END PAGE TITLE -->

                </div>
            </div>
            <!-- END PAGE HEAD-->


            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">

                <div id="overlay"><h2>Loading .. Please wait</h2></div>

                <div class="container-fluid">
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{url('/version2')}}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        {{--<li>--}}
                        {{--<a href="#">More</a>--}}
                        {{--<i class="fa fa-circle"></i>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<a href="#">Tables</a>--}}
                        {{--<i class="fa fa-circle"></i>--}}
                        {{--</li>--}}
                        <li>
                            <span>All Dogs</span>
                        </li>
                    </ul>

                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        <div class="row">
                            <div class="col-md-12">
{{--                                @include('flash::message')--}}

                                @include('flash-message')




                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            {{--                                            <i class="icon-settings font-dark"></i>--}}
                                            <span class="caption-subject bold uppercase"> {{\App\Dog::count()}} dogs registered</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="btn-group">
                                                        <a href="{{url('version2/register-new-dog')}}" id="sample_editable_1_new" class="btn sbold green"> Add Dog
                                                            <i class="fa fa-plus"></i>
                                                        </a>


                                                        <a href="{{url('version2/register-litter')}}" id="sample_editable_1_new" class="btn sbold yellow"> Add Litter
                                                            <i class="fa fa-plus"></i>
                                                        </a>


                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Query by
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
<!--                                                        <ul class="dropdown-menu pull-right">-->
<!--                                                            <li>-->
<!--                                                                <a href="{{url('/version2/all-dogs?dogs=confirmed')}}">-->
<!--                                                                    Confirmed only </a>-->
<!--                                                            </li>-->
<!---->
<!--                                                            <li>-->
<!--                                                                <a href="{{url('/version2/all-dogs?dogs=unconfirmed')}}">-->
<!--                                                                    Unconfirmed only </a>-->
<!--                                                            </li>-->
<!---->
<!--                                                            <li>-->
<!--                                                                <a href="{{url('/version2/all-dogs?dogs=male')}}">-->
<!--                                                                    Males only </a>-->
<!--                                                            </li>-->
<!---->
<!--                                                            <li>-->
<!--                                                                <a href="{{url('/version2/all-dogs?dogs=female')}}">-->
<!--                                                                    Females only </a>-->
<!--                                                            </li>-->
<!---->
<!--                                                            {{--                                                            <li>--}}-->
<!--                                                            {{--                                                            <a href="javascript:;">--}}-->
<!--                                                            {{--                                                            <i class="fa fa-file-excel-o"></i>  </a>--}}-->
<!--                                                            {{--                                                            </li>--}}-->
<!--                                                        </ul>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row margin-bottom-10">
                                            <form>
                                                <div class="col-md-3">

                                                </div>
                                                <div class="col-md-6 margin-top-10">
                                                    <input type="text" class="form-control typeahead" id="name" placeholder="Search dogs by name or registration number here..." name="q" value="{{Request::query("q")}}">

                                                    <button class="btn btn-success margin-bottom-5" type="submit">Search</button>
                                                </div>

                                            </form>
                                        </div>


                                        <table class="table table-striped table-bordered table-hover order-column">
                                            <thead>
                                            <tr>
{{--                                                <th>Image</th>--}}
                                                <th>Date</th>
                                                <th>Name</th>
{{--                                                <th>Siblings</th>--}}
{{--                                                <th>Offspring</th>--}}
                                                <th>Date of birth</th>
                                                <th>Breed </th>
                                                <th>Sex </th>
                                                <th>Registration number </th>
                                                <th>Breeder</th>
                                                <th>Status</th>
                                                <th>Member requests</th>
{{--                                                <th>Certificate</th>--}}
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            @foreach($dogsData as $dog )
                                                <tr style="{{$dog->needs_generation ? 'background-color:#FFA500' : ""}}">

                                                    <td>

                                                        {{$dog->created_at}}

{{--                                                        <img width="70" height="70"--}}
{{--                                                             src={{$dog->image_name == null ? url('https://placehold.it/300x200/cccccc/000000?text=no+image+uploaded'):"/images/catalog/$dog->image_name"}} alt="dog pic"/>--}}

                                                    </td>
                                                    <td class="dog-name">{{ strtolower($dog->name) }}</td>
{{--                                                    <td>--}}
{{--                                                        @php--}}
{{--                                                            $relationship = \App\DogRelationship::whereDogId($dog->id)->first();--}}
{{--                                                            $father = $relationship->father;--}}
{{--                                                            $mother = $relationship->mother;--}}
{{--                                                            $siblings_count = \App\DogRelationship::where("dog_id","<>",$dog->id)--}}
{{--                                                            ->where("mother","<>","")->where("father","<>","")--}}
{{--                                                            ->whereMother($mother)->whereFather($father)->count();--}}
{{--                                                        @endphp--}}

{{--                                                        {{ $siblings_count }}--}}
{{--                                                    </td>--}}

{{--                                                    <td>--}}
{{--                                                        @php--}}
{{--                                                            $count = \App\DogRelationship::orWhere("mother",$dog->registration_number)--}}
{{--                                                            ->orWhere("father",$dog->registration_number)->count();--}}
{{--                                                        @endphp--}}

{{--                                                        {{ $count }}--}}
{{--                                                    </td>--}}

                                                    <td>{{$dog->dob}}</td>
                                                    <td>
                                                        {{strtolower(strtoupper($dog->breed))}}
                                                    </td>
                                                    <td>{{ $dog->sex }}</td>
                                                    <td>{{ $dog->registration_number }}</td>
                                                    <td>{{isset($dog->user['first_name'])?$dog->user['first_name'] : ""}}
                                                        {{isset($dog->user['last_name']) ? $dog->user['last_name'] : ""}}
                                                        @if(isset($dog->user['kennel_name']))
                                                        ( {{ isset($dog->user['kennel_name']) ?$dog->user['kennel_name'] : "" }} )
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(!$dog->needs_generation)
                                                        @php
                                                            $confirmed = $dog->confirmed
                                                        @endphp
                                                        @if(!$confirmed)
                                                            <button class='btn btn-success confirm' data-title="{{$dog->name}}"
                                                                    data-confirmed="{{$dog->confirmed ? true : false}}"
                                                                    data-id="{{$dog->id}}" id="{{$dog->id}}"> confirm </button>
                                                        @else

                                                            <i>confirmed</i>

                                                        @endif

                                                        @else
                                                            <br>
                                                            <small style="color: white;"><i>Regenerating...</i></small>

                                                        @endif

                                                    </td>

                                                    <td>

                                                        @php
                                                            $confirmed = $dog->delete_request;
                                                            $certificate_request = \App\RequestedCertificate::whereDogId($dog->id)->first();

                                                        @endphp

                                                        @if($confirmed)
                                                            <button class='btn btn-danger delete-request' data-title="{{$dog->name}}" data-delete-request="{{$dog->delete_request ? true : false}}"
                                                                    data-id="{{$dog->id}}" id="{{$dog->id}}"> Approve delete </button>
                                                        @endif

                                                        <br>
                                                        <br>

                                                        @if($certificate_request && !$certificate_request->honoured)
                                                            <button class='btn btn-success approve-certificate' data-title="{{$dog->name}}"
                                                                    data-id="{{$dog->id}}" id="{{$dog->id}}"> Approve certificate </button>
                                                        @endif

                                                        @if(!$confirmed && !$certificate_request)
                                                            No requests
                                                        @endif

                                                        @if(!$confirmed && $certificate_request && $certificate_request->honoured)
                                                            No requests
                                                        @endif

                                                    </td>

{{--                                                    <td>--}}
{{--                                                        @if(!$dog->needs_generation)--}}
{{--                                                            <span><a class='btn btn-success download-certificate btn-xs add-owner-detail' data-id="{{$dog->id}}"> certificate</a></span>--}}
{{--                                                        @else--}}
{{--                                                            <br>--}}
{{--                                                            <small style="color: white;"><i>Regenerating...</i></small>--}}
{{--                                                        @endif--}}
{{--                                                    </td>--}}

                                                    <td>
                                                        @if(!$dog->needs_generation)
                                                            <span>
                                                                <a href="{{url("/pdf/$dog->id")}}" class='btn btn-default btn-xs'><i class='fa fa-download'></i></a></span>

                                                            <span><a href="dog/{{$dog->id}}" class='btn btn-default btn-xs'><i class='fa fa-eye'></i></a></span>

                                                        <span><a href="edit-dog/{{$dog->id}}" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o"></i> </a></span>

                                                        <span><button class="btn btn-danger btn-xs delete-request"  data-id="{{$dog->id}}"  data-toggle="tooltip" data-placement="left" title="move {{$dog->name}} to trash"><i class="fa fa-trash"></i></button></span>

                                                        @else
                                                            <br>
                                                            <small style="color: white;"><i>Regenerating...</i></small>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>

                                        <div class="text-center">
                                            <ul class="pagination-custom list-unstyled list-inline">

                                                {!! $dogsData->render() !!}


                                            </ul>
                                        </div>

                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->

@endsection
