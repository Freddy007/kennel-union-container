<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8" />
    <title>Pedigree Database</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css" />
    <link href="{{asset('kug_version2/assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('kug_version2/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('kug_version2/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('kug_version2/assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('kug_version2/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <link href="{{asset('kug_version2/assets/pages/css/error.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet" type="text/css" />

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('kug_version2/assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('kug_version2/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{asset('kug_version2/assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{asset('kug_version2/assets/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{asset('kug_version2/assets/layouts/layout3/css/layout.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('kug_version2/assets/layouts/layout3/css/themes/default.min.css')}}" rel="stylesheet" type="text/css" id="style_color" />
    <link href="{{asset('kug_version2/assets/layouts/layout3/css/custom.min.css')}}" rel="stylesheet" type="text/css" />
{{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}

    <style>
        .page-header .page-header-menu {
            background: #444d58;
            margin-top: 20px !important;
        }

        .page-header .page-header-top .page-logo {
            margin-top: 6px !important;
        }

         .tt-menu,
         .gist {
             text-align: left;
         }

        html {
            font: normal normal normal 18px/1.2 "Helvetica Neue", Roboto, "Segoe UI", Calibri, sans-serif;
            color: #292f33;
        }

        a {
            color: #03739c;
            text-decoration: none;
        }

        a:hover {
            text-decoration: underline;
        }

        .table-of-contents li {
            display: inline-block;
            *display: inline;
            zoom: 1;
        }

        .table-of-contents li a {
            font-size: 16px;
            color: #999;
        }

        p + p {
            margin: 30px 0 0 0;
        }

        .title {
            margin: 20px 0 0 0;
            font-size: 64px;
        }

        .example {
            padding: 30px 0;
        }

        .example-name {
            margin: 20px 0;
            font-size: 32px;
        }

        .demo {
            position: relative;
            *z-index: 1;
            margin: 50px 0;
        }

        .typeahead,
        .tt-query,
        .tt-hint {
            width: 300px;
            height: 40px;
            padding: 8px 12px;
            font-size: 17px;
            line-height: 20px;
            border: 2px solid #ccc;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border-radius: 6px;
            outline: none;
        }


        .typeahead {
            background-color: #fff;
        }

        .typeahead:focus {
            border: 2px solid #0097cf;
        }

        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
            color: #999
        }

        .tt-menu {
            width: 422px;
            margin: 12px 0;
            padding: 8px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
            -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
            box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            font-size: 18px;
            line-height: 24px;
        }

        .tt-suggestion:hover {
            cursor: pointer;
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion.tt-cursor {
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion p {
            margin: 0;
        }

        .gist {
            font-size: 14px;
        }



    </style>

<!-- END HEAD -->

@yield('styles')

<body class="page-container-bg-solid page-boxed">
<!-- BEGIN HEADER -->
<div class="page-header">
    <!-- BEGIN HEADER TOP -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="{{url('/')}}" style="margin-bottom: 15px">
                    <img src="{{asset('img/logo_changed.png')}}" alt="logo" style="margin-bottom: 15px; padding-bottom: 20px">
                </a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    @if(Auth::check() && Auth::user()->administrator == 1)
                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                    <li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-bell"></i>
                            <span class="badge badge-default">
                                {{$total_notif = \App\Dog::checkUnconfirmedDogs() + \App\User::checkUnconfirmedMembers() + \App\RequestedCertificate::getRequestedCertificates()}}

                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3>You have
                                    <strong>{{$total_notif}} pending</strong> tasks</h3>
                                <a href="">view all</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">

                                    @if(\App\User::checkUnconfirmedMembers() == 0)

                                    @else

                                        <li>
                                            <a href="{{url('/version2/all-members?members=unconfirmed')}}">
                                                <span class="time">
{{--                                                    {{ getMemberRegisteredDay(\App\User::getLastMemberTime()) }}--}}
                                                </span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-success">
                                                            <i class="fa fa-user"></i>
                                                        </span>
                                                        {{\App\User::checkUnconfirmedMembers()}} new member(s).
                                                    </span>
                                            </a>
                                        </li>

                                    @endif

                                        @if(\App\Dog::checkUnconfirmedDogs() == 0)

                                        @else

                                            <li>
                                                <a href="{{url('/version2/all-dogs?dogs=unconfirmed')}}">
                                                    <span class="time">
{{--                                                    {{ getDogRegisteredDay(\App\User::getLastMemberTime()) }}--}}
                                                    </span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-danger">
                                                            <i class="fa fa-paw"></i>
                                                        </span> {{\App\Dog::checkUnconfirmedDogs()}} New Dog(s)</span>
                                                </a>
                                            </li>
                                        @endif

                                    <li>
                                        <a href="{{url('/version2/certificate-requests')}}">
                                            {{--<span class="time">3 mins</span>--}}
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-default">
                                                            <i class="fa fa-certificate"></i>
                                                        </span>
                                                        {{\App\RequestedCertificate::getRequestedCertificates()}} New Certificate requests
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            {{--</li>--}}
                        </ul>
                    </li>
                    <!-- END NOTIFICATION DROPDOWN -->

                    <li class="droddown dropdown-separator">
                        <span class="separator"></span>
                    </li>
                    @endif
                    <!-- BEGIN INBOX DROPDOWN -->
{{--                    <li class="dropdown dropdown-extended dropdown-inbox dropdown-dark" id="header_inbox_bar">--}}
{{--                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">--}}
{{--                            <span class="circle">3</span>--}}
{{--                            <span class="corner"></span>--}}
{{--                        </a>--}}
{{--                        <ul class="dropdown-menu">--}}
{{--                            <li class="external">--}}
{{--                                <h3>--}}
{{--                                    --}}{{--You have--}}
{{--                                    --}}{{--<strong>7 New</strong> Messages</h3>--}}
{{--                                    --}}{{--<strong>New</strong> --}}
{{--                                    Notitifcations feature</h3>--}}
{{--                                <a href="#">view all</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">--}}
{{--                                    <li>--}}
{{--                                        <a href="#">--}}
{{--                                                    <span class="photo">--}}
{{--                                                        <img src="{{asset('kug_version2/assets/layouts/layout3/img/avatar2.jpg" class="img-circle" alt=""')}}"> --}}
{{--</span>--}}
{{--                                                    <span class="subject">--}}
{{--                                                        <span class="from"> Notification </span>--}}
{{--                                                        <span class="time">Just Now </span>--}}
{{--                                                    </span>--}}
{{--                                            <span class="message"> Feature under construction </span>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
                    <!-- END INBOX DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" class="img-circle" src="{{asset('kug_version2/assets/layouts/layout3/img/avatar9.jpg"')}}">
                            <span class="username username-hide-mobile"> {{Auth::user()->first_name}}  {{Auth::user()->last_name}}</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">

                            @if(Auth::check() && Auth::user()->administrator == 1)
                            <li>
                                <a href="{{url('version2/profile')}}">
                                    <i class="icon-user"></i> My Profile </a>
                            </li>

                                <li class="divider"> </li>


                            @endif
                            <li>
                                @if(Auth::user()->isAdmin())
                                <a href="{{url('version2/user-dogs',Auth::id())}}">
                                    <i class="fa fa-paw"></i> My dogs </a>
                                    @else
                                    <a href="{{url('member/user-dogs',Auth::id())}}">
                                        <i class="fa fa-paw"></i> My dogs </a>
                                @endif
                            </li>

                            <li class="divider"> </li>
                            <li>
                                <a class="dropdown-item " href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off"></i> {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>

                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER TOP -->
    <!-- BEGIN HEADER MENU -->
    <div class="page-header-menu">
        <div class="container">
            <!-- BEGIN HEADER SEARCH BOX -->
            {{--<form class="search-form" action="" method="GET">--}}
                {{--<div class="input-group">--}}
                    {{--<input type="text" class="form-control" placeholder="Search" name="query">--}}
                            {{--<span class="input-group-btn">--}}
                                {{--<a href="javascript:;" class="btn submit">--}}
                                    {{--<i class="icon-magnifier"></i>--}}
                                {{--</a>--}}
                            {{--</span>--}}
                {{--</div>--}}
            {{--</form>--}}
            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN MEGA MENU -->
            <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
            <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
            <div class="hor-menu  ">

                @if(Auth::check() && Auth::user()->administrator == true)
                    {!! $AdminNav->asUl(array('class' => 'nav navbar-nav')) !!}

                @else

                    {!! $MemberNav->asUl(array('class' => 'nav navbar-nav')) !!}


                @endif


            </div>
            <!-- END MEGA MENU -->
        </div>
    </div>
    <!-- END HEADER MENU -->
</div>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->

@yield('content')

        <!-- END PAGE CONTENT BODY -->

</div>

<div class="page-footer">
    <div class="container"> {{date('Y')}} &copy; Kennel Union of Ghana.</div>
</div>
<div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>
<!-- END INNER FOOTER -->
<!-- END FOOTER -->
<!--[if lt IE 9]>

<script src="{{asset('kug_version2/assets/global/plugins/respond.min.js')}}"></script>
<script src="{{asset('kug_version2/assets/global/plugins/excanvas.min.js')}}"></script>
<![endif]-->

<script src="{{asset('kug_version2/assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>

<script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/0.11.1/typeahead.bundle.min.js" type="text/javascript"></script>

<script src="{{asset('js/app.js')}}"></script>

<!-- BEGIN CORE PLUGINS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>

<script src="{{asset('kug_version2/assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/pages/scripts/form-wizard.min.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
{{--<script src="{{asset('kug_version2/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('js/vue.js')}}" type="text/javascript"></script>--}}
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN THEME GLOBAL SCRIPTS -->
{{--<script src="{{asset('kug_version2/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>--}}
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{{asset('kug_version2/assets/layouts/layout3/scripts/layout.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/layouts/layout3/scripts/demo.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
<script>
    window.Laravel = <?php echo json_encode([
		'csrfToken' => csrf_token(),
	]); ?>
</script>



<!-- END THEME LAYOUT SCRIPTS -->

@yield('scripts')

<script>

</script>

</body>

</html>
