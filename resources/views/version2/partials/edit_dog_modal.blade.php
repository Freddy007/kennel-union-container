<div class="modal fade" id="edit-column-modal"  role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <form action="{{url('register-new-dog-on-table')}}" id="register-dog-form">
            {!! csrf_field() !!}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">Add dog</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label class="control-label col-md-4">Name
                                    <span class="required"> * </span>
                                </label>

                                <div class="col-md-8">
{{--                                    <input type="hidden" class="form-control hidden-name-input name hide"  name="name"    required/>--}}

                                    <br>

                                    <div id="select-input">
{{--                                        <select class="dog_parent form-control"  id="father" name="dog_parent" style="width: 100%">--}}
{{--                                            <option value=""></option>--}}
{{--                                        </select>--}}
                                        <input class="typeahead parent" type="text" name="name" placeholder="name of dog">
                                        <span class="help-block">  </span>
                                    </div>


                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4">Sex
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <select class="form-control" name="sex">
                                        <option name="male">male</option>
                                        <option name="female">female</option>
                                    </select>
                                    <span class="help-block">  </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4">Date of Birth
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="date" class="form-control" name="dob" id="dob" required />
                                    <span class="help-block">  </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4">Breed
                                    <span class="required"> * </span>
                                    {{--<span> * </span>--}}

                                </label>
                                <div class="col-md-8">
                                    {{--<select ng-model="selectedName" ng-options="item as item.name for item in breeds track by item.id" class="form-control" id="breed" name="breed">--}}
                                    <select  class="form-control" id="breed" name="breeder_id" required>
                                        <option></option>
                                        @foreach(\App\Breed::all() as $breed)
                                            <option value="{{$breed->id}}">{{$breed->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block">  </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4">Colour
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="colour" id="colour" required />
                                    <span class="help-block"> </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4">Coat
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <select class="form-control" name="coat" id="coat" required>
                                        <option>short</option>
                                        <option>long</option>
                                    </select>
                                    <span class="help-block"> </span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-4">Height (cm)
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="height"  />
                                    <span class="help-block"></span>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">



                            <div class="form-group">
                                <label class="control-label col-md-4">Original No.
                                    <span class="required">  </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="custom_registration_number" placeholder="Original number"/>
                                    <span class="help-block"> </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4">Titles
                                    <span class="required">  </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="titles" placeholder="Titles" />
                                    <span class="help-block"> </span>
                                </div>
                            </div>


                            {{--<div class="form-group">--}}
                            {{--<label class="control-label col-md-4">Performance titles--}}
                            {{--<span class="required">  </span>--}}
                            {{--</label>--}}
                            {{--<div class="col-md-8">--}}
                            {{--<input type="text" class="form-control" name="performance_titles" />--}}
                            {{--<span class="help-block"> </span>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                            {{--<label class="control-label col-md-4">Elbow ED Results--}}
                            {{--<span class="required">  </span>--}}
                            {{--</label>--}}
                            {{--<div class="col-md-8">--}}
                            {{--<input type="text" class="form-control" name="elbow_ed_results" />--}}
                            {{--<span class="help-block"> </span>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            <div class="form-group">
                                <label class="control-label col-md-4">Hip HD Results
                                    <span class="required">  </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="hip_hd_results" placeholder="Hip HD results" />
                                    <span class="help-block"> </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4">Tattoo Number
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" placeholder="" class="form-control" name="tattoo_number" />
                                    <span class="help-block"> </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4">DNA ID
                                    <span class="required">  </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" placeholder="" class="form-control" name="DNA" />
                                    <span class="help-block"> </span>
                                </div>
                            </div>

{{--                            <div class="form-group">--}}
{{--                                <label class="control-label col-md-4">Other Health Checks--}}
{{--                                    <span class="required">  </span>--}}
{{--                                </label>--}}
{{--                                <div class="col-md-8">--}}
{{--                                    <input type="text" placeholder="" class="form-control" name="other_health_checks" />--}}
{{--                                    <span class="help-block"> </span>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {{--<button type="submit" class="btn btn-success">Save</button>--}}
                    <input type="submit" class="btn btn-success" value="Save" id="save-btn">
                </div>

            </div><!-- /.modal-content -->
        </form>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
