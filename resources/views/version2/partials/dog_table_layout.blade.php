<div class="portlet-body main-table" id="dog_id" data-id="{{$dog->id}}">

    <!-- Begin Pedigree Table -->

    <div class="table-container">

        <h1 class="text-center"> {{strtoupper($dog->name)}}</h1>
        <h1 class="text-right">
            <button class="btn btn-success" id="regenerate-table">Regenerate table</button></h1>

        <?php

        if ($dog->dob !== null || empty($dog->dob)){
        $dob = Carbon\Carbon::createFromFormat('Y-m-d',$dog->dob);
        $difference = $dob->diffInMonths(Carbon\Carbon::now());
        $certificate = $difference > 12 ? 'PEDIGREE ' : 'BIRTH ';
        }
        ?>

        <div>

            <hr>
            <br>

            <?php $parent = (\App\DogGeneration::leftJoin('dogs','dogs.id','=','dog_generations.dog_id')
                ->where('dog_id',$dog->id)
                ->first()) ?>

            <table border="0" cellpadding="2" width='95%'>
                <tr>
                    <td>
                        <table class="table table-responsive" id="pedigree-table" style="border: 1px solid #000000; text-transform: uppercase;
                                    border-collapse: collapse;" border="1" cellpadding="2" cellspacing="2" width="100%"  CELLSPACING="2">
                            <tr>
                                <th style="text-align: center;">I</th>
                                <th style="text-align: center;">II</th>
                                <th style="text-align: center;">III</th>
                                <th style="text-align: center;">IV</th>
                                {{--<th style="text-align: center;">V</th>--}}

                            </tr>

                            <tr>
                                <td rowspan='16' width='17%' class='male'>
                                    <div class="h5">
                                        <span class="column" data-column="1"  data-position="sire">1</span>

                                        <div class="dog-name">

                                            {{$parent->first_generation['sire']['name']}}<br>

                                        </div>

                                        {{$parent->first_generation['sire']['registration_number']}}<br>
                                        {{$parent->first_generation['sire']['titles']}}<br>

                                    </div>

                                    <button class="btn btn-sm btn-success edit-column"
                                            data-sire-parent="second,sire,sire"
                                            data-dam-parent="second,sire,dam"
                                            data-generation="first"
                                            data-position="sire" >edit
                                    </button>

                                </td>

                                <td rowspan='8' width='17%' class='male'>
                                    <div class="h5">

                                        <span class="column">3</span>


                                        <div class="dog-name">

                                            {{$parent->second_generation['sire']['sire']['name']}}<br>
                                        </div>

                                        {{$parent->second_generation['sire']['sire']['registration_number']}}<br>
                                        {{$parent->second_generation['sire']['sire']['titles']}}<br>

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-sire-parent="third,Sire,sire"
                                                data-dam-parent="third,Sire,dam"
                                                data-generation="second"
                                                data-position="sire,sire"
                                                data-offspring="first,sire">
                                            edit</button>

                                    </div>
                                </td>

                                <td rowspan='4' width='17%' class='male'>
                                    <div class="h5">

                                        <span class="column">7</span>
                                        <div class="dog-name">

                                            <div class="dog-name">

                                                {{$parent->third_generation['Sire']['sire']['name']}}<br>
                                            </div>

                                            {{$parent->third_generation['Sire']['sire']['registration_number']}}<br>
                                            {{$parent->third_generation['Sire']['sire']['titles']}}<br>

                                            {{--$parent->fourth_generation['Sire']['sire']['name']--}}

                                            {{--                                                    @if($parent->third_generation['Sire']['sire']['name'] == '')--}}

                                            <button class="btn btn-sm btn-success edit-column"

                                                    data-sire-parent="fourth,Sire,sire"
                                                    data-dam-parent="fourth,Sire,dam"

                                                    data-generation="third"
                                                    data-position="Sire,sire"
                                                    data-offspring="second,sire,sire">
                                                edit</button>

                                            {{--@endif--}}
                                        </div>
                                </td>
                                <td rowspan='2' width='17%' class='male'>
                                    <div class="h5">

                                        <span class="column">15</span>


                                        <div class="dog-name">

                                            {{$parent->fourth_generation['Sire']['sire']['name']}}<br>

                                        </div>

                                        {{$parent->fourth_generation['Sire']['sire']['registration_number']}}<br>
                                        {{$parent->fourth_generation['Sire']['sire']['titles']}}<br>

                                        {{--                                                    @if($parent->fourth_generation['Sire']['sire']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-generation="fourth" data-position="Sire,sire"
                                                data-offspring="third,Sire,sire">
                                            edit
                                        </button>

                                        {{--@endif--}}


                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp;</td>
                            </tr>

                            <tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                            </tr>

                            <tr>
                                <td rowspan='2' width='17%' class='female'>
                                    <div class="h5">

                                        <span class="column">16</span>

                                        <div class="dog-name">

                                            {{$parent->fourth_generation['Sire']['dam']['name']}}<br>
                                        </div>

                                        {{$parent->fourth_generation['Sire']['dam']['registration_number']}}<br>
                                        {{$parent->fourth_generation['Sire']['dam']['titles']}}<br>

                                        {{--                                                    @if($parent->fourth_generation['Sire']['dam']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-generation="fourth" data-position="Sire,dam"
                                                data-offspring="third,Sire,sire">

                                            edit</button>

                                        {{--@endif--}}

                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='4' width='17%' class='female'>
                                    <div class="h5">

                                        <span class="column">8</span>


                                        <div class="dog-name">
                                            {{$parent->third_generation['Sire']['dam']['name']}}<br>
                                        </div>

                                        {{$parent->third_generation['Sire']['dam']['registration_number']}}<br>
                                        {{$parent->third_generation['Sire']['dam']['titles']}}<br>

                                        {{--$parent->fourth_generation['Dam']['sire']['name']--}}

                                        {{--                                                    @if($parent->third_generation['Sire']['dam']['name'] =='')--}}

                                        <button class="btn btn-sm btn-success edit-column"

                                                data-sire-parent="fourth,Dam,sire"
                                                data-dam-parent="fourth,Dam,dam"

                                                data-generation="third"
                                                data-position="Sire,dam"
                                                data-offspring="second,sire,sire">
                                            edit</button>

                                        {{--@endif--}}
                                    </div>

                                </td>
                                <td rowspan='2' width='17%' class='male'>
                                    <div class="h5">

                                        <span class="column">17</span>

                                        <div class="dog-name">

                                            {{$parent->fourth_generation['Dam']['sire']['name']}}<br>

                                        </div>

                                        {{$parent->fourth_generation['Dam']['sire']['registration_number']}}<br>
                                        {{$parent->fourth_generation['Dam']['sire']['titles']}}<br>

                                        {{--                                                    @if($parent->fourth_generation['Dam']['sire']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-generation="fourth" data-position="Dam,sire"
                                                data-offspring="third,Sire,dam">

                                            edit</button>

                                        {{--@endif--}}

                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='2' width='17%' class='female'>
                                    <div class="h5">

                                        <span class="column">18</span>

                                        <div class="dog-name">

                                            {{$parent->fourth_generation['Dam']['dam']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['Dam']['dam']['registration_number']}}<br>
                                        {{$parent->fourth_generation['Dam']['dam']['titles']}}<br>

                                        {{--                                                    @if($parent->fourth_generation['Dam']['dam']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column" data-generation="fourth"
                                                data-position="Dam,dam"
                                                data-offspring="third,Sire,dam">


                                            edit</button>

                                        {{--@endif--}}

                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='8' width='17%' class='female'>
                                    <div class="h5">

                                        <span class="column">4</span>

                                        <div class="dog-name">

                                            {{$parent->second_generation['sire']['dam']['name']}}<br>
                                        </div>

                                        {{$parent->second_generation['sire']['dam']['registration_number']}}<br>
                                        {{$parent->second_generation['sire']['dam']['titles']}}<br>


                                        {{--                                                    @if($parent->second_generation['sire']['dam']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-sire-parent="third,Dam,sire"
                                                data-dam-parent="third,Dam,dam"

                                                data-generation="second"
                                                data-position="sire,dam"
                                                data-offspring="first,sire">edit</button>
                                        {{--@endif--}}

                                    </div>
                                </td>
                                <td rowspan='4' width='17%' class='male'>
                                    <div class="h5">


                                        <span class="column">9</span>

                                        <div class="dog-name">

                                            {{$parent->third_generation['Dam']['sire']['name']}}<br>
                                        </div>

                                        {{$parent->third_generation['Dam']['sire']['registration_number']}}<br>
                                        {{$parent->third_generation['Dam']['sire']['titles']}}<br>

                                        {{--$parent->fourth_generation['SecondSire']['sire']['name']--}}

                                        {{--@if($parent->third_generation['Dam']['sire']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"

                                                data-sire-parent="fourth,SecondSire,sire"
                                                data-dam-parent="fourth,SecondSire,dam"

                                                data-generation="third"
                                                data-position="Dam,sire"
                                                data-offspring="second,sire,dam">
                                            edit</button>
                                        {{--@endif--}}

                                    </div>
                                </td>
                                <td rowspan='2' width='17%' class='male'>

                                    <span class="column">19</span>

                                    <div class="h5">

                                        <div class="dog-name">

                                            {{$parent->fourth_generation['SecondSire']['sire']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['SecondSire']['sire']['registration_number']}}<br>
                                        {{$parent->fourth_generation['SecondSire']['sire']['titles']}}<br>

                                        {{--                                                    @if($parent->fourth_generation['SecondSire']['sire']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column" data-generation="fourth"
                                                data-position="SecondSire,sire"
                                                data-offspring="third,Dam,sire">

                                            edit</button>

                                        {{--@endif--}}


                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='2' width='17%' class='female'>

                                    <span class="column">20</span>

                                    <div class="h5">

                                        <div class="dog-name">
                                            {{$parent->fourth_generation['SecondSire']['dam']['name']}}<br>

                                        </div>
                                        {{$parent->fourth_generation['SecondSire']['dam']['registration_number']}}<br>
                                        {{$parent->fourth_generation['SecondSire']['dam']['titles']}}<br>

                                        {{--                                                    @if($parent->fourth_generation['SecondSire']['dam']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-generation="fourth"
                                                data-position="SecondSire,dam"
                                                data-offspring="third,Dam,sire">

                                            edit</button>

                                        {{--@endif--}}

                                    </div>


                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp;</td>
                            </tr>
                            <tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='4' width='17%' class='female'>

                                    <span class="column">10</span>

                                    <div class="h5">

                                        <div class="dog-name">
                                            {{$parent->third_generation['Dam']['dam']['name']}}<br>
                                        </div>

                                        <div class="dog-name">

                                            {{$parent->third_generation['Dam']['dam']['registration_number']}}<br>
                                        </div>

                                        {{$parent->third_generation['Dam']['dam']['titles']}}<br>

                                        {{--                                                    @if($parent->third_generation['Dam']['dam']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"

                                                data-sire-parent="fourth,SecondDam,sire"
                                                data-dam-parent="fourth,SecondSire,dam"

                                                data-generation="third"
                                                data-position="Dam,dam"
                                                data-offspring="second,sire,dam">
                                            edit</button>

                                        {{--@endif--}}
                                    </div>

                                </td>
                                <td rowspan='2' width='17%' class='male'>

                                    <span class="column">21</span>

                                    <div class="h5">

                                        <div class="dog-name">
                                            {{$parent->fourth_generation['SecondDam']['sire']['name']}}<br>

                                        </div>
                                        {{$parent->fourth_generation['SecondDam']['sire']['registration_number']}}<br>
                                        {{$parent->fourth_generation['SecondDam']['sire']['titles']}}<br>

                                        {{--                                                    @if($parent->fourth_generation['SecondDam']['sire']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-generation="fourth" data-position="SecondDam,sire"
                                                data-offspring="third,Dam,dam">

                                            edit</button>

                                        {{--@endif--}}


                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr>
                            <tr>
                                <td rowspan='2' width='17%' class='female'>


                                    <span class="column">22</span>

                                    <div class="h5">
                                        <div class="dog-name">
                                            {{$parent->fourth_generation['SecondDam']['dam']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['SecondDam']['dam']['registration_number']}}<br>
                                        {{$parent->fourth_generation['SecondDam']['dam']['titles']}}<br>

                                        {{--                                                    @if($parent->fourth_generation['SecondDam']['dam']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-generation="fourth" data-position="SecondDam,dam"
                                                data-offspring="third,Dam,dam">

                                            edit</button>

                                        {{--@endif--}}


                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='16' width='17%' class='female' >

                                    <span class="column">2</span>

                                    <div class="h5">

                                        <div class="dog-name">
                                            {{$parent->first_generation['dam']['name']}}<br>
                                        </div>
                                        {{$parent->first_generation['dam']['registration_number']}}<br>
                                        {{$parent->first_generation['dam']['titles']}}<br>

                                    </div>

                                    {{--                                                @if($parent->first_generation['dam']['name'] == '')--}}

                                    <button class="btn btn-sm btn-success edit-column"
                                            data-sire-parent="second,sire,sire"
                                            data-dam-parent="second,sire,dam"
                                            data-generation="first"
                                            data-position="dam" >edit
                                    </button>

                                    {{--@endif--}}
                                </td>
                                <td rowspan='8' width='17%' class='male'>
                                    <span class="column">5</span>

                                    <div class="h5">

                                        <div class="dog-name">

                                            {{$parent->second_generation['Dam']['sire']['name']}}<br>
                                        </div>

                                        {{$parent->second_generation['Dam']['sire']['registration_number']}}<br>
                                        {{$parent->second_generation['Dam']['sire']['titles']}}<br>

                                        {{--                                                    @if($parent->second_generation['Dam']['sire']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"

                                                data-sire-parent="Third,SecondSire,sire"
                                                data-dam-parent="Third,SecondSire,dam"

                                                data-generation="second"
                                                data-position="Dam,sire"
                                                data-offspring="first,dam">
                                            edit</button>

                                        {{--@endif--}}

                                    </div>
                                </td>
                                <td rowspan='4' width='17%' class='male'>
                                    <span class="column">11</span>

                                    <div class="h5">
                                        <div class="dog-name">
                                            {{$parent->third_generation['SecondSire']['sire']['name']}}<br>
                                        </div>


                                        {{$parent->third_generation['SecondSire']['sire']['registration_number']}}<br>
                                        {{$parent->third_generation['SecondSire']['sire']['titles']}}<br>

                                        {{--                                                    @if($parent->third_generation['SecondSire']['sire']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"

                                                data-sire-parent="fourth,ThirdSire,sire"
                                                data-dam-parent="fourth,ThirdSire,dam"

                                                data-generation="third"
                                                data-position="SecondSire,sire"
                                                data-offspring="second,Dam,sire">
                                            edit</button>
                                        {{--@endif--}}
                                    </div>

                                </td>
                                <td rowspan='2' width='17%' class='male'>


                                    <span class="column">23</span>


                                    <div class="h5">

                                        <div class="dog-name">

                                            {{$parent->fourth_generation['ThirdSire']['sire']['name']}}<br>
                                        </div>

                                        {{$parent->fourth_generation['ThirdSire']['sire']['registration_number']}}<br>
                                        {{$parent->fourth_generation['ThirdSire']['sire']['titles']}}<br>

                                        {{--                                                    @if($parent->fourth_generation['ThirdSire']['sire']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-generation="fourth"
                                                data-position="ThirdSire,sire"
                                                data-offspring="third,SecondSire,sire">
                                            >
                                            edit</button>

                                        {{--@endif--}}



                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='2' width='17%' class='female'>

                                    <span class="column">24</span>


                                    <div class="h5">
                                        <div class="dog-name">
                                            {{$parent->fourth_generation['ThirdSire']['dam']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['ThirdSire']['dam']['registration_number']}}<br>
                                        {{$parent->fourth_generation['ThirdSire']['dam']['titles']}}<br>

                                        {{--                                                        @if($parent->fourth_generation['ThirdSire']['dam']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-generation="fourth" data-position="ThirdSire,dam"
                                                data-offspring="third,SecondSire,sire">
                                            edit</button>

                                        {{--@endif--}}



                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='4' width='17%' class='female'>

                                    <span class="column">12</span>

                                    <div class="h5">
                                        <div class="dog-name">
                                            {{$parent->third_generation['SecondSire']['dam']['name']}}<br>
                                        </div>
                                        {{$parent->third_generation['SecondSire']['dam']['registration_number']}}<br>
                                        {{$parent->third_generation['SecondSire']['dam']['titles']}}<br>

                                        {{--                                                    @if($parent->third_generation['SecondSire']['dam']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"

                                                data-sire-parent="fourth,ThirdDam,sire"
                                                data-dam-parent="fourth,ThirdDam,dam"

                                                data-generation="third"
                                                data-position="SecondSire,dam"
                                                data-offspring="second,Dam,sire">
                                            edit</button>
                                        {{--@endif--}}
                                    </div>
                                </td>
                                <td rowspan='2' width='17%' class='male'>

                                    <span class="column">25</span>


                                    <div class="h5">
                                        <div class="dog-name">
                                            {{$parent->fourth_generation['ThirdDam']['sire']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['ThirdDam']['sire']['registration_number']}}<br>
                                        {{$parent->fourth_generation['ThirdDam']['sire']['titles']}}<br>

                                        {{--                                                    @if($parent->fourth_generation['ThirdDam']['sire']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-generation="fourth"
                                                data-position="ThirdSire,sire"
                                                data-offspring="third,SecondSire,dam">
                                            edit</button>

                                        {{--@endif--}}



                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='2' width='17%' class='female'>
                                    <span class="column">26</span>


                                    <div class="h5">
                                        <div class="dog-name">
                                            {{$parent->fourth_generation['ThirdDam']['dam']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['ThirdDam']['dam']['registration_number']}}<br>
                                        {{$parent->fourth_generation['ThirdDam']['dam']['titles']}}<br>

                                        {{--                                                    @if($parent->fourth_generation['ThirdDam']['dam']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-generation="fourth"
                                                data-position="ThirdSire,dam"
                                                data-offspring="third,SecondSire,dam">
                                            edit</button>

                                        {{--@endif--}}



                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='8' width='17%' class='female'>
                                    <div class="h5">

                                        <span class="column">6</span>

                                        <div class="dog-name">

                                            {{$parent->second_generation['Dam']['dam']['name']}}<br>
                                        </div>
                                        {{$parent->second_generation['Dam']['dam']['registration_number']}}<br>
                                        {{$parent->second_generation['Dam']['dam']['titles']}}<br>

                                        {{--                                                    @if($parent->second_generation['Dam']['dam']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"

                                                data-sire-parent="third,SecondDam,sire"
                                                data-dam-parent="third,SecondDam,dam"

                                                data-generation="second"
                                                data-position="Dam,dam"
                                                data-offspring="first,dam"
                                        >edit</button>

                                        {{--@endif--}}

                                    </div>
                                </td>
                                <td rowspan='4' width='17%' class='male'>
                                    <span class="column">13</span>

                                    <div class="h5">

                                        <div class="dog-name">
                                            {{$parent->third_generation['SecondDam']['sire']['name']}}<br>
                                        </div>
                                        {{$parent->third_generation['SecondDam']['sire']['registration_number']}}<br>
                                        {{$parent->third_generation['SecondDam']['sire']['titles']}}<br>

                                        {{--                                                    @if($parent->third_generation['SecondDam']['sire']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-sire-parent="fourth,FourthSire,sire"
                                                data-dam-parent="fourth,FourthSire,dam"

                                                data-generation="third"
                                                data-position="SecondDam,sire"
                                                data-offspring="second,Dam,dam">
                                            edit</button>
                                        {{--@endif--}}

                                    </div>

                                </td>
                                <td rowspan='2' width='17%' class='male'>


                                    <span class="column">27</span>


                                    <div class="h5">
                                        <div class="dog-name">

                                            {{$parent->fourth_generation['FourthSire']['sire']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['FourthSire']['sire']['registration_number']}}<br>
                                        {{$parent->fourth_generation['FourthSire']['sire']['titles']}}<br>

                                        {{--                                                    @if($parent->fourth_generation['FourthSire']['sire']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-generation="fourth"
                                                data-position="FourthSire,sire"
                                                data-offspring="third,SecondDam,sire">
                                            >edit</button>

                                        {{--@endif--}}


                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='2' width='17%' class='female'>

                                    <span class="column">28</span>


                                    <div class="h5">

                                        {{$parent->fourth_generation['FourthSire']['dam']['name']}}<br>
                                        {{$parent->fourth_generation['FourthSire']['dam']['registration_number']}}<br>
                                        {{$parent->fourth_generation['FourthSire']['dam']['titles']}}<br>

                                        {{--                                                    @if($parent->fourth_generation['FourthSire']['dam']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-generation="fourth"
                                                data-position="FourthSire,dam"
                                                data-offspring="third,SecondDam,sire">
                                            >edit</button>

                                        {{--@endif--}}
                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='4' width='17%' class='female'>
                                    <span class="column">14</span>

                                    <div class="h5">
                                        <div class="dog-name">

                                            {{$parent->third_generation['SecondDam']['dam']['name']}}<br>
                                        </div>
                                        {{$parent->third_generation['SecondDam']['dam']['registration_number']}}<br>
                                        {{$parent->third_generation['SecondDam']['dam']['titles']}}<br>

                                        {{--                                                    @if($parent->third_generation['SecondDam']['dam']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"

                                                data-sire-parent="fourth,FourthDam,sire"
                                                data-dam-parent="fourth,FourthDam,dam"

                                                data-generation="third"
                                                data-position="SecondDam,dam"
                                                data-offspring="second,Dam,dam">
                                            edit</button>
                                        {{--@endif--}}
                                    </div>
                                </td>
                                <td rowspan='2' width='17%' class='male'>
                                    <span class="column">29</span>


                                    <div class="h5">
                                        <div class="dog-name">

                                            {{$parent->fourth_generation['FourthDam']['sire']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['FourthDam']['sire']['registration_number']}}<br>
                                        {{$parent->fourth_generation['FourthDam']['sire']['titles']}}<br>

                                        {{--                                                    @if($parent->fourth_generation['FourthDam']['sire']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-generation="fourth"
                                                data-position="FourthDam,sire"
                                                data-offspring="third,SecondDam,dam">
                                            >edit</button>

                                        {{--@endif--}}
                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                            </tr>

                            <tr>
                                <td rowspan='2' width='17%' class='female'>


                                    <span class="column">30</span>

                                    <div class="h5">

                                        <div class="dog-name">
                                            {{$parent->fourth_generation['FourthDam']['dam']['name']}}<br>
                                        </div>
                                        {{$parent->fourth_generation['FourthDam']['dam']['registration_number']}}<br>
                                        {{$parent->fourth_generation['FourthDam']['dam']['titles']}}<br>

                                        {{--                                                    @if($parent->fourth_generation['FourthDam']['dam']['name'] == '')--}}

                                        <button class="btn btn-sm btn-success edit-column"
                                                data-generation="fourth"
                                                data-position="FourthDam,dam"
                                                data-offspring="third,SecondDam,dam">edit</button>

                                        {{--@endif--}}
                                    </div>
                                </td>
                                <td rowspan='1' class="hide-column"  width='17%' class='male'>&nbsp; </td>
                            </tr>
                            {{--<tr>--}}
                            {{--<td  style="display: none;"rowspan='1' width='17%' class='female'>&nbsp; 62</td>--}}
                            {{--</tr>--}}
                            <tr>
                        </table>
                    </td>
            </table>

        </div>
    </div>
    <!-- End Pedigree Table -->

    </table>
    @if($dog->father || $dog->mother)
        <?php
        $father_id = \App\Dog::getDogId($dog->father);
        $mother_id = \App\Dog::getDogId($dog->mother);
        ?>
    @endif

</div>
