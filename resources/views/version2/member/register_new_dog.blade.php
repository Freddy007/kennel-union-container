@extends('version2.layouts.admin_layout')

@section('scripts')

        <!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('kug_version2/assets/global/scripts/select2.min.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.min.js" type="text/javascript"></script>

{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js"></script>--}}
{{--<script src="{{asset('kug_version2/assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('kug_version2/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>--}}
<script src="{{asset('kug_version2/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->


<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{asset('kug_version2/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('kug_version2/assets/pages/scripts/form-wizard.min.js')}}" type="text/javascript"></script>
<script src="{{asset('kug_version2/assets/register_dog_scripts.js')}}" type="text/javascript"></script>

<script>
    let url = "{{ url('/member/dog') }}";
    initializeDogSelect(url);

    $('.custom_registration_number').hide();

    $('#dog-not-born-in-ghana').on('click',function () {
        if($(this).is(':checked')){
            $('.custom_registration_number').show()
        }else{
            $('.custom_registration_number').hide();
        }
    })

    $('#name').on('change',function(){
//        alert('hello');
        var name = $('#name').val();
        jQuery.ajax('dog-by-name/'+name,function(data){
        }).fail(function (data) {
//            alert('Dog already in the system');
            $('.help-block.name').text(' This dog name already exists!').css('color','red');
            $('#submit_form > div > div.form-actions > div > div > a.btn.btn-outline.green.button-next').hide()
        }).done(function (data) {
            $('#submit_form > div > div.form-actions > div > div > a.btn.btn-outline.green.button-next').show();
            $('.help-block.name').text(' ');

        })
    })

    var substringMatcher = function(strings) {
        return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strings, function(i, string) {
                if (substrRegex.test(string)) {
                    matches.push(string);
                }
            });

            cb(matches);
        };
    };

    // Suggestion engine
    var courses = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: 'type-ahead-dogs?q=%QUERY',
        remote: {
            url: 'type-ahead-dogs?q=%QUERY',
            wildcard: '%QUERY'
        }
    });

    // $('#courses').typeahead({
    $('.parent').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        name: 'courses',
        source: courses,
    });

</script>

<!-- END PAGE LEVEL SCRIPTS -->

@endsection


@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>REGISTER NEW DOG
                        <small>form wizard </small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->
                <div class="page-toolbar">
                    <!-- BEGIN THEME PANEL -->
                    <!-- END THEME PANEL -->
                </div>
                <!-- END PAGE TOOLBAR -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content" >
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{url('/member')}}">Member page</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>register new dog</span>
                    </li>
                </ul>

                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        {{--<div class="col-md-12" ng-app="adminApp">--}}
                        <div class="col-md-12">
                            {{--<div class="m-heading-1 border-green m-bordered">--}}
                                {{--<h3>Twitter Bootstrap Wizard Plugin</h3>--}}
                                {{--<p> This twitter bootstrap plugin builds a wizard out of a formatter tabbable structure. It allows to build a wizard functionality using buttons to go through the different wizard steps and using events allows to hook--}}
                                    {{--into each step individually. </p>--}}
                                {{--<p> For more info please check out--}}
                                    {{--<a class="btn red btn-outline" href="http://vadimg.com/twitter-bootstrap-wizard-example" target="_blank">the official documentation</a>--}}
                                {{--</p>--}}
                            {{--</div>--}}
                            {{--<div class="portlet light " id="form_wizard_1" ng-controller = "DogRegistrationController">--}}
                            <div class="portlet light " id="form_wizard_1">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-red"></i>
                                                <span class="caption-subject font-red bold uppercase"> Dog Registration Wizard -
                                                    <span class="step-title"> Step 1 of 4 </span>
                                                </span>
                                    </div>
                                    <div class="actions">
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-cloud-upload"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-wrench"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form action="{{url('member/register-dog')}}" class="form-horizontal" id="submit_form" method="POST">
                                    {{--<form action="{{url('admin/register-dog')}}" class="form-horizontal" id="submit_form" method="POST">--}}
                                        <div class="form-wizard">
                                            <div class="form-body">
                                                <ul class="nav nav-pills nav-justified steps">
                                                    <li>
                                                        <a href="#tab1" data-toggle="tab" class="step">
                                                            <span class="number"> 1 </span>
                                                                    <span class="desc">
                                                                        <i class="fa fa-check"></i> Basic Details </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab2" data-toggle="tab" class="step">
                                                            <span class="number"> 2 </span>
                                                                    <span class="desc">
                                                                        <i class="fa fa-check"></i> Secondary Details </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab3" data-toggle="tab" class="step active">
                                                            <span class="number"> 3 </span>
                                                                    <span class="desc">
                                                                        <i class="fa fa-check"></i> Health Details </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab4" data-toggle="tab" class="step">
                                                            <span class="number"> 4 </span>
                                                                    <span class="desc">
                                                                        <i class="fa fa-check"></i> Confirm </span>
                                                        </a>
                                                    </li>
                                                </ul>

                                                <div id="bar" class="progress progress-striped" role="progressbar">
                                                    <div class="progress-bar progress-bar-success"> </div>
                                                </div>
                                                <div class="tab-content">
                                                    <div class="alert alert-danger display-none">
                                                        <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                                    <div class="alert alert-success display-none">
                                                        <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                                    <div class="tab-pane active" id="tab1">
                                                        <h3 class="block"> Basic details</h3>
                                                        <input type="hidden" name="id" value="{{Webpatser\Uuid\Uuid::generate()}}" >
                                                        <input type="hidden" name="registration_number" value="{{\App\Dog::generateRegistrationNumber()}}" >
                                                        <input type="hidden" name="user_id" value="{{Auth::id()}}" >

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Name
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="name" id="name" required />
                                                                <span class="help-block">  </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Gender
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <select class="form-control" name="sex">
                                                                    <option></option>
                                                                    <option>Male</option>
                                                                    <option>Female</option>
                                                                </select>
                                                                <span class="help-block">  </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Date of Birth
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="date" class="form-control" name="dob" id="dob" required />
                                                                <span class="help-block">  </span>
                                                            </div>
                                                        </div>

{{--                                                        <div class="form-group">--}}
{{--                                                            <label class="control-label col-md-3">Father(Sire)--}}
{{--                                                                --}}{{--<span class="required"> * </span>--}}
{{--                                                            </label>--}}
{{--                                                            <div class="col-md-4">--}}
{{--                                                                <select class="dog_parent form-control"  id="father" name="father"  data-gender="male" >--}}
{{--                                                                    <option value=""></option>--}}
{{--                                                                </select>--}}
{{--                                                                <span class="help-block">  </span>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                        --}}
{{--                                                        <div class="form-group">--}}
{{--                                                            <label class="control-label col-md-3">Mother(Dam)--}}
{{--                                                                --}}{{--<span class="required"> * </span>--}}
{{--                                                            </label>--}}
{{--                                                            <div class="col-md-4">--}}
{{--                                                                <select class="dog_parent form-control" name="mother" id="mother" data-gender="female">--}}
{{--                                                                    <option value=""></option>--}}
{{--                                                                </select>--}}
{{--                                                                <span class="help-block">  </span>--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}


                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Father(Sire)
                                                                {{--<span class="required"> * </span>--}}
                                                            </label>
                                                            <div class="col-md-4">

                                                                <div class="typeahead-container">

                                                                    <input class="typeahead parent" type="text" name="father" placeholder="Father of dog">

                                                                </div>
                                                                <span class="help-block">  </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Mother(Dam)
                                                                {{--<span class="required"> * </span>--}}
                                                            </label>
                                                            <div class="col-md-4">

                                                                <div class="typeahead-container">
                                                                    <input class="typeahead parent" type="text" name="mother" placeholder="Mother of dog">

                                                                </div>

                                                                <span class="help-block">  </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Breed
                                                                <span class="required"> * </span>
                                                                {{--<span> * </span>--}}

                                                            </label>
                                                            <div class="col-md-4">
                                                                {{--<select ng-model="selectedName" ng-options="item as item.name for item in breeds track by item.id" class="form-control" id="breed" name="breed">--}}
                                                                <select  class="form-control" id="breed" name="breeder_id" required>
                                                                    <option></option>
                                                                    @foreach($breeds as $breed)
                                                                        <option value="{{$breed->id}}">{{$breed->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <span class="help-block">  </span>


                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Not born in Ghana:</label>
                                                            <div class="col-md-4">
                                                                <input type="checkbox" name="dog_not_born_in_ghana" class="form-control" id="dog-not-born-in-ghana">
                                                            </div>
                                                        </div>

                                                        <div class="form-group custom_registration_number">
                                                            <label class="control-label col-md-3">Dog's registration number
                                                                {{--<span class="required"> * </span>--}}
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="custom_registration_number">
                                                                <span class="help-block">  </span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="tab-pane" id="tab2">
                                                        <h3 class="block">Secondary Details</h3>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Colour
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="colour" required />
                                                                <span class="help-block"> </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Coat
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                {{--<input type="text" class="form-control" name="coat" required />--}}
                                                                <select class="form-control" name="coat" id="coat" required>
                                                                    <option>short</option>
                                                                    <option>long</option>
                                                                </select>
                                                                <span class="help-block"> </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Height(cm)
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="number" class="form-control" name="height"  />
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Titles
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="titles" />
                                                                <span class="help-block"> </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Performance titles
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="performance_titles" />
                                                                <span class="help-block"> </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Image
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="file" class="form-control" name="pic" />
                                                                <span class="help-block"> </span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="tab-pane" id="tab3">
                                                        <h3 class="block">Health details</h3>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Elbow ED Results
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="elbow_ed_results" />
                                                                <span class="help-block"> </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Hip HD Results
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="hip_hd_results" />
                                                                <span class="help-block"> </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Tattoo Number
                                                                <span class="required"> * </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" placeholder="" class="form-control" name="tattoo_number" />
                                                                <span class="help-block"> </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Microchip Number
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" placeholder="" class="form-control" name="microchip_number" />
                                                                <span class="help-block"> </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">DNA ID
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" placeholder="" class="form-control" name="DNA" />
                                                                <span class="help-block"> </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Other Health Checks
                                                                <span class="required">  </span>
                                                            </label>
                                                            <div class="col-md-4">
                                                                <input type="text" placeholder="" class="form-control" name="other_health_checks" />
                                                                <span class="help-block"> </span>
                                                            </div>
                                                        </div>



                                                    </div>
                                                    <div class="tab-pane" id="tab4">
                                                        <h3 class="block">Confirm details</h3>
                                                        <h4 class="form-section">Basic Details</h4>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Name:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="name"> </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Gender:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="sex"> </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Date of Birth:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="dob"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Father:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="father"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Mother:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="mother"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Breed:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="breed"> </p>
                                                            </div>
                                                        </div>
                                                        <h4 class="form-section">Secondary Details</h4>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Colour:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="colour"> </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Coat:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="coat"> </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Height:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="height"> </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Titles:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="titles"> </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Performance Titles:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="performance_titles"> </p>
                                                            </div>
                                                        </div>

                                                        <h4 class="form-section">Health Details</h4>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Elbow ED Results:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="elbow_ed_results"> </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">HIP HD Results:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="hip_hd_results"> </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Tattoo Number:</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="tattoo_number"> </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Microchip Number</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="microchip_number"> </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">DNA ID</label>
                                                            <div class="col-md-4">
                                                                <p class="form-control-static" data-display="DNA"> </p>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <a href="javascript:;" class="btn default button-previous">
                                                            <i class="fa fa-angle-left"></i> Back </a>
                                                        <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                                            <i class="fa fa-angle-right"></i>
                                                        </a>

                                                        <button type="submit" class="btn green button-submit">Submit
                                                            <i class="fa fa-check"></i>
                                                        </button>
                                                        {{--<a href="javascript:;" class="btn green button-submit"> Submit--}}
                                                            {{--<i class="fa fa-check"></i>--}}
                                                        {{--</a>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
@endsection
