@extends('layouts.version2_master')

@section('content')

    {{--<header class="header">--}}
        {{--<div class="header-top">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">--}}
                        {{--<div class="header-location"><i class="fa fa-home"></i> <a href="#">455 Martinson, Los Angeles</a></div>--}}
                        {{--<div class="header-email"><i class="fa fa-envelope-o"></i> <a href="mailto:info@kennelunionghana.com">info@kennelunionghana.com</a></div>--}}
                        {{--<div class="header-phone"><i class="fa fa-phone"></i> <a href="#">8 (043) 567 - 89 - 30</a></div>--}}
                    {{--</div>--}}
                    {{--<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">--}}
                        {{--<div class="header-social pull-right">--}}
                            {{--<a href="#"><i class="fa fa-twitter"></i></a>--}}
                            {{--<a href="#"><i class="fa fa-facebook"></i></a>--}}
                            {{--<a href="#"><i class="fa fa-google-plus"></i></a>--}}
                            {{--<a href="#"><i class="fa fa-dribbble"></i></a>--}}
                            {{--<a href="#"><i class="fa fa-instagram"></i></a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="header-bottom">--}}
            {{--<nav class="navbar navbar-universal navbar-custom">--}}
                {{--<div class="container">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-lg-3">--}}
                            {{--<div class="logo"><a href="http://pedigree.kennelunionghana.com" class="navbar-brand page-scroll">--}}
                                    {{--<img src="{{asset('kug_version2/frontend/assets/images/logo/logo.png')}}" alt="logo"/>--}}
                                    {{--<h3><strong>Kennel Union of Ghana</strong></h3>--}}
                                {{--</a></div>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-9">--}}
                            {{--<div class="navbar-header">--}}
                                {{--<button type="button" data-toggle="collapse" data-target=".navbar-main-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>--}}
                            {{--</div>--}}
                            {{--<div class="collapse navbar-collapse navbar-main-collapse">--}}
                                {{--<ul class="nav navbar-nav navbar-right">--}}
                                    {{--<li ><a href="{{url('/')}}">Home</a></li>--}}

                                    {{--<li><a href="http://kennelunionghana.com">Main Website</a></li>--}}
                                    {{--<li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Pages <span class="caret"></span></a>--}}
                                    {{--<ul class="dropdown-menu">--}}
                                    {{--<li><a href="about-us.html">About Us</a></li>--}}
                                    {{--<li><a href="contacts.html">Contacts</a></li>--}}
                                    {{--<li><a href="faq.html">F.A.Q</a></li>--}}
                                    {{--<li><a href="gallery.html">Gallery</a></li>--}}
                                    {{--<li><a href="404.html">404 Page Not Found</a></li>--}}
                                    {{--</ul>--}}
                                    {{--</li>--}}

                                    {{--@if(Auth::check() && Auth::user()->administrator == 1)--}}
                                        {{--<li><a href="{{url('/version2')}}">Dashboard</a></li>--}}
                                    {{--@elseif(Auth::check() && Auth::user()->administrator == 0)--}}
                                        {{--<li><a href="{{url('/member')}}">Dashboard</a></li>--}}
                                    {{--@endif--}}

                                    {{--<li>--}}
                                        {{--@if(Auth::guest())--}}
                                            {{--<a href="{{url('auth/login')}}">--}}
                                                {{--Log in--}}
                                            {{--</a>--}}
                                        {{--@else--}}

                                            {{--<a href="{{url('auth/logout')}}">--}}
                                                {{--Log out--}}
                                            {{--</a>--}}
                                        {{--@endif--}}
                                    {{--</li>--}}

                                    {{--<li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Rooms <span class="caret"></span></a>--}}
                                    {{--<ul class="dropdown-menu">--}}
                                    {{--<li><a href="best-rooms.html">All Rooms</a></li>--}}
                                    {{--<li><a href="best-rooms-detail.html">Room Detail</a></li>--}}
                                    {{--</ul>--}}
                                    {{--</li>--}}
                                    {{--<li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Blog <span class="caret"></span></a>--}}
                                    {{--<ul class="dropdown-menu">--}}
                                    {{--<li><a href="blog.html">All Posts</a></li>--}}
                                    {{--<li><a href="blog-detail.html">Post Detail</a></li>--}}
                                    {{--</ul>--}}
                                    {{--</li>--}}
                                    {{--<li><a href="#">Purchase</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</nav>--}}
        {{--</div>--}}
    {{--</header>--}}


    <!-- parallax -->
<section class="bg-parallax parallax-window">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="parallax-text">
                    <h2 class="parallax_t __white">PEDIGREE DATABASE </h2>
                    {{--<h4 class="parallax_t __white">PEDIGREE DATABASE </h4>--}}
                    {{--<h2 class="parallax_t __green">available from 10.12.2016</h2>--}}
                    {{--<button class="btn btn-success btn-lg">SIGN UP</button>--}}
                    <div class="planner-check-availability">
                        <a href="{{url('auth/register')}}" class="btn btn-default col-xs-hidden col-sm-hidden">REGISTER YOUR DOG </a>
                    </div>
                    <h3 class="parallax_t__white">
                       {{--Over {{(\App\Dog::count())}} dogs registered so far...--}}
                    </h3>
                </div>
            </div>
            <!-- planner-->
         @include('partials.home_search_form')
            <!-- /planner-->
        </div>
    </div>
</section>
<!-- /parallax -->
<!-- chose best rooms -->
<section class="best-room">
    <div class="container">
        <div class="title-main">
            {{--<h2>{{\App\Dog::count()}} dogs registered so far</h2>--}}

            <h2 class="h2">Recently Registered Dogs
                {{--<span class="title-secondary">Look Our Featured Rooms</span>--}}
            </h2>
        </div>
        <div class="best-room-carousel">
            <ul class="row best-room_ul">
                @foreach($dogs as $dog)
                    <li class="col-lg-4 col-md-4 col-sm-6 col-xs-12 best-room_li">
                        <div class="best-room_img">
                            <a href="#">
                                {{--                                <img src="{{asset('kug_version2/frontend/assets/images/best-rooms/1.jpg')}}" alt=""/>--}}
                                <img src="{{$dog->image_name == null ? url('http://placehold.it/300x200/cccccc/000000?text=no+image+uploaded'):"/images/catalog/$dog->image_name"}}" alt=""/>
                            </a>
                            <div class="best-room_overlay">
                                <div class="overlay_icn">
                                    <a href="{{url('/show-dog',$dog->id)}}"></a>
                                </div>
                            </div>
                        </div>
                        <div class="best-room-info">
                            {{--                            <div class="best-room_t"><a href="best-rooms-detail.html">{{ str_word_count($dog->name). substr($dog->name, 0, 10)}}</a></div>--}}
                            <div class="best-room_t">
                                <a href="{{url('/show-dog',$dog->id)}}">{{ explode(' ',$dog->name)[0]}}</a></div>
                            <div class="best-room_desc">
                                {{--Difficulty on insensible reasonable in. From as went he they. Preference themselves me as thoroughly partiality considered.--}}
                            </div>
                            <div class="best-room_price">
                                {{--<span>Date : </span> --}}
                                {{getDogRegisteredDay($dog->created_at)}}

                                {{--                                {{$dog->created_at}}--}}
                                {{--<span>$99</span> / two days--}}
                            </div>
                        </div>
                    </li>
                @endforeach
                {{--<li class="col-lg-4 col-md-4 col-sm-6 col-xs-12 best-room_li">--}}
                {{--<div class="best-room_img">--}}
                {{--<a href="#">--}}
                {{--<img src="{{asset('kug_version2/frontend/assets/images/best-rooms/1.jpg')}}" alt=""/>--}}

                {{--<img src="assets/images/best-rooms/2.jpg" alt=""/>--}}
                {{--</a>--}}
                {{--<div class="best-room_overlay">--}}
                {{--<div class="overlay_icn"><a href="best-rooms-detail.html"></a></div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="best-room-info">--}}
                {{--<div class="best-room_t"><a href="best-rooms-detail.html">Special Spa Room</a></div>--}}
                {{--<div class="best-room_desc">Difficulty on insensible reasonable in. From as went he they. Preference themselves me as thoroughly partiality considered.--}}
                {{--</div>--}}
                {{--<div class="best-room_price">--}}
                {{--<span>$129</span> / two days--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</li>--}}
                {{--<li class="col-lg-4 col-md-4 col-sm-6 col-xs-12 best-room_li">--}}
                {{--<div class="best-room_img">--}}
                {{--<a href="#">--}}
                {{--<img src="{{asset('kug_version2/frontend/assets/images/best-rooms/1.jpg')}}" alt=""/>--}}

                {{--<img src="assets/images/best-rooms/3.jpg" alt=""/></a>--}}
                {{--<div class="best-room_overlay">--}}
                {{--<div class="overlay_icn"><a href="best-rooms-detail.html"></a></div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="best-room-info">--}}
                {{--<div class="best-room_t"><a href="best-rooms-detail.html">President Double Luxury</a></div>--}}
                {{--<div class="best-room_desc">Difficulty on insensible reasonable in. From as went he they. Preference themselves me as thoroughly partiality considered.--}}
                {{--</div>--}}
                {{--<div class="best-room_price">--}}
                {{--<span>$349</span> / two days--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</li>--}}
                {{----}}
                {{--<li class="col-lg-4 col-md-4 col-sm-6 col-xs-12 best-room_li">--}}
                {{--<div class="best-room_img">--}}
                {{--<a href="#">--}}
                {{--<img src="{{asset('kug_version2/frontend/assets/images/best-rooms/1.jpg')}}" alt=""/>--}}

                {{--<img src="assets/images/best-rooms/4.jpg" alt=""/>--}}
                {{--</a>--}}
                {{--<div class="best-room_overlay">--}}
                {{--<div class="overlay_icn"><a href="best-rooms-detail.html"></a></div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="best-room-info">--}}
                {{--<div class="best-room_t">--}}
                {{--<a href="best-rooms-detail.html">Grand Super Luxury</a></div>--}}
                {{--<div class="best-room_desc">Difficulty on insensible reasonable in. From as went he they. Preference themselves me as thoroughly partiality considered.--}}
                {{--</div>--}}
                {{--<div class="best-room_price">--}}
                {{--<span>$49</span> / two days--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</li>--}}
                {{--<li class="col-lg-4 col-md-4 col-sm-6 col-xs-12 best-room_li">--}}
                {{--<div class="best-room_img">--}}
                {{--<a href="#">--}}
                {{--<img src="{{asset('kug_version2/frontend/assets/images/best-rooms/1.jpg')}}" alt=""/>--}}

                {{--<img src="assets/images/best-rooms/5.jpg" alt=""/>--}}
                {{--</a>--}}
                {{--<div class="best-room_overlay">--}}
                {{--<div class="overlay_icn"><a href="best-rooms-detail.html"></a></div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="best-room-info">--}}
                {{--<div class="best-room_t"><a href="best-rooms-detail.html">Special Spa Room</a></div>--}}
                {{--<div class="best-room_desc">Difficulty on insensible reasonable in. From as went he they. Preference themselves me as thoroughly partiality considered.--}}
                {{--</div>--}}
                {{--<div class="best-room_price">--}}
                {{--<span>$199</span> / two days--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</li>--}}
                {{--<li class="col-lg-4 col-md-4 col-sm-6 col-xs-12 best-room_li">--}}
                {{--<div class="best-room_img">--}}
                {{--<a href="#">--}}
                {{--<img src="{{asset('kug_version2/frontend/assets/images/best-rooms/1.jpg')}}" alt=""/>--}}

                {{--<img src="assets/images/best-rooms/6.jpg" alt=""/>--}}
                {{--</a>--}}
                {{--<div class="best-room_overlay">--}}
                {{--<div class="overlay_icn"><a href="best-rooms-detail.html"></a></div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="best-room-info">--}}
                {{--<div class="best-room_t"><a href="best-rooms-detail.html">President Double Luxury</a></div>--}}
                {{--<div class="best-room_desc">Difficulty on insensible reasonable in. From as went he they. Preference themselves me as thoroughly partiality considered.--}}
                {{--</div>--}}
                {{--<div class="best-room_price">--}}
                {{--<span>$209</span> / two days--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div>
</section>

{{--<section class="best-room">--}}
    {{--<div class="container">--}}
        {{--<div class="title-main">--}}
            {{--<h2 class="h2">Recently Registered Dogs--}}
                {{--<span class="title-secondary">Look Our Featured Rooms</span>--}}
            {{--</h2>--}}
        {{--</div>--}}
        {{--<div class="best-room-carousel">--}}
            {{--<ul class="row best-room_ul">--}}
                {{--@foreach($dogs as $dog)--}}
                    {{--<li class="col-lg-4 col-md-4 col-sm-6 col-xs-12 best-room_li">--}}
                        {{--<div class="best-room_img">--}}
                            {{--<a href="#">--}}
                                {{--                                <img src="{{asset('kug_version2/frontend/assets/images/best-rooms/1.jpg')}}" alt=""/>--}}
                                {{--<img src="{{$dog->image_name == null ? url('http://placehold.it/300x200/cccccc/000000?text=no+image+uploaded'):"/images/catalog/$dog->image_name"}}" alt=""/>--}}
                            {{--</a>--}}
                            {{--<div class="best-room_overlay">--}}
                                {{--<div class="overlay_icn">--}}
                                    {{--<a href="{{url('/show-dog',$dog->id)}}"></a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="best-room-info">--}}
                            {{--                            <div class="best-room_t"><a href="best-rooms-detail.html">{{ str_word_count($dog->name). substr($dog->name, 0, 10)}}</a></div>--}}
                            {{--<div class="best-room_t">--}}
                                {{--<a href="{{url('/show-dog',$dog->id)}}">{{ explode(' ',$dog->name)[0]}}</a></div>--}}
                            {{--<div class="best-room_desc">--}}
                                {{--Difficulty on insensible reasonable in. From as went he they. Preference themselves me as thoroughly partiality considered.--}}
                            {{--</div>--}}
                            {{--<div class="best-room_price">--}}
                                {{--<span>Date : </span> --}}
                                {{--{{getDogRegisteredDay($dog->created_at)}}--}}

                                {{--                                {{$dog->created_at}}--}}
                                {{--<span>$99</span> / two days--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                {{--@endforeach--}}
                {{--<li class="col-lg-4 col-md-4 col-sm-6 col-xs-12 best-room_li">--}}
                {{--<div class="best-room_img">--}}
                {{--<a href="#">--}}
                {{--<img src="{{asset('kug_version2/frontend/assets/images/best-rooms/1.jpg')}}" alt=""/>--}}

                {{--<img src="assets/images/best-rooms/2.jpg" alt=""/>--}}
                {{--</a>--}}
                {{--<div class="best-room_overlay">--}}
                {{--<div class="overlay_icn"><a href="best-rooms-detail.html"></a></div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="best-room-info">--}}
                {{--<div class="best-room_t"><a href="best-rooms-detail.html">Special Spa Room</a></div>--}}
                {{--<div class="best-room_desc">Difficulty on insensible reasonable in. From as went he they. Preference themselves me as thoroughly partiality considered.--}}
                {{--</div>--}}
                {{--<div class="best-room_price">--}}
                {{--<span>$129</span> / two days--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</li>--}}
                {{--<li class="col-lg-4 col-md-4 col-sm-6 col-xs-12 best-room_li">--}}
                {{--<div class="best-room_img">--}}
                {{--<a href="#">--}}
                {{--<img src="{{asset('kug_version2/frontend/assets/images/best-rooms/1.jpg')}}" alt=""/>--}}

                {{--<img src="assets/images/best-rooms/3.jpg" alt=""/></a>--}}
                {{--<div class="best-room_overlay">--}}
                {{--<div class="overlay_icn"><a href="best-rooms-detail.html"></a></div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="best-room-info">--}}
                {{--<div class="best-room_t"><a href="best-rooms-detail.html">President Double Luxury</a></div>--}}
                {{--<div class="best-room_desc">Difficulty on insensible reasonable in. From as went he they. Preference themselves me as thoroughly partiality considered.--}}
                {{--</div>--}}
                {{--<div class="best-room_price">--}}
                {{--<span>$349</span> / two days--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</li>--}}
                {{----}}
                {{--<li class="col-lg-4 col-md-4 col-sm-6 col-xs-12 best-room_li">--}}
                {{--<div class="best-room_img">--}}
                {{--<a href="#">--}}
                {{--<img src="{{asset('kug_version2/frontend/assets/images/best-rooms/1.jpg')}}" alt=""/>--}}

                {{--<img src="assets/images/best-rooms/4.jpg" alt=""/>--}}
                {{--</a>--}}
                {{--<div class="best-room_overlay">--}}
                {{--<div class="overlay_icn"><a href="best-rooms-detail.html"></a></div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="best-room-info">--}}
                {{--<div class="best-room_t">--}}
                {{--<a href="best-rooms-detail.html">Grand Super Luxury</a></div>--}}
                {{--<div class="best-room_desc">Difficulty on insensible reasonable in. From as went he they. Preference themselves me as thoroughly partiality considered.--}}
                {{--</div>--}}
                {{--<div class="best-room_price">--}}
                {{--<span>$49</span> / two days--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</li>--}}
                {{--<li class="col-lg-4 col-md-4 col-sm-6 col-xs-12 best-room_li">--}}
                {{--<div class="best-room_img">--}}
                {{--<a href="#">--}}
                {{--<img src="{{asset('kug_version2/frontend/assets/images/best-rooms/1.jpg')}}" alt=""/>--}}

                {{--<img src="assets/images/best-rooms/5.jpg" alt=""/>--}}
                {{--</a>--}}
                {{--<div class="best-room_overlay">--}}
                {{--<div class="overlay_icn"><a href="best-rooms-detail.html"></a></div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="best-room-info">--}}
                {{--<div class="best-room_t"><a href="best-rooms-detail.html">Special Spa Room</a></div>--}}
                {{--<div class="best-room_desc">Difficulty on insensible reasonable in. From as went he they. Preference themselves me as thoroughly partiality considered.--}}
                {{--</div>--}}
                {{--<div class="best-room_price">--}}
                {{--<span>$199</span> / two days--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</li>--}}
                {{--<li class="col-lg-4 col-md-4 col-sm-6 col-xs-12 best-room_li">--}}
                {{--<div class="best-room_img">--}}
                {{--<a href="#">--}}
                {{--<img src="{{asset('kug_version2/frontend/assets/images/best-rooms/1.jpg')}}" alt=""/>--}}

                {{--<img src="assets/images/best-rooms/6.jpg" alt=""/>--}}
                {{--</a>--}}
                {{--<div class="best-room_overlay">--}}
                {{--<div class="overlay_icn"><a href="best-rooms-detail.html"></a></div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="best-room-info">--}}
                {{--<div class="best-room_t"><a href="best-rooms-detail.html">President Double Luxury</a></div>--}}
                {{--<div class="best-room_desc">Difficulty on insensible reasonable in. From as went he they. Preference themselves me as thoroughly partiality considered.--}}
                {{--</div>--}}
                {{--<div class="best-room_price">--}}
                {{--<span>$209</span> / two days--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</section>--}}

<!-- /choose best rooms -->
<!-- lux banner parallax -->
<section class="banner bg-parallax2">
    <div class="overlay"></div>
    <div class="banner-parallax">
        <div class="container">
            <div class="text-center">
                {{--<div class="banner-parallax_raiting">--}}
                {{--<a href="#"><span class="star __selected"></span></a>--}}
                {{--<a href="#"><span class="star __selected"></span></a>--}}
                {{--<a href="#"><span class="star __selected"></span></a>--}}
                {{--<a href="#"><span class="star"></span></a>--}}
                {{--<a href="#"><span class="star"></span></a>--}}
                {{--</div>--}}
                <h2 class="banner-parallax_t">REGISTER YOUR DOG NOW</h2>
                {{--<div class="banner-parallax_price"><span>$560</span> / Night (Only This Week)</div>--}}
                <a href="{{url('auth/register')}}" class="btn btn-default">Register</a>
            </div>
        </div>
    </div>
</section>
<!-- /lux banner parallax -->
<!-- enjoy our services -->


<section class="section">
    <div class="container">
        <div class="title-main"><h2 class="h2">Popular breeds registered<span class="title-secondary">Internationally certified breeds.</span></h2></div>
        <div class="row">
            @foreach($breeders->toArray() as $key => $breed)
                @if($key % 2 == 0)
            <div class="col-lg-4 col-md-4 col-sm-6">
                <h3 class="service_title"><i class="fa fa-paw"></i>
                   <a href="{{url('/search-results?name='.$breed)}}">{{$breed}}</a>
                </h3>
            </div>
                @endif
            @endforeach
        </div>
    </div>
</section>

<script>
    $('#search-modal').modal()
</script>
@endsection