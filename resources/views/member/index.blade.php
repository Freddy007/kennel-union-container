@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        jQuery(document).ready(function(){
            $('#report-modal').modal();

            $('#report').on('click',function(){
                if($('#report-input') .val() == ''){
                    $('.report-message').text('provide the registration number');

                }else {
                    $.post('/admin/report-number',$('#report-form').serialize(),function(data){
                        $('.report-message').text(data);
                    })
                }

            });

            $('#add-new-dog').off('click').on('click',function(){
                $('#register-dog-modal').modal();
            });

            @foreach($dogs as $dog)
         $('#request-certificate-<?php echo $dog->id?>').off('click').on('click',function(){
                        $('#request-certificate-modal-<?php echo $dog->id ?>').modal();
                    })

            $('#request-delete-<?php echo $dog->id?>').off('click').on('click',function(){
                $('#request-delete-modal-<?php echo $dog->id ?>').modal();
            })

            @endforeach

           $(".breeders").select2();

        });

    </script>
    @stop

@section('content')

    <div class="content">
        <div class="container">
            @include('flash::message')

            {{--@if($dogs->count())--}}
                <h3 class="portlet-title text-center"><u>{{Auth::getUser()->first_name .' '. Auth::getUser()->last_name ."'s Dogs"}} </u></h3>


                <div class="portlet-header">

                <h3 class="portlet-title"><u>Search </u></h3>

            </div> <!-- /.portlet-header -->
            <div class="row" style="margin-bottom: 45px;">
                <form  action="{{url('/member')}}" method="get">
                    {{csrf_field()}}
                <div class="col-md-7">
                    <input type="text" name="term" class="form-control input-lg  " value="{{Request::get('term')}}">

                </div>

                <div class="col-md-2 col-xs-6">
                    <input type="submit" class="form-control input-lg btn btn-success">
                </div>

                </form>

                <div class="col-md-2 col-xs-6" >
                    <div class="dropdown">
                        <button class="btn btn-default btn-lg dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Add New Dog
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a >Add Dog</a></li>
                            <li><a>Add Litter</a></li>

{{--                            <li><a href="{{url('/member/register-dog')}}">Add Dog</a></li>--}}
{{--                            <li><a href="{{url('member/register-litter')}}">Add Litter</a></li>--}}

                        </ul>
                    </div>
                    {{--<input type="button" class="form-control input-lg btn btn-success btn-sm" id="add-new-dog" value="ADD NEW DOG">--}}
                    {{--<a href="{{url('/member/register-dog')}}" class="form-control input-lg btn btn-success btn-sm"><h4 class="text-center">ADD NEW DOG</h4></a>--}}
                </div>
            </div>

            <div class="portlet portlet-default">
                {{--<div class="portlet-header">--}}
                {{--<h3 class="portlet-title"><u>Table Gallery</u></h3>--}}
                {{--</div> <!-- /.portlet-header -->--}}
                <div class="portlet-body">

                    <div class="table-responsive">
                        @if($dogs->count())
                        <table class="table table-striped table-bordered thumbnail-table">
                            <thead>
                            <tr>
                                <th>Name of Dog</th>
                                <th>Breeder</th>
                                <th>Date of Birth</th>
                                <th>Registration Number</th>
                                <th>Actions</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($dogs as $dog)
                            <tr>
                                <td class="valign-middle" style="text-transform: capitalize"><a href="javascript:;" title="">{{strtolower($dog->name) }}</a>
                                </td>

                                <td class="valign-middle">{{$dog->breeder_name}}</td>


                                <td class="valign-middle">{{$dog->dob}}</td>

                                <td class="file-info valign-middle">{{$dog->registration_number}}</td>

                                <td class="text-center valign-middle">
                                    @if($dog->confirmed == true)
                                    <a href="{{url('/member/show',$dog->id)}}" class="btn btn-success btn-sm">View</a>
                                    &nbsp;
                                    <a href="{{url('/member/edit',$dog->id)}}" class="btn btn-success btn-sm">Edit</a>
                                    &nbsp;
                                    @if($dog->delete_request == true)
                                        <em>Delete request pending.</em>
                                        @else
                                    <button  class="btn btn-danger btn-sm" id="request-delete-{{$dog->id}}">Request delete</button>
                                        @endif

                                    <?php $request = \App\RequestedCertificate::where('dog_id',$dog->id)->first(); ?>
                                    @if($request)
                                        @if(\App\RequestedCertificate::where('dog_id',$dog->id)->first()->honoured == 0)
                                            {{--<p>Certificate Request Pending</p>--}}
                                            <button class="btn btn-warning btn-sm">Certificate request pending </button>

                                        @elseif(\App\RequestedCertificate::where('dog_id',$dog->id)->first()->honoured == 1)
                                            <button class="btn btn-success btn-sm">Certficate issued </button>
                                        @endif
                                        @elseif(!$request)
                                        <button id="request-certificate-{{$dog->id}}" class="btn btn-success btn-sm">Request Certificate </button>
                                    @endif

                                    @elseif($dog->confirmed == false)
                                        <p><em>Pending confirmation</em></p>
                                    @endif

                                    {{--<a href="{{url('/member/delete',$dog->id)}}" class="btn btn-success btn-sm">Request Certificate</a>--}}
                                </td>
                            </tr>

                            <div class="modal fade" id="request-certificate-modal-{{$dog->id}}" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel"><strong>REQUEST CERTIFICATE</strong></h4>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{url('member/request-certificate',$dog->id)}}" method="post">
                                                {!! csrf_field() !!}
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <label></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                    <h4>Are you sure you want to request certificate for <br><strong><u>{{strtoupper($dog->name)}}</u> </strong>?</h4>
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-success">Request Certificate</button>
                                                </div>
                                            </form>

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="request-delete-modal-{{$dog->id}}" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel"><strong>REQUEST DELETE OF DOG ( {{$dog->name}} )</strong></h4>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{url('member/request-delete',$dog->id)}}" method="post">
                                                {!! csrf_field() !!}
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <label></label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <h4>Are you sure you want Admin to delete  <br><strong><u>{{strtoupper($dog->name)}}</u> </strong>?</h4>
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-success">Request Delete</button>
                                                </div>
                                            </form>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            @endforeach
                            </tbody>
                        </table>

                    </div> <!-- /.table-responsive -->

                    @else
                        <div style="height: 400px;">
                            <div class="text-center" style="height: 100px; border: 5px inset #cccccc; border-radius: 5px; padding-top : 10px;
                                    background-color: #cccccc; color: white;">
                                <div class="h2">  No registered dogs yet .</div>
                                <div class="h4"><a href="javascript:history.back()"> Back </a></div>
                            </div>
                        </div>
                        @endif

                </div> <!-- /.portlet-body -->

            </div> <!-- /.portlet -->
            <div class="modal fade" id="register-dog-modal" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><strong>ADD NEW DOG</strong></h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{url('member/register-dog')}}" method="post" id="dog-form">
                                {!! csrf_field() !!}
                                <div class="row">
                                    <div class="col-sm-12">

                                        <div class="portlet portlet-boxed portlet-success">

                                            <div class="portlet-header">
                                                <h4 class="portlet-title">
                                                    <u><i class="fa fa-check portlet-icon"></i>New Dog</u>
                                                </h4>
                                            </div> <!-- /.portlet-header -->

                                            <div class="portlet-body">

                                                <form id="demo-validation" action="" class="form parsley-form">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label" for="name">Name</label>
                                                                <input type="text" id="name" name="name" class="form-control" data-parsley-required="true" required minlength="3">
                                                            </div> <!-- /.form-group -->

                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label" for="dob">Date of Birth</label>
                                                                <input type="date" id="dob" name="dob" class="form-control" data-parsley-required="true" required >
                                                            </div> <!-- /.form-group -->

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label" for="breed">Breed</label>
                                                                <select class=" breeders" name="breed">
                                                                    <option></option>
                                                                    @foreach($breeders as $breeder)
                                                                <option value="{{$breeder->id}}">{{$breeder->name}}</option>
                                                                        @endforeach
                                                                </select>
                                                                {{--<input type="text" id="breed" name="breed" class="form-control" data-parsley-required="true" required>--}}
                                                            </div> <!-- /.form-group -->

                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label" for="sex">Sex</label>
                                                                <select class="form-control"  id="sex" name="sex" data-parsley-required="true">
                                                                    <option value=""></option>
                                                                    <option value="male">Male</option>
                                                                    <option value="female">Female</option>
                                                                </select>
                                                            </div> <!-- /.form-group -->

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label" for="father_reg_no">Father's Registration Number</label>
                                                                <input type="text" id="father_reg_no" name="father" class="form-control" data-parsley-required="true">
                                                                <div id="message_two">

                                                                </div>

                                                            </div> <!-- /.form-group -->


                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label" for="mother_reg_no">Mother's Registration Number</label>
                                                                <input type="text" id="mother_reg_no" name="mother" class="form-control" data-parsley-required="true">
                                                            </div> <!-- /.form-group -->

                                                        </div>
                                                    </div>

                                                    {{--<div class="form-group">--}}
                                                        {{--<button type="button" id="add-dog" class="btn btn-default">ADD NEW DOG</button>--}}
                                                    {{--</div> <!-- /.form-group -->--}}

                                                    <div id="message">

                                                    </div>

                                                </form>

                                            </div> <!-- /.portlet-body -->


                                        </div> <!-- /.portlet -->

                                    </div> <!-- /.col -->

                                </div>


                            </form>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" id="add-dog" class="btn btn-success">ADD NEW DOG</button>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="modal fade" id="empty-modal" tabindex="-1" role="dialog" aria-labelledby="empty-ModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel"><strong>ADD NEW DOG</strong></h4>
                            </div>
                            <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-12">

                                            <div class="portlet portlet-boxed portlet-success">

                                                <div class="portlet-header">
                                                    <h4 class="portlet-title">
                                                        <u><i class="fa fa-check portlet-icon"></i></u>
                                                    </h4>
                                                </div> <!-- /.portlet-header -->

                                                <div class="portlet-body">
                                                    {{--<div class="alert alert-danger" role="alert"  style="height: 70px;"><h1 class="text-center"><strong>No Dogs Registered ! </strong></h1> </div>--}}

                                                    {{--<img width="100" height="100" src="{{url('/img/DogWalk.gif')}}"/>--}}
                                                    <div class="text-center">
                                                    <a href="{{url('/member/register-dog')}}" class="btn btn-default btn-lg">ADD NEW DOG</a>

                                                    </div>
                                                </div> <!-- /.portlet-body -->

                                            </div> <!-- /.portlet -->

                                        </div> <!-- /.col -->

                                    </div>

                                {{--<div class="modal-footer">--}}
                                    {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                                    {{--<button type="button" id="add-dog" class="btn btn-success">ADD NEW DOG</button>--}}
                                {{--</div>--}}

                            </div>

                        </div>
                    </div>
                </div>
            <div class="modal fade" id="unconfirmed-modal" tabindex="-1" role="dialog" aria-labelledby="unconfirmed-ModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><strong>DOGS CONFIRMATION STATUS</strong></h4>
                        </div>
                        <div class="modal-body">

                            <em class="text-center">You have {{\App\Dog::where('user_id',Auth::getUser()->id)->where('confirmed',false)->count()}} Dog(s) waiting for confirmation.<br><br>
                                <?php $dogs = \App\Dog::where('user_id',Auth::getUser()->id)->where('confirmed',false)->get();
                                foreach($dogs as $unconfirmed){
                                    echo 'Name : ' . html_entity_decode('<strong>'.$unconfirmed->name .'</strong>.<br>');
                                    echo 'Assigned No. : ' . html_entity_decode('<strong>'.$unconfirmed->registration_number .'</strong>.<br>');

                                }
                                ?>
                            </em>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="modal fade" id="report-modal" tabindex="-1" role="dialog" aria-labelledby="report-ModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><strong>REPORT DUPLICATED REGISTRATION NUMBER</strong></h4>
                        </div>
                        <div class="modal-body">
                            <form id="report-form">
                            <p>We experienced some challenges with dogs registration numbers.
                                Report any of such incident here with the registration number.</p>
                            <label>Registration Number</label>
                            <input type="text" class="form-control" required id="report-input" name="report_registration_number" placeholder="Suspected dog registration number">

                                <div class="report-message h3"></div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-default" id="report">Report</button>
                            </div>
                            </form>

                        </div>

                    </div>
                </div>
            </div>

            <br>

            <br>
            {{--@else--}}
                {{--<div class="alert alert-danger" role="alert"  style="height: 100px;"><h1 class="text-center"><strong>No Dogs Registered or Confirmed Yet !--}}

                            {{--<a href="{{url('/member/register-dog')}}" class="btn btn-success btn-lg">ADD NEW DOG</a> </strong></h1></div>--}}
                {{--@endif--}}
        </div> <!-- /.container -->

    </div> <!-- .content -->

@stop
