@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        jQuery(document).ready(function(){
            $('#add-new-dog').off('click').on('click',function(){
                $('#register-dog-modal').modal();
            });

            $('#add-dog').off('click').on('click',function(){

                $.post("{{url('/member/register-dog')}}",$('#register-dog-modal form').serialize(), function (data) {
                    $('#message').text(data);
                    // $('#register-dog-modal').modal('hide');
                });

            })

            //$('#parentage-table').hide();
            $('#show-parent').on('click',function(){
                $('#parentage-table').show()
            });

        });

    </script>

@section('content')

    <div class="content">

        <div class="container">

            <div class="portlet portlet-default">
                {{--<div class="portlet-header">--}}
                {{--<h3 class="portlet-title"><u>Table Gallery</u></h3>--}}
                {{--</div> <!-- /.portlet-header -->--}}
                <div class="portlet-body">

                    <div class="row">
                        <table class="table-responsive">

                            <table class="table table-striped table-bordered thumbnail-table">
                                <tr>
                                    <td>Name</td>
                                    <td>
                                        {{$dog->name }}
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>image</td>
                                    <td> <figure><img src="/images/catalog/{{$dog->image_name}}" style="width: 200px; height: 150px;"></figure>

                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Breed</td>
                                    <td>{{$dog->breeder_name}}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Sex</td>
                                    <td>{{$dog->sex}}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Registration Number</td>
                                    <td>{{$dog->registration_number}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Microchip Number</td>
                                    <td>{{$dog->microchip_number}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Colour</td>
                                    <td>{{$dog->colour}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Height</td>

                                    <td>  @if($dog->height == 0 )
                                            <em> height not set</em>
                                        @else
                                            {{$dog->height}}
                                        @endif
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Tattoo Number</td>
                                    <td>{{$dog->tattoo_number}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>DNA ID</td>
                                    <td>{{$dog->DNA}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Elbow ED Results</td>
                                    <td>{{$dog->elbow_ed_results}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Appraisal Score</td>
                                    <td>
                                        @if($dog->appraisal_score == 0)
                                            <em>Appraisal score not set</em>
                                        @else
                                            {{$dog->appraisal_score}}
                                        @endif
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Other Health Checks</td>
                                    <td>{{$dog->other_health_checks}}</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Titles</td>
                                    <td>{{$dog->titles}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Performance Titles</td>
                                    <td>{{$dog->performance_titles}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Registered Date</td>
                                    <td>{{$dog->created_at}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>Status</td>
                                    @if($dog->dead == true)
                                        <strong>  <td>{{'Dead'}}</td></strong>
                                        <td></td>
                                    @else
                                        <strong> <td>{{'Alive'}}</td></strong>
                                        <td></td>
                                    @endif

                                </tr>


                                <tr>
                                    <td>Other Certifications</td>
                                    <td>{{$dog->other_certifications}}</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td class="text-center"><h3></h3></td>
                                    <td>

                                    </td>
                                </tr>
                    </div>


                    </table>
                    {{--<h3 class="text-center"> <button class="btn btn-success" id="show-parent">Show Parentage</button></h3>--}}
                    @if($dog->father || $dog->mother)
                        <?php
                        $father_id = \App\Dog::getDogId($dog->father);
                        $mother_id = \App\Dog::getDogId($dog->mother);
                        ?>

                        <h1 class="text-center">
                            <a class="btn btn-success btn-lg" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                Parentage Table
                            </a>

                        </h1>
                        <div class="collapse" id="collapseExample">
                            <div>
                                @include('partials.parentage_table_no_images')
                                @else
                                    <div class="text-center"> <h3>No Parents Data Yet !</h3> </div>
                                @endif
                            </div>
                        </div>
                </div>
            </div>

        </div> <!-- /.table-responsive -->

    </div> <!-- /.portlet-body -->

    </div> <!-- /.portlet -->
    <br>

    </div> <!-- /.container -->

    </div> <!-- .content -->

@stop