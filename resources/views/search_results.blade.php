@extends('layouts.version2_master')

@section('styles')
        <style>
            /*@import "http://fonts.googleapis.com/css?family=Roboto:300,400,500,700";*/

            /*.container { margin-top: 20px; }*/
            /*.mb20 { margin-bottom: 20px; }*/

            /*hgroup { padding-left: 15px; border-bottom: 1px solid #ccc; }*/
            /*hgroup h1 { font: 500 normal 1.625em "Roboto",Arial,Verdana,sans-serif; color: #2a3644; margin-top: 0; line-height: 1.15; }*/
            /*hgroup h2.lead { font: normal normal 1.125em "Roboto",Arial,Verdana,sans-serif; color: #2a3644; margin: 0; padding-bottom: 10px; }*/

            /*.search-result .thumbnail { border-radius: 0 !important; }*/
            /*.search-result:first-child { margin-top: 0 !important; }*/
            /*.search-result { margin-top: 20px; }*/
            /*.search-result .col-md-2 { border-right: 1px dotted #ccc; min-height: 140px; }*/
            /*.search-result ul { padding-left: 0 !important; list-style: none;  }*/
            /*.search-result ul li { font: 400 normal .85em "Roboto",Arial,Verdana,sans-serif;  line-height: 30px; }*/
            /*.search-result ul li i { padding-right: 5px; }*/
            /*.search-result .col-md-7 { position: relative; }*/
            /*.search-result h3 { font: 500 normal 1.375em "Roboto",Arial,Verdana,sans-serif; margin-top: 0 !important; margin-bottom: 10px !important; }*/
            /*.search-result h3 > a, .search-result i { color: #248dc1 !important; }*/
            /*.search-result p { font: normal normal 1.125em "Roboto",Arial,Verdana,sans-serif; }*/
            /*.search-result span.plus { position: absolute; right: 0; top: 126px; }*/
            /*.search-result span.plus a { background-color: #248dc1; padding: 5px 5px 3px 5px; }*/
            /*.search-result span.plus a:hover { background-color: #414141; }*/
            /*.search-result span.plus a i { color: #fff !important; }*/
            /*.search-result span.border { display: block; width: 97%; margin: 0 15px; border-bottom: 1px dotted #ccc; }*/



            #custom-search-input .fa-search{
                font-size: 20px;
            }


            .pager li>a {
                padding: 5px 10px;
                background-color: grey;
                color: #fff;
            }

            ul > li.active{
                background-color: green;
                color: green
            }

            .activeLink {
                background-color: #BA0100;
            }

            .pager li>a:hover {
                padding: 5px 10px;
                background-color: #BA0100;
                color: #fff;
            }

            .pager li:nth-child(10)>a {
                padding: 5px 13px !important;
            }


            .card .content {
                position: relative;
                margin: 15px;
                z-index: 100;
                text-align:left;
            }

            .card .avatar {
                position: relative;
                margin:15px 15px 5px 15px;
                z-index: 100;
                float:left;
            }
            .profileImg
            {

            }

            .card .avatar img {
                width:80px ;
                height:80px;
                -webkit-border-radius: 50%;
                -moz-border-radius: 50%;
                border-radius: 50%;
            }

            .pager li {
                margin-left: 5px !important;
            }

            .table-user-information {
                text-align: left;
                font-size: 12px;
            }

            .displayProfiles
            {
                background-color: #fff;
                /*box-shadow: 3px 1px 8px rgba(0,0,0,0.2) !important;*/
                background-image: none !important;

            }

            .personName {
                text-transform: uppercase;
                font-weight: bold;
                font-family: sans-serif;
                color: #BA0100;
                letter-spacing: 1px;
            }
            .arrow
            {
                margin-top:25px;
            }
            .profInfo
            {
                font-size:12px;
                letter-spacing:1px;
            }
        </style>
@endsection

@section('content')

<!-- breadcrumbs -->
<section class="breadcrumbs"
        {{--style="background-image: url(/kug_version2/frontend/assets/images/breadcrumbs/best-room.jpg)"--}}
         style="background-color: #000000"
>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="h1"> Search results</h1>
            </div>
            <div class="col-md-6">
                <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">Home</a><i class="fa fa-angle-right"></i></li>
                    {{--<li><a href="#">Rooms</a><i class="fa fa-angle-right"></i></li>--}}
                    <li class="active">Search results</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- /breadcrumbs -->
<!-- room details-->
<section class="room-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <form action="{{url('/search-results')}}">
                    <h2 class="text-center">Search</h2>
                    <div id="custom-search-input">
                        <div class="input-group col-md-12">
                            <input type="text" class="form-control input-lg" name="name" value="{{Request::get('name')}}" placeholder="Search by name, registration number, breed, tattoo number" />
                    <span class="input-group-btn">
                        <button class="btn btn-success btn-lg">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <div class="container">

        <br>
        <br>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="container">

                <hgroup class="mb20">
                    <h1 class="text-center">Search Results</h1>
                    <h2 class="lead h2"><strong class="text-danger text-center">{{$dogs->total()}}</strong> results were found for the search for
                        <strong class="text-danger">{{Request::get('name')}}</strong></h2>
                </hgroup>

                <h3 class="text-center">Page {{Request::has('page')?Request::query('page'):1}}</h3><br>

                <div class="best-room-carousel">
                    <ul class="row best-room_ul">
                        @foreach($dogs as $dog)
                            <li class="col-lg-3 col-md-4 col-sm-6 col-xs-12 best-room_li">
                                <div class="best-room_img">
                                    <a href="#">
                                        <?php
                                        $image = File::exists("/images/catalog/$dog->image_name")? "/images/catalog/$dog->image_name":"http://placehold.it/300x200/cccccc/000000?text=no+image+uploaded";
                                        ?>
                                        {{--                                <img src="{{asset('kug_version2/frontend/assets/images/best-rooms/1.jpg')}}" alt=""/>--}}
{{--                                        <img src="{{$dog->image_name == null ? url('http://placehold.it/300x200/cccccc/000000?text=no+image+uploaded'):"/images/catalog/$dog->image_name"}}" alt=""/>--}}
                                        <img src="{{$dog->image_name == null ? url('http://placehold.it/300x200/cccccc/000000?text=no+image+uploaded'):$image}}"  />
                                    </a>
                                    <div class="best-room_overlay">
                                        <div class="overlay_icn">
                                            <a href="{{url('/show-dog',$dog->id)}}"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="best-room-info" style="font-size: 10px; border: 1px solid #ccc; border-radius: 4px; border-top: 0;">
                                    {{--                            <div class="best-room_t"><a href="best-rooms-detail.html">{{ str_word_count($dog->name). substr($dog->name, 0, 10)}}</a></div>--}}
                                    <div class="best-room_t" style="font-size: 10px;">
                                        {{--<a href="{{url('/show-dog',$dog->id)}}">{{ explode(' ',strtoupper($dog->name))[0].' '.explode(' ',strtoupper($dog->name))[1]}}...</a>--}}
                                        <a href="{{url('/show-dog',$dog->id)}}">{{substr($dog->name,0,10)}}...</a>
                                    </div>
                                    <div class="best-room_desc">
                                        {{--Difficulty on insensible reasonable in. From as went he they. Preference themselves me as thoroughly partiality considered.--}}
                                    </div>
                                    <div class="best-room_price">
                                        <span><i class="fa fa-clock-o"></i> </span>
{{--                                        {{getDogRegisteredDay($dog->created_at)}}--}}
                                        <span><a href="{{url('/show-dog',$dog->id)}}" class="btn btn-success">View</a></span>
                                    </div>
                                </div>
                            </li>
                        @endforeach

                    </ul>
                </div>


                <div class="text-center">

                    <?php
                    if(isset($paginator)){ ?>
                    {{--{!! $paginator->render() !!}--}}
                    {!! $paginator->appends(
                    ['name' => Request::get('name'),'breed'=>Request::get('breed'),
                     'sex'=>Request::get('sex'),'colour'=>Request::get('colour')
                                            ])->render()
                    !!}

                    <?php   }else {?>
                    {!! $dogs->render() !!}
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
