
<html>
<head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.js')}}"></script>

    <style>
        .hide-column{
            display: none;
        }
        .water-mark{
            z-index: 20000;
            position: absolute;
            top: 450px;
            left: 320px;
            font-weight: bold;
            font-size: 60px;
            opacity: 0;
            -ms-transform: rotate(45deg); /* IE 9 */
            -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
            transform: rotate(45deg);
        }

        .first{
            border: 1px solid #000000;
            height: 50px;
            width: 90%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }

        .second{
            border: 1px solid #000000;
            height: 50px;
            width: 100%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }
        .third{
            border: 1px solid #000000;
            height: 50px;
            width: 100%;
            margin: auto;
            background-color: #ffffff;
            border-top: 0;
            border-bottom: 0;
        }

        .fourth{
            border: 1px solid #000000;
            height: 50px;
            width: 95%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }

        .table-container{
            border: 1px solid #000;
            /*border: 25px solid transparent;*/
            /*padding: 15px;*/
            /*border-image-source: url(/img/main-2.png);*/
            /*border-image-repeat: round;*/
            /*border-image-slice: 85;*/
        }

        /*hr {*/
        /*padding: 0;*/
        /*border: none;*/
        /*border-top: medium double #333;*/
        /*color: #333;*/
        /*text-align: center;*/
        /*width: 95%;*/
        /*margin-left: 0px;*/
        /*}*/
        /*hr:after {*/
        /*/!*content: "�";*!/*/
        /*display: inline-block;*/
        /*position: relative;*/
        /*top: -0.7em;*/
        /*font-size: 1.5em;*/
        /*padding: 0 0.25em;*/
        /*background: white;*/
        /*}*/
        .male {
            /*background-color: rgba(191,133,10,1);*/
            /*background-color: #FFC86E;*/
            /*background-color: rgba(255, 200, 110,1);*/
            background-color:rgb(172, 209, 165);
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        .female {
            background-color: #FFFFFF;
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        table,.dog-info{
            margin:auto;
        }

        .dog-info{
            width: 94%;
        }

        hr{
            width: 94%;
            margin:auto;
        }


    </style>

</head>


    <div class="content">

        <div class="container-fluid">

            <div class="portlet portlet-default">



                <div class="portlet-body main-table">

                    <!-- Begin Pedigree Table -->

                    <div class="table-container">

                        <?php
                        $dob = Carbon\Carbon::createFromFormat('Y-m-d',$dog->dob);
                        $difference = $dob->diffInMonths(Carbon\Carbon::now());
                        $certificate = $difference > 12 ? 'PEDIGREE ' : 'BIRTH ';
                        ?>

                        <table class="table">
                            {{--<tr>--}}
                                {{--<td><img src="{{url('/img/logo_01.png')}}"></td>--}}
                                {{--<td> <h3 class="text-center" style="margin-top: 6px; margin-bottom: 10px;">KENNEL UNION OF GHANA {{$certificate}} CERTIFICATE</h3></td>--}}
                                {{--<td><img src="{{url('/img/logo_01.png')}}"></td>--}}
                            {{--</tr>--}}

{{--                            <tr>--}}
{{--                                <td>--}}
{{--                                    @if($dog->breed == 'German Shepherd Dog' )--}}
{{--                                        <img width="85" height="85" style="border-radius: 50%" src="{{url('/img/shepherd_logo.jpg')}}">--}}
{{--                                    @elseif($dog->breed == 'Rottweiler')--}}
{{--                                        <img width="85" height="85" style="border-radius: 50%" src="{{url('/img/rottweiler_logo.jpg')}}">--}}
{{--                                    @else--}}
{{--                                        <img width="85" height="85" style="border-radius: 50%; position: relative; left: 45px" src="{{url('/img/kug_logo.jpg')}}">--}}
{{--                                    @endif--}}
{{--                                </td>--}}
{{--                                <td> <h3 class="text-center" style="margin-top: 6px; margin-bottom: 10px;">KENNEL UNION OF GHANA {{$certificate}} CERTIFICATE</h3></td>--}}
{{--                                <td>--}}
{{--                                    <img width="85" height="85" style="border-radius: 50%; position: relative; left: 45px" src="{{url('/img/kug_logo.jpg')}}">--}}
{{--                                </td>--}}
{{--                            </tr>--}}

                        </table>

                        <div class="header">
                            {{--<h3 class="text-center" style="margin-top: 6px; margin-bottom: 10px;">KENNEL UNION OF GHANA  CERTIFIED {{$certificate}} CERTIFICATE</h3><br>--}}
                            {{--<h3 class="text-center">CERTIFIED PEDIGREE </h3>--}}
                        </div>
                        {{--<div class="print"><button id="print" class="btn btn-success pull-right" style="margin-top: -25px;" onclick="window.print()">PRINT CERTIFICATE</button></div><br>--}}
                        {{--                <div class="print"><a href="{{url('admin/pdf',$dog->id)}}" class="btn btn-success pull-right" style="margin-top: -1px;" >GENERATE PDF</a></div><br>--}}

                        <div class="water-mark">
                            KENNEL UNION OF GHANA
                        </div>

{{--                            <img width="50" height="50" style="border-radius: 50%;" src="{{url('/img/kug_logo.jpg')}}">--}}
                            <img width="60" height="60" style="border-radius: 50%; position: absolute; top: 15px; left: 5%;" src="{{url('/img/kug_logo.jpg')}}">


                            <div class="row dog-info" style="color:#283846">

                            <div class="row first" style="margin-bottom: 10px;" >
                                <table class="table" style="border-right: 1px solid #000;">
                                    <tr>
                                        <td>Name: {{$dog->name}}</td>
                                        <td > GH {{$dog->registration_number}}</td>
                                    </tr>
                                </table>

                            </div>

{{--                            <img width="50" height="50" style="border-radius: 50%;" src="{{url('/img/kug_logo.jpg')}}">--}}

                            <div class="clearfix"></div>
                                <img width="60" height="60" style="border-radius: 50%; position: absolute; top: 15px; left: 92%;" src="{{url('/img/kug_logo.jpg')}}">

                                <div class="row second">

                                <table class="table ">
                                    <?php
                                    $kennel_name = $dog->kennel_name != null?"(".$dog->kennel_name.")": "";
                                    ?>
                                    <tr>
                                        <td>BREEDER : {{ strtoupper($dog->first_name.' '. $dog->last_name).$kennel_name}}</td>
                                        <td>COAT : {{$dog->coat}}</td>
                                        <td>DOB : {{$dog->dob}}</td>
                                    </tr>

                                </table>

                            </div>

                            <div class="row third">
                                <table class="table">
                                    <tr>
                                        <td>BREED : {{$dog->breed}} </td>
                                        <td>COLOUR : {{$dog->colour}}</td>
                                        <td>SEX : {{$dog->sex}}</td>
                                    </tr>

                                </table>

                            </div>

                            <div class="row second">
                                <div class="col-md-12  col-xs-hidden" style="padding-top: 13px;">

                                    @if(Request::query('name'))
                                    <div class="col-md-4 col-sm-4 col-xs-4">OWNER: {{Request::query('name')}} </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4"> {{Request::query('phone')}} </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">{{Request::query('address')}} </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div>
                            {{--<img class="image_one" src="{{url('/img/logo_01.png')}}"> <img src="{{url('/img/logo_01.png')}}" class="image_two">--}}
                            {{--<button class="btn btn-success" id="print" onclick="window.print()">Print</button>--}}

                            <hr>
                            <br>



                            <table border="0" cellpadding="2" width='95%'>
                                <tr><td>
                                        <table style="border: 1px solid #000000; border-collapse: collapse;" border="1" cellpadding="2" cellspacing="2" width="100%"  CELLSPACING="2">
                                            <tr>
                                                <th style="text-align: center;">I Parents</th>
                                                <th style="text-align: center;">II Grand Parents</th>
                                                <th style="text-align: center;">III Great Grand Parents</th>
                                                <th style="text-align: center;">IV Great Great Grand Parents</th>
                                                {{--<th
                                                 style="text-align: center;">V</th>--}}

                                            </tr>
                                            <tr>
                                                <td rowspan='16' width='17%' class='male'>
                                                    <div class="h5">
                                                        @if ($dog->father)
                                                            {{\App\Dog::getRelationship($dog->father)}}<br>
                                                            {{$dog->father}}
{{--                                                            <br> {{$dog->hip_hd_results}}--}}
{{--                                                            <br> {{$dog->elbow_hd_results}}--}}

                                                        @endif

                                                    </div>
                                                </td>
                                                <td rowspan='8' width='17%' class='male'>
                                                    <div class="h5">
                                                        {{\App\Dog::getParent($dog->father,'father')}}<br/>
                                                        {{$secondgen = \App\Dog::getParentId($dog->father,'father','registration_number')}}

                                                    </div>
                                                </td>
                                                <td rowspan='4' width='17%' class='male'>
                                                    <div class="h5">
                                                        {{\App\Dog::getParent($secondgen,'father')}}<br/>
                                                        {{$third_generation = \App\Dog::getParentId($secondgen,'father','registration_number')}}

                                                    </div>
                                                </td>
                                                <td rowspan='2' width='17%' class='male'>
                                                    <div class="h5">
                                                        {{\App\Dog::getParent($third_generation,'father')}}<br/>
                                                        {{$fifth_generation = \App\Dog::getParentId($third_generation,'father','registration_number')}}

                                                    </div>
                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp;</td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                                            </tr><tr>
                                                <td rowspan='2' width='17%' class='female'>
                                                    <div class="h5">

                                                        {{\App\Dog::getParent($third_generation,'mother')}}<br/>
                                                        {{$generation_five = \App\Dog::getParentId($third_generation,'mother','registration_number')}}
                                                    </div>
                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='4' width='17%' class='female'>
                                                    <div class="h5">

                                                        {{\App\Dog::getParent($secondgen,'mother')}}<br/>
                                                        {{$fourth_generation = \App\Dog::getParentId($secondgen,'mother','registration_number')}}
                                                    </div>

                                                </td>
                                                <td rowspan='2' width='17%' class='male'>
                                                    <div class="h5">

                                                        {{\App\Dog::getParent($fourth_generation,'father')}}<br/>
                                                        {{$generation_five_b = \App\Dog::getParentId($fourth_generation,'father','registration_number')}}
                                                    </div>

                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='2' width='17%' class='female'>
                                                    <div class="h5">
                                                        {{\App\Dog::getParent($fourth_generation,'mother')}}<br>
                                                        {{$generation_five_c = \App\Dog::getParentId($fourth_generation,'mother','registration_number')}}
                                                    </div>

                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='8' width='17%' class='female'>
                                                    <div class="h5">

                                                        {{\App\Dog::getParent($dog->father,'mother')}} <br>
                                                        {{$thirdgen_mother = \App\Dog::getParentId($dog->father,'mother','registration_number')}}
                                                    </div>
                                                </td>
                                                <td rowspan='4' width='17%' class='male'>
                                                    <div class="h5">
                                                        {{\App\Dog::getParent($thirdgen_mother,'father')}}<br>
                                                        {{$fourth_gen = \App\Dog::getParentId($thirdgen_mother,'father','registration_number')}}
                                                    </div>
                                                </td>
                                                <td rowspan='2' width='17%' class='male'>
                                                    <div class="h5">{{\App\Dog::getParent($fourth_gen,'father')}}<br/>
                                                        {{$generation_five_d = \App\Dog::getParentId($fourth_gen,'father','registration_number')}}
                                                    </div>

                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='2' width='17%' class='female'>
                                                    <div class="h5">{{\App\Dog::getParent($fourth_gen,'mother')}}<br/>
                                                        {{$generation_five_e = \App\Dog::getParentId($fourth_gen,'mother','registration_number')}}
                                                    </div>


                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp;</td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='4' width='17%' class='female'>
                                                    <div class="h5">{{\App\Dog::getParent($thirdgen_mother,'mother')}}<br/>
                                                        {{$generation_four = \App\Dog::getParentId($thirdgen_mother,'mother','registration_number')}}
                                                    </div>

                                                </td>
                                                <td rowspan='2' width='17%' class='male'>
                                                    <div class="h5">{{\App\Dog::getParent($generation_four,'father')}}<br/>
                                                        {{$generation_five_f = \App\Dog::getParentId($generation_four,'father','registration_number')}}

                                                    </div>

                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='2' width='17%' class='female'>
                                                    <div class="h5">
                                                        {{\App\Dog::getParent($generation_four,'mother')}}<br/>
                                                        {{$generation_five_g = \App\Dog::getParentId($generation_four,'mother','registration_number')}}
                                                    </div>
                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                                            </tr><tr>
                                                <td rowspan='16' width='17%' class='female' >

                                                    <div class="h5">

                                                        @if ($dog->mother)
                                                            {{\App\Dog::getRelationship($dog->mother)}}<br/>
                                                            {{$dog->mother}}
                                                        @endif
                                                    </div>
                                                </td>
                                                <td rowspan='8' width='17%' class='male'>
                                                    <div class="h5">

                                                        {{\App\Dog::getParent($dog->mother,'father')}}<br/>
                                                        {{$generation_two_a = \App\Dog::getParentId($dog->mother,'father','registration_number')}}
                                                    </div>
                                                </td>
                                                <td rowspan='4' width='17%' class='male'>
                                                    <div class="h5">

                                                        {{\App\Dog::getParent($generation_two_a,'father')}}<br/>
                                                        {{ $generation_two_c = \App\Dog::getParentId($generation_two_a,'father','registration_number')}}
                                                    </div>

                                                </td>
                                                <td rowspan='2' width='17%' class='male'>
                                                    <div class="h5">{{\App\Dog::getParent($generation_two_c,'father')}}<br/>
                                                        {{$generation_4_d =  \App\Dog::getParentId($generation_two_c,'father','registration_number')}}
                                                    </div>

                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='2' width='17%' class='female'>
                                                    <div class="h5">{{\App\Dog::getParent($generation_two_c,'mother')}}<br/>
                                                        {{$generation_5_a =  \App\Dog::getParentId($generation_two_c,'mother','registration_number')}}
                                                    </div>

                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='4' width='17%' class='female'>
                                                    <div class="h5">{{\App\Dog::getParent($generation_two_a,'mother')}}<br/>
                                                        {{$generation_3_a =  \App\Dog::getParentId($generation_two_a,'mother','registration_number')}}
                                                    </div>
                                                </td>
                                                <td rowspan='2' width='17%' class='male'>

                                                    <div class="h5">{{\App\Dog::getParent($generation_3_a,'father')}}<br/>
                                                        {{$generation_5_e = \App\Dog::getParentId($generation_3_a,'father','registration_number')}}
                                                    </div>

                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='2' width='17%' class='female'>
                                                    <div class="h5">{{\App\Dog::getParent($generation_3_a,'mother')}}<br/>
                                                        {{$generation_5_f = \App\Dog::getParentId($generation_3_a,'mother','registration_number')}}
                                                    </div>
                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='8' width='17%' class='female'>
                                                    <div class="h5">
                                                        {{\App\Dog::getParent($dog->mother,'mother')}}<br/>
                                                        {{$generation_two_b = \App\Dog::getParentId($dog->mother,'mother','registration_number')}}
                                                    </div>
                                                </td>
                                                <td rowspan='4' width='17%' class='male'>
                                                    <div class="h5">
                                                        {{\App\Dog::getParent($generation_two_b,'father')}}<br/>
                                                        {{$generation_3_c = \App\Dog::getParentId($generation_two_b,'father','registration_number')}}
                                                    </div>

                                                </td>
                                                <td rowspan='2' width='17%' class='male'>

                                                    <div class="h5">{{\App\Dog::getParent($generation_3_c,'father')}}<br/>
                                                        {{$generation_5_h = \App\Dog::getParentId($generation_3_c,'father','registration_number')}}
                                                    </div>
                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr>

                                            <tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr>

                                            <tr>
                                                <td rowspan='2' width='17%' class='female'>
                                                    <div class="h5">{{\App\Dog::getParent($generation_3_c,'mother')}}<br/>
                                                        {{$generation_5_i= \App\Dog::getParentId($generation_3_c,'mother','registration_number')}}
                                                    </div>
                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr>

                                            <tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr>

                                            <tr>
                                                <td rowspan='4' width='17%' class='female'>
                                                    <div class="h5">
                                                        {{\App\Dog::getParent($generation_two_b,'mother')}}<br/>
                                                        {{$generation_4_b = \App\Dog::getParentId($generation_two_b,'mother','registration_number')}}
                                                    </div>
                                                </td>

                                                <td rowspan='2' width='17%' class='male'>
                                                    <div class="h5">{{\App\Dog::getParent($generation_4_b,'father')}}<br/>
                                                        {{$generation_5_j =\App\Dog::getParentId($generation_4_b,'father','registration_number')}}lll
                                                    </div>
                                                </td>

                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr>

{{--                                            <tr>--}}
{{--                                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>--}}
{{--                                                    <td></td>--}}
{{--                                                <td></td>--}}
{{--                                            </tr>--}}

                                            <tr>
                                                <td rowspan='2' width='17%' class='female hide-column'>
                                                    <div class="h5">{{\App\Dog::getParent($generation_4_b,'mother')}} <br/>
                                                        {{$generation_5_k = \App\Dog::getParentId($generation_4_b,'mother','registration_number')}}
                                                    </div>
                                                </td>

                                                <td rowspan='1' class="hide-column"  width='17%' class='male'>&nbsp; </td>
                                            </tr>

                                            {{--<tr>--}}
                                            {{--<td  style="display: none;"rowspan='1' width='17%' class='female'>&nbsp; 62</td>--}}
                                            {{--</tr>--}}
{{--                                            <tr>--}}
                                        </table>
                                    </td>
                                    {{--<tr>--}}
                                    {{--<tr><td align='right' valign='top' style='padding-top: 0px margin-top: 0px; font-size:8pt; font-family: Arial'><a href='http://www.kennelunionghana.com/'>Pedigree</a> generated by Kennel Union of Ghana Pedigree Database--}}
                                    {{--</td><tr>--}}
                            </table>


                            <div class="row fourth">

                                <table class="table table-bordered">
                                    <tr>
                                        <td style="padding-top: 20px">Microchip/Tattoo: {{$dog->microchip_number}}</td>
                                        <td style="padding-top: 20px">Date of Issue:<em> <?php echo date('d-m-y'); ?> </em></td>
                                        <td>Certified : <img width="80px" height="50px" src="{{asset("images/kug_signature.png")}}" alt=""/>
                                        </td>
                                    </tr>

                                </table>

                            </div>

                            <div class="row" style="padding-top: 30px;">
                                <div class="col-md-3">

                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- End Pedigree Table -->

{{--                    </table>--}}
                    {{--<h3 class="text-center"> <button class="btn btn-success" id="show-parent">Show Parentage</button></h3>--}}
                    @if($dog->father || $dog->mother)
                        <?php
                        $father_id = \App\Dog::getDogId($dog->father);
                        $mother_id = \App\Dog::getDogId($dog->mother);
                        ?>
                    @endif

                </div>



            </div>

        </div> <!-- /.table-responsive -->

    </div> <!-- /.portlet-body -->
</html>

