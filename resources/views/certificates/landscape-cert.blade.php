
<html>
<head>

    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    {{--<script src="{{asset('js/jquery.validate.js')}}"></script>--}}

    <style>

        body{
            width: 100%;
        }
        .hide-column{
            display: none;
        }
        .water-mark{
            z-index: 20000;
            position: absolute;
            top: 450px;
            left: 320px;
            font-weight: bold;
            font-size: 60px;
            opacity: 0;
            -ms-transform: rotate(45deg); /* IE 9 */
            -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
            transform: rotate(45deg);
        }

        .first{
            border: 1px solid #000000;
            height: 50px;
            width: 90%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }

        .second{
            border: 1px solid #000000;
            height: 50px;
            width: 100%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }
        .third{
            border: 1px solid #000000;
            height: 50px;
            width: 100%;
            margin: auto;
            background-color: #ffffff;
            border-top: 0;
            border-bottom: 0;
        }

        .fourth{
            /*border: 1px solid #000000;*/
            height: 50px;
            width: 95%;
            margin: auto;
            /*background-color: rgb(172, 209, 165);*/
        }

        .table-container{
            border: 25px solid transparent;
            /*padding: 15px;*/
            border-image-source: url(/img/main-2.png);
            border-image-repeat: round;
            border-image-slice: 85;
        }

        .male {
            /*background-color: rgba(191,133,10,1);*/
            /*background-color: #FFC86E;*/
            /*background-color: rgba(255, 200, 110,1);*/
            background-color:rgb(172, 209, 165);
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        .female {
            background-color: #FFFFFF;
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        table,.dog-info{
            margin:auto;
        }

        .dog-info{
            width: 94%;
        }

        hr{
            width: 80%;
            margin:auto;
        }

        .image_one{
            background-image: url("/img/logo_01.png");
            background-repeat: none;
        }


        table, td, th {
            /*border: 1px solid #ddd;*/
            border: 1px solid #000;
            border-radius: 7px;
            /*background-color:rgb(172, 209, 165);*/
            text-align: left;
        }


         /*td, th {*/
            /*border: 1px solid #ddd;*/
            /*border: 1px solid #000;*/
            /*background-color:rgb(172, 209, 165);*/
            /*text-align: left;*/
        /*}*/

         #second_row table td{
             background-color: #ffffff;
         }

        table {
            /*border-collapse: collapse;*/
            width: 100%;
        }

        th, td {
            padding: 12px;
        }

        #logo_1{
            background-image: url("{{asset('/img/logo_01.png')}}");
            /*background-repeat: none;*/
        }

        /*tr:nth-child(even) {background-color: #ffffff}*/
    </style>

</head>


<div class="content">

    <div class="container-fluid">

        <div class="portlet portlet-default">



            <div class="portlet-body main-table">

                <!-- Begin Pedigree Table -->

                <div class="table-container">

                    <?php
                    $dob = Carbon\Carbon::createFromFormat('Y-m-d',$dog->dob);
                    $difference = $dob->diffInMonths(Carbon\Carbon::now());
                    $certificate = $difference > 12 ? 'PEDIGREE ' : 'BIRTH ';
                    ?>

                    <h3 style="text-align:center;"><span id="logo_1"></span>KENNEL UNION OF GHANA {{$certificate}} CERTIFICATE </h3>

                    {{--<table class="table">--}}
                        {{--<tr>--}}
                            {{--<td class="image_one"></td>--}}
                            {{--<td> <h3 class="text-center" style="margin-top: 6px; margin-bottom: 10px;">KENNEL UNION OF GHANA {{$certificate}} CERTIFICATE</h3></td>--}}
                            {{--<td><img src="{{url('/img/logo_01.png')}}"></td>--}}
                        {{--</tr>--}}

                    {{--</table>--}}

                    {{--<div class="print"><button id="print" class="btn btn-success pull-right" style="margin-top: -25px;" onclick="window.print()">PRINT CERTIFICATE</button></div><br>--}}
                    {{--                <div class="print"><a href="{{url('admin/pdf',$dog->id)}}" class="btn btn-success pull-right" style="margin-top: -1px;" >GENERATE PDF</a></div><br>--}}
                    <div>
                        {{--<img class="image_one" src="{{url('/img/logo_01.png')}}"> <img src="{{url('/img/logo_01.png')}}" class="image_two">--}}
                        {{--<button class="btn btn-success" id="print" onclick="window.print()">Print</button>--}}

                        <hr>
                        <br>

                        <table class="info-table" width="80%">
                            <tr style="background-color:rgb(172, 209, 165);">
                                <th>Dog Name: {{$dog->name}}</th>
                                <th> REG ID: {{$dog->registration_number}}</th>
                            </tr>
                        </table>
                        <table class="info-table" width="95%">
                            <tr style="background-color:rgb(172, 209, 165);">
                                <td>Breeder : {{ strtoupper($dog->first_name.' '. $dog->last_name)}}</td>
                                <td>Coat : {{$dog->coat}}</td>
                                <th>DOB : {{$dog->dob}}</th>
                            </tr>

                            <tr id="second_row">
                                <td>Breed : {{$dog->breed}}</td>
                                <td>Colour : {{$dog->colour}}</td>
                                <td>Sex : {{$dog->sex}}</td>
                            </tr>
                            <tr  style="background-color:rgb(172, 209, 165);">
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>

                        <br>
                        <table border="0" cellpadding="2" width='100%'>
                            <tr><td>
                                    <table style="border: 1px solid #000000; border-collapse: collapse;" border="1" cellpadding="2" cellspacing="2" width="100%"  CELLSPACING="2">
                                        <tr>
                                            <th style="text-align: center;">I</th>
                                            <th style="text-align: center;">II</th>
                                            <th style="text-align: center;">III</th>
                                            <th style="text-align: center;">IV</th>
                                            {{--<th style="text-align: center;">V</th>--}}

                                        </tr>
                                        <tr>
                                            <td rowspan='16' width='17%' class='male'>
                                                <div class="h5">
                                                    @if ($dog->father)
                                                        {{\App\Dog::getRelationship($dog->father)}}<br>
                                                        {{$dog->father}}
                                                    @endif

                                                </div>
                                            </td>
                                            <td rowspan='8' width='17%' class='male'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($dog->father,'father')}}<br/>
                                                    {{$secondgen = \App\Dog::getParentId($dog->father,'father','registration_number')}}

                                                </div>
                                            </td>
                                            <td rowspan='4' width='17%' class='male'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($secondgen,'father')}}<br/>
                                                    {{$third_generation = \App\Dog::getParentId($secondgen,'father','registration_number')}}

                                                </div>
                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($third_generation,'father')}}<br/>
                                                    {{$fifth_generation = \App\Dog::getParentId($third_generation,'father','registration_number')}}

                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">

                                                    {{\App\Dog::getParent($third_generation,'mother')}}<br/>
                                                    {{$generation_five = \App\Dog::getParentId($third_generation,'mother','registration_number')}}
                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='4' width='17%' class='female'>
                                                <div class="h5">

                                                    {{\App\Dog::getParent($secondgen,'mother')}}<br/>
                                                    {{$fourth_generation = \App\Dog::getParentId($secondgen,'mother','registration_number')}}
                                                </div>

                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h5">

                                                    {{\App\Dog::getParent($fourth_generation,'father')}}<br/>
                                                    {{$generation_five_b = \App\Dog::getParentId($fourth_generation,'father','registration_number')}}
                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($fourth_generation,'mother')}}<br>
                                                    {{$generation_five_c = \App\Dog::getParentId($fourth_generation,'mother','registration_number')}}
                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='8' width='17%' class='female'>
                                                <div class="h5">

                                                    {{\App\Dog::getParent($dog->father,'mother')}} <br>
                                                    {{$thirdgen_mother = \App\Dog::getParentId($dog->father,'mother','registration_number')}}
                                                </div>
                                            </td>
                                            <td rowspan='4' width='17%' class='male'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($thirdgen_mother,'father')}}<br>
                                                    {{$fourth_gen = \App\Dog::getParentId($thirdgen_mother,'father','registration_number')}}
                                                </div>
                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h5">{{\App\Dog::getParent($fourth_gen,'father')}}<br/>
                                                    {{$generation_five_d = \App\Dog::getParentId($fourth_gen,'father','registration_number')}}
                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">{{\App\Dog::getParent($fourth_gen,'mother')}}<br/>
                                                    {{$generation_five_e = \App\Dog::getParentId($fourth_gen,'mother','registration_number')}}
                                                </div>


                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp;</td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='4' width='17%' class='female'>
                                                <div class="h5">{{\App\Dog::getParent($thirdgen_mother,'mother')}}<br/>
                                                    {{$generation_four = \App\Dog::getParentId($thirdgen_mother,'mother','registration_number')}}
                                                </div>

                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h5">{{\App\Dog::getParent($generation_four,'father')}}<br/>
                                                    {{$generation_five_f = \App\Dog::getParentId($generation_four,'father','registration_number')}}

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($generation_four,'mother')}}<br/>
                                                    {{$generation_five_g = \App\Dog::getParentId($generation_four,'mother','registration_number')}}
                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td rowspan='16' width='17%' class='female' >

                                                <div class="h5">

                                                    @if ($dog->mother)
                                                        {{\App\Dog::getRelationship($dog->mother)}}<br/>
                                                        {{$dog->mother}}
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='8' width='17%' class='male'>
                                                <div class="h5">

                                                    {{\App\Dog::getParent($dog->mother,'father')}}<br/>
                                                    {{$generation_two_a = \App\Dog::getParentId($dog->mother,'father','registration_number')}}
                                                </div>
                                            </td>
                                            <td rowspan='4' width='17%' class='male'>
                                                <div class="h5">

                                                    {{\App\Dog::getParent($generation_two_a,'father')}}<br/>
                                                    {{ $generation_two_c = \App\Dog::getParentId($generation_two_a,'father','registration_number')}}
                                                </div>

                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h5">{{\App\Dog::getParent($generation_two_c,'father')}}<br/>
                                                    {{$generation_4_d =  \App\Dog::getParentId($generation_two_c,'father','registration_number')}}
                                                </div>

                                            </td>
                                            {{--<td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>--}}
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">{{\App\Dog::getParent($generation_two_c,'mother')}}<br/>
                                                    {{$generation_5_a =  \App\Dog::getParentId($generation_two_c,'mother','registration_number')}}
                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='4' width='17%' class='female'>
                                                <div class="h5">{{\App\Dog::getParent($generation_two_a,'mother')}}<br/>
                                                    {{$generation_3_a =  \App\Dog::getParentId($generation_two_a,'mother','registration_number')}}
                                                </div>
                                            </td>
                                            <td rowspan='2' width='17%' class='male'>

                                                <div class="h5">{{\App\Dog::getParent($generation_3_a,'father')}}<br/>
                                                    {{$generation_5_e = \App\Dog::getParentId($generation_3_a,'father','registration_number')}}
                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">{{\App\Dog::getParent($generation_3_a,'mother')}}<br/>
                                                    {{$generation_5_f = \App\Dog::getParentId($generation_3_a,'mother','registration_number')}}
                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='8' width='17%' class='female'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($dog->mother,'mother')}}<br/>
                                                    {{$generation_two_b = \App\Dog::getParentId($dog->mother,'mother','registration_number')}}
                                                </div>
                                            </td>
                                            <td rowspan='4' width='17%' class='male'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($generation_two_b,'father')}}<br/>
                                                    {{$generation_3_c = \App\Dog::getParentId($generation_two_b,'father','registration_number')}}
                                                </div>

                                            </td>
                                            <td rowspan='2' width='17%' class='male'>

                                                <div class="h5">{{\App\Dog::getParent($generation_3_c,'father')}}<br/>
                                                    {{$generation_5_h = \App\Dog::getParentId($generation_3_c,'father','registration_number')}}
                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">{{\App\Dog::getParent($generation_3_c,'mother')}}<br/>
                                                    {{$generation_5_i= \App\Dog::getParentId($generation_3_c,'mother','registration_number')}}
                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='4' width='17%' class='female'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($generation_two_b,'mother')}}<br/>
                                                    {{$generation_4_b = \App\Dog::getParentId($generation_two_b,'mother','registration_number')}}
                                                </div>
                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h5">{{\App\Dog::getParent($generation_4_b,'father')}}<br/>
                                                    {{$generation_5_j =\App\Dog::getParentId($generation_4_b,'father','registration_number')}}
                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>
                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td rowspan='2' width='17%' class='female hide-column'>
                                                <div class="h5">{{\App\Dog::getParent($generation_4_b,'mother')}} <br/>
                                                    {{$generation_5_k = \App\Dog::getParentId($generation_4_b,'mother','registration_number')}}
                                                </div>
                                            </td>
                                            {{--<td rowspan='1'   width='17%' class='male'>&nbsp; </td>--}}
                                            {{--<td rowspan='1'   width='17%' class='male'>&nbsp; </td>--}}
                                        </tr>
                                        {{--<tr>--}}
                                        {{--<td  style="display: none;"rowspan='1' width='17%' class='female'>&nbsp; 62</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                        {{--</td>--}}
                                    </table>

                                {{--<tr>--}}
                                {{--<tr><td align='right' valign='top' style='padding-top: 0px margin-top: 0px; font-size:8pt; font-family: Arial'><a href='http://www.kennelunionghana.com/'>Pedigree</a> generated by Kennel Union of Ghana Pedigree Database--}}
                                {{--</td><tr>--}}
                        </table>

                        <div class="row fourth" style="margin-top:-80px;" width="100%">

                            <table class="table table-bordered">
                                <tr>
                                    <td><small>Microchip/Tattoo: {{$dog->microchip_number}}</small></td>
                                    <td><small>Issue Date: <?php echo date('d-m-Y'); ?> </small></td>
                                    <td><small>Certified :</small></td>
                                </tr>

                            </table>

                        </div>

                    </div>
                </div>
                <!-- End Pedigree Table -->

                {{--</table>--}}
                {{--<h3 class="text-center"> <button class="btn btn-success" id="show-parent">Show Parentage</button></h3>--}}
                @if($dog->father || $dog->mother)
                    <?php
                    $father_id = \App\Dog::getDogId($dog->father);
                    $mother_id = \App\Dog::getDogId($dog->mother);
                    ?>
                @endif

            </div>



        </div>

    </div> <!-- /.table-responsive -->

</div> <!-- /.portlet-body -->
</html>

