<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 planner">
    <div class="planner-block">
        <form class="form-planner form-horizontal" action="{{url('/search-results')}}">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="text-center">Search</label>
                        <input class="form-control" required type="text" name="name" placeholder="Search by name, registration number, microchip number, sex"/>
                    </div>
                </div>

            </div>


            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="planner-check-availability">
                        <button class="btn btn-default">SEARCH</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
