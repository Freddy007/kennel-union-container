<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->

<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script src="{{url('/bower_components/jquery/dist/jquery.js')}}"></script>

    <script src="{{url('/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

    <style>



        .tt-input-group {
            width: 100%;
        }

        .dog-name{
            font-weight: bolder;
        }

        .hide-column{
            display: none;
        }
        .water-mark{
            z-index: 20000;
            position: absolute;
            top: 450px;
            left: 320px;
            font-weight: bold;
            font-size: 60px;
            opacity: 0;
            -ms-transform: rotate(45deg); /* IE 9 */
            -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
            transform: rotate(45deg);
        }

        .first{
            border: 1px solid #000000;
            height: 50px;
            width: 90%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }

        .second{
            border: 1px solid #000000;
            height: 50px;
            width: 100%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }
        .third{
            border: 1px solid #000000;
            height: 50px;
            width: 100%;
            margin: auto;
            background-color: #ffffff;
            border-top: 0;
            border-bottom: 0;
        }

        .fourth{
            border: 1px solid #000000;
            height: 50px;
            width: 95%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }

        .table-container{
            border: 25px solid transparent;
            padding: 15px;
            /*border-image-source: url(/img/main-2.png);*/
            /*border-image-repeat: round;*/
            /*border-image-slice: 85;*/
        }

        .male {
            /*background-color: rgba(191,133,10,1);*/
            /*background-color: #FFC86E;*/
            /*background-color: rgba(255, 200, 110,1);*/
            background-color:rgb(172, 209, 165);
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        .female {
            background-color: #FFFFFF;
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        table,.dog-info{
            margin:auto;
        }

        .dog-info{
            width: 94%;
        }

        hr{
            width: 94%;
            margin:auto;
        }

        .overlay {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: rgb(76, 160, 73);
            background-color: rgba(76, 160, 73,1);
            overflow-x: hidden;
            transition: 0.5s;
        }

        .overlay-content {
            position: relative;
            top: 25%;
            width: 100%;
            text-align: center;
            margin-top: 30px;
        }

        .overlay a {
            padding: 8px;
            text-decoration: none;
            font-size: 36px;
            color: #818181;
            display: block;
            transition: 0.3s;
        }

        .overlay a:hover, .overlay a:focus {
            color: #f1f1f1;
        }

        .overlay .closebtn {
            position: absolute;
            top: 20px;
            right: 45px;
            font-size: 60px;
            color: #ffffff;
        }

        @media screen and (max-height: 450px) {
            .overlay a {font-size: 20px}
            .overlay .closebtn {
                font-size: 40px;
                top: 15px;
                right: 35px;
            }
        }

        input[type="text"],input[type="date"],select  {
            height: 60px;
            border-radius: 7px;
        }
    </style>

    <title></title>

</head>

<body>
<div id="app">

    <div class="content">

        <div id="myNav" class="overlay">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

            <br>
            <br>
            <br>

            <fieldset>
            <div class="row" style="color: #FFFFFF;">
                <div class="col-md-3"></div>
                <div class="col-md-6">

                    <div class="form-group">

{{--                        <label class="control-label col-md-4">Name--}}
{{--                            <span class="required"> * </span>--}}
{{--                        </label>--}}

                        <div class="col-md-8">
                            <input type="text" class="form-control hidden-name-input name"  name="name" style="height: 60px; border-radius: 7px;"   required/>

                            <br>

                            <div id="select-input">
                                <select class="dog_parent form-control"  id="father" name="dog_parent" style="
                                width: 100%; ">
                                    <option value=""></option>
                                </select>
                                <span class="help-block">  </span>
                            </div>

                            {{--<select class="dog_parent form-control"  id="name" name="father"  data-gender="male" >--}}
                            {{--<option value=""></option>--}}
                            {{--</select>--}}
                            {{--<span class="help-block">  </span>--}}

                        </div>

                    </div>

                    <div class="form-group">

{{--                        <label class="control-label col-md-4">Date of Birth--}}
{{--                            <span class="required"> * </span>--}}
{{--                        </label>--}}

                        <div class="col-md-12">
                            <input type="date" class="form-control" name="dob" id="dob" required />
                            <span class="help-block">  </span>
                        </div>
                    </div>

                    <div class="form-group">

{{--                        <label class="control-label col-md-4">Breed--}}
{{--                            <span class="required"> * </span>--}}
                            {{--<span> * </span>--}}

{{--                        </label>--}}

                        <div class="col-md-12">
                            {{--<select ng-model="selectedName" ng-options="item as item.name for item in breeds track by item.id" class="form-control" id="breed" name="breed">--}}
                            <select  class="form-control" id="breed" name="breeder_id" required>
                                <option></option>
                                @foreach(\App\Breed::all() as $breed)
                                    <option value="{{$breed->id}}">{{$breed->name}}</option>
                                @endforeach
                            </select>
                            <span class="help-block">  </span>
                        </div>
                    </div>

                    <div class="form-group">
{{--                        <label class="control-label col-md-4">Colour--}}
{{--                            <span class="required"> * </span>--}}
{{--                        </label>--}}
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="colour" id="colour" required />
                            <span class="help-block"> </span>
                        </div>
                    </div>

                    <div class="form-group">
{{--                        <label class="control-label col-md-4">Coat--}}
{{--                            <span class="required"> * </span>--}}
{{--                        </label>--}}

                        <div class="col-md-12">
                            <select class="form-control" name="coat" id="coat" required>
                                <option>short</option>
                                <option>long</option>
                            </select>
                            <span class="help-block"> </span>
                        </div>
                    </div>


                    <div class="form-group">

{{--                        <label class="control-label col-md-4">Height (cm)--}}
{{--                            <span class="required"> * </span>--}}
{{--                        </label>--}}

                        <div class="col-md-12">
                            <input type="text" class="form-control" name="height"  />
                            <span class="help-block"></span>
                        </div>
                    </div>


                </div>
                <div class="col-md-3"></div>
            </div>
            </fieldset>

        </div>

{{--        <h2>Fullscreen Overlay Nav Example</h2>--}}
{{--        <p>Click on the element below to open the fullscreen overlay navigation menu.</p>--}}
{{--        <p>In this example, the navigation menu will slide in, from left to right:</p>--}}
{{--        <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; open</span>--}}

        <div class="container-fluid">

            <div class="portlet portlet-default">

                <div class="portlet-body main-table" id="dog_id" data-id="{{$dog->id}}">

                    <!-- Begin Pedigree Table -->

                    <div class="table-container">

                        <h1 class="text-center"> {{$dog->name}}</h1>

                        <?php

                        $dob = Carbon\Carbon::createFromFormat('Y-m-d',$dog->dob);
                        $difference = $dob->diffInMonths(Carbon\Carbon::now());
                        $certificate = $difference > 12 ? 'PEDIGREE ' : 'BIRTH ';
                        ?>

                        <div>

                            <hr>
                            <br>

                            <?php $parent = (\App\DogGeneration::leftJoin('dogs','dogs.id','=','dog_generations.dog_id')
                                                               ->where('dog_id',$dog->id)
                                                               ->first()) ?>

                            <table border="0" cellpadding="2" width='95%'>
                                <tr>
                                    <td>
                                        <table id="pedigree-table" style="border: 1px solid #000000; text-transform: uppercase;
                                    border-collapse: collapse;" border="1" cellpadding="2" cellspacing="2" width="100%"  CELLSPACING="2">
                                            <tr>
                                                <th style="text-align: center;">I</th>
                                                <th style="text-align: center;">II</th>
                                                <th style="text-align: center;">III</th>
                                                <th style="text-align: center;">IV</th>
                                                {{--<th style="text-align: center;">V</th>--}}

                                            </tr>

                                            <tr>
                                                <td rowspan='16' width='17%' class='male'>
                                                    <div class="h5">
                                                        <span class="column" data-column="1"  data-position="sire">1</span>



                                                        <div class="dog-name">

                                                            {{$parent->first_generation['sire']['name']}}<br>

                                                        </div>

                                                        {{$parent->first_generation['sire']['registration_number']}}<br>
                                                        {{$parent->first_generation['sire']['titles']}}<br>

                                                    </div>

                                                    <button class="btn btn-sm btn-success edit-column"
                                                            data-sire-parent="second,sire,sire"
                                                            data-dam-parent="second,sire,dam"
                                                            data-generation="first"
                                                            data-position="sire" >edit
                                                    </button>

                                                </td>
                                                <td rowspan='8' width='17%' class='male'>
                                                    <div class="h5">

                                                        <span class="column">3</span>


                                                        <div class="dog-name">

                                                            {{$parent->second_generation['sire']['sire']['name']}}<br>
                                                        </div>

                                                        {{$parent->second_generation['sire']['sire']['registration_number']}}<br>
                                                        {{$parent->second_generation['sire']['sire']['titles']}}<br>

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-sire-parent="third,Sire,sire"
                                                                data-dam-parent="third,Sire,dam"

                                                                data-generation="second"
                                                                data-position="sire,sire"
                                                                data-offspring="first,sire">
                                                            edit</button>

                                                    </div>
                                                </td>
                                                <td rowspan='4' width='17%' class='male'>
                                                    <div class="h5">

                                                        <span class="column">7</span>
                                                        <div class="dog-name">

                                                            <div class="dog-name">

                                                                {{$parent->third_generation['Sire']['sire']['name']}}<br>
                                                            </div>

                                                            {{$parent->third_generation['Sire']['sire']['registration_number']}}<br>
                                                            {{$parent->third_generation['Sire']['sire']['titles']}}<br>

                                                            {{--$parent->fourth_generation['Sire']['sire']['name']--}}

                                                            {{--                                                    @if($parent->third_generation['Sire']['sire']['name'] == '')--}}

                                                            <button class="btn btn-sm btn-success edit-column"

                                                                    data-sire-parent="fourth,Sire,sire"
                                                                    data-dam-parent="fourth,Sire,dam"

                                                                    data-generation="third"
                                                                    data-position="Sire,sire"
                                                                    data-offspring="second,sire,sire">
                                                                edit</button>

                                                            {{--@endif--}}
                                                        </div>
                                                </td>
                                                <td rowspan='2' width='17%' class='male'>
                                                    <div class="h5">

                                                        <span class="column">15</span>


                                                        <div class="dog-name">

                                                            {{$parent->fourth_generation['Sire']['sire']['name']}}<br>

                                                        </div>

                                                        {{$parent->fourth_generation['Sire']['sire']['registration_number']}}<br>
                                                        {{$parent->fourth_generation['Sire']['sire']['titles']}}<br>

                                                        {{--                                                    @if($parent->fourth_generation['Sire']['sire']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-generation="fourth" data-position="Sire,sire"
                                                                data-offspring="third,Sire,sire">
                                                            edit
                                                        </button>

                                                        {{--@endif--}}


                                                    </div>
                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td rowspan='2' width='17%' class='female'>
                                                    <div class="h5">

                                                        <span class="column">16</span>

                                                        <div class="dog-name">

                                                            {{$parent->fourth_generation['Sire']['dam']['name']}}<br>
                                                        </div>

                                                        {{$parent->fourth_generation['Sire']['dam']['registration_number']}}<br>
                                                        {{$parent->fourth_generation['Sire']['dam']['titles']}}<br>

                                                        {{--                                                    @if($parent->fourth_generation['Sire']['dam']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-generation="fourth" data-position="Sire,dam"
                                                                data-offspring="third,Sire,sire">

                                                            >edit</button>

                                                        {{--@endif--}}

                                                    </div>
                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr>
                                            <tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr>
                                            <tr>
                                                <td rowspan='4' width='17%' class='female'>
                                                    <div class="h5">

                                                        <span class="column">8</span>


                                                        <div class="dog-name">
                                                            {{$parent->third_generation['Sire']['dam']['name']}}<br>
                                                        </div>

                                                        {{$parent->third_generation['Sire']['dam']['registration_number']}}<br>
                                                        {{$parent->third_generation['Sire']['dam']['titles']}}<br>

                                                        {{--$parent->fourth_generation['Dam']['sire']['name']--}}

                                                        {{--                                                    @if($parent->third_generation['Sire']['dam']['name'] =='')--}}

                                                        <button class="btn btn-sm btn-success edit-column"

                                                                data-sire-parent="fourth,Dam,sire"
                                                                data-dam-parent="fourth,Dam,dam"

                                                                data-generation="third"
                                                                data-position="Sire,dam"
                                                                data-offspring="second,sire,sire">
                                                            edit</button>

                                                        {{--@endif--}}
                                                    </div>

                                                </td>
                                                <td rowspan='2' width='17%' class='male'>
                                                    <div class="h5">

                                                        <span class="column">17</span>

                                                        <div class="dog-name">

                                                            {{$parent->fourth_generation['Dam']['sire']['name']}}<br>

                                                        </div>

                                                        {{$parent->fourth_generation['Dam']['sire']['registration_number']}}<br>
                                                        {{$parent->fourth_generation['Dam']['sire']['titles']}}<br>

                                                        {{--                                                    @if($parent->fourth_generation['Dam']['sire']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-generation="fourth" data-position="Dam,sire"
                                                                data-offspring="third,Sire,dam">

                                                            >edit</button>

                                                        {{--@endif--}}

                                                    </div>

                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr>
                                            <tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr>
                                            <tr>
                                                <td rowspan='2' width='17%' class='female'>
                                                    <div class="h5">

                                                        <span class="column">18</span>

                                                        <div class="dog-name">

                                                            {{$parent->fourth_generation['Dam']['dam']['name']}}<br>
                                                        </div>
                                                        {{$parent->fourth_generation['Dam']['dam']['registration_number']}}<br>
                                                        {{$parent->fourth_generation['Dam']['dam']['titles']}}<br>

                                                        {{--                                                    @if($parent->fourth_generation['Dam']['dam']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column" data-generation="fourth"
                                                                data-position="Dam,dam"
                                                                data-offspring="third,Sire,dam">


                                                            >edit</button>

                                                        {{--@endif--}}

                                                    </div>

                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr>
                                            <tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr>
                                            <tr>
                                                <td rowspan='8' width='17%' class='female'>
                                                    <div class="h5">

                                                        <span class="column">4</span>

                                                        <div class="dog-name">

                                                            {{$parent->second_generation['sire']['dam']['name']}}<br>
                                                        </div>

                                                        {{$parent->second_generation['sire']['dam']['registration_number']}}<br>
                                                        {{$parent->second_generation['sire']['dam']['titles']}}<br>


                                                        {{--                                                    @if($parent->second_generation['sire']['dam']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-sire-parent="third,Dam,sire"
                                                                data-dam-parent="third,Dam,dam"

                                                                data-generation="second"
                                                                data-position="sire,dam"
                                                                data-offspring="first,sire">edit</button>
                                                        {{--@endif--}}

                                                    </div>
                                                </td>
                                                <td rowspan='4' width='17%' class='male'>
                                                    <div class="h5">


                                                        <span class="column">9</span>

                                                        <div class="dog-name">

                                                            {{$parent->third_generation['Dam']['sire']['name']}}<br>
                                                        </div>

                                                        {{$parent->third_generation['Dam']['sire']['registration_number']}}<br>
                                                        {{$parent->third_generation['Dam']['sire']['titles']}}<br>

                                                        {{--$parent->fourth_generation['SecondSire']['sire']['name']--}}

                                                        {{--@if($parent->third_generation['Dam']['sire']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"

                                                                data-sire-parent="fourth,SecondSire,sire"
                                                                data-dam-parent="fourth,SecondSire,dam"

                                                                data-generation="third"
                                                                data-position="Dam,sire"
                                                                data-offspring="second,sire,dam">
                                                            edit</button>
                                                        {{--@endif--}}

                                                    </div>
                                                </td>
                                                <td rowspan='2' width='17%' class='male'>

                                                    <span class="column">19</span>

                                                    <div class="h5">

                                                        <div class="dog-name">

                                                            {{$parent->fourth_generation['SecondSire']['sire']['name']}}<br>
                                                        </div>
                                                        {{$parent->fourth_generation['SecondSire']['sire']['registration_number']}}<br>
                                                        {{$parent->fourth_generation['SecondSire']['sire']['titles']}}<br>

                                                        {{--                                                    @if($parent->fourth_generation['SecondSire']['sire']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column" data-generation="fourth"
                                                                data-position="SecondSire,sire"
                                                                data-offspring="third,Dam,sire">

                                                            >edit</button>

                                                        {{--@endif--}}


                                                    </div>

                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr>
                                            <tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr>
                                            <tr>
                                                <td rowspan='2' width='17%' class='female'>

                                                    <span class="column">20</span>

                                                    <div class="h5">

                                                        <div class="dog-name">
                                                            {{$parent->fourth_generation['SecondSire']['dam']['name']}}<br>

                                                        </div>
                                                        {{$parent->fourth_generation['SecondSire']['dam']['registration_number']}}<br>
                                                        {{$parent->fourth_generation['SecondSire']['dam']['titles']}}<br>

                                                        {{--                                                    @if($parent->fourth_generation['SecondSire']['dam']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-generation="fourth"
                                                                data-position="SecondSire,dam"
                                                                data-offspring="third,Dam,sire">

                                                            >edit</button>

                                                        {{--@endif--}}

                                                    </div>


                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr>
                                            <tr>
                                                <td rowspan='4' width='17%' class='female'>

                                                    <span class="column">10</span>

                                                    <div class="h5">

                                                        <div class="dog-name">
                                                            {{$parent->third_generation['Dam']['dam']['name']}}<br>
                                                        </div>

                                                        <div class="dog-name">

                                                            {{$parent->third_generation['Dam']['dam']['registration_number']}}<br>
                                                        </div>

                                                        {{$parent->third_generation['Dam']['dam']['titles']}}<br>

                                                        {{--                                                    @if($parent->third_generation['Dam']['dam']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"

                                                                data-sire-parent="fourth,SecondDam,sire"
                                                                data-dam-parent="fourth,SecondSire,dam"

                                                                data-generation="third"
                                                                data-position="Dam,dam"
                                                                data-offspring="second,sire,dam">
                                                            edit</button>

                                                        {{--@endif--}}
                                                    </div>

                                                </td>
                                                <td rowspan='2' width='17%' class='male'>

                                                    <span class="column">21</span>

                                                    <div class="h5">

                                                        <div class="dog-name">
                                                            {{$parent->fourth_generation['SecondDam']['sire']['name']}}<br>

                                                        </div>
                                                        {{$parent->fourth_generation['SecondDam']['sire']['registration_number']}}<br>
                                                        {{$parent->fourth_generation['SecondDam']['sire']['titles']}}<br>

                                                        {{--                                                    @if($parent->fourth_generation['SecondDam']['sire']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-generation="fourth" data-position="SecondDam,sire"
                                                                data-offspring="third,Dam,dam">

                                                            >edit</button>

                                                        {{--@endif--}}


                                                    </div>

                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr>
                                            <tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr>
                                            <tr>
                                                <td rowspan='2' width='17%' class='female'>


                                                    <span class="column">22</span>

                                                    <div class="h5">
                                                        <div class="dog-name">
                                                            {{$parent->fourth_generation['SecondDam']['dam']['name']}}<br>
                                                        </div>
                                                        {{$parent->fourth_generation['SecondDam']['dam']['registration_number']}}<br>
                                                        {{$parent->fourth_generation['SecondDam']['dam']['titles']}}<br>

                                                        {{--                                                    @if($parent->fourth_generation['SecondDam']['dam']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-generation="fourth" data-position="SecondDam,dam"
                                                                data-offspring="third,Dam,dam">

                                                            >edit</button>

                                                        {{--@endif--}}


                                                    </div>
                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                                            </tr><tr>
                                                <td rowspan='16' width='17%' class='female' >

                                                    <span class="column">2</span>

                                                    <div class="h5">

                                                        <div class="dog-name">
                                                            {{$parent->first_generation['dam']['name']}}<br>
                                                        </div>
                                                        {{$parent->first_generation['dam']['registration_number']}}<br>
                                                        {{$parent->first_generation['dam']['titles']}}<br>

                                                    </div>

                                                    {{--                                                @if($parent->first_generation['dam']['name'] == '')--}}

                                                    <button class="btn btn-sm btn-success edit-column"
                                                            data-sire-parent="second,sire,sire"
                                                            data-dam-parent="second,sire,dam"
                                                            data-generation="first"
                                                            data-position="dam" >edit
                                                    </button>

                                                    {{--@endif--}}
                                                </td>
                                                <td rowspan='8' width='17%' class='male'>
                                                    <span class="column">5</span>

                                                    <div class="h5">

                                                        <div class="dog-name">

                                                            {{$parent->second_generation['Dam']['sire']['name']}}<br>
                                                        </div>

                                                        {{$parent->second_generation['Dam']['sire']['registration_number']}}<br>
                                                        {{$parent->second_generation['Dam']['sire']['titles']}}<br>

                                                        {{--                                                    @if($parent->second_generation['Dam']['sire']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"

                                                                data-sire-parent="Third,SecondSire,sire"
                                                                data-dam-parent="Third,SecondSire,dam"

                                                                data-generation="second"
                                                                data-position="Dam,sire"
                                                                data-offspring="first,dam"
                                                        >edit</button>

                                                        {{--@endif--}}

                                                    </div>
                                                </td>
                                                <td rowspan='4' width='17%' class='male'>
                                                    <span class="column">11</span>

                                                    <div class="h5">
                                                        <div class="dog-name">
                                                            {{$parent->third_generation['SecondSire']['sire']['name']}}<br>
                                                        </div>


                                                        {{$parent->third_generation['SecondSire']['sire']['registration_number']}}<br>
                                                        {{$parent->third_generation['SecondSire']['sire']['titles']}}<br>

                                                        {{--                                                    @if($parent->third_generation['SecondSire']['sire']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"

                                                                data-sire-parent="fourth,ThirdSire,sire"
                                                                data-dam-parent="fourth,ThirdSire,dam"

                                                                data-generation="third"
                                                                data-position="SecondSire,sire"
                                                                data-offspring="second,Dam,sire"
                                                        >edit</button>
                                                        {{--@endif--}}
                                                    </div>

                                                </td>
                                                <td rowspan='2' width='17%' class='male'>


                                                    <span class="column">23</span>


                                                    <div class="h5">

                                                        <div class="dog-name">

                                                            {{$parent->fourth_generation['ThirdSire']['sire']['name']}}<br>
                                                        </div>

                                                        {{$parent->fourth_generation['ThirdSire']['sire']['registration_number']}}<br>
                                                        {{$parent->fourth_generation['ThirdSire']['sire']['titles']}}<br>

                                                        {{--                                                    @if($parent->fourth_generation['ThirdSire']['sire']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-generation="fourth"
                                                                data-position="ThirdSire,sire"
                                                                data-offspring="third,SecondSire,sire">
                                                            >
                                                            edit</button>

                                                        {{--@endif--}}



                                                    </div>

                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='2' width='17%' class='female'>

                                                    <span class="column">24</span>


                                                    <div class="h5">
                                                        <div class="dog-name">
                                                            {{$parent->fourth_generation['ThirdSire']['dam']['name']}}<br>
                                                        </div>
                                                        {{$parent->fourth_generation['ThirdSire']['dam']['registration_number']}}<br>
                                                        {{$parent->fourth_generation['ThirdSire']['dam']['titles']}}<br>

                                                        {{--                                                        @if($parent->fourth_generation['ThirdSire']['dam']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-generation="fourth" data-position="ThirdSire,dam"
                                                                data-offspring="third,SecondSire,sire">
                                                            >edit</button>

                                                        {{--@endif--}}



                                                    </div>

                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='4' width='17%' class='female'>

                                                    <span class="column">12</span>

                                                    <div class="h5">
                                                        <div class="dog-name">
                                                            {{$parent->third_generation['SecondSire']['dam']['name']}}<br>
                                                        </div>
                                                        {{$parent->third_generation['SecondSire']['dam']['registration_number']}}<br>
                                                        {{$parent->third_generation['SecondSire']['dam']['titles']}}<br>

                                                        {{--                                                    @if($parent->third_generation['SecondSire']['dam']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"

                                                                data-sire-parent="fourth,ThirdDam,sire"
                                                                data-dam-parent="fourth,ThirdDam,dam"

                                                                data-generation="third"
                                                                data-position="SecondSire,dam"
                                                                data-offspring="second,Dam,sire">
                                                            edit</button>
                                                        {{--@endif--}}
                                                    </div>
                                                </td>
                                                <td rowspan='2' width='17%' class='male'>

                                                    <span class="column">25</span>


                                                    <div class="h5">
                                                        <div class="dog-name">
                                                            {{$parent->fourth_generation['ThirdDam']['sire']['name']}}<br>
                                                        </div>
                                                        {{$parent->fourth_generation['ThirdDam']['sire']['registration_number']}}<br>
                                                        {{$parent->fourth_generation['ThirdDam']['sire']['titles']}}<br>

                                                        {{--                                                    @if($parent->fourth_generation['ThirdDam']['sire']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-generation="fourth"
                                                                data-position="ThirdSire,sire"
                                                                data-offspring="third,SecondSire,dam">
                                                            >edit</button>

                                                        {{--@endif--}}



                                                    </div>

                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='2' width='17%' class='female'>
                                                    <span class="column">26</span>


                                                    <div class="h5">
                                                        <div class="dog-name">
                                                            {{$parent->fourth_generation['ThirdDam']['dam']['name']}}<br>
                                                        </div>
                                                        {{$parent->fourth_generation['ThirdDam']['dam']['registration_number']}}<br>
                                                        {{$parent->fourth_generation['ThirdDam']['dam']['titles']}}<br>

                                                        {{--                                                    @if($parent->fourth_generation['ThirdDam']['dam']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-generation="fourth"
                                                                data-position="ThirdSire,dam"
                                                                data-offspring="third,SecondSire,dam">
                                                            >edit</button>

                                                        {{--@endif--}}



                                                    </div>
                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='8' width='17%' class='female'>
                                                    <div class="h5">

                                                        <span class="column">6</span>

                                                        <div class="dog-name">

                                                            {{$parent->second_generation['Dam']['dam']['name']}}<br>
                                                        </div>
                                                        {{$parent->second_generation['Dam']['dam']['registration_number']}}<br>
                                                        {{$parent->second_generation['Dam']['dam']['titles']}}<br>

                                                        {{--                                                    @if($parent->second_generation['Dam']['dam']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"

                                                                data-sire-parent="third,SecondDam,sire"
                                                                data-dam-parent="third,SecondDam,dam"

                                                                data-generation="second"
                                                                data-position="Dam,dam"
                                                                data-offspring="first,dam"
                                                        >edit</button>

                                                        {{--@endif--}}

                                                    </div>
                                                </td>
                                                <td rowspan='4' width='17%' class='male'>
                                                    <span class="column">13</span>

                                                    <div class="h5">

                                                        <div class="dog-name">
                                                            {{$parent->third_generation['SecondDam']['sire']['name']}}<br>
                                                        </div>
                                                        {{$parent->third_generation['SecondDam']['sire']['registration_number']}}<br>
                                                        {{$parent->third_generation['SecondDam']['sire']['titles']}}<br>

                                                        {{--                                                    @if($parent->third_generation['SecondDam']['sire']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-sire-parent="fourth,FourthSire,sire"
                                                                data-dam-parent="fourth,FourthSire,dam"

                                                                data-generation="third"
                                                                data-position="SecondDam,sire"
                                                                data-offspring="second,Dam,dam">
                                                            edit</button>
                                                        {{--@endif--}}

                                                    </div>

                                                </td>
                                                <td rowspan='2' width='17%' class='male'>


                                                    <span class="column">27</span>


                                                    <div class="h5">
                                                        <div class="dog-name">

                                                            {{$parent->fourth_generation['FourthSire']['sire']['name']}}<br>
                                                        </div>
                                                        {{$parent->fourth_generation['FourthSire']['sire']['registration_number']}}<br>
                                                        {{$parent->fourth_generation['FourthSire']['sire']['titles']}}<br>

                                                        {{--                                                    @if($parent->fourth_generation['FourthSire']['sire']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-generation="fourth"
                                                                data-position="FourthSire,sire"
                                                                data-offspring="third,SecondDam,sire">
                                                            >edit</button>

                                                        {{--@endif--}}


                                                    </div>
                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='2' width='17%' class='female'>

                                                    <span class="column">28</span>


                                                    <div class="h5">

                                                        {{$parent->fourth_generation['FourthSire']['dam']['name']}}<br>
                                                        {{$parent->fourth_generation['FourthSire']['dam']['registration_number']}}<br>
                                                        {{$parent->fourth_generation['FourthSire']['dam']['titles']}}<br>

                                                        {{--                                                    @if($parent->fourth_generation['FourthSire']['dam']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-generation="fourth"
                                                                data-position="FourthSire,dam"
                                                                data-offspring="third,SecondDam,sire">
                                                            >edit</button>

                                                        {{--@endif--}}
                                                    </div>
                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='4' width='17%' class='female'>
                                                    <span class="column">14</span>

                                                    <div class="h5">
                                                        <div class="dog-name">

                                                            {{$parent->third_generation['SecondDam']['dam']['name']}}<br>
                                                        </div>
                                                        {{$parent->third_generation['SecondDam']['dam']['registration_number']}}<br>
                                                        {{$parent->third_generation['SecondDam']['dam']['titles']}}<br>

                                                        {{--                                                    @if($parent->third_generation['SecondDam']['dam']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"

                                                                data-sire-parent="fourth,FourthDam,sire"
                                                                data-dam-parent="fourth,FourthDam,dam"

                                                                data-generation="third"
                                                                data-position="SecondDam,dam"
                                                                data-offspring="second,Dam,dam">
                                                            edit</button>
                                                        {{--@endif--}}
                                                    </div>
                                                </td>
                                                <td rowspan='2' width='17%' class='male'>
                                                    <span class="column">29</span>


                                                    <div class="h5">
                                                        <div class="dog-name">

                                                            {{$parent->fourth_generation['FourthDam']['sire']['name']}}<br>
                                                        </div>
                                                        {{$parent->fourth_generation['FourthDam']['sire']['registration_number']}}<br>
                                                        {{$parent->fourth_generation['FourthDam']['sire']['titles']}}<br>

                                                        {{--                                                    @if($parent->fourth_generation['FourthDam']['sire']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-generation="fourth"
                                                                data-position="FourthDam,sire"
                                                                data-offspring="third,SecondDam,dam">
                                                            >edit</button>

                                                        {{--@endif--}}
                                                    </div>

                                                </td>
                                                <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                            </tr><tr>
                                                <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td rowspan='2' width='17%' class='female'>


                                                    <span class="column">30</span>

                                                    <div class="h5">

                                                        <div class="dog-name">
                                                            {{$parent->fourth_generation['FourthDam']['dam']['name']}}<br>
                                                        </div>
                                                        {{$parent->fourth_generation['FourthDam']['dam']['registration_number']}}<br>
                                                        {{$parent->fourth_generation['FourthDam']['dam']['titles']}}<br>

                                                        {{--                                                    @if($parent->fourth_generation['FourthDam']['dam']['name'] == '')--}}

                                                        <button class="btn btn-sm btn-success edit-column"
                                                                data-generation="fourth"
                                                                data-position="FourthDam,dam"
                                                                data-offspring="third,SecondDam,dam">edit</button>

                                                        {{--@endif--}}
                                                    </div>
                                                </td>
                                                <td rowspan='1' class="hide-column"  width='17%' class='male'>&nbsp; </td>
                                            </tr>
                                            {{--<tr>--}}
                                            {{--<td  style="display: none;"rowspan='1' width='17%' class='female'>&nbsp; 62</td>--}}
                                            {{--</tr>--}}
                                            <tr>
                                        </table>
                                    </td>
                            </table>

                        </div>
                    </div>
                    <!-- End Pedigree Table -->

                    </table>
                    @if($dog->father || $dog->mother)
                        <?php
                        $father_id = \App\Dog::getDogId($dog->father);
                        $mother_id = \App\Dog::getDogId($dog->mother);
                        ?>
                    @endif

                </div>
            </div>

        </div> <!-- /.table-responsive -->

        <div class="modal fade" id="edit-column-modal"  role="dialog" aria-labelledby="gridSystemModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <form action="{{url('/version2/add-dog-to-generations')}}" id="register-dog-form">
                    {!! csrf_field() !!}
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="gridSystemModalLabel">Add dog</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label class="control-label col-md-4">Name
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control hidden-name-input name hide"  name="name"    required/>

                                            <br>

                                            <div id="select-input">
                                                <select class="dog_parent form-control"  id="father" name="dog_parent" style="width: 100%">
                                                    <option value=""></option>
                                                </select>
                                                <span class="help-block">  </span>
                                            </div>

                                            {{--<select class="dog_parent form-control"  id="name" name="father"  data-gender="male" >--}}
                                            {{--<option value=""></option>--}}
                                            {{--</select>--}}
                                            {{--<span class="help-block">  </span>--}}

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-4">Date of Birth
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="date" class="form-control" name="dob" id="dob" required />
                                            <span class="help-block">  </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-4">Breed
                                            <span class="required"> * </span>
                                            {{--<span> * </span>--}}

                                        </label>
                                        <div class="col-md-8">
                                            {{--<select ng-model="selectedName" ng-options="item as item.name for item in breeds track by item.id" class="form-control" id="breed" name="breed">--}}
                                            <select  class="form-control" id="breed" name="breeder_id" required>
                                                <option></option>
                                                @foreach(\App\Breed::all() as $breed)
                                                    <option value="{{$breed->id}}">{{$breed->name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="help-block">  </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-4">Colour
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="colour" id="colour" required />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-4">Coat
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class="form-control" name="coat" id="coat" required>
                                                <option>short</option>
                                                <option>long</option>
                                            </select>
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-4">Height (cm)
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="height"  />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>


                                </div>

                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label class="control-label col-md-4">Titles
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="titles" />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>


                                    {{--<div class="form-group">--}}
                                    {{--<label class="control-label col-md-4">Performance titles--}}
                                    {{--<span class="required">  </span>--}}
                                    {{--</label>--}}
                                    {{--<div class="col-md-8">--}}
                                    {{--<input type="text" class="form-control" name="performance_titles" />--}}
                                    {{--<span class="help-block"> </span>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="form-group">--}}
                                    {{--<label class="control-label col-md-4">Elbow ED Results--}}
                                    {{--<span class="required">  </span>--}}
                                    {{--</label>--}}
                                    {{--<div class="col-md-8">--}}
                                    {{--<input type="text" class="form-control" name="elbow_ed_results" />--}}
                                    {{--<span class="help-block"> </span>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="form-group">
                                        <label class="control-label col-md-4">Hip HD Results
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="hip_hd_results" />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-4">Tattoo Number
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="text" placeholder="" class="form-control" name="tattoo_number" />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-4">DNA ID
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="text" placeholder="" class="form-control" name="DNA" />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-4">Other Health Checks
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="text" placeholder="" class="form-control" name="other_health_checks" />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            {{--<button type="submit" class="btn btn-success">Save</button>--}}
                            <input type="submit" class="btn btn-success" value="Save" id="save-btn">
                        </div>

                    </div><!-- /.modal-content -->
                </form>
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


    </div> <!-- /.portlet-body -->

</div>

<script>
    @if($dog->needs_generation == false)

    @endif
</script>

<script src="{{url('/js/tables.js')}}"></script>

{{--<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>--}}
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.min.js"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script>
    @if(!Auth::check())
    $('.edit-column').hide()
    @endif
</script>

<script>
    function openNav() {
        document.getElementById("myNav").style.width = "100%";
    }

    function closeNav() {
        document.getElementById("myNav").style.width = "0%";
    }
</script>

</body>

</html>

