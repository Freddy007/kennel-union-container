<table class="table table-bordered" style="border: 3px solid #000;">

    <tr>
        <td rowspan="16">

            {{--@if ($dog->father)--}}
                {{--<div class="thumbnail " style="margin-top: 1500px; border: 2px solid #666;">--}}
                    {{--<figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getDog($father_id)->image_name}}" style="width: 200px; height: 150px;"></figure>--}}
                    {{--<div class="caption">--}}
                        {{--<h3> Name :<strong>{{\App\Dog::getRelationship($dog->father)}}</strong>(Sire)</h3>--}}
                        {{--No.{{$dog->father}}--}}
                    {{--</div>--}}

                    {{--@endif--}}
                {{--</div>--}}

                @if ($dog->father)
                    <div class="thumbnail " style="margin-top: 1500px; border: 2px solid #666;">
                        <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getDog($father_id)->image_name}}" style="width: 200px; height: 150px;"></figure>
                        <div class="caption">
                            <h3> Name :<strong>{{\App\Dog::getRelationship($dog->father)}}</strong>(Sire)</h3>
                            No.{{$dog->father}}
                        </div>

                    </div>
                @endif
        </td>
        <td rowspan="8">
            <div class="thumbnail" style="margin-top: 700px; border: 2px solid #666;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($dog->father,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <div class="caption">
                    <h3>Name : <strong>{{\App\Dog::getParent($dog->father,'father')}}</strong>(Sire)</h3>
                    No :{{$secondgen = \App\Dog::getParentId($dog->father,'father','registration_number')}}
                </div>
            </div>


        </td>
        <td rowspan="4"><i class="identifier">right 2 third generation</i> <br>


            <div class="thumbnail" style="border : 2px solid #666;margin-top: 250px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($secondgen,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($secondgen,'father')}}</strong>(Sire)</h3>
                No: {{$third_generation = \App\Dog::getParentId($secondgen,'father','registration_number')}}
            </div>


        </td>
        <td rowspan="2"><i class="identifier">right 3 </i>

            <div class="thumbnail" style="border : 2px solid #666;margin-top: 30px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($third_generation,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($third_generation,'father')}}</strong>(Sire)</h3>
                No: {{$fifth_generation = \App\Dog::getParentId($third_generation,'father','registration_number')}}
            </div>

        </td>
        <td><i class="identifier">right 4 </i>
            {{\App\Dog::getParent($fifth_generation ,'father')}}
            <figure><img src="/images/catalog/{{\App\Dog::getParentId($fifth_generation,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
            {{$fifth_generation = \App\Dog::getParentId($fifth_generation,'father','registration_number')}}

        </td>


    </tr>
    <tr>
        <td><i class="identifier">bottom right a </i>
            {{\App\Dog::getParent($fifth_generation ,'mother')}}
            <figure><img src="/images/catalog/{{\App\Dog::getParentId($fifth_generation,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
            {{$fifth_generation = \App\Dog::getParentId($fifth_generation,'mother','registration_number')}}
        </td>

    </tr>

    <tr>
        <td rowspan="2"><i class="identifier">new 1 </i>


            <div class="thumbnail" style="border : 2px solid #666;margin-top: 30px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($third_generation,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($third_generation,'mother')}}</strong>(Dam)</h3>
                No: {{$generation_five = \App\Dog::getParentId($third_generation,'mother','registration_number')}}
            </div>


        </td>
        <td><i class="identifier">new 1 a </i>

            {{\App\Dog::getParent($generation_five ,'father')}}
            <figure><img src="/images/catalog/{{\App\Dog::getParentId($generation_five,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>

            {{$generation_five = \App\Dog::getParentId($generation_five,'father','registration_number')}}


        </td>

    </tr>

    <tr>
        <td><i class="identifier">new 2 </i>
            {{\App\Dog::getParent($generation_five ,'mother')}}
            <figure><img src="/images/catalog/{{\App\Dog::getParentId($generation_five,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>

            {{$generation_five = \App\Dog::getParentId($generation_five,'mother','registration_number')}}

        </td>
    </tr>

    <tr>
        <td rowspan="4"><i class="identifier">new 9 </i>

            <div class="thumbnail" style="border : 2px solid #666;margin-top: 250px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($secondgen,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($secondgen,'mother')}}</strong>(Dam)</h3>
                No: {{$fourth_generation = \App\Dog::getParentId($secondgen,'mother','registration_number')}}
            </div>



        </td>
        <td rowspan="2"><i class="identifier">9a </i>

            <div class="thumbnail" style="border : 2px solid #666;margin-top: 30px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($fourth_generation,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($fourth_generation,'father')}}</strong>(Dam)</h3>
                No: {{$generation_five_b = \App\Dog::getParentId($fourth_generation,'father','registration_number')}}
            </div>

        </td>
        <td><i class="identifier">9b </i>
            {{\App\Dog::getParent($generation_five_b,'father')}}
            <figure><img src="/images/catalog/{{\App\Dog::getParentId($generation_five_b,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
            {{ \App\Dog::getParentId($generation_five_b,'father','registration_number')}}
        </td>

    </tr>

    <tr>
        <td><i class="identifier">new 10 </i>
            {{\App\Dog::getParent($generation_five_b,'mother')}}
            <figure><img src="/images/catalog/{{\App\Dog::getParentId($generation_five_b,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
            {{ \App\Dog::getParentId($generation_five_b,'mother','registration_number')}}
        </td>
    </tr>

    <tr>
        <td rowspan="2"><i class="identifier">new 11 </i>


            <div class="thumbnail" style="border : 2px solid #666;margin-top: 30px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($fourth_generation,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($fourth_generation,'mother')}}</strong>(Dam)</h3>
                No: {{$generation_five_c = \App\Dog::getParentId($fourth_generation,'mother','registration_number')}}
            </div>

        </td>
        <td><i class="identifier">11a </i>
            {{\App\Dog::getParent($generation_five_c,'father')}}
            <figure><img src="/images/catalog/{{\App\Dog::getParentId($generation_five_c,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
            {{ \App\Dog::getParentId($generation_five_c,'father','registration_number')}}
        </td>

    </tr>

    <tr>
        <td><i class="identifier">new 13 </i>
            {{\App\Dog::getParent($generation_five_c,'mother')}}
            <figure><img src="/images/catalog/{{\App\Dog::getParentId($generation_five_c,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
            {{ \App\Dog::getParentId($generation_five_c,'mother','registration_number')}}
        </td>
    </tr>

    <tr>
        <td rowspan="8"><i class="identifier">new 3 second generation</i> <br>
            <div class="thumbnail" style="margin-top: 600px; border: 2px solid #666;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($dog->father,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <div class="caption">
                    <h3>Name: <strong>{{\App\Dog::getParent($dog->father,'mother')}}</strong></h3>
                    {{$thirdgen_mother = \App\Dog::getParentId($dog->father,'mother','registration_number')}}
                </div>
            </div>


        </td>
        <td rowspan="4"><i class="identifier">new 3 a third generation </i><br>

            <div class="thumbnail" style="border : 2px solid #666;margin-top: 250px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($thirdgen_mother,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($thirdgen_mother,'father')}}</strong>(Sire)</h3>
                No: {{$fourth_gen = \App\Dog::getParentId($thirdgen_mother,'father','registration_number')}}
            </div>

        </td>
        <td rowspan="2"><i class="identifier">new 3 b </i>


            <div class="thumbnail" style="border : 2px solid #666;margin-top: 30px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($fourth_gen,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($fourth_gen,'father')}}</strong>(Sire)</h3>
                No: {{$generation_five_d = \App\Dog::getParentId($fourth_gen,'father','registration_number')}}
            </div>


        </td>
        <td><i class="identifier">new 3 c </i>
            {{\App\Dog::getParent($generation_five_d,'father')}}
            <figure><img src="/images/catalog/{{\App\Dog::getParentId($generation_five_d,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
            {{ \App\Dog::getParentId($generation_five_d,'father','registration_number')}}

        </td>


    </tr>

    <tr>
        <td>new 4
            {{\App\Dog::getParent($generation_five_d,'mother')}}
            <figure><img src="/images/catalog/{{\App\Dog::getParentId($generation_five_d,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
            {{ \App\Dog::getParentId($generation_five_d,'mother','registration_number')}}
        </td>
    </tr>

    <tr>
        <td rowspan="2"><i class="identifier">new 5 </i>

            <div class="thumbnail" style="border : 2px solid #666;margin-top: 30px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($fourth_gen,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($fourth_gen,'mother')}}</strong>(Dam)</h3>
                No: {{$generation_five_e = \App\Dog::getParentId($fourth_gen,'mother','registration_number')}}
            </div>


        </td>
        <td><i class="identifier">5a </i>
            {{\App\Dog::getParent($generation_five_e,'father')}}
            <figure><img src="/images/catalog/{{\App\Dog::getParentId($generation_five_e,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
            {{$generation_five_e =  \App\Dog::getParentId($generation_five_e,'father','registration_number')}}
        </td>
    </tr>
    <tr>
        <td><i class="identifier">new 6 </i>
            {{\App\Dog::getParent($generation_five_e,'mother')}}
            <figure><img src="/images/catalog/{{\App\Dog::getParentId($generation_five_e,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
            {{$generation_five_e =  \App\Dog::getParentId($generation_five_e,'mother','registration_number')}}
        </td>
    </tr>

    <tr>
        <td rowspan="4"><i class="identifier">new 7 </i>

            <div class="thumbnail" style="border : 2px solid #666;margin-top: 250px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($thirdgen_mother,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($thirdgen_mother,'mother')}}</strong>(Dam)</h3>
                No: {{$generation_four = \App\Dog::getParentId($thirdgen_mother,'mother','registration_number')}}
            </div>

        </td>
        <td rowspan="2"><i class="identifier">7a </i>

            <div class="thumbnail" style="border : 2px solid #666;margin-top: 30px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_four,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_four,'father')}}</strong>(Sire)</h3>
                No: {{$generation_five_f = \App\Dog::getParentId($generation_four,'father','registration_number')}}
            </div>

        </td>
        <td><i class="identifier">7b </i>
            {{\App\Dog::getParent($generation_five_f,'father')}}
            <figure><img src="/images/catalog/{{\App\Dog::getParentId($generation_five_f,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
            {{$generation_five_f = \App\Dog::getParentId($generation_five_f,'father','registration_number')}}
        </td>

    </tr>


    <tr>
        <td><i class="identifier">8a </i>
            {{\App\Dog::getParent($generation_five_f,'mother')}}
            <figure><img src="/images/catalog/{{\App\Dog::getParentId($generation_five_f,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
            {{$generation_five_f = \App\Dog::getParentId($generation_five_f,'mother','registration_number')}}
        </td>
    </tr>

    <tr>
        <td rowspan="2"><i class="identifier">12a </i>

            <div class="thumbnail" style="border : 2px solid #666;margin-top: 30px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_four,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_four,'mother')}}</strong>(Dam)</h3>
                No: {{$generation_five_g = \App\Dog::getParentId($generation_four,'mother','registration_number')}}
            </div>

        </td>
        <td><i class="identifier">12b </i>

            {{\App\Dog::getParent($generation_five_g,'father')}}
            <figure><img src="/images/catalog/{{\App\Dog::getParentId($generation_five_g,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
            {{ \App\Dog::getParentId($generation_five_g,'father','registration_number')}}
        </td>
    </tr>

    <tr>
        <td><i class="identifier">14a </i>
            {{\App\Dog::getParent($generation_five_g,'mother')}}
            <figure><img src="/images/catalog/{{\App\Dog::getParentId($generation_five_g,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
            {{ \App\Dog::getParentId($generation_five_g,'mother','registration_number')}}
        </td>


    </tr>

    <tr>
        <td rowspan="16">
            <div class="thumbnail" style="border:2px solid #666; margin-top: 2400px;">
                @if ($dog->mother)
                    <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getDog($mother_id)->image_name}}" style="width: 200px; height: 150px;"></figure>
                    <h3> Name: <strong>{{\App\Dog::getRelationship($dog->mother)}}</strong>(Dam)</h3>
                    <div class="caption">
                        No.: {{$dog->mother}}
                    </div>
                @endif
            </div>
        </td>
        <td rowspan="8"><i class="identifier">top right 2 </i>
            <div class="thumbnail" style="border : 2px solid #666; margin-top: 1100px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($dog->mother,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($dog->mother,'father')}}</strong>(Sire)</h3>
                No: {{$generation_two_a = \App\Dog::getParentId($dog->mother,'father','registration_number')}}
            </div>

        </td>
        <td rowspan="4"><i class="identifier">right 2 </i>
            <div class="thumbnail" style="border : 2px solid #666; margin-top: 450px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_two_a,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_two_a,'father')}}</strong>(Sire)</h3>
                No: {{ $generation_two_c = \App\Dog::getParentId($generation_two_a,'father','registration_number')}}
            </div>
        </td>
        <td rowspan="2"><i class="identifier">right 3 huu </i>
            <div class="thumbnail" style="border : 2px solid #666; margin-top: 150px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_two_c,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_two_c,'father')}}</strong>(Sire)</h3>
                No: {{$generation_4_d =  \App\Dog::getParentId($generation_two_c,'father','registration_number')}}
            </div>
        </td>
        <td><i class="identifier">right 4 </i>
            <div class="thumbnail" style="border : 2px solid #666">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_4_d,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_4_d,'father')}}</strong>(Sire)</h3>
                No: {{  \App\Dog::getParentId($generation_4_d,'father','registration_number')}}
            </div>
        </td>


    </tr>
    <tr>
        <td><i class="identifier">bottom right a </i>
            <div class="thumbnail" style="border : 2px solid #666">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_4_d,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_4_d,'mother')}}</strong>(Dam)</h3>
                No: {{  \App\Dog::getParentId($generation_4_d,'mother','registration_number')}}
            </div>
        </td>

    </tr>

    <tr>
        <td rowspan="2"><i class="identifier">new 1 jk </i>
            <div class="thumbnail" style="border : 2px solid #666;margin-top:150px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_two_c,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_two_c,'mother')}}</strong>(Dam)</h3>
                No: {{$generation_5_a =  \App\Dog::getParentId($generation_two_c,'mother','registration_number')}}
            </div>

        </td>
        <td><i class="identifier">new 1 a </i>
            <div class="thumbnail" style="border : 2px solid #666">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_5_a,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_5_a,'father')}}</strong>(Sire)</h3>
                No: {{ \App\Dog::getParentId($generation_5_a,'father','registration_number')}}
            </div>
        </td>

    </tr>

    <tr>
        <td><i class="identifier">new 2 </i>
            <div class="thumbnail" style="border : 2px solid #666">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_5_a,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_5_a,'mother')}}</strong>(Dam)</h3>
                No: {{ \App\Dog::getParentId($generation_5_a,'mother','registration_number')}}
            </div>
        </td>
    </tr>

    <tr>
        <td rowspan="4"><i class="identifier">new 9 h </i>

            <div class="thumbnail" style="border : 2px solid #666;margin-top: 450px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_two_a,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_two_a,'mother')}}</strong>(Dam)</h3>
                No: {{$generation_3_a =  \App\Dog::getParentId($generation_two_a,'mother','registration_number')}}
            </div>


        </td>
        <td rowspan="2"><i class="identifier">9a n </i>
            <div class="thumbnail" style="border : 2px solid #666; margin-top: 150px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_3_a,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_3_a,'father')}}</strong>(Sire)</h3>
                No: {{$generation_5_e = \App\Dog::getParentId($generation_3_a,'father','registration_number')}}
            </div>
        </td>
        <td><i class="identifier">9b </i>
            <div class="thumbnail" style="border : 2px solid #666">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_5_e,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_5_e,'father')}}</strong>(Sire)</h3>
                No: {{ \App\Dog::getParentId($generation_5_e,'father','registration_number')}}
            </div>
        </td>

    </tr>

    <tr>
        <td><i class="identifier">new 10 </i>
            <div class="thumbnail" style="border : 2px solid #666">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_5_e,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_5_e,'mother')}}</strong>(Dam)</h3>
                No: {{ \App\Dog::getParentId($generation_5_e,'mother','registration_number')}}
            </div>
        </td>
    </tr>

    <tr>
        <td rowspan="2"><i class="identifier">new 11 e </i>
            <div class="thumbnail" style="border : 2px solid #666;margin-top: 150px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_3_a,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_3_a,'mother')}}</strong>(Dam)</h3>
                No: {{$generation_5_f = \App\Dog::getParentId($generation_3_a,'mother','registration_number')}}
            </div>
        </td>
        <td><i class="identifier">11a </i>
            <div class="thumbnail" style="border : 2px solid #666">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_5_f,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_5_f,'father')}}</strong>(Sire)</h3>
                No: {{ \App\Dog::getParentId($generation_5_f,'father','registration_number')}}
            </div>
        </td>

    </tr>


    <tr>
        <td><i class="identifier">new 13 </i>
            <div class="thumbnail" style="border : 2px solid #666">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_5_f,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_5_f,'mother')}}</strong>(Dam)</h3>
                No: {{ \App\Dog::getParentId($generation_5_f,'mother','registration_number')}}
            </div>
        </td>
    </tr>

    <tr>
        <td rowspan="8"><i class="identifier">new 3 hello </i>
            <div class="thumbnail" style="border: 2px solid #666; margin-top: 1100px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($dog->mother,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <div class="caption">
                    <h3>Name:<strong>{{\App\Dog::getParent($dog->mother,'mother')}}</strong>(Dam)</h3>
                    No: {{$generation_two_b = \App\Dog::getParentId($dog->mother,'mother','registration_number')}}

                </div>
            </div>


        </td>
        <td rowspan="4"><i class="identifier">new 3 a huy </i>
            <div class="thumbnail" style="border : 2px solid #666; margin-top: 450px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_two_b,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_two_b,'father')}}</strong>(Sire)</h3>
                No: {{$generation_3_c = \App\Dog::getParentId($generation_two_b,'father','registration_number')}}
            </div>

        </td>
        <td rowspan="2"><i class="identifier">new 3 b w </i>
            <div class="thumbnail" style="border : 2px solid #666;margin-top: 150px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_3_c,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_3_c,'father')}}</strong>(Sire)</h3>
                No: {{$generation_5_h = \App\Dog::getParentId($generation_3_c,'father','registration_number')}}
            </div>
        </td>
        <td><i class="identifier">new 3 c </i>
            <div class="thumbnail" style="border : 2px solid #666">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_5_h,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_5_h,'father')}}</strong>(Sire)</h3>
                No: {{ \App\Dog::getParentId($generation_5_h,'father','registration_number')}}
            </div>

        </td>


    </tr>

    <tr>
        <td><i class="identifier">new 4 </i>
            <div class="thumbnail" style="border : 2px solid #666">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_5_h,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_5_h,'mother')}}</strong>(Dam)</h3>
                No: {{ \App\Dog::getParentId($generation_5_h,'mother','registration_number')}}
            </div>
        </td>
    </tr>

    <tr>
        <td rowspan="2"><i class="identifier">new 5 r </i>
            <div class="thumbnail" style="border : 2px solid #666; margin-top:150px ">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_3_c,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_3_c,'mother')}}</strong>(Dam)</h3>
                No: {{$generation_5_i= \App\Dog::getParentId($generation_3_c,'mother','registration_number')}}
            </div>
        </td>
        <td><i class="identifier">5a </i>
            <div class="thumbnail" style="border : 2px solid #666">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_5_i,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_5_i,'father')}}</strong>(Sire)</h3>
                No: {{ \App\Dog::getParentId($generation_5_i,'father','registration_number')}}
            </div>
        </td>
    </tr>
    <tr>
        <td><i class="identifier">new 6 </i>
            <div class="thumbnail" style="border : 2px solid #666">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_5_i,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_5_i,'mother')}}</strong>(Sire)</h3>
                No: {{ \App\Dog::getParentId($generation_5_i,'mother','registration_number')}}
            </div>

        </td>
    </tr>

    <tr>
        <td rowspan="4"><i class="identifier">new 7 </i>
            <div class="thumbnail" style="border : 2px solid #666;margin-top: 450px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_two_b,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_two_b,'mother')}}</strong>(Dam)</h3>
                No: {{$generation_4_b = \App\Dog::getParentId($generation_two_b,'mother','registration_number')}}
            </div>
        </td>
        <td rowspan="2"><i class="identifier">7a w </i>
            <div class="thumbnail" style="border : 2px solid #666;margin-top: 150px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_4_b,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_4_b,'father')}}</strong>(Sire)</h3>
                No: {{$generation_5_j =\App\Dog::getParentId($generation_4_b,'father','registration_number')}}
            </div>

        </td>
        <td><i class="identifier">7b</i>
            <div class="thumbnail" style="border : 2px solid #666">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_5_j,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_5_j,'father')}}</strong>(Sire)</h3>
                No: {{\App\Dog::getParentId($generation_5_j,'father','registration_number')}}
            </div>

        </td>

    </tr>


    <tr>
        <td><i class="identifier">8a </i>
            <div class="thumbnail" style="border : 2px solid #666">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_5_j,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_5_j,'mother')}}</strong>(Sire)</h3>
                No: {{\App\Dog::getParentId($generation_5_j,'mother','registration_number')}}
            </div>

        </td>
    </tr>

    <tr>
        <td rowspan="2"><i class="identifier">12a w </i>
            <div class="thumbnail" style="border : 2px solid #666; margin-top: 150px;">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_4_b,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_4_b,'mother')}}</strong>(Dam)</h3>
                No: {{$generation_5_k = \App\Dog::getParentId($generation_4_b,'mother','registration_number')}}
            </div>
        </td>
        <td><i class="identifier">12b </i>
            <div class="thumbnail" style="border : 2px solid #666">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_5_k,'father','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_5_k,'father')}}</strong>(Sire)</h3>
                No: {{ \App\Dog::getParentId($generation_5_k,'father','registration_number')}}
            </div>
        </td>
    </tr>

    <tr>
        <td><i class="identifier">14a </i>
            <div class="thumbnail" style="border : 2px solid #666">
                <figure><img class="img-rounded" src="/images/catalog/{{\App\Dog::getParentId($generation_5_k,'mother','image_name')}}" style="width: 200px; height: 150px;"></figure>
                <h3>Name : <strong>{{\App\Dog::getParent($generation_5_k,'mother')}}</strong>(Dam)</h3>
                No: {{ \App\Dog::getParentId($generation_5_k,'mother','registration_number')}}
            </div>
        </td>


    </tr>

</table>