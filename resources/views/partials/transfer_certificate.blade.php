
<div class="portlet-body main-table">

    <!-- Begin Pedigree Table -->

    <style>
        .water-mark{
            z-index: 20000;
            position: absolute;
            top: 450px;
            left: 320px;
            font-weight: bold;
            font-size: 60px;
            opacity: 0;
            -ms-transform: rotate(45deg); /* IE 9 */
            -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
            transform: rotate(45deg);
        }

        .first{
            border: 1px solid #000000;
            height: 50px;
            width: 90%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }

        .second{
            border: 1px solid #000000;
            height: 50px;
            width: 100%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }
        .third{
            border: 1px solid #000000;
            height: 50px;
            width: 100%;
            margin: auto;
            background-color: #ffffff;
            border-top: 0;
            border-bottom: 0;
        }

        .fourth{
            border: 1px solid #000000;
            height: 50px;
            width: 95%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }

        .table-container{
            border: 25px solid transparent;
            padding: 15px;
            border-image-source: url(/img/main-2.png);
            border-image-repeat: round;
            border-image-slice: 85;
        }

        /*hr {*/
        /*padding: 0;*/
        /*border: none;*/
        /*border-top: medium double #333;*/
        /*color: #333;*/
        /*text-align: center;*/
        /*width: 95%;*/
        /*margin-left: 0px;*/
        /*}*/
        /*hr:after {*/
        /*/!*content: "�";*!/*/
        /*display: inline-block;*/
        /*position: relative;*/
        /*top: -0.7em;*/
        /*font-size: 1.5em;*/
        /*padding: 0 0.25em;*/
        /*background: white;*/
        /*}*/
        .male {
            /*background-color: rgba(191,133,10,1);*/
            /*background-color: #FFC86E;*/
            /*background-color: rgba(255, 200, 110,1);*/
            background-color:rgb(172, 209, 165);
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        .female {
            background-color: #FFFFFF;
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        table,.dog-info{
            margin:auto;
        }

        .dog-info{
            width: 94%;
        }

        hr{
            width: 94%;
            margin:auto;
        }

        @media print {
            .first{
                border: 1px solid #000000;
                height: 50px;
                width: 90%;
                margin: auto;
                background-color: rgb(172, 209, 165) !important;
            }

            .second{
                border: 1px solid #000000;
                height: 50px;
                width: 100%;
                margin: auto;
                background-color: rgb(172, 209, 165) !important;
            }
            .third{
                border: 1px solid #000000;
                height: 50px;
                width: 100%;
                margin: auto;
                background-color: #ffffff;
                border-top: 0;
                border-bottom: 0;
            }

            .fourth{
                border: 1px solid #000000;
                height: 50px;
                width: 95%;
                /*margin: auto;*/
                margin-top: -90px;
                background-color: rgb(172, 209, 165) !important;
            }

            table{
                height: 70%;
                margin-top: -42px;
                /*background-color: yellowgreen !important;*/
                -webkit-print-color-adjust: exact;
            }

            .table-container{
                border: 25px solid transparent;
                padding: 12px;
                border-image-source: url(/img/main-2.png);
                border-image-repeat: round;
                border-image-slice: 75;
            }

            .image_one{
                position: absolute;
                top: 60px;
                left: 40px;
            }

            .image_two{
                position: absolute;
                right: 40px;
                top: 60px;
            }

            .left-border{
                position: absolute;
                top: 5px;
                left: -14px;

            }

            .right-border{
                position: absolute;
                top: 5px;
                right: -14px;
            }

            .full-border{
                width: 100%;
                margin-top: 25px;

            }

            .full-border-flag{
                margin-left: -15px;;
                /*margin-right: -30px;;*/
                width: 103%;
                height:8px;
                /*margin-bottom: 2px;*/
            }

            .flag{
                width: 320px;
                height:8px;
                /*margin-bottom: 2px;*/
            }

            .male{
                background-color: rgb(172, 209, 165) !important;
                /*background-color: rgba(191,133,10,1)!important;*/
                -webkit-print-color-adjust: exact;
            }
            .image_one{
                position: absolute;
                left: 230px;
                top: 40px;
            }

            .image_two{
                position: absolute;
                right: 230px;
                top: 40px;
            }


            .flag{
                width: 320px;
                height:8px;
                /*margin-bottom: 2px;*/
            }
            header {
                display: none;
            }
            .mainnav{
                display: none;
            }
            footer,title,.print{
                display:none;
            }

            .flag_one{
                background-color: red !important;
            }
            .flag_two{
                background-color: yellow !important;
            }
            .flag_three{
                background-color: green !important;
            }

            #print{
                display: none;
            }
            /*hr{*/
            /*margin-bottom: -60px;;*/
            /*}*/

            @page { size: auto;  margin: 0mm; }

        }

        .image_one{
            position: absolute;
            left: 230px;
            top: 40px;
        }

        .image_two{
            position: absolute;
            right: 230px;
            top: 40px;
        }

        /*.left-border{*/
        /*position: absolute;*/
        /*top: 5px;*/
        /*left: -14px;*/

        /*}*/

        /*.right-border{*/
        /*position: absolute;*/
        /*top: 5px;*/
        /*right: -14px;*/
        /*}*/

        /*.full-border{*/
        /*width: 100%;*/
        /*margin-top: 25px;*/

        /*}*/

        /*.full-border-flag{*/
        /*margin-left: -15px;;*/
        /*/!*margin-right: -30px;;*!/*/
        /*width: 103%;*/
        /*height:8px;*/
        /*/!*margin-bottom: 2px;*!/*/
        /*}*/

        /*.flag{*/
        /*width: 320px;*/
        /*height:8px;*/
        /*/!*margin-bottom: 2px;*!/*/
        /*}*/

    </style>

    <div class="table-container">
        {{--<div class="left-border">--}}
        {{--<div class="flag flag_one" style="background-color: red;"></div>--}}
        {{--<div class="flag flag_two" style="background-color: yellow"></div>--}}
        {{--<div class="flag flag_three" style="background-color: green"></div>--}}
        {{--</div>--}}

        {{--<div class="right-border">--}}
        {{--<div class="flag flag_one" style="background-color: red;"></div>--}}
        {{--<div class="flag flag_two" style="background-color: yellow"></div>--}}
        {{--<div class="flag flag_three" style="background-color: green"></div>--}}
        {{--</div>--}}
        <?php
        $dob = Carbon\Carbon::createFromFormat('Y-m-d',$dog->dob);
        $difference = $dob->diffInMonths(Carbon\Carbon::now());
        $certificate = $difference > 12 ? 'PEDIGREE ' : 'BIRTH ';
        ?>

        <div class="header">
            <h2 class="text-center" style="margin-top: 6px; margin-bottom: 10px;">KENNEL UNION OF GHANA  CERTIFIED {{$certificate}} CERTIFICATE</h2><br>
            {{--<h3 class="text-center">CERTIFIED PEDIGREE </h3>--}}
        </div>
        {{--<div class="print"><button id="print" class="btn btn-success pull-right" style="margin-top: -25px;" onclick="window.print()">PRINT CERTIFICATE</button></div><br>--}}
        {{--                <div class="print"><a href="{{url('admin/pdf',$dog->id)}}" class="btn btn-success pull-right" style="margin-top: -1px;" >GENERATE PDF</a></div><br>--}}

        <div class="water-mark">
            KENNEL UNION OF GHANA
        </div>

        <div>
            <img class="image_one" src="{{url('/img/logo_01.png')}}"> <img src="{{url('/img/logo_01.png')}}" class="image_two">
          {{--<button class="btn btn-success" id="print" onclick="window.print()">Print</button>--}}
          <a href="{{url('transfer-cert',$dog_id)}}" class="btn btn-success" id="print" >Print</a>
            <div class="row dog-info" style="color:#283846">
                <div class="row first" style="margin-bottom: 10px;" >
                    <div class="col-md-8 h5" style=" padding-top: 13px;">Dog Name: {{$dog->name}}
                    </div>
                    <div class="col-md-4 h5" style="border-left: 1px solid #000000; height: 48px; padding-top: 13px;">REG ID: {{$dog->registration_number}}
                    </div>

                </div>

                <div class="clearfix"></div>

                <div class="row second">
                    <div class="col-md-8 col-xs-12 h5" style=" padding-top: 13px;">BREEDER : {{ strtoupper($dog->first_name.' '. $dog->last_name)}}  </div>
                    <div class="col-md-2 col-xs-12 h5" style="border-left: 1px solid #000000; height: 48px; padding-top: 13px;">COAT : {{$dog->coat}}</div>
                    <div class="col-md-2 col-xs-12 h5" style="border-left: 1px solid #000000; height: 48px; padding-top: 13px;">DOB : {{$dog->dob}}</div>

                </div>

                <div class="row third">
                    <div class="col-md-6 col-xs-12 h5"  style="padding-top: 13px;">BREED : {{$dog->breed}} </div>
                    <div class="col-md-3 col-xs-12 h5"  style="border-left: 1px solid #000000; height: 35px; padding-top: 13px;">COLOUR : {{$dog->colour}} </div>
                    <div class="col-md-3 col-xs-12 h5"  style="border-left: 1px solid #000000; height: 35px; padding-top: 13px;">SEX : {{$dog->sex}} </div>

                </div>

                <div class="row second">
                    {{--<div class="col-md-12 col-xs-12 h4" style="padding-top: 13px;">LINE BREEDING: </div>--}}
                    <div class="col-md-6 col-xs-12 h5" style="padding-top: 13px;">NEW OWNER: {{$dog->new_owner}} </div>
                    <div class="col-md-6 col-xs-12 h5" style="padding-top: 13px;">ADDRESS: {{$dog->notes}} </div>
                </div>


            </div>
            <hr>
            <br>


            <table border="0" cellpadding="2" width='95%'>
                <tr><td>
                        <table style="border: 1px solid #000000; border-collapse: collapse;" border="1" cellpadding="2" cellspacing="2" width="100%"  CELLSPACING="2">
                            <tr>
                                <th style="text-align: center;">I</th>
                                <th style="text-align: center;">II</th>
                                <th style="text-align: center;">III</th>
                                <th style="text-align: center;">IV</th>
                                <th style="text-align: center;">V</th>
                            </tr>
                            <tr>
                                <td rowspan='16' width='17%' class='male'>
                                    <div class="h5">
                                        @if ($dog->father)
                                            {{\App\Dog::getRelationship($dog->father)}}<br>
                                            {{$dog->father}}
                                        @endif
                                    </div>
                                </td>
                                <td rowspan='8' width='17%' class='male'>
                                    <div class="h5">
                                        {{\App\Dog::getParent($dog->father,'father')}}<br/>
                                        {{$secondgen = \App\Dog::getParentId($dog->father,'father','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='4' width='17%' class='male'>
                                    <div class="h5">{{\App\Dog::getParent($secondgen,'father')}}<br/>
                                        {{$third_generation = \App\Dog::getParentId($secondgen,'father','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='2' width='17%' class='male'>
                                    <div class="h5">{{\App\Dog::getParent($third_generation,'father')}}<br/>
                                        {{$fifth_generation = \App\Dog::getParentId($third_generation,'father','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='2' width='17%' class='female'>
                                    <div class="h5">{{\App\Dog::getParent($third_generation,'mother')}}<br/>
                                        {{$generation_five = \App\Dog::getParentId($third_generation,'mother','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='4' width='17%' class='female'>
                                    <div class="h5">{{\App\Dog::getParent($secondgen,'mother')}}<br/>
                                        {{$fourth_generation = \App\Dog::getParentId($secondgen,'mother','registration_number')}}
                                    </div>

                                </td>
                                <td rowspan='2' width='17%' class='male'>
                                    <div class="h5">{{\App\Dog::getParent($fourth_generation,'father')}}<br/>
                                        {{$generation_five_b = \App\Dog::getParentId($fourth_generation,'father','registration_number')}}
                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='2' width='17%' class='female'>
                                    <div class="h5">{{\App\Dog::getParent($fourth_generation,'mother')}}<br>
                                        {{$generation_five_c = \App\Dog::getParentId($fourth_generation,'mother','registration_number')}}
                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='8' width='17%' class='female'>
                                    <div class="h5">
                                        {{\App\Dog::getParent($dog->father,'mother')}} <br>
                                        {{$thirdgen_mother = \App\Dog::getParentId($dog->father,'mother','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='4' width='17%' class='male'>
                                    <div class="h5">
                                        {{\App\Dog::getParent($thirdgen_mother,'father')}}<br>
                                        {{$fourth_gen = \App\Dog::getParentId($thirdgen_mother,'father','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='2' width='17%' class='male'>
                                    <div class="h5">{{\App\Dog::getParent($fourth_gen,'father')}}<br/>
                                        {{$generation_five_d = \App\Dog::getParentId($fourth_gen,'father','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='2' width='17%' class='female'>
                                    <div class="h5">{{\App\Dog::getParent($fourth_gen,'mother')}}<br/>
                                        {{$generation_five_e = \App\Dog::getParentId($fourth_gen,'mother','registration_number')}}
                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='4' width='17%' class='female'>
                                    <div class="h5">{{\App\Dog::getParent($thirdgen_mother,'mother')}}<br/>
                                        {{$generation_four = \App\Dog::getParentId($thirdgen_mother,'mother','registration_number')}}
                                    </div>

                                </td>
                                <td rowspan='2' width='17%' class='male'>
                                    <div class="h5">{{\App\Dog::getParent($generation_four,'father')}}<br/>
                                        {{$generation_five_f = \App\Dog::getParentId($generation_four,'father','registration_number')}}

                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='2' width='17%' class='female'>
                                    <div class="h5">{{\App\Dog::getParent($generation_four,'mother')}}<br/>
                                        {{$generation_five_g = \App\Dog::getParentId($generation_four,'mother','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='16' width='17%' class='female' >

                                    <div class="h5">
                                        @if ($dog->mother)
                                            {{\App\Dog::getRelationship($dog->mother)}}<br/>
                                            {{$dog->mother}}
                                        @endif
                                    </div>
                                </td>
                                <td rowspan='8' width='17%' class='male'>
                                    <div class="h5">
                                        {{\App\Dog::getParent($dog->mother,'father')}}<br/>
                                        {{$generation_two_a = \App\Dog::getParentId($dog->mother,'father','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='4' width='17%' class='male'>
                                    <div class="h5">
                                        {{\App\Dog::getParent($generation_two_a,'father')}}<br/>
                                        {{ $generation_two_c = \App\Dog::getParentId($generation_two_a,'father','registration_number')}}
                                    </div>

                                </td>
                                <td rowspan='2' width='17%' class='male'>
                                    <div class="h5">{{\App\Dog::getParent($generation_two_c,'father')}}<br/>
                                        {{$generation_4_d =  \App\Dog::getParentId($generation_two_c,'father','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='2' width='17%' class='female'>
                                    <div class="h5">{{\App\Dog::getParent($generation_two_c,'mother')}}<br/>
                                        {{$generation_5_a =  \App\Dog::getParentId($generation_two_c,'mother','registration_number')}}
                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='4' width='17%' class='female'>
                                    <div class="h5">{{\App\Dog::getParent($generation_two_a,'mother')}}<br/>
                                        {{$generation_3_a =  \App\Dog::getParentId($generation_two_a,'mother','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='2' width='17%' class='male'>
                                    <div class="h5">{{\App\Dog::getParent($generation_3_a,'father')}}<br/>
                                        {{$generation_5_e = \App\Dog::getParentId($generation_3_a,'father','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='2' width='17%' class='female'>
                                    <div class="h5">{{\App\Dog::getParent($generation_3_a,'mother')}}<br/>
                                        {{$generation_5_f = \App\Dog::getParentId($generation_3_a,'mother','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='8' width='17%' class='female'>
                                    <div class="h5">
                                        {{\App\Dog::getParent($dog->mother,'mother')}}<br/>
                                        {{$generation_two_b = \App\Dog::getParentId($dog->mother,'mother','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='4' width='17%' class='male'>
                                    <div class="h5">
                                        {{\App\Dog::getParent($generation_two_b,'father')}}<br/>
                                        {{$generation_3_c = \App\Dog::getParentId($generation_two_b,'father','registration_number')}}
                                    </div>

                                </td>
                                <td rowspan='2' width='17%' class='male'>
                                    <div class="h5">{{\App\Dog::getParent($generation_3_c,'father')}}<br/>
                                        {{$generation_5_h = \App\Dog::getParentId($generation_3_c,'father','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='2' width='17%' class='female'>
                                    <div class="h5">{{\App\Dog::getParent($generation_3_c,'mother')}}<br/>
                                        {{$generation_5_i= \App\Dog::getParentId($generation_3_c,'mother','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='4' width='17%' class='female'>
                                    <div class="h5">
                                        {{\App\Dog::getParent($generation_two_b,'mother')}}<br/>
                                        {{$generation_4_b = \App\Dog::getParentId($generation_two_b,'mother','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='2' width='17%' class='male'>
                                    <div class="h5">{{\App\Dog::getParent($generation_4_b,'father')}}<br/>
                                        {{$generation_5_j =\App\Dog::getParentId($generation_4_b,'father','registration_number')}}
                                    </div>

                                </td>
                                <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='2' width='17%' class='female'>
                                    <div class="h5">{{\App\Dog::getParent($generation_4_b,'mother')}} <br/>
                                        {{$generation_5_k = \App\Dog::getParentId($generation_4_b,'mother','registration_number')}}
                                    </div>
                                </td>
                                <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                            </tr><tr>
                                <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                            </tr><tr>
                        </table>
                    </td>
                    {{--<tr>--}}
                    {{--<tr><td align='right' valign='top' style='padding-top: 0px margin-top: 0px; font-size:8pt; font-family: Arial'><a href='http://www.kennelunionghana.com/'>Pedigree</a> generated by Kennel Union of Ghana Pedigree Database--}}
                    {{--</td><tr>--}}
            </table>
            <div class="row fourth" >
                <div class="col-md-3 h5" style=" padding-top: 13px;">MICROCHIP/TATTOO: {{$dog->microchip_number}}
                </div>
                <div class="col-md-3 h5" style="border-left: 1px solid #000000; height: 48px; padding-top: 13px;">ISSUE DATE:<em> <?php echo date('d-m-y'); ?></em>
                </div>
                <div class="col-md-3 h5" style="border-left: 1px solid #000000; height: 48px; padding-top: 13px;">SERIAL NO : {{Cache::get('serial_number')}}
                </div>
                <div class="col-md-3 h5" style="border-left: 1px solid #000000; height: 48px; padding-top: 13px;">CERTIFIED :
                </div>

            </div>

            {{--<div class="full-border">--}}
            {{--<div class="full-border-flag flag_one" style="background-color: red;"></div>--}}
            {{--<div class="full-border-flag flag_two" style="background-color: yellow"></div>--}}
            {{--<div class="full-border-flag flag_three" style="background-color: green"></div>--}}
            {{--</div>--}}

            <div class="row" style="padding-top: 30px;">
                <div class="col-md-3">

                </div>

            </div>
        </div>
    </div>


    <!-- End Pedigree Table -->

    </table>
    {{--<h3 class="text-center"> <button class="btn btn-success" id="show-parent">Show Parentage</button></h3>--}}
    @if($dog->father || $dog->mother)
        <?php
        $father_id = \App\Dog::getDogId($dog->father);
        $mother_id = \App\Dog::getDogId($dog->mother);
        ?>
    @endif

</div>