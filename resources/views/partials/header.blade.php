
<header class="navbar" role="banner" style="background-color: white; border: none;">

    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-cog"></i>
            </button>

            <a href="{{url('/')}}" class="navbar-brand navbar-brand-img" id="navbar" style="margin-top: 10px;">
                {{--<img src="img/logo.png" alt="MVP Ready">--}}

                <h2 id="title">KENNEL UNION OF GHANA PEDIGREE DATABASE</h2>
            </a>
        </div> <!-- /.navbar-header -->

        <nav class="collapse navbar-collapse" role="navigation">

            <ul class="nav navbar-nav navbar-left">

                @can('administer')

                <li class="dropdown navbar-notification">

                    <a href="" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell navbar-notification-icon"></i>
                        <span class="visible-xs-inline">&nbsp;Notifications</span>
                        <b class="badge badge-primary">
                            {{\App\Dog::checkUnconfirmedDogs() + \App\User::checkUnconfirmedMembers() + \App\RequestedCertificate::getRequestedCertificates()}}
                        </b>
                    </a>

                    <div class="dropdown-menu">

                        <div class="dropdown-header">&nbsp;Notifications</div>

                        <div class="notification-list">

                            @if(\App\User::checkUnconfirmedMembers() == 0)

                                @else
                            <a href="{{url('/admin/members?members=unconfirmed')}}" class="notification">
                                <span class="notification-icon"><i class="fa fa-user text-primary"></i></span>
                                <span class="notification-title">New Members</span>
                                 {{--{{\App\User::getLastMemberTime()}}--}}
                                <span class="notification-description"><strong>{{\App\User::checkUnconfirmedMembers()}}</strong> Member(s) awaiting confirmation.</span>
                                {{--<span class="notification-time"> {{getDogRegisteredDay(\App\User::getLastMemberTime())}}</span>--}}
                            </a> <!-- / .notification -->
                            @endif

                                @if(\App\Dog::checkUnconfirmedDogs() == 0)

                                    @else
                            <a href="{{url('/admin/all-dogs?dogs=unconfirmed')}}" class="notification">
                                <span class="notification-icon"><i class="fa fa-list text-secondary"></i></span>
                                <span class="notification-title">New Dogs </span>
                                <span class="notification-description"><strong>{{\App\Dog::checkUnconfirmedDogs()}}</strong> New Dog(s) Awaiting confirmation</span>
                                {{--<span class="notification-time">{{getDogRegisteredDay(\App\Dog::getLastDogTime())}}</span>--}}
                            </a> <!-- / .notification -->
                                @endif

                                @if(\App\RequestedCertificate::getRequestedCertificates() == 0)

                                @else
                                    <a href="{{url('/admin/certificate-requests')}}" class="notification">
                                        <span class="notification-icon"><i class="fa fa-list text-secondary"></i></span>
                                        <span class="notification-title">New Certficate Requests </span>
                                        <span class="notification-description"><strong>{{\App\RequestedCertificate::getRequestedCertificates()}}</strong> New Certificates Requests </span>
                                        {{--<span class="notification-time">{{getDogRegisteredDay(\App\RequestedCertificate::getLastRequestedCertificateTime())}}</span>--}}
                                    </a> <!-- / .notification -->
                                @endif

                        </div> <!-- / .notification-list -->

                    </div> <!-- / .dropdown-menu -->

                </li>

                <li class="dropdown navbar-notification">

                </li>

                <li class="dropdown navbar-notification empty">

                    <a href="page-notifications.html" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-warning navbar-notification-icon"></i>
                        <span class="visible-xs-inline">&nbsp;&nbsp;Alerts</span>
                    </a>

                    <div class="dropdown-menu">

                        <div class="dropdown-header">Alerts</div>

                        <div class="notification-list">

                            <h4 class="notification-empty-title">No alerts here.</h4>
                            <p class="notification-empty-text">Check out what other makers are doing on Explore!</p>

                        </div> <!-- / .notification-list -->

                        <a href="page-notifications.html" class="notification-link">View All Alerts</a>

                    </div> <!-- / .dropdown-menu -->

                </li>
                @endcan

            </ul>

            <ul class="nav navbar-nav navbar-right">

                <li>
                    {{--<a href="javsacript:;"> {{'Hello ' . Auth::getUser()->first_name .' '. Auth::getUser()->last_name }}</a>--}}
                </li>

                {{--<li>--}}
                    {{--<a href="javascript:;">Support</a>--}}
                {{--</li>--}}
                @can('administer')
                <li class="dropdown navbar-profile">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
                        {{--<img src="" class="navbar-profile-avatar" alt="">--}}
                        {{ Auth::getUser()->first_name .' '. Auth::getUser()->last_name }}
                        {{--<span class="visible-xs-inline">@peterlandt &nbsp;</span>--}}
                        <i class="fa fa-caret-down"></i>
                    </a>

                    <ul class="dropdown-menu" role="menu">

                        <li>
                            <a href="{{url('/profile-settings')}}">
                                <i class="fa fa-user"></i>
                                &nbsp;&nbsp;Profile
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="{{url('auth/logout')}}">
                                <i class="fa fa-sign-out"></i>
                                &nbsp;&nbsp;Logout
                            </a>
                        </li>

                    </ul>

                </li>
                @elseif(Auth::check() == true )
                    <li class="dropdown navbar-profile">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
                            {{--<img src="" class="navbar-profile-avatar" alt="">--}}
                            {{ Auth::getUser()->first_name .' '. Auth::getUser()->last_name }}
                            {{--<span class="visible-xs-inline">@peterlandt &nbsp;</span>--}}
                            <i class="fa fa-caret-down"></i>
                        </a>

                        <ul class="dropdown-menu" role="menu">

                            <li>
                                <a href="{{url('/profile-settings')}}">
                                    <i class="fa fa-user"></i>
                                    &nbsp;&nbsp;Profile
                                </a>
                            </li>

                            <li class="divider"></li>

                            <li>
                                <a href="{{url('auth/logout')}}">
                                    <i class="fa fa-sign-out"></i>
                                    &nbsp;&nbsp;Logout
                                </a>
                            </li>

                        </ul>

                    </li>

                    @else
                    <li><a href="{{url('auth/register')}}">Register Now </a></li>
                <li> <a  href="{{url('auth/login')}}">Log In</a></li>
                    @endcan


            </ul>

        </nav>

    </div> <!-- /.container -->

</header>
<div class="mainnav ">

    <div class="container">

    </div> <!-- /.container -->

</div> <!-- /.mainnav -->

@include('partials.analyticstracking');