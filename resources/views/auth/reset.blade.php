@extends('layouts.auth-layout')


@section('content')

    <div class="account-wrapper">

        <div class="account-body">
            @include('flash::message')
            <h3> Kennel Union Of Ghana Pedigree Database.</h3>

            <h5>Please sign in to get access.</h5>

            <form class="form account-form" method="POST" action="/password/reset">
                {!! csrf_field() !!}

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group">
                    <label for="login-username" class="placeholder-hidden">Email</label>
                    <input type="text" class="form-control" id="login-username" placeholder="Email" name="email"  value="{{old('email')}}" tabindex="1">
                </div> <!-- /.form-group -->

                <div class="form-group">
                    <label for="login-password" class="placeholder-hidden">Password</label>
                    <input type="password" class="form-control" id="login-password" placeholder="Password" name="password" value="{{old('password')}}" tabindex="2">
                </div> <!-- /.form-group -->

                <div class="form-group">
                    <label for="login-password" class="placeholder-hidden">Confirm Password</label>
                    <input type="password" class="form-control" id="login-password" placeholder="Confirm Password" name="password_confirmation" value="{{old('password')}}" tabindex="2">
                </div> <!-- /.form-group -->

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-lg" tabindex="4">
                        Reset  &nbsp; <i class="fa fa-play-circle"></i>
                    </button>
                </div> <!-- /.form-group -->

            </form>

        </div> <!-- /.account-body -->

    </div> <!-- /.account-wrapper -->
@stop
