<!DOCTYPE html>
<html lang="en">
<head>
    <title>Register - Kennel Union of Ghana</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{asset("vendor/bootstrap/css/bootstrap.min.css")}}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{asset("fonts/font-awesome-4.7.0/css/font-awesome.min.css")}}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{asset("fonts/Linearicons-Free-v1.0.0/icon-font.min.css")}}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{asset("vendor/animate/animate.css")}}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{asset("vendor/css-hamburgers/hamburgers.min.css")}}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{asset("vendor/animsition/css/animsition.min.css")}}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{asset("vendor/select2/select2.min.css")}}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{asset("vendor/daterangepicker/daterangepicker.css")}}>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href={{asset("css/util.css")}}>
    <link rel="stylesheet" type="text/css" href={{asset("css/main.css")}}>

    {!! \Artesaos\SEOTools\Facades\SEOMeta::generate() !!}

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <style>
        .input100 {
            font-family: Raleway-Medium;
            color: #555555;
            line-height: 1.2;
            font-size: 16px;
            display: block;
            width: 100%;
            background: transparent;
            height: 37px;
            padding: 0 25px 0 25px;
        }
    </style>

</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
{{--            @include('flash::message')--}}
            <form class="login100-form validate-form flex-sb flex-w" action="register" method="post">
                {!! csrf_field() !!}
                <span class="login100-form-title p-b-32" >
						Register
					</span>

                <input type="hidden" name="confirmed" value="0">

                <span class="txt1 p-b-11">
						Title
					</span>
                <div class="wrap-input100 validate-input m-b-36" data-validate = "Title is required">
                    <select class="input100" name="title" value="{{old('title')}}" required>
                        <option value="">Select title</option>
                        <option value="Mr">Mr</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Miss">Miss</option>
                        <option value="Dr">Dr</option>
                        <option value="Sir">Sir</option>
                    </select>

                    {{--                    <input class="input100" type="text" name="first_name" value="{{old('first_name')}}">--}}
                    <span class="focus-input100"></span>
                </div>

                <span class="txt1 p-b-11">
						First Name
					</span>
                <div class="wrap-input100 validate-input m-b-36" data-validate = "Username is required">
                    <input class="input100" type="text" name="first_name" value="{{old('first_name')}}">
                    <span class="focus-input100"></span>
                </div>

                <span class="txt1 p-b-11">
						Last Name
					</span>
                <div class="wrap-input100 validate-input m-b-36" data-validate = "First Name is required">
                    <input class="input100" type="text" name="last_name" value="{{old('last_name')}}">
                    <span class="focus-input100"></span>
                </div>


                <span class="txt1 p-b-11">
						Kennel Name
					</span>
                <div class="wrap-input100 validate-input m-b-36" data-validate = "Kennel Name is required">
                    <input class="input100" type="text" name="kennel_name" value="{{old('kennel_name')}}">
                    <span class="focus-input100"></span>
                </div>

                <span class="txt1 p-b-11">
						Email
					</span>
                <div class="wrap-input100 validate-input m-b-36" data-validate = "Email is required">
                    <input class="input100" type="text" name="email" value="{{old('email')}}">
                    <span class="focus-input100"></span>
                </div>

                <span class="txt1 p-b-11">
						Phone
					</span>
                <div class="wrap-input100 validate-input m-b-36" data-validate = "Phone is required">
                    <input class="input100" type="text" name="phone" value="{{old('phone')}}">
                    <span class="focus-input100"></span>
                </div>

                <span class="txt1 p-b-11">
						Password
					</span>
                <div class="wrap-input100 validate-input m-b-12" data-validate = "Password is required">
						<span class="btn-show-pass">
							<i class="fa fa-eye"></i>
						</span>
                    <input class="input100" type="password" name="password" value="{{old('password')}}" >
                    <span class="focus-input100"></span>
                </div>

                <span class="txt1 p-b-11">
						Confirm Password
					</span>
                <div class="wrap-input100 validate-input m-b-12" data-validate = "Confirm password is required">
						<span class="btn-show-pass">
							<i class="fa fa-eye"></i>
						</span>
                    <input class="input100" type="password" name="password_confirmation" value="{{old('password')}}" >
                    <span class="focus-input100"></span>
                </div>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="flex-sb-m w-full p-b-48">
                    <div class="contact100-form-checkbox">
                        <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                        <label class="label-checkbox100" for="ckb1">
                            Remember me
                        </label>
                    </div>

                    <div>
                        <a href="{{url('/login')}}" class="txt3">
                            Login
                        </a>
                    </div>
                </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn" type="submit">
                        Register
                    </button>

                    {{--                    <button class="login100-form-btn" type="submit">--}}
                    {{--                        Register--}}
                    {{--                    </button>--}}
                </div>

            </form>
        </div>
    </div>
</div>


<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src={{asset("vendor/jquery/jquery-3.2.1.min.js")}}></script>
<!--===============================================================================================-->
<script src={{asset("vendor/animsition/js/animsition.min.js")}}></script>
<!--===============================================================================================-->
<script src={{asset("vendor/bootstrap/js/popper.js")}}></script>
<script src={{asset("vendor/bootstrap/js/bootstrap.min.js")}}></script>
<!--===============================================================================================-->
<script src={{asset("vendor/select2/select2.min.js")}}></script>
<!--===============================================================================================-->
<script src={{asset("vendor/daterangepicker/moment.min.js")}}></script>
<script src={{asset("vendor/daterangepicker/daterangepicker.js")}}></script>
<!--===============================================================================================-->
<script src={{asset("vendor/countdowntime/countdowntime.js")}}></script>
<!--===============================================================================================-->
<script src={{asset("js/main.js")}}></script>

</body>
</html>
