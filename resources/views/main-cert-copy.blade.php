
<html lang="en">
<head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.js')}}"></script>

    <style>
        .hide-column{
            display: none;
        }
        .water-mark{
            z-index: 20000;
            position: absolute;
            top: 450px;
            left: 320px;
            font-weight: bold;
            font-size: 60px;
            opacity: 0;
            -ms-transform: rotate(45deg); /* IE 9 */
            -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
            transform: rotate(45deg);
        }

        .first{
            border: 1px solid #000000;
            height: 50px;
            width: 90%;
            margin: auto;
            background-color:rgba(200, 247, 197, 0.9);
        }

        .second{
            border: 1px solid #000000;
            height: 50px;
            width: 100%;
            margin: auto;
            background-color:rgba(200, 247, 197, 0.9);
        }
        .third{
            border: 1px solid #000000;
            height: 50px;
            width: 100%;
            margin: auto;
            background-color: #ffffff;
            border-top: 0;
            border-bottom: 0;
        }

        .fourth{
            border: 1px solid #000000;
            height: 50px;
            width: 95%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }

        .table-container{
            border: 1px solid #000;
        }


        .male {
            background-color:rgba(200, 247, 197, 0.5);
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        .female {
            background-color: #FFFFFF;
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        table,.dog-info{
            margin:auto;
        }

        .dog-info{
            width: 94%;
        }

        hr{
            width: 94%;
            margin:auto;
        }


    </style>

</head>


<div class="content">

    <div class="container-fluid">

        <div class="portlet portlet-default">

            <div class="portlet-body main-table">

                <div>
                    <div class="colored-border"
                         style="border: 2px solid #000; height: 10px; position: relative; top: 55px; background-color: red;">
                    </div>

                    <div class="colored-border"
                         style="border: 2px solid #000; height: 10px; position: relative; top: 55px; border-top: 0; background-color: yellow;">
                    </div>

                    <div class="colored-border"
                         style="border: 2px solid #000; height: 10px; position: relative; top: 55px; border-top: 0; background-color: green;">
                    </div>
                </div>

                <div class="table-container" style="margin-top: 10px; padding-top: 70px; padding-bottom: 70px; margin-bottom: 10px">

                    <?php
//                    $dob = Carbon\Carbon::createFromFormat('Y-m-d',$dog->dob);
//                    $difference = $dob->diffInMonths(Carbon\Carbon::now());
//                    $certificate = $difference > 12 ? 'PEDIGREE ' : 'BIRTH ';
                    ?>

                    <div class="water-mark" style="display: none">
                        KENNEL UNION OF GHANA
                    </div>

{{--                    <img width="60" height="60" style="border-radius: 50%;--}}
{{--                      position: absolute; top: 110px; left: 4%;" src="{{url('/img/kug_logo.jpg')}}" alt="">--}}

{{--                    <div class="row dog-info" style="color:#283846">--}}

{{--                        <div class="row first" style="margin-bottom: 10px;" >--}}
{{--                            <table class="table" style="border-right: 1px solid #000;">--}}
{{--                                <tr>--}}
{{--                                    <td>Name: {{ucwords(strtolower($dog->name))}}</td>--}}
{{--                                    <td > GH {{$dog->registration_number}}</td>--}}
{{--                                </tr>--}}
{{--                            </table>--}}

{{--                        </div>--}}

{{--                        <div class="clearfix"></div>--}}
{{--                        <img width="60" height="60"--}}
{{--                             style="border-radius: 50%;--}}
{{--                             position: absolute; top: 110px; left: 100%;"--}}
{{--                             src="{{url('/img/kug_logo.jpg')}}">--}}

{{--                        <div class="row second" style="margin-top: 100px;">--}}

{{--                            <table class="table ">--}}
{{--                                <?php--}}
{{--                                $kennel_name = $dog->kennel_name != null?"(".$dog->kennel_name.")": "";--}}
{{--                                ?>--}}
{{--                                    <tr>--}}
{{--                                        <td>Sex : {{  $dog->sex == "male" ? "Sire":"Dam" }} </td>--}}
{{--                                        <td>&nbsp;&nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;--}}
{{--                                            &nbsp;  &nbsp;  &nbsp;  &nbsp; &nbsp; &nbsp;  &nbsp;  &nbsp;  &nbsp;--}}
{{--                                            &nbsp;  &nbsp; &nbsp; &nbsp;  &nbsp;  &nbsp;  &nbsp;--}}
{{--                                            &nbsp;  &nbsp; &nbsp;--}}
{{--                                        </td>--}}
{{--                                        <td>Breeder : {{ ucwords(strtolower($dog->first_name.' '. $dog->last_name)).$kennel_name}}</td>--}}
{{--                                    </tr>--}}

{{--                            </table>--}}

{{--                        </div>--}}

{{--                        <div class="row third" style="position: relative; top: 37px">--}}
{{--                            <table class="table">--}}
{{--                                <tr>--}}

{{--                                    <td>Colour : {{$dog->colour}}</td>--}}
{{--                                    <td>Coat : {{$dog->coat}}</td>--}}
{{--                                    <td>Location : {{Request::query('address')}}</td>--}}
{{--                                </tr>--}}
{{--                            </table>--}}
{{--                        </div>--}}

{{--                        <div class="row second">--}}
{{--                            <table class="table">--}}

{{--                                <tr>--}}
{{--                                    <td>Breed : {{$dog->breed}} </td>--}}
{{--                                    <td>Dob : {{$dog->dob}}</td>--}}
{{--                                </tr>--}}

{{--                            </table>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                    <div>

{{--                        <hr>--}}
                        <br>

                        <table border="0" cellpadding="2" width='95%' style="margin-top: 140px;">
                            <tr><td>
                                    <table
                                        style=" border-collapse: collapse;" border="1" cellpadding="2" cellspacing="2" width="100%"  CELLSPACING="2">
                                        <tr>
                                            <th style="text-align: center;">I Parents</th>
                                            <th style="text-align: center;">II Grand Parents</th>
                                            <th style="text-align: center;">III Great Grand Parents</th>
                                            <th style="text-align: center;">IV Great Great Grand Parents</th>
                                        </tr>
                                        <tr>
                                            <td rowspan='16' width='17%' class='male'>
                                                <div class="h5">
                                                    @if ($dog->father)
                                                        {{\App\Dog::getRelationship($dog->father)}}<br>
                                                        {{$dog->father}}<br>
                                                        @php
                                                            $exist = \App\Dog::whereRegistrationNumber($dog->father)->first();
                                                        @endphp
                                                        @if($exist)
                                                            {{$exist->other_registration_number}}
                                                        @endif
                                                        {{--     <br> {{$dog->hip_hd_results}}--}}
                                                        {{--    <br> {{$dog->elbow_hd_results}}--}}

                                                    @endif

                                                </div>
                                            </td>
                                            <td rowspan='8' width='17%' class='male'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($dog->father,'father')}}<br/>
                                                    {{$secondgen = \App\Dog::getParentId($dog->father,'father','registration_number')}}

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($secondgen)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='4' width='17%' class='male'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($secondgen,'father')}}<br/>
                                                    {{$third_generation = \App\Dog::getParentId($secondgen,'father','registration_number')}}

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($third_generation)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif

                                                </div>
                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($third_generation,'father')}}<br/>
                                                    {{$fifth_generation = \App\Dog::getParentId($third_generation,'father','registration_number')}}

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($fifth_generation)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp;</td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                                        </tr><tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">

                                                    {{\App\Dog::getParent($third_generation,'mother')}}<br/>
                                                    {{$generation_five = \App\Dog::getParentId($third_generation,'mother','registration_number')}}

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_five)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='4' width='17%' class='female'>
                                                <div class="h5">

                                                    {{\App\Dog::getParent($secondgen,'mother')}}<br/>
                                                    {{$fourth_generation = \App\Dog::getParentId($secondgen,'mother','registration_number')}}

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($fourth_generation)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif
                                                </div>

                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h5">

                                                    {{\App\Dog::getParent($fourth_generation,'father')}}<br/>
                                                    {{$generation_five_b = \App\Dog::getParentId($fourth_generation,'father','registration_number')}}

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_five_b)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($fourth_generation,'mother')}}<br>
                                                    {{$generation_five_c = \App\Dog::getParentId($fourth_generation,'mother','registration_number')}}

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_five_c)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='8' width='17%' class='female'>
                                                <div class="h5">

                                                    {{\App\Dog::getParent($dog->father,'mother')}} <br>
                                                    {{$thirdgen_mother = \App\Dog::getParentId($dog->father,'mother','registration_number')}}

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($thirdgen_mother)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif

                                                </div>
                                            </td>
                                            <td rowspan='4' width='17%' class='male'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($thirdgen_mother,'father')}}<br>
                                                    {{$fourth_gen = \App\Dog::getParentId($thirdgen_mother,'father','registration_number')}}<br>


                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($fourth_gen)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h5">{{\App\Dog::getParent($fourth_gen,'father')}}<br/>
                                                    {{$generation_five_d = \App\Dog::getParentId($fourth_gen,'father','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_five_d)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">{{\App\Dog::getParent($fourth_gen,'mother')}}<br/>
                                                    {{$generation_five_e = \App\Dog::getParentId($fourth_gen,'mother','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_five_e)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif
                                                </div>


                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp;</td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='4' width='17%' class='female'>
                                                <div class="h5">{{\App\Dog::getParent($thirdgen_mother,'mother')}}<br/>
                                                    {{$generation_four = \App\Dog::getParentId($thirdgen_mother,'mother','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_four)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif
                                                </div>

                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h5">{{\App\Dog::getParent($generation_four,'father')}}<br/>
                                                    {{$generation_five_f = \App\Dog::getParentId($generation_four,'father','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_five_f)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($generation_four,'mother')}}<br/>
                                                    {{$generation_five_g = \App\Dog::getParentId($generation_four,'mother','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_five_g)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif

                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp;</td>
                                        </tr><tr>
                                            <td rowspan='16' width='17%' class='female' >

                                                <div class="h5">

                                                    @if ($dog->mother)
                                                        {{\App\Dog::getRelationship($dog->mother)}}<br/>
                                                        {{$dog->mother}}<br>

                                                        @php
                                                            $exist = \App\Dog::whereRegistrationNumber($dog->mother)->first();
                                                        @endphp
                                                        @if($exist)
                                                            {{$exist->other_registration_number}}
                                                        @endif
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='8' width='17%' class='male'>
                                                <div class="h5">

                                                    {{\App\Dog::getParent($dog->mother,'father')}}<br/>
                                                    {{$generation_two_a = \App\Dog::getParentId($dog->mother,'father','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_two_a)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='4' width='17%' class='male'>
                                                <div class="h5">

                                                    {{\App\Dog::getParent($generation_two_a,'father')}}<br/>
                                                    {{ $generation_two_c = \App\Dog::getParentId($generation_two_a,'father','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_two_c)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif

                                                </div>

                                            </td>
                                            <td rowspan='2' width='17%' class='male'>
                                                <div class="h5">{{\App\Dog::getParent($generation_two_c,'father')}}<br/>
                                                    {{$generation_4_d =  \App\Dog::getParentId($generation_two_c,'father','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_4_d)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">{{\App\Dog::getParent($generation_two_c,'mother')}}<br/>
                                                    {{$generation_5_a =  \App\Dog::getParentId($generation_two_c,'mother','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_5_a)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif
                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='4' width='17%' class='female'>
                                                <div class="h5">{{\App\Dog::getParent($generation_two_a,'mother')}}<br/>
                                                    {{$generation_3_a =  \App\Dog::getParentId($generation_two_a,'mother','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_3_a)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='2' width='17%' class='male'>

                                                <div class="h5">{{\App\Dog::getParent($generation_3_a,'father')}}<br/>
                                                    {{$generation_5_e = \App\Dog::getParentId($generation_3_a,'father','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_5_e)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif

                                                </div>

                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">{{\App\Dog::getParent($generation_3_a,'mother')}}<br/>
                                                    {{$generation_5_f = \App\Dog::getParentId($generation_3_a,'mother','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_5_f)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr><tr>
                                            <td rowspan='8' width='17%' class='female'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($dog->mother,'mother')}}<br/>
                                                    {{$generation_two_b = \App\Dog::getParentId($dog->mother,'mother','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_two_b)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='4' width='17%' class='male'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($generation_two_b,'father')}}<br/>
                                                    {{$generation_3_c = \App\Dog::getParentId($generation_two_b,'father','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_3_c)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif

                                                </div>

                                            </td>
                                            <td rowspan='2' width='17%' class='male'>

                                                <div class="h5">{{\App\Dog::getParent($generation_3_c,'father')}}<br/>
                                                    {{$generation_5_h = \App\Dog::getParentId($generation_3_c,'father','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_5_h)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif

                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>

                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>

                                        <tr>
                                            <td rowspan='2' width='17%' class='female'>
                                                <div class="h5">{{\App\Dog::getParent($generation_3_c,'mother')}}<br/>
                                                    {{$generation_5_i= \App\Dog::getParentId($generation_3_c,'mother','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_5_i)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif
                                                </div>
                                            </td>
                                            <td rowspan='1' width='17%' class='male hide-column'>&nbsp; </td>
                                        </tr>

                                        <tr>
                                            <td rowspan='1' width='17%' class='female hide-column'>&nbsp; </td>
                                        </tr>

                                        <tr>
                                            <td rowspan='4' width='17%' class='female'>
                                                <div class="h5">
                                                    {{\App\Dog::getParent($generation_two_b,'mother')}}<br/>
                                                    {{$generation_4_b = \App\Dog::getParentId($generation_two_b,'mother','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_4_b)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif
                                                </div>
                                            </td>

                                            <td rowspan='1' width='17%' class='male'>
                                                <div class="h5">{{\App\Dog::getParent($generation_4_b,'father')}}<br/>
                                                    {{$generation_5_j =\App\Dog::getParentId($generation_4_b,'father','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_5_j)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif
                                                </div>
                                            </td>

                                        </tr>

                                        <tr>
                                            <td rowspan='1' width='17%' class='female'>
                                                <div class="h5">{{\App\Dog::getParent($generation_4_b,'mother')}}<br/>
                                                    {{$generation_5_j =\App\Dog::getParentId($generation_4_b,'mother','registration_number')}}<br>

                                                    @php
                                                        $exist = \App\Dog::whereRegistrationNumber($generation_5_j)->first();
                                                    @endphp
                                                    @if($exist)
                                                        {{$exist->other_registration_number}}
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                        </table>

                        <div class="row fourth" style="margin-top: -40px;">
                            <table class="table">
                                <tr style="border-bottom: 0">
                                    <td style="padding-top: 20px">Microchip/Tattoo: {{$dog->microchip_number}}</td>
                                    <td style="padding-top: 20px">Date of Issue:<em> <?php echo date('d-m-y'); ?> </em></td>
                                    <td>Certified : <img width="80px" height="45px" src="{{asset("images/kug_signature.png")}}" alt=""/>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div style="margin-top: -30px">
                            <div class="colored-border"
                                 style="border: 2px solid #000; height: 10px; position: relative;
                                 border-left: 0; border-right: 0; top: 55px; background-color: red;">
                            </div>

                            <div class="colored-border"
                                 style="border: 2px solid #000; height: 10px; position: relative; top: 55px;
                                 border-left: 0; border-right: 0; border-top: 0; background-color: yellow;">
                            </div>

                            <div class="colored-border"
                                 style="border: 2px solid #000; height: 10px; position: relative; top: 55px;
                                 border-right: 0; border-left: 0; border-top: 0; background-color: green;">
                            </div>
                        </div>
                    </div>
                </div>

                @if($dog->father || $dog->mother)
                    <?php
                    $father_id = \App\Dog::getDogId($dog->father);
                    $mother_id = \App\Dog::getDogId($dog->mother);
                    ?>
                @endif
            </div>
        </div>
    </div>
</div>
</html>

