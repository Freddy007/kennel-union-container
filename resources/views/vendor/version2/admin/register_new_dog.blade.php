@extends('version2.layouts.admin_layout')

@section('scripts')

@include('version2.admin.partials.register_dog_partials')

<script>
    initializeDogSelect('/version2/dog');

    $('.custom_registration_number').hide();

    $('#dog-not-born-in-ghana').on('click',function () {
        if($(this).is(':checked')){
            $('.custom_registration_number').show()
        }else{
            $('.custom_registration_number').hide();
        }
    });

    $('#name').on('change',function(){
//        alert('hello');
        var name = $('#name').val();
        jQuery.ajax('/dog-by-name/'+name,function(data){
        }).fail(function (data) {
            $('.help-block.name').text(" This dog's name already exists!").css('color','red');
            $('#submit_form > div > div.form-actions > div > div > a.btn.btn-outline.green.button-next').hide()
        }).done(function (data) {
            $('#submit_form > div > div.form-actions > div > div > a.btn.btn-outline.green.button-next').show();
            $('.help-block.name').text(' ');
        })
    })

</script>

@endsection


@section('content')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>REGISTER NEW DOG
                        <small>form wizard </small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->
                <div class="page-toolbar">
                    <!-- BEGIN THEME PANEL -->
                    <!-- END THEME PANEL -->
                </div>
                <!-- END PAGE TOOLBAR -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content" >
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{url('/version2/all-dogs')}}">Dashboard</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                    <a href="{{url('/version2/all-dogs')}}">dogs</a>
                    <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>register new dog</span>
                    </li>
                </ul>

                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">

                    @include('version2.admin.partials.register_dog_form')

                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
@endsection
