@extends('version2.layouts.admin_layout')

@section('styles')

    <link href="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

    @endsection

    @section('scripts')
    {{--<script src="{{asset('datatables/jquery.dataTables.min.js')}}"></script>--}}

    {{--    <script src="{{asset('kug_version2/assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>--}}

            <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('kug_version2/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>


    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        jQuery(document).ready(function() {
            $('#add-new-dog').off('click').on('click', function () {
                $('#register-dog-modal').modal();
            });


            $('#print').off('click').on('click', function() {

                $.post("{{url('admin/issue-certificate',$dog->id)}}",function(data){
                    alert(data);
                });
            });

            $('#show-parent').on('click',function(){
                $('#parentage-table').show()
            });

        });

    </script>

    @endsection


    @section('content')
            <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <div class="container">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                        <h1>Certificate
                            {{--<small>managed datatable samples</small>--}}
                        </h1>
                    </div>
                    <!-- END PAGE TITLE -->

                </div>
            </div>
            <!-- END PAGE HEAD-->

            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{url('/version2')}}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{url('/version2/all-dogs')}}">All dogs</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        {{--<li>--}}
                            {{--<a href="#">Tables</a>--}}
                            {{--<i class="fa fa-circle"></i>--}}
                        {{--</li>--}}
                        <li>
                            <span>Pedigree certificate</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">

                        <div class="row">
                            <div class="col-md-12">
                                @include('flash::message')

                                @include('partials.pdfcert')
                            </div>
                        </div>

                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->

@endsection