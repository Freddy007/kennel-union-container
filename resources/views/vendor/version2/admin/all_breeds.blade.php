@extends('version2.layouts.admin_layout')

@section('styles')

    <link href="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('scripts')
    {{--<script src="{{asset('datatables/jquery.dataTables.min.js')}}"></script>--}}

    <script src="{{asset('kug_version2/assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>


    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('kug_version2/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('kug_version2/assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>


    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        jQuery(document).ready(function() {
            $('#add-new-dog').off('click').on('click', function () {
                $('#register-breeder-modal').modal();
            });

            @foreach($breeders as $breeder)
            $('#edit-breeder-<?php echo $breeder->id ?>').off('click').on('click', function () {
                $("edit-name-<?php $breeder->id ?>").val('loading...').attr('disabled', 'true'); //show when submitting
                $('#edit-breeder-modal-<?php echo  $breeder->id ?>').modal();
                $.post("{{url('admin/breeder',[$breeder->id])}}", function (data) {
                    $('#edit-name-<?php echo $breeder->id ?>').val(data);
                    $("edit-name-<?php $breeder->id ?>").attr('disabled', 'false'); //show when submitting
                });
            });

            @endforeach

            });

    </script>

    @endsection


    @section('content')
            <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <div class="container">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                        <h1>All Breeds
                            {{--<small>managed datatable samples</small>--}}
                        </h1>
                    </div>
                    <!-- END PAGE TITLE -->
                </div>
            </div>
            <!-- END PAGE HEAD-->

            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{url('/version2')}}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        {{--<li>--}}
                            {{--<a href="#">More</a>--}}
                            {{--<i class="fa fa-circle"></i>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">Tables</a>--}}
                            {{--<i class="fa fa-circle"></i>--}}
                        {{--</li>--}}
                        <li>
                            <span>All Breeds</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        {{--<div class="m-heading-1 border-green m-bordered">--}}
                            {{--<h3>DataTables jQuery Plugin</h3>--}}
                            {{--<p> DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, and will add advanced interaction controls to any HTML table. </p>--}}
                            {{--<p> For more info please check out--}}
                                {{--<a class="btn red btn-outline" href="http://datatables.net/" target="_blank">the official documentation</a>--}}
                            {{--</p>--}}
                        {{--</div>--}}
                        <div class="row">
                            <div class="col-md-12">
                                @include('flash::message')

                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            {{--<i class="icon-settings font-dark"></i>--}}
                                            <span class="caption-subject bold uppercase"> Breeds</span>
                                        </div>
                                        {{--<div class="actions">--}}
                                            {{--<div class="btn-group btn-group-devided" data-toggle="buttons">--}}
                                                {{--<label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">--}}
                                                    {{--<input type="radio" name="options" class="toggle" id="option1">Actions</label>--}}
                                                {{--<label class="btn btn-transparent dark btn-outline btn-circle btn-sm">--}}
                                                    {{--<input type="radio" name="options" class="toggle" id="option2">Settings</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="btn-group" id="add-new-dog">
                                                        <button id="sample_editable_1_new" class="btn sbold green"> Add New
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    {{--<div class="btn-group pull-right">--}}
                                                        {{--<button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools--}}
                                                            {{--<i class="fa fa-angle-down"></i>--}}
                                                        {{--</button>--}}
                                                        {{--<ul class="dropdown-menu pull-right">--}}
                                                            {{--<li>--}}
                                                                {{--<a href="javascript:;">--}}
                                                                    {{--<i class="fa fa-print"></i> Print </a>--}}
                                                            {{--</li>--}}
                                                            {{--<li>--}}
                                                                {{--<a href="javascript:;">--}}
                                                                    {{--<i class="fa fa-file-pdf-o"></i> Save as PDF </a>--}}
                                                            {{--</li>--}}
                                                            {{--<li>--}}
                                                                {{--<a href="javascript:;">--}}
                                                                    {{--<i class="fa fa-file-excel-o"></i> Export to Excel </a>--}}
                                                            {{--</li>--}}
                                                        {{--</ul>--}}
                                                    {{--</div>--}}
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover order-column" id="datatable-buttons">
                                            <thead>
                                            <tr>
                                            <tr>
                                                <th>No.</th>
                                                <th>Breed</th>
                                                <th class="text-center">Actions</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $i = $breeders->firstItem(); ?>
                                            @foreach($breeders as $breeder)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td class="valign-middle">
                                                        {{$breeder->name}}
                                                    </td>


                                                    <td class="text-center valign-middle">
{{--                                                        <a href="{{url('/admin/breeder-dogs',$breeder->id)}}" class="btn btn-success btn-sm">dogs</a>--}}
                                                        &nbsp;
                                                        {{--<a href="{{url('/admin/breeder-edit',$breeder->id)}}" class="btn btn-success btn-sm">Edit</a>--}}
                                                        <button  class="btn btn-success btn-sm" id="edit-breeder-{{$breeder->id}}">edit</button>
                                                        &nbsp;
                                                        {{--<a href="{{url('/admin/breeder-delete',$breeder->id)}}" class="btn btn-danger btn-sm">Delete</a>--}}
                                                    </td>
                                                </tr>

                                                <div class="modal fade" id="edit-breeder-modal-{{$breeder->id}}" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title" id="myModalLabel"><strong>EDIT BREED</strong></h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{url('admin/edit-breeder',$breeder->id)}}" method="post" id="breeder-form">
                                                                    {!! csrf_field() !!}
                                                                    <div class="row">
                                                                        <div class="col-sm-12">

                                                                            <div class="portlet portlet-boxed portlet-success">

                                                                                <div class="portlet-body">
                                                                                    <div class="row">
                                                                                        <div class="col-sm-6 col-sm-offset-3">
                                                                                            <div class="form-group">
                                                                                                <label class="control-label" for="name">Name</label>

                                                                                                <input type="text" id="edit-name-{{$breeder->id}}" name="edited_name" class="form-control" required placeholder="Edit Breed">
                                                                                            </div> <!-- /.form-group -->

                                                                                        </div>
                                                                                    </div>

                                                                                </div> <!-- /.portlet-body -->

                                                                            </div> <!-- /.portlet -->

                                                                        </div> <!-- /.col -->

                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <input type="submit" id="add-breeder" class="btn btn-success" value="Edit {{$breeder->name}} breed">
                                                                    </div>

                                                                </form>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            @endforeach
                                            </tbody>
                                        </table>
                                        {{--{!! $breeders->render() !!}--}}

                                        <?php
                                        if(isset($paginator)){ ?>
                                        {{--{!! $paginator->render() !!}--}}
                                        {!! $paginator->appends(['term' => Request::get('term')])->render() !!}

                                        <?php   }else {?>
                                        {!! $breeders->render() !!}
                                        <?php } ?>

                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->

    <div class="modal fade" id="register-breeder-modal" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong>ADD NEW BREED</strong></h4>
                </div>
                <div class="modal-body">
                    <form action="{{url('admin/register-breeder')}}" method="post" id="dog-form">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-sm-12">

                                <div class="portlet portlet-boxed portlet-success">

                                    {{-- <div class="portlet-header">
                                        <h4 class="portlet-title">
                                            <u><i class="fa fa-check portlet-icon"></i>New Breed</u>
                                        </h4>
                                    </div> <!-- /.portlet-header --> --}}

                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <div class="form-group">
                                                    <label class="control-label" for="name">Name</label>
                                                    <input type="text" id="name" name="name" class="form-control" required placeholder="Add New Breed">
                                                </div> <!-- /.form-group -->

                                            </div>
                                        </div>

                                        <div id="message">

                                        </div>

                                    </div> <!-- /.portlet-body -->

                                </div> <!-- /.portlet -->

                            </div> <!-- /.col -->

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" id="add-breeder" class="btn btn-success" value="ADD NEW">
                        </div>

                    </form>

                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-breeder-modal" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong>EDIT BREED</strong></h4>
                </div>
                <div class="modal-body">
                    <form action="{{url('admin/edit-breeder')}}" method="post" id="breeder-form">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-sm-12">

                                <div class="portlet portlet-boxed portlet-success">

                                    {{--<div class="portlet-header">--}}
                                        {{--<h4 class="portlet-title">--}}
                                            {{--<u><i class="fa fa-check portlet-icon"></i>New Breed</u>--}}
                                        {{--</h4>--}}
                                    {{--</div> <!-- /.portlet-header -->--}}

                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <div class="form-group">
                                                    <label class="control-label" for="name">Name</label>
                                                    <input type="text" id="name" name="name" class="form-control" required placeholder="Add New Breeder">
                                                </div> <!-- /.form-group -->

                                            </div>
                                        </div>

                                        <div id="message">

                                        </div>

                                    </div> <!-- /.portlet-body -->

                                </div> <!-- /.portlet -->

                            </div> <!-- /.col -->

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="submit" id="add-breeder" class="btn btn-success" value="ADD NEW">
                        </div>

                    </form>

                </div>

            </div>
        </div>
    </div>

@endsection