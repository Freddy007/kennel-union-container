
<div class="columns" style="border: 1px solid #cccccc; border-radius: 7px; width: 50%; margin: auto; margin-bottom: 10px; padding: 10px 5px 0 5px;">
    <div class="form-group">
        <label class="control-label col-md-3">Name
            <span class="required"> * </span>
        </label>
        <div class="col-md-9">
            <input type="text" class="form-control name" name="name[]" id="name" required />
            <span class="help-block name">  </span>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-3">Gender
            <span class="required"> * </span>
        </label>
        <div class="col-md-9">
            <select class="form-control sex" name="sex[]">
                <option></option>
                <option>Male</option>
                <option>Female</option>
            </select>
            <span class="help-block">  </span>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-3">Colour
            <span class="required"> * </span>
        </label>
        <div class="col-md-9">
            <input type="text" class="form-control colour" name="colour[]" id="colour" required />
            <span class="help-block">  </span>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-3">Height
            <span class="required"> * </span>
        </label>
        <div class="col-md-9">
            <input type="text" class="form-control height" name="height[]" id="height" required />
            <span class="help-block">  </span>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-3">Coat
            <span class="required"> * </span>
        </label>
        <div class="col-md-9">

            <select class="form-control coat" name="coat[]" id="coat" required>

                <option>short</option>
                <option>long</option>
                <option>flat</option>
                <option>combination</option>
                <option>double</option>
                <option>heavy</option>
                <option>silky</option>
                <option>curly</option>
                <option>wire</option>

            </select>
            {{--<input type="text" class="form-control coat" name="coat[]" id="coat" required />--}}
            <span class="help-block">  </span>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-3">Microchip number
            <span class="required"> </span>
        </label>
        <div class="col-md-9">
            <input type="text" class="form-control microchip_number" name="microchip_number[]" id="coat" required />
            <span class="help-block">  </span>
        </div>
    </div>

</div>


