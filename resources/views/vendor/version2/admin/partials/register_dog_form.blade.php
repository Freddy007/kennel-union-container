<div class="row">
    <div class="col-md-12">
        <div class="portlet light " id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-red"></i>
                    <span class="caption-subject font-red bold uppercase"> Dog Registration Wizard -
                                                    <span class="step-title"> Step 1 of 4 </span>
                                                </span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-cloud-upload"></i>
                    </a>
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-wrench"></i>
                    </a>
                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                        <i class="icon-trash"></i>
                    </a>
                </div>
            </div>

            <div class="portlet-body form">
                <form action="{{url('version2/register-new-dog')}}" class="form-horizontal" id="submit_form" method="POST">
                    {{--<form action="{{url('admin/register-dog')}}" class="form-horizontal" id="submit_form" method="POST">--}}
                    <div class="form-wizard">
                        <div class="form-body">
                            <ul class="nav nav-pills nav-justified steps">
                                <li>
                                    <a href="#tab1" data-toggle="tab" class="step">
                                        <span class="number"> 1 </span>
                                        <span class="desc">

                                            <i class="fa fa-check"></i> Basic Details </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab2" data-toggle="tab" class="step">
                                        <span class="number"> 2 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Secondary Details </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab3" data-toggle="tab" class="step active">
                                        <span class="number"> 3 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Health Details </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab4" data-toggle="tab" class="step">
                                        <span class="number"> 4 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Confirm </span>
                                    </a>
                                </li>
                            </ul>

                            <div id="bar" class="progress progress-striped" role="progressbar">
                                <div class="progress-bar progress-bar-success"> </div>
                            </div>
                            <div class="tab-content">
                                <div class="alert alert-danger display-none">
                                    <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-none">
                                    <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                <div class="tab-pane active" id="tab1">
                                    <h3 class="block">Dog's Basic details</h3>
{{--                                    <input type="hidden" name="id" value="{{Webpatser\Uuid\Uuid::generate()}}" >--}}
                                    <input type="hidden" name="registration_number" id="registration_number" value="{{\App\Dog::generateRegistrationNumber()}}" >
                                    <input type="hidden" name="user_id" value="{{Auth::id()}}" >

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Name
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control name" name="name" id="name" required/>
                                            <i><span class="help-block name">  </span></i>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Gender
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <select class="form-control" name="sex">
                                                <option></option>
                                                <option>Male</option>
                                                <option>Female</option>
                                            </select>
                                            <span class="help-block">  </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Date of Birth
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="date" class="form-control" name="dob" id="dob" required />
                                            <span class="help-block">  </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Father(Sire)
                                            {{--<span class="required"> * </span>--}}
                                        </label>
                                        <div class="col-md-4">
                                            <select class="dog_parent form-control"  id="father" name="father"  data-gender="male" >
                                                <option value=""></option>
                                            </select>
                                            <span class="help-block">  </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Mother(Dam)
                                            {{--<span class="required"> * </span>--}}
                                        </label>
                                        <div class="col-md-4">
                                            <select class="dog_parent form-control" name="mother" id="mother" data-gender="female">
                                                <option value=""></option>
                                            </select>
                                            <span class="help-block">  </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Breed
                                            <span class="required"> * </span>
                                            {{--<span> * </span>--}}

                                        </label>
                                        <div class="col-md-4">
                                            {{--<select ng-model="selectedName" ng-options="item as item.name for item in breeds track by item.id" class="form-control" id="breed" name="breed">--}}
                                            <select  class="form-control" id="breed" name="breeder_id" required>
                                                <option></option>
                                                @foreach($breeds as $breed)
                                                    <option value="{{$breed->id}}">{{$breed->name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="help-block">  </span>

                                            {{--<select ng-options="item as item.label for item in items track by item.id" ng-model="selected"></select>--}}
                                            {{--<select  name="breed" id="breed" class="form-control" required>--}}
                                            {{--<option ng-repeat="breed in breeds"  >@{{ breed.name }}</option>--}}
                                            {{--</select>--}}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Dog not born in Ghana:</label>
                                        <div class="col-md-4">
                                            <input type="checkbox" name="dog_not_born_in_ghana" class="form-control" id="dog-not-born-in-ghana">
                                        </div>
                                    </div>

                                    <div class="form-group custom_registration_number">
                                        <label class="control-label col-md-3">Dog's registration number
                                            {{--<span class="required"> * </span>--}}
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="custom_registration_number">
                                            <span class="help-block">  </span>
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane" id="tab2">
                                    <h3 class="block">Secondary Details</h3>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Colour
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="colour" id="colour" required />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Coat
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            {{--<input type="text" class="form-control" name="coat" required />--}}
                                            <select class="form-control" name="coat" id="coat" required>
                                                <option>short</option>
                                                <option>long</option>
                                            </select>
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Height (cm)
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="height"  />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Titles
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="titles" />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Performance titles
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="performance_titles" />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane" id="tab3">
                                    <h3 class="block">Health details</h3>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Elbow ED Results
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="elbow_ed_results" />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Hip HD Results
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="hip_hd_results" />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Tattoo Number
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" placeholder="" class="form-control" name="tattoo_number" />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Microchip Number
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" placeholder="" class="form-control" name="microchip_number" />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">DNA ID
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" placeholder="" class="form-control" name="DNA" />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Other Health Checks
                                            <span class="required">  </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" placeholder="" class="form-control" name="other_health_checks" />
                                            <span class="help-block"> </span>
                                        </div>
                                    </div>



                                </div>
                                <div class="tab-pane" id="tab4">
                                    <h3 class="block">Confirm details</h3>

                                    <div class="alert alert-danger print-error-msg" style="display:none">

                                        <ul></ul>

                                    </div>

                                    <h4 class="form-section">Basic Details</h4>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Name:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="name"> </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Gender:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="sex"> </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Date of Birth:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" id="dob-val"> </p>
                                        </div>
                                    </div>


                                    {{--<div class="form-group">--}}
                                    {{--<label class="control-label col-md-3">Father:</label>--}}
                                    {{--<div class="col-md-4">--}}
                                    {{--<p class="form-control-static" data-display="father"> </p>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="form-group">--}}
                                    {{--<label class="control-label col-md-3">Mother:</label>--}}
                                    {{--<div class="col-md-4">--}}
                                    {{--<p class="form-control-static" data-display="mother"> </p>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="form-group">--}}
                                    {{--<label class="control-label col-md-3">Breed:</label>--}}
                                    {{--<div class="col-md-4">--}}
                                    {{--<p class="form-control-static" id="breed-val"> </p>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}

                                    <h4 class="form-section">Secondary Details</h4>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Colour:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="colour"> </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Coat:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="coat"> </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Height:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="height"> </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Titles:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="titles"> </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Performance Titles:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="performance_titles"> </p>
                                        </div>
                                    </div>

                                    <h4 class="form-section">Health Details</h4>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Elbow ED Results:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="elbow_ed_results"> </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">HIP HD Results:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="hip_hd_results"> </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Tattoo Number:</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="tattoo_number"> </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Microchip Number</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="microchip_number"> </p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">DNA ID</label>
                                        <div class="col-md-4">
                                            <p class="form-control-static" data-display="DNA"> </p>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <a href="javascript:;" class="btn default button-previous">
                                        <i class="fa fa-angle-left"></i> Back </a>
                                    <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                        <i class="fa fa-angle-right"></i>
                                    </a>

                                    <button type="submit" class="btn green button-submit">Submit
                                        <i class="fa fa-check"></i>
                                    </button>
                                    {{--<a href="javascript:;" class="btn green button-submit"> Submit--}}
                                    {{--<i class="fa fa-check"></i>--}}
                                    {{--</a>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>