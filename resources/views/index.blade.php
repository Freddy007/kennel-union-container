@extends('layouts.admin_master')

@section('scripts')
        <!-- Plugin JS -->
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
   <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="../../bower_components/flot/excanvas.min.js"></script>
    <script src="{{asset('../../bower_components/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('../../bower_components/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('../../bower_components/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('../../bower_components/flot/jquery.flot.time.js')}}"></script>
    <script src="{{asset('../../bower_components/flot.tooltip/js/jquery.flot.tooltip.js')}}"></script>
    <script src="{{asset('js/build/pdfmake.js')}}"></script>
    <script src="{{asset('js/build/vfs_fonts.js')}}"></script>
    <!-- App JS -->
    <script src="{{asset('../../global/js/mvpready-core.js')}}"></script>
    <script src="{{asset('../../global/js/mvpready-helpers.js')}}"></script>
    <script src="{{asset('js/mvpready-admin.js')}}"></script>

<script src="../../bower_components/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Plugin JS -->
<script src="../../bower_components/select2/dist/js/select2.min.js"></script>
<script src="../../bower_components/jquery-icheck/icheck.min.js"></script>
<script src="../../bower_components/parsleyjs/dist/parsley.js"></script>
<script src="../../bower_components/bootstrap-3-timepicker/js/bootstrap-timepicker.js"></script>

<script src="../../bower_components/bootstrap-jasny/js/fileinput.js"></script>


    <script src="{{asset('/js/typeahead.bundle.js')}}"></script>
    {{--<script>--}}
        {{--var docDefinition = { content: 'This is an sample PDF printed with pdfMake' };--}}
        {{--pdfMake.createPdf(docDefinition).download();--}}
    {{--</script>--}}

    <script>

        var dogs = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            // url points to a json file that contains an array of country names, see
            // https://github.com/twitter/typeahead.js/blob/gh-pages/data/countries.json
            prefetch: '/dogs'
        });

        dogs.initialize();

        // passing in `null` for the `options` arguments will result in the default
        // options being used
        $('.typeahead').typeahead(null,{
            name: 'dogs',
            source: dogs,

            template: [

                '<p class="repo-name">@{{name}}</p>',
            ].join(''),

            // template engine
            engine: Hogan

        });
        $(document).ready(function(){
            $('#advanced-search').on('click',function(){
                $('#advanced-search-modal').modal();
            })
        })

    </script>
    @stop
@section('content')

    <div class="content">
        <div class="container">
            {!! Breadcrumbs::render('home') !!}

            @include('flash::message')
            <div class="portlet-header">

                <h3 class="portlet-title"><u>Search Pedigree Database</u></h3>

            </div> <!-- /.portlet-header -->
            <div class="row" style="margin-bottom: 80px;">
                <form action="{{url('/')}}">
                    {!! csrf_field() !!}
                <div class="col-md-3">
                    <input type="text" class="form-control search-btn input-lg typeahead " name="term" value="{{Request::get('term')}}">
                </div>

                <div class="col-md-2 col-xs-5 col-md-offset-2 ">
                    <input type="submit" class="form-control btn btn-success input-lg" id="btnsearch">
                </div>
                </form>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-success btn-lg" id="advanced-search">Advanced Search</button>
                    </div>
            </div>

            <h3 class="portlet-title pull-right"><u>{{\App\Dog::where('confirmed',true)->count()}} Currently Registered Dogs</u></h3>


            <div class="portlet portlet-default">

                <div class="portlet-header">

                    <h3 class="portlet-title text-center"><u>Recently Registered Dogs</u></h3>

                </div> <!-- /.portlet-header -->


                <div class="portlet-body">

                    <div class="table-responsive">

                        <table class="table table-striped table-bordered thumbnail-table">
                            <thead>
                            <tr>
                                <th style="width: 150px">Image</th>
                                <th>Name</th>
                                <th>Breed</th>
                                <th>Kennel Name</th>
                                <th>Registered Date</th>
                                <th>Sex</th>
                                <th>Progeny</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($dogs as $dog)
                            <tr>
                                <td>
                                    <div class="thumbnail">
                                        <div class="thumbnail-view">
                                            <a class="thumbnail-view-hover ui-lightbox" href="{{url('/show-dog',$dog->id)}}">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                            <?php $image = $dog->image_name == null  ? "http://placehold.it/100x100" : "/images/catalog/$dog->image_name"  ?>
                                            <img src="{{$image}}" style="width: 120px; height: 70px;" alt="Gallery Image">
                                        </div>
                                    </div> <!-- /.thumbnail -->
                                </td>

                                <td class="valign-middle"><a href="javascript:;"></a>
                                    <p>{{$dog->name}}</p>
                                </td>

                                <td class="valign-middle">{{$dog->breeder_name}}</td>

                                <td>{{$dog->kennel_name}}</td>

                                <td class="file-info valign-middle">
                                  {{$dog->created_at}}
                                </td>

                                <td>
                                    {{$dog->sex}}
                                </td>

                                <?php
                                $offspring =  \App\DogRelationship::where('father',$dog->registration_number)->orWhere('mother',$dog->registration_number)->first();
                                ?>
                                <td class="text-center valign-middle">
                                    @if($offspring != null)
                                    <a href="{{url('/show-offspring',$dog->registration_number)}}" class="btn btn-success btn-sm" id="offspring-{{$dog->registration_number}}">Progeny</a><br>
                                        @else
                                    <em>No offspring yet !</em>
                                        @endif
                                </td>

                                <td class="text-center valign-middle">
                                   <a href="{{url('/show-dog',$dog->id)}}" class="btn btn-success btn-sm">View</a>
                                    {{--<a href="{{url('/pdf',[$dog->id])}}" class="btn btn-success btn-sm">Print PDF</a>--}}
                                </td>
                            </tr>
                                @endforeach

                            </tbody>
                        </table>
                        <div class="text-center">

                            <?php
                            if(isset($paginator)){ ?>
                            {{--{!! $paginator->render() !!}--}}
                                {!! $paginator->appends(['term' => Request::get('term')])->render() !!}

                          <?php   }else {?>
                                {!! $dogs->render() !!}
                            <?php } ?>

                        </div>
                    </div> <!-- /.table-responsive -->

                </div> <!-- /.portlet-body -->

            </div> <!-- /.portlet -->

            <br>



            <div class="portlet portlet-default">

                <div class="portlet-header">
                    <h3 class="portlet-title"><u>Most Viewed Dogs</u></h3>
                </div> <!-- /.portlet-header -->

                <div class="portlet-body">

                    <div class="row">

                        @foreach($viewed_dogs as $viewed)
                        <div class="col-md-3 col-sm-6">
                            <div class="thumbnail">
                                <div class="thumbnail-view">
                                    <a href="{{url('/show-dog',$viewed->id)}}" class="thumbnail-view-hover thumbnail-view-hover-lg ui-lightbox">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <?php $image = $viewed->image_name == null  ? "http://placehold.it/100x100" : "/images/catalog/$viewed->image_name"  ?>

                                    <img src="{{$image}}" style="width: 250px; height: 150px;" alt="Gallery Image">
                                </div>

                                <div class="caption">
                                    <h3><strong>{{$viewed->name}}</strong> <span>({{\App\Dog::getGender($viewed->sex)}})</span></h3>
                                    {{--<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>--}}
                                    <p>
                                        <a href="{{url('/show-dog',$viewed->id)}}" class="btn btn-primary btn-sm btn-sm text-center">View</a>
                                        &nbsp;
                                        {{--<a href="javascript:;" class="btn btn-secondary btn-sm btn-sm">Button</a>--}}
                                    </p>
                                </div>

                                <div class="thumbnail-footer">
                                    <div class="pull-left">
                                        <a href="javascript:;"><i class="fa fa-eye"></i> {{$viewed->viewed}}</a>
                                        {{--<a href="javascript:;"><i class="fa fa-thumbs-down"></i> 29</a>--}}
                                    </div>

                                    <div class="pull-right">
                                        <a href="javascript:;"><i class="fa fa-clock-o"></i>
                                           {{getDogRegisteredDay($viewed->created_at)}}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- /.col -->
                            @endforeach
                    </div> <!-- /.row -->

                </div> <!-- /.portlet-body -->

            </div> <!-- /.portlet -->

            <br>


        </div> <!-- /.container -->


    </div> <!-- .content -->

    <div class="modal fade" id="search-modal" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel" style="z-index: 2000000;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong>ADVANCED SEARCH</strong></h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>head</th>
                            <th>head</th>
                            <th>head</th>
                            <th>head</th>
                        </tr>
                        <tr>
                            <td>one</td>
                            <td>one</td>
                            <td>one</td>
                            <td>one</td>
                        </tr>

                        <tr>
                            <td>one</td>
                            <td>one</td>
                            <td>one</td>
                            <td>one</td>
                        </tr>
                    </table>
                </div>

            </div>
        </div>
    </div>

@stop