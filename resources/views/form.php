<?php

$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$email      = $_POST['email'];
$phone      = $_POST['phone'];
$message    = $_POST['message'];

if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(empty($first_name)){
        $first_name_error = "The first name is empty";
    }

    if(empty($last_name)){
        $last_name_error = "The last name is empty";
    }

    if(empty($email)){
        $email_error = "Email is empty";

    }elseif(!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$email)){
        $email_error = "Invalid email format";
    }

    if(empty($phone)){
        $phone_error = "phone number is empty";
    }

    echo $first_name;
}