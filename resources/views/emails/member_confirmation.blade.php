<!--
Responsive Email Template by @keenthemes
        A component of Metronic Theme - #1 Selling Bootstrap 3 Admin Theme in Themeforest: http://j.mp/metronictheme
        Licensed under MIT
        -->
<html>
<head>
    <title></title>
    <style>
        /***
        User Profile Sidebar by @keenthemes
A component of Metronic Theme - #1 Selling Bootstrap 3 Admin Theme in Themeforest: http://j.mp/metronictheme
Licensed under MIT
***/

        body {
            padding: 0;
            margin: 0;
        }

        html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
        @media only screen and (max-device-width: 680px), only screen and (max-width: 680px) {
            *[class="table_width_100"] {
                width: 96% !important;
            }
            *[class="border-right_mob"] {
                border-right: 1px solid #dddddd;
            }
            *[class="mob_100"] {
                width: 100% !important;
            }
            *[class="mob_center"] {
                text-align: center !important;
            }
            *[class="mob_center_bl"] {
                float: none !important;
                display: block !important;
                margin: 0px auto;
            }
            .iage_footer a {
                text-decoration: none;
                color: #929ca8;
            }
            img.mob_display_none {
                width: 0px !important;
                height: 0px !important;
                display: none !important;
            }
            img.mob_width_50 {
                width: 40% !important;
                height: auto !important;
            }
        }
        .table_width_100 {
            width: 680px;
        }

        * {
            font-family: 'Open Sans', sans-serif;
        }
        .button-fill {
            text-align: center;
            background: #ccc;
            display: inline-block;
            position: relative;
            text-transform: uppercase;
            margin: 25px;
        }
        .button-fill.grey {
            background: #445561;
            color: white;
        }
        .button-fill.orange {
            background: #f26b43;
            color: #fff;
        }
        .button-fill.orange .button-inside {
            color: #f26b43;
        }
        .button-fill.orange .button-inside.full {
            border: 1px solid #f26b43;
        }
        .button-text {
            padding: 0 25px;
            line-height: 56px;
            letter-spacing: .1em;
        }
        .button-inside {
            width: 0px;
            height: 54px;
            margin: 0;
            float: left;
            position: absolute;
            top: 1px;
            left: 50%;
            line-height: 54px;
            color: #445561;
            background: #fff;
            text-align: center;
            overflow: hidden;
            -webkit-transition: width 0.5s, left 0.5s, margin 0.5s;
            -moz-transition: width 0.5s, left 0.5s, margin 0.5s;
            -o-transition: width 0.5s, left 0.5s, margin 0.5s;
            transition: width 0.5s, left 0.5s, margin 0.5s;
        }
        .button-inside.full {
            width: 100%;
            left: 0%;
            top: 0;
            margin-right: -50px;
            border: 1px solid #445561;
        }
        .inside-text {
            text-align: center;
            position: absolute;
            right: 50%;
            letter-spacing: .1em;
            text-transform: uppercase;
            -webkit-transform: translateX(50%);
            -moz-transform: translateX(50%);
            -ms-transform: translateX(50%);
            transform: translateX(50%);
        }

    </style>
</head>
<body>
<div id="mailsub" class="notification" align="center">

    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" bgcolor="#eff3f8">


                <!--[if gte mso 10]>
                <table width="680" border="0" cellspacing="0" cellpadding="0">
                    <tr><td>
                <![endif]-->

                <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
                    <tr><td>
                            <!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;">�</div>
                        </td></tr>
                    <!--header -->
                    <tr><td align="center" bgcolor="#ffffff">
                            <!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">�</div>
                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                <tr><td align="left"><!--

				Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;">
                                            <table class="mob_center" width="115" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
                                                <tr><td align="left" valign="middle">
                                                        <!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">�</div>
                                                        <table width="115" border="0" cellspacing="0" cellpadding="0" >
                                                            <tr><td align="left" valign="top" class="mob_center">
                                                                    <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
                                                                        <font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
                                                                            {{--<img src="{{url('/img/kug.PNG')}}" width="115" height="100" alt="Metronic" border="0" style="display: block;" /></font></a>--}}
                                                                    {{--<h2  width="115" height="19" alt="Metronic" border="0" style="display: block;">KENNEL UNION OF GHANA </h2>--}}
                                                                        </font>
                                                                    </a>
                                                                </td></tr>
                                                        </table>
                                                    </td></tr>
                                            </table></div><!-- Item END--><!--[if gte mso 10]>
                                        </td>
                                        <td align="right">
                                        <![endif]--><!--

				Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;">
                                            <table width="88" border="0" cellspacing="0" cellpadding="0" align="right" style="border-collapse: collapse;">
                                                <tr><td align="right" valign="middle">
                                                        <!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">�</div>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                                                            <tr><td align="right">
                                                                    <!--social -->
                                                                    <div class="mob_center_bl" style="width: 88px;">
                                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                                            <tr><td width="30" align="center" style="line-height: 19px;">
                                                                                    <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                                                                                        <font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
                                                                                            <img src="http://artloglab.com/metromail/images/facebook.gif" width="10" height="19" alt="Facebook" border="0" style="display: block;" /></font></a>
                                                                                </td><td width="39" align="center" style="line-height: 19px;">
                                                                                    <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                                                                                        <font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
                                                                                            <img src="http://artloglab.com/metromail/images/twitter.gif" width="19" height="16" alt="Twitter" border="0" style="display: block;" /></font></a>
                                                                                </td><td width="29" align="right" style="line-height: 19px;">
                                                                                    <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                                                                                        <font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
                                                                                            <img src="http://artloglab.com/metromail/images/dribbble.gif" width="19" height="19" alt="Dribbble" border="0" style="display: block;" /></font></a>
                                                                                </td></tr>
                                                                        </table>
                                                                    </div>
                                                                    <!--social END-->
                                                                </td></tr>
                                                        </table>
                                                    </td></tr>
                                            </table></div><!-- Item END--></td>
                                </tr>
                            </table>
                            <!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;">�</div>
                        </td></tr>
                    <!--header END-->

                    <!--content 1 -->
                    <tr><td align="center" bgcolor="#fbfcfd">
                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                <tr><td align="center">
                                        <!-- padding --><div style="height: 60px; line-height: 60px; font-size: 10px;">�</div>
                                        <div style="line-height: 44px;">
                                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
						KENNEL UNION OF GHANA
					</span></font>
                                        </div>
                                        <!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;">�</div>
                                    </td></tr>
                                <tr><td align="center">
                                        <div style="line-height: 24px;">
                                            <font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 15px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">
						<strong>
                           <h3> New Registration on Kennel Union of Ghana website.</h3> <br>
                          Name :  {{$user->first_name}} {{$user->last_name}} <br> <br>
                          Email : {{$user->email}} <br> <br>
                          Phone : {{$user->phone}} <br> <br>
                          Date  : {{$user->created_at}}

                       <a href="{{url('/admin/members?members=unconfirmed')}}"> <h2>Confirm Now</h2></a>
                        </strong>
					</span></font>
                                        </div>
                                        <!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;">�</div>
                                    </td></tr>
                                <tr><td align="center">
                                        <div style="line-height: 24px;">
                                            <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
                                                <font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
                                                    {{--<img src="http://artloglab.com/metromail/images/trial.gif" width="193" height="43" alt="30-DAYS FREE TRIAL" border="0" style="display: block;" />--}}
                                                </font></a>


                                            <a href="#">
                                                <img src="http://kug.techimagegh.com/img/kug2.png"/>
                                               </a>
                                            <a href="#">
                                                <img src="http://kug.techimagegh.com/img/delete.png"/>
                                                </a>



                                        </div>
                                        <!-- padding --><div style="height: 60px; line-height: 60px; font-size: 10px;">�</div>
                                    </td></tr>
                            </table>
                        </td></tr>
                    <!--content 1 END-->

                    <!--content 2 -->
                    <tr><td align="center" bgcolor="#ffffff" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #eff2f4;">
                            <table width="94%" border="0" cellspacing="0" cellpadding="0">
                                <tr><td align="center">
                                        <!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;">�</div>

                                    </td></tr>
                                <tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;">�</div></td></tr>
                            </table>
                        </td></tr>
                    <!--content 2 END-->


                    <!--content 3 -->
                    <tr><td align="center" bgcolor="#ffffff" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #eff2f4;">
                            <table width="94%" border="0" cellspacing="0" cellpadding="0">
                                <tr><td align="center">
                                        <!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;">�</div>

                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr><td align="center">
                                                    <font face="Arial, Helvetica, sans-serif" size="3" color="#57697e" style="font-size: 26px;">
						{{--<span style="font-family: Arial, Helvetica, sans-serif; font-size: 26px; color: #57697e;">--}}
							{{--Dogs awaiting confirmation--}}
						{{--</span>--}}
                                                    </font>
                                                </td></tr>
                                        </table>


                                      {{--@foreach($unconfirmed_dogs as $dog)--}}

                                            {{--<div class="mob_100" style="float: left; display: inline-block; width: 33%;">--}}
                                                {{--<table class="mob_100" width="100%" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">--}}
                                                    {{--<tr><td align="center" style="line-height: 14px; padding: 0 10px;">--}}
                                                            {{--<!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;">�</div>--}}
                                                            {{--<div style="line-height: 14px;">--}}
                                                                {{--<a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">--}}
                                                                    {{--<font face="Arial, Helvetica, sans-serif" size="2" color="#596167">--}}
                                                                        {{--<img src="/images/catalog/{{$dog->image_name}}" width="185" alt="Image 1" border="0" style="display: block; width: 100%; height: 120;" /></font></a>--}}
                                                            {{--</div>--}}
                                                        {{--</td></tr>--}}
                                                {{--</table>--}}
                                            {{--</div>--}}

                                          {{--@endforeach--}}
                                        {{--<a href="#">--}}
                                            {{--<div class="button-fill orange">--}}
                                                {{--<div class="button-text">VIEW ALL</div>--}}
                                                {{--<div class="button-inside">--}}
                                                    {{--<div class="inside-text">Learn&nbspMore</div>--}}
                                                {{--</div>--}}
                                            {{--</div></a>--}}
                                    </td></tr>
                                <tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;">�</div></td></tr>
                            </table>
                        </td></tr>
                    <!--content 3 END-->

                    <!--footer -->
                    <tr><td class="iage_footer" align="center" bgcolor="#ffffff">
                            <!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;">�</div>

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr><td align="center">
                                        <font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5" style="font-size: 13px;">
				<span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">
					<?php echo date('Y') ?>  � Kennel Union of Ghana.
				</span></font>
                                    </td></tr>
                            </table>

                            <!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">�</div>
                        </td></tr>
                    <!--footer END-->
                    <tr><td>
                            <!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;">�</div>
                        </td></tr>
                </table>
                <!--[if gte mso 10]>
                </td></tr>
                </table>
                <![endif]-->

            </td></tr>
    </table>

</div>

<br>
<br>
<center>
    <strong>Powered by <a href="http://sourcecodenetwork.com" target="_blank">SourceCode Network</a></strong>
</center>
<br>
<br>
</body>
</html>