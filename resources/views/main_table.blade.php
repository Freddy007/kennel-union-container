@extends('layouts.index3_layout')

@section("styles")

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script src="{{url('/bower_components/jquery/dist/jquery.js')}}"></script>

    <script src="{{url('/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <style>

        .tt-input-group {
            width: 100%;
        }

        .dog-name{
            font-weight: bolder;
        }

        .hide-column{
            display: none;
        }
        .water-mark{
            z-index: 20000;
            position: absolute;
            top: 450px;
            left: 320px;
            font-weight: bold;
            font-size: 60px;
            opacity: 0;
            -ms-transform: rotate(45deg); /* IE 9 */
            -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
            transform: rotate(45deg);
        }

        .first{
            border: 1px solid #000000;
            height: 50px;
            width: 90%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }

        .second{
            border: 1px solid #000000;
            height: 50px;
            width: 100%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }
        .third{
            border: 1px solid #000000;
            height: 50px;
            width: 100%;
            margin: auto;
            background-color: #ffffff;
            border-top: 0;
            border-bottom: 0;
        }

        .fourth{
            border: 1px solid #000000;
            height: 50px;
            width: 95%;
            margin: auto;
            background-color: rgb(172, 209, 165);
        }

        .table-container{
            border: 25px solid transparent;
            padding: 15px;
            /*border-image-source: url(/img/main-2.png);*/
            /*border-image-repeat: round;*/
            /*border-image-slice: 85;*/
        }

        .male {
            background-color:rgb(172, 209, 165);
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        .female {
            background-color: #FFFFFF;
            color: #000000;
            font-family: Sans Serif;
            font-size: 8pt;
            text-align: center;
            vertical-align: middle;
            border-collapse: collapse;
            border: 1px solid #000000;
        }

        table,.dog-info{
            margin:auto;
        }

        .dog-info{
            width: 94%;
        }

        hr{
            width: 94%;
            margin:auto;
        }

        .overlay {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: rgb(76, 160, 73);
            background-color: rgba(76, 160, 73,1);
            overflow-x: hidden;
            transition: 0.5s;
        }

        .overlay-content {
            position: relative;
            top: 25%;
            width: 100%;
            text-align: center;
            margin-top: 30px;
        }

        .overlay a {
            padding: 8px;
            text-decoration: none;
            font-size: 36px;
            color: #818181;
            display: block;
            transition: 0.3s;
        }

        .overlay a:hover, .overlay a:focus {
            color: #f1f1f1;
        }

        .overlay .closebtn {
            position: absolute;
            top: 20px;
            right: 45px;
            font-size: 60px;
            color: #ffffff;
        }

        @media screen and (max-height: 450px) {
            .overlay a {font-size: 20px}
            .overlay .closebtn {
                font-size: 40px;
                top: 15px;
                right: 35px;
            }
        }

        input[type="text"],input[type="date"],select  {
            height: 40px;
            border-radius: 7px;
        }

        .tt-menu,
        .gist {
            text-align: left;
        }

        html {
            font: normal normal normal 18px/1.2 "Helvetica Neue", Roboto, "Segoe UI", Calibri, sans-serif;
            color: #292f33;
        }

        a {
            color: #03739c;
            text-decoration: none;
        }

        a:hover {
            text-decoration: underline;
        }

        .table-of-contents li {
            display: inline-block;
            *display: inline;
            zoom: 1;
        }

        .table-of-contents li a {
            font-size: 16px;
            color: #999;
        }

        p + p {
            margin: 30px 0 0 0;
        }

        .title {
            margin: 20px 0 0 0;
            font-size: 64px;
        }

        .example {
            padding: 30px 0;
        }

        .example-name {
            margin: 20px 0;
            font-size: 32px;
        }

        .demo {
            position: relative;
            *z-index: 1;
            margin: 50px 0;
        }

        .typeahead,
        .tt-query,
        .tt-hint {
            width: 250px;
            height: 40px;
            padding: 8px 12px;
            font-size: 17px;
            line-height: 20px;
            border: 2px solid #ccc;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border-radius: 6px;
            outline: none;
        }


        .typeahead {
            background-color: #fff;
        }

        .typeahead:focus {
            border: 2px solid #0097cf;
        }

        .tt-query {
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
            color: #999
        }

        .tt-menu {
            width: 422px;
            margin: 12px 0;
            padding: 8px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
            -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
            box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            font-size: 18px;
            line-height: 24px;
        }

        .tt-suggestion:hover {
            cursor: pointer;
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion.tt-cursor {
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion p {
            margin: 0;
        }

        .gist {
            font-size: 14px;
        }

        .empty-message {
            padding: 5px 10px;
            text-align: center;
        }


    </style>

@endSection

@section('content')

    <!-- Page Heading -->
    <section class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>Pedigree</h1>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="{{url("/")}}">Home</a></li>
                        <li class="active">Pedigree </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- Page Heading / End -->

    <!-- Page Content -->
    <section class="page-content">

        <div class="container-fluid">

            <div class="portlet portlet-default">

               @include('version2.partials.dog_table_layout')

            </div>

        </div>


        @include('version2.partials.edit_dog_modal')
    </section>

    <!-- Footer -->
    <footer class="footer" id="footer">

        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        Copyright &copy; 2020 <a href="/">Kennel Union of Ghana</a> &nbsp;| &nbsp;All Rights Reserved
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <div class="social-links-wrapper">
                            <span class="social-links-txt">Keep in Touch</span>
                            <ul class="social-links social-links__light">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer / End -->

@endsection

@section("scripts")

    <script>
        @if($dog->needs_generation == false)

        @endif
    </script>

{{--    <script src="{{url('/js/tables.js')}}"></script>--}}

    {{--<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>--}}
{{--    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>--}}

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.min.js"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/0.11.1/typeahead.bundle.min.js" type="text/javascript"></script>s

    <script>
        var substringMatcher = function(strings) {
            return function findMatches(q, cb) {
                var matches, substringRegex;

                matches = [];
                substrRegex = new RegExp(q, 'i');

                $.each(strings, function(i, string) {
                    if (substrRegex.test(string)) {
                        matches.push(string);
                    }
                });

                cb(matches);
            };
        };

        // Suggestion engine
        const courses = new Bloodhound({
            datumTokenizer: datum => Bloodhound.tokenizers.whitespace(datum.value),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            // datumTokenizer: Bloodhound.tokenizers.whitespace,
            // queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: 'type-ahead-dogs?q=%QUERY',
            remote: {
                url: 'type-ahead-dogs?q=%QUERY',
                wildcard: '%QUERY',
                filter: courses => $.map(courses, dog => ({
                    value: dog.name,
                    id: dog.id,
                    registration_number: dog.registration_number,
                    sex: dog.sex
                }))
            }
        });

         courses.initialize();
        // Instantiate the Typeahead UI
        $('.parent').typeahead(null, {
            displayKey: 'value',
            display: 'registration_number',
            source: courses.ttAdapter(),
            templates: {
                empty: [
                    '<div class="empty-message">',
                    'unable to find any dog that matches the current query',
                    '</div>'
                ].join('\n'),
                suggestion: Handlebars.compile('<div><strong>@{{value}}</strong> – @{{registration_number}} – @{{sex}}</div>')
            }
        });


        $( document ).ready(function() {

            let generation = '';
            let position = '';
            let sire_parent = '';
            let dam_parent = '';
            let offspring = '';
            let gender = '';
            let dog_parent = $('.dog_parent');
            let dog_id = $("#dog_id").data("id");

            $("#regenerate-table").off("click").on("click", function () {
                regenerateTable(dog_id);
            })

            $(document).off('click').on('click', '.edit-column', function (e) {

                let positionString = ($(this).data('position')).split(",");

                generation = $(this).data('generation');
                position = positionString.length > 1 ? positionString[1] : positionString[0];
                sire_parent = $(this).data('sire-parent');
                dam_parent = $(this).data('dam-parent');
                offspring = $(this).data('offspring');

                self = $(this);

                $('#edit-column-modal').modal();

            });

            $('.parent').on('typeahead:selected', function (evt, item) {

                console.log(item);

                let name = $(this).val();
                position_array = position.split(",");
                let gender = typeof position_array[1] !== 'undefined' ? position_array[1] : position_array[0];

                $.ajax({
                    url: 'dog-by-name/' + name + '?gender=' + gender,
                    type: "GET",
                    statusCode: statusCodeResponses,
                }).done(function (data) {

                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You will be adding " + data.name + " to this table!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, insert!'

                    }).then((result) => {
                        if (result.isConfirmed) {
                            appendPedigree(data);
                        }
                    })
                });
            });

            function appendPedigree(data) {
                let formData = appendPedigreeDetails(data, generation, position, sire_parent, dam_parent, offspring);
                $.ajax({
                    url: "{{url('add-to-pedigree-table')}}",
                    type: "POST",                                 // Type of request to be send, called as method
                    data: formData,                              // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,                         // The content type used when sending data to the server.
                    cache: false,                              // To unable request pages to be cached
                    processData: false,
                    statusCode: {
                        404: function () {
                            alert("page not found");
                        },
                        500: function () {
                            Swal.fire(
                                'Failed!',
                                'Failed to save!',
                                'warning'
                            )
                        },
                        200: function () {
                            // swal("Added!", "This dog will be added to your table.", "success");
                        }
                    }

                }).done(function (data) {

                }).then(function () {
                    regenerateTable(dog_id);
                })
            }

            function appendPedigreeDetails(data, generation, position, sire_parent, dam_parent, offspring) {
                let formData = new FormData();
                formData.append('generation', generation);
                formData.append('position', position);
                formData.append('name', data.name);
                formData.append('registration_number', data.registration_number);
                formData.append('titles', data.titles);
                formData.append('id', data.id);
                formData.append('dog_id', dog_id);
                formData.append('father', sire_parent);
                formData.append('mother', dam_parent);
                formData.append('offspring', offspring);
                return formData;
            }

            function addToPedigreeTableFailed() {
                $('#save-btn').removeAttr('disabled');
                $('.help-block.name').text(' ');
                // location.reload();
            }

            let statusCodeResponses = {
                403: function () {
                    alert("You can't put this dog here!");
                    $('#save-btn').attr('disabled', true);
                    $('#name').val('')
                }
            };

            function addToPedigreeTableSuccess() {
                $('#edit-column-modal').modal('hide');

                $.get("", function () {

                }).done(function (data) {
                    $('#pedigree-table').html(data);
                });

            }

            $('#register-dog-form').off('submit').on('submit', function (e) {

                e.preventDefault();
                var self = $(this);
                var _self = this;

                $('#save-btn').attr('disabled', true);

                let formData = new FormData(this);
                formData.append('generation', generation);
                formData.append('position', position);
                formData.append('dog_id', dog_id);
                // formData.append('father', sire_parent);
                // formData.append('mother', dam_parent);
                formData.append('offspring', offspring);
                if (typeof self.data('sire-parent') !== undefined) {
                    // formData.append('father', self.data('sire-parent'));
                    // formData.append('mother', self.data('dam-parent'));
                }

                formData.append('offspring', offspring);

                let initiate = $.ajax({
                    url: $(this).attr('action'),                   // Url to which the request is send
                    type: "POST",                                 // Type of request to be send, called as method
                    data: formData,                              // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,                         // The content type used when sending data to the server.
                    cache: false,                              // To unable request pages to be cached
                    processData: false,                        // To send DOMDocument or non processed data file it is set to false
                }).then(function (data) {
                    regenerateTable(dog_id)
                });

                initiate.done(function (data) {
                    $('#edit-column-modal').modal('hide');
                    $('#save-btn').removeAttr('disabled');

                });

                initiate.fail(function () {
                    $('#save-btn').removeAttr('disabled');
                })
            });

            function regenerateTable(id) {
                // alert(dog_id);
                let generateFormData = new FormData();
                generateFormData.append("id", id);

                $.ajax({
                    url: "{{url('regenerate-table')}}",
                    type: "POST",                                 // Type of request to be send, called as method
                    data: generateFormData,                                  // Type of request to be send, called as method
                    contentType: false,                         // The content type used when sending data to the server.
                    cache: false,                              // To unable request pages to be cached
                    processData: false
                }).done(function (data) {
                    let timerInterval;
                    Swal.fire({
                        title: 'Regenerating table!',
                        html: 'table will be regenerated in <b></b> milliseconds.',
                        timer: 7000,
                        timerProgressBar: true,
                        willOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                                const content = Swal.getContent()
                                if (content) {
                                    const b = content.querySelector('b')
                                    if (b) {
                                        b.textContent = Swal.getTimerLeft()
                                    }
                                }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {
                            location.reload();
                            console.log('I was closed by the timer')
                        }
                    });
                })
            }
        });

    </script>


    <script>
        @if(!Auth::check())
        $('.edit-column').hide()
        @endif
    </script>

@endSection
