@extends("layouts.index3_layout")

@section("title")
    Results
@endsection

@section("content")

    <!-- Page Heading -->
    <section class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>Search Results</h1>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li class="active">Search Results</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- Page Heading / End -->

    <section class="page-content">
        <div class="container">

            <div class="clearfix">
                <!-- Project Feed Filter -->
                <ul class="project-feed-filter">
                    @if($breed_query == "all")
                    <li><a href="#" class="btn btn-sm btn-default btn-primary" data-filter="*">All</a></li>
                    @foreach($breeds as $breed)
                    <li><a href="#" class="btn btn-sm btn-default" data-filter=".{{ str_replace(' ', '_', $breed->name) }}">{{$breed->name}}</a></li>
                    @endforeach
                    @else
                    <li><a href="#" class="btn btn-sm btn-default" data-filter=".{{ str_replace(' ', '_', $breeds->name) }}">{{$breeds->name}}</a></li>
                    @endif

                </ul>
                <!-- Project Feed Filter / End -->
            </div>

            <!-- Project Feed -->
            <div class="project-feed project-feed__4cols row">

                @foreach($dogs as $dog)

                <div class="col-sm-6 col-md-3 project-item {{ str_replace(' ', '_', $dog->breeder_name) }}">
                    <div class="project-item-inner">
                        <figure class="alignnone project-img">
{{--                            <img src="images/samples/portfolio-2.jpg" alt="">--}}
                            <img src="{{$dog->image_name == null ? url('https://placehold.it/300x200/cccccc/000000?text=no+image+uploaded'):  url('https://placehold.it/300x200/cccccc/000000?text=no+image+uploaded')}}"  />
                            <div class="overlay">
                                <a href="{{url('/show-dog',$dog->id)}}" class="dlink"><i class="fa fa-link"></i></a>
                                <a href="{{url('/show-dog',$dog->id)}}" class="popup-link zoom"><i class="fa fa-search"></i></a>
                            </div>
                        </figure>
                        <div class="project-desc">
                            <h4 class="title"><a href="{{url('/show-dog',$dog->id)}}">{{$dog->name}}</a></h4>
                            <span class="desc">{{$dog->breeder_name}} / {{$dog->sex}}</span>
                        </div>
                    </div>
                </div>

                @endforeach

            </div>
            <!-- Project Feed / End -->

            <div class="text-center">
                <ul class="pagination-custom list-unstyled list-inline">

                    <?php
                    if(isset($paginator)){ ?>
                    {{--{!! $paginator->render() !!}--}}
                    {!! $paginator->appends(
                    ['name' => Request::get('name'),'breed'=>Request::get('breed'),
                     'sex'=>Request::get('sex'),'colour'=>Request::get('colour')
                                            ])->render()
                    !!}

                    <?php   }else {?>
                    {!! $dogs->render() !!}
                    <?php } ?>

{{--                    <li><a href="#" class="btn btn-sm btn-default">&laquo;</a></li>--}}
{{--                    <li><a href="#" class="btn btn-sm btn-success">1</a></li>--}}
{{--                    <li><a href="#" class="btn btn-sm btn-default">2</a></li>--}}
{{--                    <li><a href="#" class="btn btn-sm btn-default">3</a></li>--}}
{{--                    <li><a href="#" class="btn btn-sm btn-default">4</a></li>--}}
{{--                    <li><a href="#" class="btn btn-sm btn-default">5</a></li>--}}
{{--                    <li><a href="#" class="btn btn-sm btn-default">&raquo;</a></li>--}}
                </ul>
            </div>

        </div>
    </section>
    <!-- Page Content / End -->

    <!-- Footer -->
    <footer class="footer" id="footer">

        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        Copyright &copy; 2020 <a href="/">Kennel Union of Ghana</a> &nbsp;| &nbsp;All Rights Reserved
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <div class="social-links-wrapper">
                            <span class="social-links-txt">Keep in Touch</span>
                            <ul class="social-links social-links__light">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer / End -->

@endsection
