@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Required datatable js -->
    <script src="{{asset('datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('datatables/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('datatables/jszip.min.js')}}"></script>
    <script src="{{asset('datatables/pdfmake.min.js')}}"></script>
    <script src="{{asset('datatables/vfs_fonts.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.colVis.min.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{asset('datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('datatables/responsive.bootstrap4.min.js')}}"></script>

    <script>
        jQuery(document).ready(function(){

//            var table = $('#datatable-buttons').DataTable({
//                buttons: ['copy', 'excel', 'pdf', 'colvis'],
//                "ajax" : "/admin/all-dogs-data",
//                'ordering': false,
//                "columns": [
//                    { "data": "name" },
//                    { "data": "dob" },
//                    { "data": "breed" },
//                    { "data": "sex" },
//                    { "data": "registration_number"},
//                    {"data" : "breeder"},
//                    {"data" : "status"},
//                    {"data" : "certificate"},
//                    {"data" : "action"},
//                ],
//                "initComplete": function( settings, json ) {
//                    addButtons();
//                },
//                lengthChange: false,
//            });
//
//            function addButtons(){
//                table.buttons().container()
//                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
//            }
//
//            $('#datatable-buttons').on( 'page.dt', function () {
//                var info = table.page.info();
//                $('#pageInfo').html( 'Showing page: '+info.page+' of '+info.pages );
////               alert( 'Showing page: '+info.page+' of '+info.pages );
//            } );

            $('#datatable-buttons').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/admin/array-data',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'action', name: 'action'}
                ]
            });

        });

    </script>

@stop

@section('content')

    <div class="content">

        <div class="container">
            {!! Breadcrumbs::render('all-dogs') !!}

            @include('flash::message')
            {{--<h2 class="">{{\App\Dog::count()}} Currently Submitted Dogs</h2>--}}

            {{--<h2 class="pull-right">Hello </h2>--}}

            <br>

            <div class="portlet portlet-default">

                <div class="portlet-header">
                    {{--<h4 class="portlet-title">--}}
                    {{--<u>Pagination</u>--}}
                    {{--</h4>--}}
                </div> <!-- /.portlet-header -->

                <div class="row">

                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        {{--<tr>--}}
                            {{--<th>Name</th>--}}
                            {{--<th>Date of Birth</th>--}}
                            {{--<th>Breed </th>--}}
                            {{--<th>Sex </th>--}}
                            {{--<th>Registration number </th>--}}
                            {{--<th>Breeder</th>--}}
                            {{--<th>Status</th>--}}
                            {{--<th>Certificate</th>--}}
                            {{--<th>Action</th>--}}
                        {{--</tr>--}}

                        <tr>
                            <th>id</th>
                            <th>Name</th>
                            <th>Email </th>
                            <th>Created at </th>
                            <th>Updated at</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                    </table>

                    <div id="pageInfo"></div>

                </div>

            </div> <!-- /.portlet -->

        </div> <!-- /.container -->

    </div> <!-- .content -->

    <div class="modal fade" id="report-modal" tabindex="-1" role="dialog" aria-labelledby="report-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong>REPORT DUPLICATED REGISTRATION NUMBER</strong></h4>
                </div>
                <div class="modal-body">
                    <form id="report-form">
                        <p>We experienced some challenges with dogs registration numbers.
                            Report any of such incident here with the registration number.</p>
                        <label>Registration Number</label>
                        <input type="number" class="form-control" required id="report-input" name="report_registration_number" placeholder="Suspected dog registration number">

                        <div class="report-message h3"></div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-default" id="report">Report</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>

@stop

