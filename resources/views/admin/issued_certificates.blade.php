@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <script>
        jQuery(document).ready(function(){
            @foreach($certificates as $certificate)
           $('#btnConfirm-<?php echo $certificate->id?>').off('click').on('click',function(){
                        $('#confirm-modal-<?php echo $certificate->id ?>').modal();
                    })

            @endforeach

//               $('#add-new-dog').off('click').on('click',function(){
//                        $('#register-dog-modal').modal();
//                    });

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        });

    </script>

@stop

@section('content')

    <div class="content">

        <div class="container">
            {!! Breadcrumbs::render('certificate-requests') !!}

            @include('flash::message')
            <h2 class="">{{\App\IssuedCertificate::count()}} Issued Certificates</h2>

            {{--<h2 class="pull-right">Hello </h2>--}}

            <br>

            {{--<div class="row" style="margin-bottom: 45px;">--}}
            {{--<form  action="{{url('/admin/all-dogs')}}" method="get">--}}
            {{--{{csrf_field()}}--}}
            {{--<div class="col-md-7">--}}
            {{--<input type="text" name="term" class="form-control input-lg  " placeholder="Dog by  Name , Color , Tattoo Number , microchip Number " value="{{Request::get('term')}}">--}}

            {{--</div>--}}

            {{--<div class="col-md-2">--}}
            {{--<input type="submit" class="form-control input-lg btn btn-success" value="Search">--}}
            {{--</div>--}}

            {{--</form>--}}

            {{--<div class="col-md-2" >--}}
            {{--<input type="submit" class="form-control input-lg btn btn-success btn-sm"  value="ADD NEW DOG">--}}
            {{--</div>--}}
            {{--</div>--}}

            <div class="portlet portlet-default">

                <div class="portlet-header">
                    {{--<h4 class="portlet-title">--}}
                    {{--<u>Pagination</u>--}}
                    {{--</h4>--}}
                </div> <!-- /.portlet-header -->

                @if($certificates->count())
                    <div class="portlet-body">
                        <div class="table-top-menu">
                            <ul>
                                <li class="table-top-pagination"> {!! $certificates->render() !!}</li>
                                {{--<li class="trash">--}}
                                {{--<a href="{{url('/admin/trash')}}">Trash</a>--}}
                                {{--</li>--}}

                            </ul>
                        </div>

                        <table
                                class="table table-striped table-bordered table-hover ui-datatable"
                                data-ajax="../../global/js/demos/json/table_data.json"
                                data-global-search="false"
                                data-length-change="false"
                                data-info="true"
                                data-paging="true"
                                data-page-length="10"
                                >
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Serial Number</th>
                                <th>Dog</th>
                                <th>Registration Number</th>
                                <th>Staff</th>
                                <th>Date</th>
                            </tr>

                            </thead>
                            <tbody>
                            <?php $i = $certificates->firstItem(); ?>
                            @foreach($certificates as $certificate)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$certificate->serial_number}}</td>
                                    <td>{{$certificate->dog}}</td>
                                    <td>{{$certificate->registration_number}}</td>
                                    <td>{{$certificate->member}}</td>
                                    <td>{{$certificate->created_at}}</td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $certificates->render() !!}
                        </div>
                    </div> <!-- /.portlet-body -->
                @else
                    <div style="height: 400px;">
                        <div class="text-center" style="height: 100px; border: 5px inset #cccccc; border-radius: 5px; padding-top : 10px;
                                    background-color: #cccccc; color: white;">
                            <div class="h2">There are no current dogs submitted. </div>
                            <div class="h4"><a href="javascript:history.back()"> Back </a></div>
                        </div>
                    </div>

                @endif

            </div> <!-- /.portlet -->

        </div> <!-- /.container -->

    </div> <!-- .content -->

@stop

