@extends('layouts.admin_master')

@section('scripts')

    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Plugin JS -->
    <script src="../../bower_components/flot/excanvas.min.js"></script>
    <script src="{{asset('../../bower_components/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('../../bower_components/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('../../bower_components/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('../../bower_components/flot/jquery.flot.time.js')}}"></script>
    <!-- App JS -->
    <script src="{{asset('../../global/js/mvpready-core.js')}}"></script>
    <script src="{{asset('../../global/js/mvpready-helpers.js')}}"></script>
    <script src="{{asset('js/mvpready-admin.js')}}"></script>

    <script src="../../bower_components/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Plugin JS -->
    <script src="../../bower_components/select2/dist/js/select2.min.js"></script>

    <script src="../../bower_components/bootstrap-jasny/js/fileinput.js"></script>

    <!-- Demo JS -->
    <script src="{{asset('../../global/js/demos/flot/pie.js')}}"></script>


    <script>
        $(document).ready(function(){
//            setTimeout(function(){
//               displayModal(); }, 3000);

            $('#reports').modal();

        });

        $(function () {

            var d1, d2,d3,d4, data, chartOptions

            d1 = [
                [1262304000000,   {{\App\Dog::dogsByMonth('1')}}], [1264982400000,   {{\App\Dog::dogsByMonth('2')}}], [1267401600000,   {{\App\Dog::dogsByMonth('3')}}], [1270080000000,   {{\App\Dog::dogsByMonth('4')}}],
                [1272672000000,   {{\App\Dog::dogsByMonth('5')}}], [1275350400000,{{\App\Dog::dogsByMonth('6')}}], [1277942400000,{{\App\Dog::dogsByMonth('7')}}], [1280620800000,{{\App\Dog::dogsByMonth('8')}}],
                [1283299200000,   {{\App\Dog::dogsByMonth('9')}}], [1285891200000, {{\App\Dog::dogsByMonth('10')}}], [1288569600000, {{\App\Dog::dogsByMonth('11')}}], [1291161600000,   {{\App\Dog::dogsByMonth('12')}}]
            ]

            d2 = [
                [1262304000000, {{\App\User::membersByMonth('1')}}], [1264982400000, {{\App\User::membersByMonth('2')}}], [1267401600000, {{\App\User::membersByMonth('3')}}], [1270080000000, {{\App\User::membersByMonth('4')}}],
                [1272672000000, {{\App\User::membersByMonth('5')}}], [1275350400000, {{\App\User::membersByMonth('6')}}], [1277942400000, {{\App\User::membersByMonth('7')}}], [1280620800000, {{\App\User::membersByMonth('8')}}],
                [1283299200000, {{\App\User::membersByMonth('9')}}], [1285891200000, {{\App\User::membersByMonth('10')}}], [1288569600000, {{\App\User::membersByMonth('11')}}], [1291161600000, {{\App\User::membersByMonth('12')}}]
            ]

            d3 = [
                [1262304000000, {{\App\Dog::dogTransfersByMonth('1')}}], [1264982400000, {{\App\Dog::dogTransfersByMonth('2')}}], [1267401600000, {{\App\Dog::dogTransfersByMonth('3')}}], [1270080000000, {{\App\Dog::dogTransfersByMonth('4')}}],
                [1272672000000, {{\App\Dog::dogTransfersByMonth('5')}}], [1275350400000, {{\App\Dog::dogTransfersByMonth('6')}}], [1277942400000, {{\App\Dog::dogTransfersByMonth('7')}}], [1280620800000, {{\App\Dog::dogTransfersByMonth('8')}}],
                [1283299200000, {{\App\Dog::dogTransfersByMonth('9')}}], [1285891200000, {{\App\Dog::dogTransfersByMonth('10')}}], [1288569600000, {{\App\Dog::dogTransfersByMonth('11')}}], [1291161600000, {{\App\Dog::dogTransfersByMonth('12')}}]
            ]

            d4 = [
                [1262304000000, {{\App\RequestedCertificate::RequestsByMonth('1')}}], [1264982400000, {{\App\RequestedCertificate::RequestsByMonth('2')}}], [1267401600000, {{\App\RequestedCertificate::RequestsByMonth('3')}}], [1270080000000, {{\App\RequestedCertificate::RequestsByMonth('4')}}],
                [1272672000000, {{\App\RequestedCertificate::RequestsByMonth('5')}}], [1275350400000, {{\App\RequestedCertificate::RequestsByMonth('6')}}], [1277942400000, {{\App\RequestedCertificate::RequestsByMonth('7')}}], [1280620800000, {{\App\RequestedCertificate::RequestsByMonth('8')}}],
                [1283299200000, {{\App\RequestedCertificate::RequestsByMonth('9')}}], [1285891200000, {{\App\RequestedCertificate::RequestsByMonth('10')}}], [1288569600000, {{\App\RequestedCertificate::RequestsByMonth('11')}}], [1291161600000, {{\App\RequestedCertificate::RequestsByMonth('12')}}]
            ]

            data = [{
                label: "Dogs Registered",
                data: d1
            }, {
                label: "Members Registered",
                data: d2
            },{
                label: "Dogs Transferred",
                data: d3
            },
                {
                    label: "Certificates Requested",
                    data: d4
                }
            ]

            chartOptions = {
                xaxis: {
                    min: (new Date(2009, 12, 1)).getTime(),
                    max: (new Date(2010, 11, 2)).getTime(),
                    mode: "time",
                    tickSize: [1, "month"],
                    monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                    tickLength: 0
                },
                yaxis: {

                },
                series: {
                    lines: {
                        show: true,
                        fill: false,
                        lineWidth: 3
                    },
                    points: {
                        show: true,
                        radius: 3,
                        fill: true,
                        fillColor: "#ffffff",
                        lineWidth: 2
                    }
                },
                grid: {
                    hoverable: true,
                    clickable: false,
                    borderWidth: 0
                },
                legend: {
                    show: true
                },
                tooltip: true,
                tooltipOpts: {
                    content: '%s: %y'
                },
                colors: mvpready_core.layoutColors
            }

            var holder = $('#line-chart')

            if (holder.length) {
                $.plot(holder, data, chartOptions )
            }

        })
    </script>


    @stop
@section('content')
    <div class="content">

        <div class="container">
            {!! Breadcrumbs::render('dashboard') !!}
            <div class="row">

                <div class="col-md-4 col-sm-5">

                    <div class="portlet portlet-default">

                        <div class="portlet-header">
                            <h4 class="portlet-title">
                                Statistics
                            </h4>
                        </div> <!-- /.portlet-header -->

                        <div class="portlet-body">

                            {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, fugiat, dolores, laborum sit.</p>--}}

                            <hr>

                            <table class="table keyvalue-table">
                                <tbody>
                                <tr>
                                    <td class="kv-key"><i class="fa fa-a kv-icon kv-icon-primary"></i> Total Dogs Registered</td>
                                    <td class="kv-value">{{\App\Dog::all()->count()}} </td>
                                </tr>
                                <tr>
                                    <td class="kv-key"><i class="fa fa-user kv-icon kv-icon-secondary"></i> Total Members Registered</td>
                                    <td class="kv-value">{{\App\User::all()->count()}} </td>
                                </tr>

                                <tr>
                                    <td class="kv-key"><i class="fa fa-list kv-icon kv-icon-tertiary"></i> Total Dogs Transferred</td>
                                    <td class="kv-value">{{\App\DogTransfer::all()->count()}} </td>
                                </tr>
                                <tr>
                                    <td class="kv-key"><i class="fa fa-certificate kv-icon kv-icon-default"></i>Total Certificates Requested</td>
                                    <td class="kv-value">{{\App\RequestedCertificate::count()}}</td>
                                </tr>
                                {{--<tr>--}}
                                    {{--<td class="kv-key"><i class="fa fa-envelope-o kv-icon kv-icon-default"></i> Inquiries</td>--}}
                                    {{--<td class="kv-value">39 </td>--}}
                                {{--</tr>--}}
                                </tbody>
                            </table>

                        </div> <!-- /.portlet-body -->

                    </div> <!-- /.portlet -->

                </div> <!-- /.col -->

                <div class="col-md-8 col-sm-7">
                    <div class="portlet portlet-default">

                        <div class="portlet-header">
                            <h4 class="portlet-title">
                                Monthly Statistics
                            </h4>
                        </div> <!-- /.portlet-header -->

                        <div class="portlet-body">

                            <div id="line-chart" class="chart-holder-300"></div>
                        </div> <!-- /.portlet-body -->

                    </div> <!-- /.portlet -->

                </div> <!-- /.col -->

            </div> <!-- /.row -->

            <br>

                <div class="col-md-4">
                    <div class="portlet portlet-default">

                        <div class="portlet-header">

                        </div> <!-- /.portlet-header -->

                        <div class="portlet-body">

                            <div id="auto-chart" class="chart-holder-250"></div>
                        </div> <!-- /.portlet-body -->

                    </div> <!-- /.portlet -->

                </div> <!-- /.col -->

            </div> <!-- /.row -->

        </div> <!-- /.container -->

    </div> <!-- .content -->

    <div class="modal fade" id="reports" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong>ALL REPORTS</strong></h4>
                </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">

                                <div class="portlet portlet-boxed portlet-success">

                                    <div class="portlet-header">
                                        <h4 class="portlet-title">
                                            <u><i class="fa fa-check portlet-icon"></i>Reports </u>
                                        </h4>
                                    </div> <!-- /.portlet-header -->

                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">

                                                <p>Hello,  {{Auth::getUser()->first_name}} <em>You have <a href="{{url('/admin/all-dogs?dogs=unconfirmed')}}">{{\App\Dog::where('confirmed',false)->count()}}</a> dogs and
                                                        <a href="{{url('/admin/members?members=unconfirmed')}}">{{\App\User::where('confirmed',false)->count()}}</a> members to attend to .</em></p>

                                            </div>
                                        </div>

                                    </div> <!-- /.portlet-body -->

                                </div> <!-- /.portlet -->

                            </div> <!-- /.col -->

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            {{--<input type="submit" id="add-breeder" class="btn btn-success" value="ADD NEW">--}}
                        </div>
                </div>

            </div>
        </div>
    </div>


@stop
