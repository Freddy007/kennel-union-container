@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <script>
        jQuery(document).ready(function() {
            $(".confirm").on("click", function () {
                var confirmed = $(this).data("confirmed");
                var name = $(this).data("title");
                var id = $(this).data("id");
                let page = table.page.info().page;

                console.log(page);

                Swal.fire({
                    title: 'Are you sure?',
                    text: "This dog will be confirmed!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, confirm this dog!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        confirmDog(id);
                        table.page(page).draw('page');
                        // table.draw();
                    }
                })
            });

            $(".delete-request").off().on("click", function () {
                let dog_id = $(this).data("id");
                let page = table.page.info().page;

                Swal.fire({
                    title: 'Are you sure?',
                    text: "This dog will be moved to archives!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        archiveDog(dog_id);
                        table.page(page).draw('page');
                    }

                })
            })

            $(".approve-certificate").off().on("click", function () {
                let dog_id = $(this).data("id");
                let page = table.page.info().page;

                Swal.fire({
                    title: 'Are you sure?',
                    text: "This will approve certificate request!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Approve it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        approveCertificate(dog_id);
                        table.page(page).draw('page');
                    }

                });
            });

            $(".download-certificate").off().on("click", function (event) {
                event.preventDefault();
                const download_base_url = "http://18.194.254.77/certification/public";

                let dog_id = $(this).data("id");
                enterNewOwner(dog_id);

                $(document).on('click', '.download', function () {
                    let name = $("#swal2-input").val();
                    Swal.fire('Downloading...', '', 'info');
                    if (name) {
                        window.location.href = `${download_base_url}/${dog_id}?name=${name}`;
                    } else {
                        window.location.href = `${download_base_url}/${dog_id}`;
                    }
                    console.log('Button 1');
                });
                $(document).on('click', '.sendToMember', function () {
                    console.log('Button 2');
                    let name = $("#swal2-input").val();
                    approveCertificate(dog_id, name);
                    Swal.fire('Certificate will be sent to the member', '', 'info');
                });
            })

            function confirmDog(id) {
                $.post("/version2/confirm-dog/" + id, function (data) {
                    Swal.fire(
                        'Confirmed!',
                        'Dog is now confirmed!',
                        'success'
                    )

                });
            }

            function archiveDog(id) {
                $.post("/version2/archive-dog/" + id, function (data) {
                    Swal.fire('Archived!', 'Dog is now archived.', 'success')
                });
            }

            function approveCertificate(id, owner_name) {
                let url = name === "" ? "/version2/approve-certificate-request/" + id : "/version2/approve-certificate-request/" + id + "?name=" + owner_name;
                $.post(url, function (data) {
                    Swal.fire(
                        'Approved!',
                        'Download url has been generated and send to member.',
                        'success'
                    )
                });
            }

            async function enterNewOwner(dog_id) {
                const download_base_url = "http://18.194.254.77/certification/public";
                const {value: name} = await Swal.fire({
                    title: "Enter new owner's name",
                    html:
                        '<input maxlength="100" autocapitalize="off" autocorrect="off" class="swal2-input" id="swal2-input" placeholder="Enter name(not required)" type="text" style="display: flex;">' +
                        '<button type="button" role="button" tabindex="0" class="SwalBtn1 download customSwalBtn">' + 'Download' + '</button>' +
                        '<button type="button" role="button" tabindex="0" class="SwalBtn2 sendToMember customSwalBtn">' + 'Send to member' + '</button>',
                    showCancelButton: false,
                    showConfirmButton: false
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    let owner_name = result.value;
                    console.log(result);

                    if (result.isDenied) {
                        approveCertificate(dog_id, owner_name);
                        Swal.fire('Certificate will be sent to the member ' + owner_name, '', 'info');
                    }

                    if (result.isConfirmed) {
                        Swal.fire("Downloading..." + owner_name, '', 'success');
                        if (owner_name) {
                            window.location.href = `${download_base_url}/${dog_id}?name=${owner_name}`;
                        } else {
                            window.location.href = `${download_base_url}/${dog_id}`;
                        }
                    }
                });
            }
        });

    </script>


@stop

@section('content')

    <div class="content">

        <div class="container">

            <h2 class="">{{$first_name .' '.  $last_name."'s dogs" }}</h2>

            {{--<h2 class="pull-right">Hello {{$user}} </h2>--}}

            <br>

            <div class="portlet portlet-default">

                <div class="portlet-header">
                    {{--<h4 class="portlet-title">--}}
                    {{--<u>Pagination</u>--}}
                    {{--</h4>--}}
                </div> <!-- /.portlet-header -->
                @if ($dogs->count())

                <div class="portlet-body">

                    <table
                            class="table table-striped table-bordered table-hover ui-datatable"
                            >
                        <thead>
                        <tr>
                            <th> Name</th>
                            <th>Breed</th>
                            <th>Sex</th>
                            <th>Registration No.</th>
                            <th>Registered Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>

                        </thead>
                        <tbody>
                        @foreach($dogs as $dog)
                            <tr>
                                <td>{{$dog->name}}</td>
                                <td>{{$dog->breeder_name}}</td>
                                <td>{{$dog->sex}}</td>
                                <td>{{$dog->registration_number}}</td>
                                <td>{{$dog->created_at}}</td>
                                @if($dog->confirmed == true)
                                    <td>{{'Confirmed'}}</td>
                                @else
                                    <td><button  class="btn btn-success btn-sm confirm">Confirm</button></td>
                                @endif
                                <td>
                                    <a class="btn btn-success btn-sm" href="{{url('admin/dog-show',$dog->id)}}"> View</a>
                                    <a class="btn btn-success btn-sm" href="{{url('admin/dog-edit',$dog->id)}}"> Edit</a>
                                    <button id="delete-{{$dog->id}}" class="btn btn-danger btn-sm">Delete</button>
                                </td>
                            </tr>

                        @endforeach

                        </tbody>
                    </table>

                </div> <!-- /.portlet-body -->

            </div> <!-- /.portlet -->
            @else
                    <div style="height: 400px;">
                        <div class="text-center" style="height: 100px; border: 5px inset #cccccc; border-radius: 5px; padding-top : 10px;
                                    background-color: #cccccc; color: white;">
                            <div class="h2">This Member has not yet submitted any dog !</div>
                            <div class="h4"><a href="javascript:history.back()"> Back </a></div>
                        </div>
                    </div>
                @endif

        </div> <!-- /.container -->

    </div> <!-- .content -->
@stop

