@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <script>
        jQuery(document).ready(function(){
            @foreach($dogs as $dog)
           $('#btnConfirm-<?php echo $dog->id?>').off('click').on('click',function(){
                        $('#confirm-modal-<?php echo $dog->id ?>').modal();
                    })
            $('#delete-<?php echo $dog->id?>').off('click').on('click',function(){
                $('#delete-modal-<?php echo $dog->id ?>').modal();
            })
            @endforeach

//               $('#add-new-dog').off('click').on('click',function(){
//                        $('#register-dog-modal').modal();
//                    });

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        });

    </script>

@stop

@section('content')

    <div class="content">

        <div class="container">
            {!! Breadcrumbs::render('all-transfers') !!}

            @include('flash::message')
            <h2 class="">{{\App\DogTransfer::count()}} Currently Transferred Dogs</h2>

            {{--<h2 class="pull-right">Hello </h2>--}}

            <br>

            @if($dogs->count())

            <div class="row" style="margin-bottom: 45px;">
                <form  action="{{url('/admin/dog-transfers')}}">
                  {!! csrf_field() !!}
                    <div class="col-md-7">
                        <input type="text" name="term" class="form-control input-lg  " placeholder="search by dog name , microchip Number and Serial Number " value="">

                    </div>

                    <div class="col-md-2">
                        <input type="submit" class="form-control input-lg btn btn-success" value="Search">
                    </div>

                </form>

                <div class="col-md-2" >
                    {{--<input type="submit" class="form-control input-lg btn btn-success btn-sm"  value="ADD NEW DOG">--}}
                </div>
            </div>

            <div class="portlet portlet-default">

                <div class="portlet-header">
                    {{--<h4 class="portlet-title">--}}
                    {{--<u>Pagination</u>--}}
                    {{--</h4>--}}
                </div> <!-- /.portlet-header -->

                    <div class="portlet-body">
                        <div class="table-top-menu">
                            <ul>
                                <li class="table-top-pagination"> {!! $dogs->render() !!}</li>
                                {{--<li class="trash">--}}
                                    {{--<a href="{{url('/admin/trash')}}">Trash</a>--}}
                                {{--</li>--}}

                            </ul>
                        </div>

                        <table
                                class="table table-striped table-bordered table-hover ui-datatable"
                                data-ajax="../../global/js/demos/json/table_data.json"
                                data-global-search="false"
                                data-length-change="false"
                                data-info="true"
                                data-paging="true"
                                data-page-length="10"
                                >
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>Date of Birth</th>
                                <th>Breed</th>
                                <th>Sex</th>
                                <th>Registration Number</th>
                                <th>New Owner</th>
                                <th>Breeder</th>
                                <th>Serial Number</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>

                            </thead>
                            <tbody>
                            <?php $i = $dogs->firstItem(); ?>
                            @foreach($dogs as $dog)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$dog->name}}</td>
                                    <td>{{$dog->dob}}</td>
                                    <td>{{$dog->breeder_name}}</td>
                                    <td>{{$dog->sex}}</td>
                                    <td>{{$dog->registration_number}}</td>
                                    <td>{{$dog->owner}}</td>
                                    <td>{{$dog->member}}</td>
                                    <td>{{$dog->transfer_serial_number}}</td>
                                    <td>{{$dog->transfer_date}}</td>
                                    <td>
                                        @if($dog->dog_status == true)
                                            <em>confirmed</em>
                                            @else
                                    <a href="" class="btn btn-success">Confirm</a>
                                            @endif
                                    </td>
                                    <td>

                                        {{--<a href="{{url('admin/dog-show',$dog->id)}}" class="btn btn-default btn-sm">View</a>--}}
                                        <a href="{{url('admin/transfer-certificate',$dog->id)}}" id="certificate" class="btn btn-default btn-sm"
                                           data-toggle="tooltip" data-placement="left" title="print birth certificate"><i class="fa fa-certificate"></i></a>
                                        <a href="{{url('admin/edit-transfer-dog',$dog->transfer_id)}}" class="btn btn-warning btn-sm"
                                           data-toggle="tooltip" data-placement="left" title="edit transfer details"><i class="fa fa-pencil-square-o"></i>
                                        </a>

                                        {{--<button class="btn btn-danger btn-sm" id="delete-{{$dog->id}}" data-toggle="tooltip" data-placement="left" title="move {{$dog->name}} to trash"><i class="fa fa-trash"></i></button>--}}

                                    </td>
                                </tr>


                            @endforeach

                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $dogs->render() !!}
                        </div>
                    </div> <!-- /.portlet-body -->
                @else
                    <div style="height: 400px;">
                        <div class="text-center" style="height: 100px; border: 5px inset #cccccc; border-radius: 5px; padding-top : 10px;
                                    background-color: #cccccc; color: white;">
                            <div class="h2">No Dog has been transferred yet !. </div>
                            <div class="h4"><a href="javascript:history.back()"> Back </a></div>
                        </div>
                    </div>

                @endif

            </div> <!-- /.portlet -->

        </div> <!-- /.container -->

    </div> <!-- .content -->

@stop

