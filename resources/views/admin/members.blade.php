@extends('layouts.admin_master')

@section('scripts')
    {{--<script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>--}}
    {{--<script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>--}}

    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Required datatable js -->
    <script src="{{asset('datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('datatables/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{asset('datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('datatables/jszip.min.js')}}"></script>
    <script src="{{asset('datatables/pdfmake.min.js')}}"></script>
    <script src="{{asset('datatables/vfs_fonts.js')}}"></script>
    <script src="{{asset('datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('datatables/buttons.colVis.min.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{asset('datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('datatables/responsive.bootstrap4.min.js')}}"></script>


    <script>
        jQuery(document).ready(function(){

            var query = "{{$query}}";

           var table = $('#datatable-buttons').DataTable({
//                buttons: ['copy', 'excel', 'pdf'],

                processing: true,
                serverSide: true,
                ajax: '/admin/members-data'+query,
                columns: [

                    { data: "name",name:'name'},
                    { data: "kennel_name",name:'kennel_name' },
                    { data: "email", name:'email' },
                    { data: "phone",name:'phone' },
                    { data: "created_at", name:'created_at'},
                    {data : 'dogs', name: 'dogs'},
                    {data : "role", name:'role'},
                    {data : "status", name:'status'},
                    {data : 'action', name : 'action'}
                ],
                "initComplete": function( settings, json ) {

                    registerDeleteEvent();
                },
            });

                   registerDeleteEvent();


//            function addButtons(){
//                table.buttons().container()
//                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
//            }


            function registerDeleteEvent(){
                $.get('/admin/members-data',function(xhr){
                    $.each(xhr.data, function(index, element) {
                        $('#delete-member-'+element.id).off('click').on('click',function(){
//                            console.log(element.id);
//                            alert(element.id);
                            $('#delete-modal').modal();
                            $('.delete-name').text(element.name);
                            $('#delete-member-form').attr('action','/admin/delete-member/'+element.id);
                        })
                        $('#confirm-member-'+element.id).off('click').on('click',function(){
                            $('#confirm-modal').modal();
                            $('.confirm-name').text(element.name);
                            $('#confirm-member-form').attr('action','/admin/confirm/'+element.id);
                        })
                    });
                })
            }

            table.on( 'draw.dt', function () {
                console.log( 'Redraw occurred at: '+new Date().getTime() );
                registerDeleteEvent();

            });

        });

    </script>

    @stop

@section('content')

    <div class="content">

        <div class="container">

            {!! Breadcrumbs::render('all-members') !!}

            @include('flash::message')

            <h2 class="">Members</h2>

            {{--<h2 class="pull-right">Hello </h2>--}}

            <br>

            <div class="portlet portlet-default">

                <div class="portlet-header">
                    {{--<h4 class="portlet-title">--}}
                        {{--<u>Pagination</u>--}}
                    {{--</h4>--}}
                </div> <!-- /.portlet-header -->

                <div class="portlet-body">

                    <table class="table table-striped table-bordered table-hover ui-datatable" id="datatable-buttons">
                        <thead>
                        <tr>
                            {{--<th>No.</th>--}}
                            <th> Name</th>
                            {{--<th>Last Name</th>--}}
                            <th>Kennel Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Registered Date</th>
                            <th>Dog(s)</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>

                </div> <!-- /.portlet-body -->


            </div> <!-- /.portlet -->

        </div> <!-- /.container -->

    </div> <!-- .content -->

    <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="delete-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong>Delete  <span class="delete-name"></span> </strong></h4>
                </div>
                <div class="modal-body">
                    <form method="post" id="delete-member-form" >
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-2">
                                <label>This Action will delete <em><span class="delete-name"></span></em> and dogs submitted . <br><br>
                                    Are you sure you want to continue with this action ?
                                </label>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Delete <span class="delete-name"></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-labelledby="confirm-ModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><strong><span class="confirm-name"></span> </strong></h4>
                </div>
                <div class="modal-body">
                    <form id="confirm-member-form" method="post">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-2">

                                <label>Confirm Action ?</label>
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control" name="status">
                                        <option value="{{'0'}}">Revoke</option>
                                        <option value="{{'1'}}">Confirm</option>

                                </select>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Effect Change</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    @stop

