@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js"></script>
    <script>
    </script>

    <script>
        $(document).ready(function () {

            $(".breeders").select2();
            $('.users').select2();

            $(".dog_parent").select2({
                ajax: {
                    url: "{{url('admin/dogs')}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                            //g: $(this).data('gender')
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatRepo, // omitted for brevity, see the source of this page
                templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
            });

            function formatRepo (dog) {
                if (dog.loading) return dog.text;

                var markup = "<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__avatar'><img width='70' height='50' src='{{url('images/catalog')}}/" + dog.image_name+ "' /></div>" +
                        "<div class='select2-result-repository__meta'>" +
                        "<div class='select2-result-repository__title'>" + dog.name + "</div>"+
                        "<div class='select2-result-repository__title'>" + dog.registration_number + "</div>";

                if (dog.breeder_name) {
                    markup += "<div class='select2-result-repository__description'>" +" Breeder :"+ dog.breeder_name + "</div>";
                }
//                 += "<div class='select2-result-repository__statistics'>" +
//                        "<div class='select2-result-repository__forks'><i class='fa fa-flash'></i> " + repo.forks_count + " Forks</div>" +
//                        "<div class='select2-result-repository__stargazers'><i class='fa fa-star'></i> " + repo.stargazers_count + " Stars</div>" +
//                        "<div class='select2-result-repository__watchers'><i class='fa fa-eye'></i> " + repo.watchers_count + " Watchers</div>" +
//                        "</div>" +
                markup += "</div></div>";

                return markup;
            }

            function formatRepoSelection (dog) {
                return dog.registration_number;
            }


            var navListItems = $('div.setup-panel div a'),
                    allWells = $('.setup-content'),
                    allNextBtn = $('.nextBtn'),
                    allPrevBtn = $('.prevBtn');

            allWells.hide();

            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                        $item = $(this);

                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-primary').addClass('btn-default');
                    $item.addClass('btn-primary');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus();
                }
            });

            allNextBtn.click(function(){
                var curStep = $(this).closest(".setup-content"),
                        curStepBtn = curStep.attr("id"),
                        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                        curInputs = curStep.find("input[type='text'],input[type='url'],select,input[type='date'],input[type='file']"),
                        isValid = true;

                $(".form-group").removeClass("has-error");
                for(var i=0; i<curInputs.length; i++){
                    if (!curInputs[i].validity.valid){
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }

                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });

            allPrevBtn.click(function(){
                var curStep = $(this).closest(".setup-content"),
                        curStepBtn = curStep.attr("id"),
                        prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

                $(".form-group").removeClass("has-error");
                prevStepWizard.removeAttr('disabled').trigger('click');
            });

            $('div.setup-panel div a.btn-primary').trigger('click');



        });

    </script>

@stop
@section('content')

    <div class="container">
        {!! Breadcrumbs::render('transfer-dog') !!}
        <div id="message"></div>
        <div class="row">
            <div class="col-md-7 col-md-offset-2">
                <div class="portlet-header">
                    <h3 class="portlet-title"><u>Transfer Dog </u></h3>
                </div> <!-- /.portlet-header -->

                <div class="stepwizard">
                    <div class="stepwizard-row setup-panel">
                        <div class="stepwizard-step">
                            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                            <p>Step 1</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                            <p>Step 2</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                            <p>Step 3</p>
                        </div>
                    </div>
                </div>
                <form action="{{url('/admin/update-transfer-dog',$dog->transfer_id)}}" method="post" role="form">
                    {!! csrf_field() !!}
                    <div class="row setup-content" id="step-1">
                        <div class="col-xs-12">
                            <div class="col-md-12">
                                <h3> Dog to be Transferred</h3>
                                <div class="row">

                                    <div class="form-group">
                                        <label class="control-label"> Name of Dog</label>
                                        <select class="dog_parent form-control" name="dog" id="dog"  required>
                                            <option></option>
                                        </select>
                                        {{--<input  maxlength="100" type="text" class="form-control" id="mother" name="mother" placeholder="Enter Dog's Mother"  />--}}
                                    </div>
                                </div>
                            </div>

                            <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                        </div>
                    </div>

                    @if (count($errors) > 0)
                        @foreach ($errors->all() as $error)
                            <div> <span>{{ $error }}</span></div>
                        @endforeach
                    @endif

                    <div class="row setup-content" id="step-2">
                        <div class="col-xs-12">
                            <div class="col-md-12">
                                <h3>New Owner's Details</h3>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">First Name</label>
                                            <input  maxlength="100" type="text" class="form-control" name="first_name" required placeholder="First Name" value="{{$dog->first_name}}" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Last Name</label>
                                            <input maxlength="100" type="text" class="form-control" name="last_name" required placeholder="Last Name " value="{{$dog->last_name}}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Phone Number </label>
                                            <input  maxlength="100" type="text" name="phone_number" required class="form-control" placeholder="Phone Number" value="{{$dog->phone_number}}" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Secondary Phone Number</label>
                                            <input maxlength="100" type="text" class="form-control" name="sec_phone_number"  placeholder="Secondary Phone Number " value="{{$dog->sec_phone_number}}" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Email </label>
                                            <input  maxlength="100" type="text" name="email" class="form-control" required placeholder="Owner's Email " value="{{$dog->email}}"  />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Notes</label>
                                            <textarea class="form-control" name="notes"></textarea>
                                            {{--<input maxlength="100" type="text" class="form-control" name="other_health_checks" placeholder="Other Health Checks" />--}}
                                        </div>
                                    </div>
                                </div>

                                <button class="btn btn-default prevBtn btn-lg pull-left" type="button" >Prev</button>
                                <button class="btn btn-primary nextBtn btn-lg pull-right secondBtn" type="button" >Next</button>
                            </div>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-3">
                        <div class="col-xs-12">
                            <div class="col-md-12">
                                <h3 class="text-center"> SAVE NOW !</h3>
                                <h5 class="text-center">YOU CAN GO BACK TO CHANGE ANY INFO.</h5>


                                <button class="btn btn-default prevBtn btn-lg pull-left" type="button" >Prev</button>
                                <button class="btn btn-success btn-lg pull-right" type="submit">Update Transfer Details </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop