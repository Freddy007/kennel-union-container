@extends("layouts.index3_layout")

@section("title")
    Results
@endsection

@section("content")

    <!-- Page Heading -->
    <section class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>Search Results</h1>
                </div>
                <div class="col-md-6">
                    <ul class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li class="active">Search Results</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- Page Heading / End -->

    <section class="page-content">
        <div class="container">

            <div class="clearfix">
                <!-- Project Feed Filter -->
                <ul class="project-feed-filter">
                    <li><a href="#" class="btn btn-sm btn-default btn-primary" data-filter="*">All</a></li>

                    @foreach($breeds as $breed)
                    <li><a href="#" class="btn btn-sm btn-default" data-filter=".{{ str_replace(' ', '_', $breed->name) }}">{{$breed->name}}</a></li>
                    @endforeach
                </ul>
                <!-- Project Feed Filter / End -->
            </div>

            <!-- Project Feed -->
            <div class="project-feed project-feed__4cols row">

            <h3>No dog found matching your query!</h3>
            <a href="/">Go back</a>
        
            </div>
            <!-- Project Feed / End -->

            <div class="text-center">
                <ul class="pagination-custom list-unstyled list-inline">

                    <?php
                    if(isset($paginator)){ ?>
                    {{--{!! $paginator->render() !!}--}}
                    {!! $paginator->appends(
                    ['name' => Request::get('name'),'breed'=>Request::get('breed'),
                     'sex'=>Request::get('sex'),'colour'=>Request::get('colour')
                                            ])->render()
                    !!}

                    <?php   }else {?>
                    {!! $dogs->render() !!}
                    <?php } ?>

{{--                    <li><a href="#" class="btn btn-sm btn-default">&laquo;</a></li>--}}
{{--                    <li><a href="#" class="btn btn-sm btn-success">1</a></li>--}}
{{--                    <li><a href="#" class="btn btn-sm btn-default">2</a></li>--}}
{{--                    <li><a href="#" class="btn btn-sm btn-default">3</a></li>--}}
{{--                    <li><a href="#" class="btn btn-sm btn-default">4</a></li>--}}
{{--                    <li><a href="#" class="btn btn-sm btn-default">5</a></li>--}}
{{--                    <li><a href="#" class="btn btn-sm btn-default">&raquo;</a></li>--}}
                </ul>
            </div>

        </div>
    </section>
    <!-- Page Content / End -->

    <!-- Footer -->
    <footer class="footer" id="footer">

        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        Copyright &copy; 2020 <a href="/">Kennel Union of Ghana</a> &nbsp;| &nbsp;All Rights Reserved
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <div class="social-links-wrapper">
                            <span class="social-links-txt">Keep in Touch</span>
                            <ul class="social-links social-links__light">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer / End -->

@endsection
