@extends('layouts.admin_master')

@section('scripts')
    <script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
    <script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        jQuery(document).ready(function(){
            $('#add-new-dog').off('click').on('click',function(){
                $('#register-dog-modal').modal();
            });

            $('#add-dog').off('click').on('click',function(){

                $.post("{{url('/member/register-dog')}}",$('#register-dog-modal form').serialize(), function (data) {
                    $('#message').text(data);
                    // $('#register-dog-modal').modal('hide');
                });

            })

            //$('#parentage-table').hide();
            $('#show-parent').on('click',function(){
                $('#parentage-table').show()
            });

        });

    </script>

@section('content')

    <div class="content">

        <div class="container">

            <div class="portlet portlet-default">

                <div class="portlet-body main-table">

                    <!-- Begin Pedigree Table -->

                    <style>
                        .water-mark{
                           z-index: 20000;
                            position: absolute;
                            top: 600px;
                            left: 200px;
                            font-weight: bold;
                            font-size: 60px;
                            opacity: 0.3;
                            -ms-transform: rotate(45deg); /* IE 9 */
                            -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
                            transform: rotate(45deg);
                        }

                        hr {
                            padding: 0;
                            border: none;
                            border-top: medium double #333;
                            color: #333;
                            text-align: center;
                            width: 95%;
                            margin-left: 0px;
                        }
                        hr:after {
                            /*content: "�";*/
                            display: inline-block;
                            position: relative;
                            top: -0.7em;
                            font-size: 1.5em;
                            padding: 0 0.25em;
                            background: white;
                        }
                        .male {
                            background-color: rgb(191,133,10);
                            color: #000000;
                            font-family: Sans Serif;
                            font-size: 8pt;
                            text-align: center;
                            vertical-align: middle;
                            border-collapse: collapse;
                            border: 1px solid #000000;
                        }

                        .female {
                            background-color: #FFFFFF;
                            color: #000000;
                            font-family: Sans Serif;
                            font-size: 8pt;
                            text-align: center;
                            vertical-align: middle;
                            border-collapse: collapse;
                            border: 1px solid #000000;
                        }

                        @media print {
                            .male{
                                background-color: rgb(191,133,10)!important;
                                -webkit-print-color-adjust: exact;
                            }
                            header {
                                display: none;
                            }
                            .mainnav{
                                display: none;
                            }
                            footer,title{
                                display:none;
                            }

                            table{
                                height: 95%;
                                /*background-color: yellowgreen !important;*/
                                -webkit-print-color-adjust: exact;
                            }


                        }
                    </style>

                    <div class="header">
                        <h2 class="text-center">  <img src="{{url('/img/logo_01.png')}}"> KENNEL UNION OF GHANA </h2><br>
                        {{--<h3>CERTIFIED PEDIGREE </h3>--}}
                    </div>
                    <div class="water-mark">
                       KENNEL UNION OF GHANA <br>
                        {{--<img src="{{url('/img/kug.png')}}"/>--}}
                    </div>

                    <div>
                        <div class="row" style="color:#283846">
                          <div class="col-md-3 h4">Name : {{$dog->name}} </div>
                            <div class="col-md-3 h4">Breeder : {{$dog->breeder_name}} </div>
                            <div class="col-md-3 h4">Registration Number : {{$dog->registration_number}} </div>
                            <div class="col-md-3 h4">Owner : {{$dog->first_name .' '. $dog->last_name}} </div>


                            <div class="clearfix"></div>
                            <div class="col-md-3 h4">Tattoo No. : {{$dog->tattoo_number}} </div>
                            <div class="col-md-3 h4">Date of Birth : {{$dog->dob}} </div>
                            <div class="col-md-3 h4">Colour : {{$dog->colour}} </div>
                            <div class="col-md-3 h4">Height : {{$dog->height}} </div>

                            <div class="clearfix"></div>
                            <div class="col-md-3 h4">Sex : {{$dog->sex}} </div>
                            <div class="col-md-3 h4">Microchip No. : {{$dog->microchip_number}} </div>
                            <div class="col-md-3 h4">DNA ID : {{$dog->DNA}} </div>

                        </div>
                        <hr>
                        <br>

                    <table border="0" cellpadding="2" width='95%'>
                        <tr><td>
                                <table style="border: 1px solid #000000; border-collapse: collapse;" border="1" cellpadding="2" cellspacing="2" width="100%"  CELLSPACING="2">
                                    <tr>
                                        <th>1st</th>
                                        <th>2nd</th>
                                        <th>3rd</th>
                                        <th>4th</th>
                                        <th>5th</th>
                                    </tr>
                                    <tr>
                                        <td rowspan='16' width='17%' class='male'>
                                            <div class="h4">
                                                @if ($dog->father)
                                                Name :{{\App\Dog::getRelationship($dog->father)}}(Sire)<br>
                                                No.{{$dog->father}}
                                                @endif
                                            </div>
                                        </td>
                                        <td rowspan='8' width='17%' class='male'>
                                            <div class="h4">
                                                  Name : {{\App\Dog::getParent($dog->father,'father')}}(Sire)<br/>
                                                  No :{{$secondgen = \App\Dog::getParentId($dog->father,'father','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='4' width='17%' class='male'>
                                            <div class="h4">
                                                    Name : <strong>{{\App\Dog::getParent($secondgen,'father')}}</strong>(Sire)<br/>
                                                    No: {{$third_generation = \App\Dog::getParentId($secondgen,'father','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='2' width='17%' class='male'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($third_generation,'father')}}</strong>(Sire)<br/>
                                                No: {{$fifth_generation = \App\Dog::getParentId($third_generation,'father','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='2' width='17%' class='female'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($third_generation,'mother')}}</strong>(Dam)<br/>
                                                No: {{$generation_five = \App\Dog::getParentId($third_generation,'mother','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='4' width='17%' class='female'>
                                            <div class="h4">
                                                    Name : <strong>{{\App\Dog::getParent($secondgen,'mother')}}</strong>(Dam)<br/>
                                                    No: {{$fourth_generation = \App\Dog::getParentId($secondgen,'mother','registration_number')}}
                                            </div>

                                        </td>
                                        <td rowspan='2' width='17%' class='male'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($fourth_generation,'father')}}</strong>(Sire)<br/>
                                                No: {{$generation_five_b = \App\Dog::getParentId($fourth_generation,'father','registration_number')}}
                                            </div>

                                        </td>
                                        <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='2' width='17%' class='female'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($fourth_generation,'mother')}}</strong>(Dam)<br>
                                                No: {{$generation_five_c = \App\Dog::getParentId($fourth_generation,'mother','registration_number')}}
                                            </div>

                                        </td>
                                        <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='8' width='17%' class='female'>
                                            <div class="h4">
                                                    Name: {{\App\Dog::getParent($dog->father,'mother')}} (Dam)<br>
                                                    {{$thirdgen_mother = \App\Dog::getParentId($dog->father,'mother','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='4' width='17%' class='male'>
                                         <div class="h4">
                                             Name : {{\App\Dog::getParent($thirdgen_mother,'father')}}(Sire)<br>
                                             No: {{$fourth_gen = \App\Dog::getParentId($thirdgen_mother,'father','registration_number')}}
                                         </div>
                                        </td>
                                        <td rowspan='2' width='17%' class='male'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($fourth_gen,'father')}}</strong>(Sire)<br/>
                                                No: {{$generation_five_d = \App\Dog::getParentId($fourth_gen,'father','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='2' width='17%' class='female'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($fourth_gen,'mother')}}</strong>(Dam)<br/>
                                                No: {{$generation_five_e = \App\Dog::getParentId($fourth_gen,'mother','registration_number')}}
                                            </div>

                                        </td>
                                        <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='4' width='17%' class='female'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($thirdgen_mother,'mother')}}</strong>(Dam)<br/>
                                                No: {{$generation_four = \App\Dog::getParentId($thirdgen_mother,'mother','registration_number')}}
                                            </div>

                                        </td>
                                        <td rowspan='2' width='17%' class='male'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($generation_four,'father')}}</strong>(Sire)<br/>
                                                No: {{$generation_five_f = \App\Dog::getParentId($generation_four,'father','registration_number')}}

                                            </div>
                                        </td>
                                        <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='2' width='17%' class='female'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($generation_four,'mother')}}</strong>(Dam)<br/>
                                                No: {{$generation_five_g = \App\Dog::getParentId($generation_four,'mother','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='16' width='17%' class='female' >

                                            <div class="h4">
                                                @if ($dog->mother)
                                                    Name: {{\App\Dog::getRelationship($dog->mother)}}(Dam)<br/>
                                                        No.: {{$dog->mother}}
                                                @endif
                                                    </div>
                                        </td>
                                        <td rowspan='8' width='17%' class='male'>
                                            <div class="h4">
                                                Name : {{\App\Dog::getParent($dog->mother,'father')}}(Sire)<br/>
                                                No: {{$generation_two_a = \App\Dog::getParentId($dog->mother,'father','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='4' width='17%' class='male'>
                                            <div class="h4">
                                                Name : {{\App\Dog::getParent($generation_two_a,'father')}}(Sire)<br/>
                                                No: {{ $generation_two_c = \App\Dog::getParentId($generation_two_a,'father','registration_number')}}
                                            </div>

                                        </td>
                                        <td rowspan='2' width='17%' class='male'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($generation_two_c,'father')}}</strong>(Sire)<br/>
                                                No: {{$generation_4_d =  \App\Dog::getParentId($generation_two_c,'father','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='2' width='17%' class='female'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($generation_two_c,'mother')}}</strong>(Dam)<br/>
                                                No: {{$generation_5_a =  \App\Dog::getParentId($generation_two_c,'mother','registration_number')}}
                                            </div>

                                        </td>
                                        <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='4' width='17%' class='female'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($generation_two_a,'mother')}}</strong>(Dam)<br/>
                                                No: {{$generation_3_a =  \App\Dog::getParentId($generation_two_a,'mother','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='2' width='17%' class='male'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($generation_3_a,'father')}}</strong>(Sire)<br/>
                                                No: {{$generation_5_e = \App\Dog::getParentId($generation_3_a,'father','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='2' width='17%' class='female'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($generation_3_a,'mother')}}</strong>(Dam)<br/>
                                                No: {{$generation_5_f = \App\Dog::getParentId($generation_3_a,'mother','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='8' width='17%' class='female'>
                                            <div class="h4">
                                                    Name:{{\App\Dog::getParent($dog->mother,'mother')}}(Dam)<br/>
                                                    No: {{$generation_two_b = \App\Dog::getParentId($dog->mother,'mother','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='4' width='17%' class='male'>
                                            <div class="h4">
                                                Name :{{\App\Dog::getParent($generation_two_b,'father')}}(Sire)<br/>
                                                No: {{$generation_3_c = \App\Dog::getParentId($generation_two_b,'father','registration_number')}}
                                            </div>

                                        </td>
                                        <td rowspan='2' width='17%' class='male'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($generation_3_c,'father')}}</strong>(Sire)<br/>
                                                No: {{$generation_5_h = \App\Dog::getParentId($generation_3_c,'father','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='2' width='17%' class='female'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($generation_3_c,'mother')}}</strong>(Dam)<br/>
                                                No: {{$generation_5_i= \App\Dog::getParentId($generation_3_c,'mother','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='4' width='17%' class='female'>
                                            <div class="h4">
                                                Name : {{\App\Dog::getParent($generation_two_b,'mother')}}(Dam)<br/>
                                                No: {{$generation_4_b = \App\Dog::getParentId($generation_two_b,'mother','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='2' width='17%' class='male'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($generation_4_b,'father')}}</strong>(Sire)<br/>
                                                No: {{$generation_5_j =\App\Dog::getParentId($generation_4_b,'father','registration_number')}}
                                            </div>

                                        </td>
                                        <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='2' width='17%' class='female'>
                                            <div class="h4">
                                                Name : <strong>{{\App\Dog::getParent($generation_4_b,'mother')}}</strong>(Dam) <br/>
                                                No: {{$generation_5_k = \App\Dog::getParentId($generation_4_b,'mother','registration_number')}}
                                            </div>
                                        </td>
                                        <td rowspan='1' width='17%' class='male'>&nbsp;</td>
                                    </tr><tr>
                                        <td rowspan='1' width='17%' class='female'>&nbsp;</td>
                                    </tr><tr>
                                </table>
                            </td><tr>
                        <tr><td align='right' valign='top' style='padding-top: 0px margin-top: 0px; font-size:8pt; font-family: Arial'><a href='http://www.kennelunionghana.com/'>Pedigree</a> generated by Kennel Union of Ghana Pedigree Database
                            </td><tr>
                    </table>

                    </div>

                    <!-- End Pedigree Table -->

                    </table>
                    {{--<h3 class="text-center"> <button class="btn btn-success" id="show-parent">Show Parentage</button></h3>--}}
                    @if($dog->father || $dog->mother)
                        <?php
                        $father_id = \App\Dog::getDogId($dog->father);
                        $mother_id = \App\Dog::getDogId($dog->mother);
                        ?>
                    @endif

                </div>
            </div>

        </div> <!-- /.table-responsive -->

    </div> <!-- /.portlet-body -->

    </div> <!-- /.portlet -->
    <br>

    </div> <!-- /.container -->

    </div> <!-- .content -->

@stop