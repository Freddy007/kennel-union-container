@extends('layouts.auth-layout')

@section('content')

    <div class="account-wrapper">

        <div class="account-body">
            @include('flash::message')
            <h3> Kennel Union Of Ghana.</h3>

            <h2>Password Reset</h2>

            <h5>We'll email you instructions on how to reset your password.</h5>


            <form class="form account-form" method="POST" action="/password/email">
                {!! csrf_field() !!}

                <div class="form-group">
                    <label for="login-username" class="placeholder-hidden">Email</label>
                    <input type="text" class="form-control" id="login-username" placeholder="Email" name="email"  value="{{old('email')}}" tabindex="1">
                </div> <!-- /.form-group -->
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-lg" tabindex="4">
                        Send Password Reset Link  &nbsp; <i class="fa fa-play-circle"></i>
                    </button>
                </div> <!-- /.form-group -->

                <div class="form-group">
                    <a href="{{url('auth/login')}}"><i class="fa fa-angle-double-left"></i> &nbsp;Back to Login</a>
                </div> <!-- /.form-group -->

            </form>

        </div> <!-- /.account-body -->

    </div> <!-- /.account-wrapper -->
@stop