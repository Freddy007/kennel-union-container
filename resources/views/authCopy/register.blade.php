@extends('layouts.admin_master')
        <!-- Plugin JS -->
<script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
<script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

@section('content')
    <div class="account-wrapper">

        <div class="account-body">

            <h3>Register</h3>

            <h5>Your Details</h5>

            <form class="form account-form" method="POST" action="/auth/register">
                {!! csrf_field() !!}

                <div class="form-group">
                    <label for="signup-title" class="placeholder-hidden">Title</label>
                    <select class="form-control" name="title" value="{{old('title')}}" required>
                        <option value="">Select title</option>
                        <option value="Mr">Mr</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Miss">Miss</option>
                        <option value="Dr">Dr</option>
                        <option value="Sir">Sir</option>
                    </select>
                    {{--<input type="text" class="form-control" id="signup-title" placeholder="Your Title" tabindex="1">--}}
                </div> <!-- /.form-group -->

                <div class="form-group">
                    <label for="signup-first_name" class="placeholder-hidden">First Name</label>
                    <input type="text" name="first_name" class="form-control" id="signup-first_name" value="{{old('first_name')}}" placeholder="Your First Name" tabindex="1" required>
                </div> <!-- /.form-group -->

                <div class="form-group">
                    <label for="signup-last_name" class="placeholder-hidden">Last Name</label>
                    <input type="text" name="last_name" class="form-control" id="signup-last_name"  value="{{old('last_name')}}" placeholder="Your Last Name" tabindex="1" required>
                </div> <!-- /.form-group -->

                <div class="form-group">
                    <label for="signup-username" class="placeholder-hidden">Kennel Name</label>
                    <input type="text"  name="kennel_name"  class="form-control" id="kennel_name"  value="{{old('kennel_name')}}" placeholder="Kennel Name" tabindex="3" >
                </div> <!-- /.form-group -->

                <div class="form-group">
                    <label for="signup-email" class="placeholder-hidden">Email Address</label>
                    <input type="text" name="email" class="form-control" id="signup-email"  value="{{old('email')}}" placeholder="Your Email" tabindex="1" required>
                </div> <!-- /.form-group -->


                <div class="form-group">
                    <label for="signup-phone" class="placeholder-hidden">Phone</label>
                    <input type="text" name="phone" class="form-control" id="signup-phone"  value="{{old('phone')}}" placeholder="Your Phone" tabindex="1" maxlength="10" required>
                    <span class="help-block"> <em>Type phone number without country code * </em></span>
                </div> <!-- /.form-group -->

                <div class="form-group">
                    <label for="login-password" class="placeholder-hidden">Password</label>
                    <input type="password" name="password" class="form-control" id="login-password"  placeholder="Password" tabindex="4" required>
                </div> <!-- /.form-group -->

                <div class="form-group">
                    <label for="login-password" class="placeholder-hidden">Confirm Password</label>
                    <input type="password" class="form-control" id="confirm-password" placeholder="Confirm Password" name="password_confirmation" tabindex="4" required>
                </div> <!-- /.form-group -->

                <div class="form-group">
                    <label class="checkbox-inline">
                        <input type="checkbox" class="" value="" tabindex="5"> I agree to the <a href="javascript:;" target="_blank">Terms of Service</a> &amp; <a href="javascript:;" target="_blank">Privacy Policy</a>
                    </label>
                </div> <!-- /.form-group -->

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="form-group">
                    <button type="submit" class="btn btn-secondary btn-block btn-lg" tabindex="6">
                        Create My Account &nbsp; <i class="fa fa-play-circle"></i>
                    </button>
                </div> <!-- /.form-group -->

            </form>

        </div> <!-- /.account-body -->

        <div class="account-footer">
            <p>
                Already have an account? &nbsp;
                <a href="{{url('auth/login')}}" class="">Login to your Account!</a>
            </p>
        </div> <!-- /.account-footer -->

    </div> <!-- /.account-wrapper -->
    @stop