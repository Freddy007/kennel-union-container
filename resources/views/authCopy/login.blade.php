@extends('layouts.auth-layout')


@section('content')

    <div class="account-wrapper">

        <div class="account-body">
            @include('flash::message')
            <h3> Kennel Union Of Ghana Pedigree Database.</h3>

            <h5>Please sign in to get access.</h5>

            <form class="form account-form" method="POST" action="/auth/login">
                {!! csrf_field() !!}

                <div class="form-group">
                    <label for="login-username" class="placeholder-hidden">Email</label>
                    <input type="text" class="form-control" id="login-username" placeholder="Email" name="email"  value="{{old('email')}}" tabindex="1">
                </div> <!-- /.form-group -->

                <div class="form-group">
                    <label for="login-password" class="placeholder-hidden">Password</label>
                    <input type="password" class="form-control" id="login-password" placeholder="Password" name="password" value="{{old('password')}}" tabindex="2">
                </div> <!-- /.form-group -->

                <div class="form-group clearfix">
                    <div class="pull-left">
                        <label class="checkbox-inline">
                            <input type="checkbox" class="" name="remember" tabindex="3"> <small>Remember me</small>
                        </label>
                    </div>

                    <div class="pull-right">
                        <small><a href="{{url('/password/email')}}">Forgot Password?</a></small>
                    </div>
                </div> <!-- /.form-group -->

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-lg" tabindex="4">
                        Signin &nbsp; <i class="fa fa-play-circle"></i>
                    </button>
                </div> <!-- /.form-group -->

            </form>


        </div> <!-- /.account-body -->

        <div class="account-footer">
            <p>
                Not A member yet? &nbsp;
                <a href="{{url('/auth/register')}}" class="">Become a Member!</a>
            </p>
        </div> <!-- /.account-footer -->

    </div> <!-- /.account-wrapper -->
@stop