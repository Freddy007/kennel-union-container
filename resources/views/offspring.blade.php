@extends('layouts.admin_master')

@section('scripts')
        <!-- Plugin JS -->
<script src="{{asset('../../bower_components/jquery/dist/jquery.js')}}"></script>
<script src="{{asset('../../bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="../../bower_components/flot/excanvas.min.js"></script>
<script src="{{asset('../../bower_components/flot/jquery.flot.js')}}"></script>
<script src="{{asset('../../bower_components/flot/jquery.flot.pie.js')}}"></script>
<script src="{{asset('../../bower_components/flot/jquery.flot.resize.js')}}"></script>
<script src="{{asset('../../bower_components/flot/jquery.flot.time.js')}}"></script>
<script src="{{asset('../../bower_components/flot.tooltip/js/jquery.flot.tooltip.js')}}"></script>
<!-- App JS -->
<script src="{{asset('../../global/js/mvpready-core.js')}}"></script>
<script src="{{asset('../../global/js/mvpready-helpers.js')}}"></script>
<script src="{{asset('js/mvpready-admin.js')}}"></script>

<script src="../../bower_components/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Plugin JS -->
<script src="../../bower_components/select2/dist/js/select2.min.js"></script>
<script src="../../bower_components/jquery-icheck/icheck.min.js"></script>
<script src="../../bower_components/parsleyjs/dist/parsley.js"></script>
<script src="../../bower_components/bootstrap-3-timepicker/js/bootstrap-timepicker.js"></script>

<script src="../../bower_components/bootstrap-jasny/js/fileinput.js"></script>


<script src="{{asset('/js/typeahead.bundle.js')}}"></script>
<script>

</script>

<script>

    $(document).ready(function(){
        $('#login').modal();
    })

    var dogs = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        // url points to a json file that contains an array of country names, see
        // https://github.com/twitter/typeahead.js/blob/gh-pages/data/countries.json
        prefetch: '/dogs'
    });

    dogs.initialize();

    // passing in `null` for the `options` arguments will result in the default
    // options being used
    $('.typeahead').typeahead(null,{
        name: 'dogs',
        source: dogs,

        template: [

            '<p class="repo-name">@{{name}}</p>',
        ].join(''),

        // template engine
        engine: Hogan

    });

</script>
@stop
@section('content')

    <div class="content">

        <div class="container">
            {!! Breadcrumbs::render('progeny',$dog) !!}

            @include('flash::message')

            <div class="portlet portlet-default">

                <div class="portlet-header">

                    <h3 class="portlet-title text-center"><u>{{$dog->name . " 's"}} Progeny</u></h3>

                </div> <!-- /.portlet-header -->


                <div class="portlet-body">

                    <div class="table-responsive">

                        @if($offspring->count())
                        <table class="table table-striped table-bordered thumbnail-table">
                            <thead>
                            <tr>
                                <th style="width: 150px">Image</th>
                                <th>Name</th>
                                <th>Breed</th>
                                <th>Registered Date</th>
                                <th>Sex</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($offspring as $dog)
                                <tr>
                                    <td>
                                        <div class="thumbnail">
                                            <div class="thumbnail-view">
                                                <a class="thumbnail-view-hover ui-lightbox" href="{{url('/show-dog',$dog->id)}}">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                                <img src="/images/catalog/{{$dog->image_name}}" style="width: 120px; height: 70px;" alt="Gallery Image">
                                            </div>
                                        </div> <!-- /.thumbnail -->
                                    </td>

                                    <td class="valign-middle"><a href="javascript:;"></a>
                                        <p>{{$dog->name}}</p>
                                    </td>

                                    <td class="valign-middle">{{$dog->breeder_name}}</td>

                                    <td class="file-info valign-middle">
                                        {{$dog->created_at}}
                                    </td>

                                    <td>
                                        {{$dog->sex}}
                                    </td>

                                    <td class="text-center valign-middle">
                                        <a href="{{url('/show-dog',$dog->id)}}" class="btn btn-success btn-sm">View</a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $offspring->render() !!}
                        </div>

                    </div> <!-- /.table-responsive -->
                    @else
                        <div style="height: 400px;">
                            <div class="text-center" style="height: 100px; border: 5px inset #cccccc; border-radius: 5px; padding-top : 10px;
                                    background-color: #cccccc; color: white;">
                                 <div class="h2">{{$dog->name }} does not currently have an offspring</div>
                                <div class="h4"><a href="javascript:history.back()"> Back </a></div>
                            </div>
                        </div>

                    @endif

                </div> <!-- /.portlet-body -->

            </div> <!-- /.portlet -->


            <br>


            <br>


        </div> <!-- /.container -->


    </div> <!-- .content -->

@stop
